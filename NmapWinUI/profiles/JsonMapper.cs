﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using System.IO;

namespace NmapWinCommon.Entities
{
    public class JsonMapper
    {
        public void WriteJsonFile(Profile[] profiles)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(@"C:\Program Files (x86)\Nmap\win\profiles.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, profiles);
            }
        }
        public Profile[] ReadJsonFile()
        {
            // deserialize JSON directly from a file
            using (StreamReader file = File.OpenText(@"C:\Program Files (x86)\Nmap\win\profiles.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                Profile[] profiles = (Profile[])serializer.Deserialize(file, typeof(Profile[]));
                // Loop over profiles.
                foreach (Profile p in profiles)
                {
                    p.Command = p.Command.Replace("\\", "");
                }
                return profiles;
            }
        }
    }
}
