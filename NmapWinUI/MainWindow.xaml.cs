﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinCommon.Worker;
using NmapWinUI.dialogs;
using NmapWinUI.Dialogs;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NmapWinUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(MainWindow));

        /// <summary>
        /// Database context
        /// </summary>
        private readonly DatabaseContext DatabaseContext = new DatabaseContext();

        /// <summary>
        /// Options database singleton
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly NmapWorkerManager NmapWorkerManager = NmapWorkerManager.Instance;

        /// <summary>
        /// Input field validator.
        /// </summary>
        private readonly FieldValidation FieldValidation = FieldValidation.Instance;

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Command singleton
        /// </summary>
        public Command Command { get; set; } = Command.Instance;

        /// <summary>
        /// Collection for the scan table
        /// </summary>
        public ObservableCollection<Scan> ScanData { get; set; }

        /// <summary>
        /// Collection for the scan table
        /// </summary>
        public ObservableCollection<Profile> ProfileData { get; set; }

        /// <summary>
        /// Auto-update cancellation source
        /// </summary>
        private CancellationTokenSource AutoUpdateTokenSource;

        /// <summary>
        /// Auto-update interval
        /// </summary>
        private TimeSpan AutoUpdateInterval = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Methods timer
        /// </summary>
        private readonly Stopwatch stopwatch = new Stopwatch();
        #endregion

        #region Bindings
        /// <summary>
        /// Selected profile binding
        /// </summary>
        private Profile _selectedProfile;
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set
            {
                if (value != null)
                {
                    _selectedProfile = value;
                    Command.Clear();
                    Command.CommandString = CommandText = _selectedProfile.Command;
                    if (!string.IsNullOrEmpty(_selectedProfile.Target))
                    {
                        TargetText = _selectedProfile.Target;
                    }
                    OnPropertyChanged("SelectedProfile");
                }
            }
        }

        /// <summary>
        /// Name text binding
        /// </summary>
        private string _nameText;
        public string NameText
        {
            get { return _nameText; }
            set
            {
                if (value != null && !value.Equals(_nameText))
                {
                    _nameText = value;
                    OnPropertyChanged("NameText");
                }
            }
        }

        /// <summary>
        /// Target text binding
        /// </summary>
        private string _targetText;
        public string TargetText
        {
            get { return _targetText; }
            set
            {
                if (value != null && !value.Equals(_targetText))
                {
                    _targetText = value;
                    OnPropertyChanged("TargetText");
                }
            }
        }

        /// <summary>
        /// Command text binding
        /// </summary>
        private string _commandText;
        public string CommandText
        {
            get { return _commandText; }
            set
            {
                if (_commandText == null || !_commandText.Equals(value))
                {
                    _commandText = value;
                    OnPropertyChanged("CommandText");
                }
            }
        }

        /// <summary>
        /// Scroll to end flag.
        /// </summary>
        private bool _isScrollToEnd = false;
        public bool IsScrollToEnd
        {
            get
            {
                return _isScrollToEnd;
            }
            set
            {
                _isScrollToEnd = value;
                if (_isScrollToEnd)
                {
                    ScanDatagrid.ScrollIntoView(ScanData[ScanData.Count - 1]);
                }
                OnPropertyChanged("ScriptData");
            }
        }

        /// <summary>
        /// Can start scan flag.
        /// </summary>
        private bool _canStart = true;
        public bool CanStart
        {
            get
            {
                return _canStart;
            }
            set
            {
                _canStart = value;
                OnPropertyChanged("CanStart");
            }
        }

        /// <summary>
        /// Auto update flag.
        /// </summary>
        private bool _isAutoUpdate = true;
        public bool IsAutoUpdate
        {
            get
            {
                return _isAutoUpdate;
            }
            set
            {
                _isAutoUpdate = value;
                ConfigureAutoUpdater();
                OnPropertyChanged("IsAutoUpdate");
            }
        }
        #endregion

        #region Field validation
        public string Error { get; set; } = string.Empty;

        public string this[string propertyName]
        {
            get
            {
                string errorString = ValidateProperty(propertyName);
                CanStart = string.IsNullOrEmpty(errorString);
                return errorString;
            }
        }

        private string ValidateProperty(string propertyName)
        {
            switch (propertyName)
            {
                case "TargetText":
                    return FieldValidation.ValidateTarget("TargetText", TargetText, false);
                case "CommandText":
                    return FieldValidation.ValidateCommand("CommandText", CommandText);
                case "SelectedProfile":
                    return FieldValidation.ValidateTarget("TargetText", TargetText, false);
            }
            return string.Empty;
        }
        #endregion

        #region Constrcutor
        /// <summary>
        /// Main window entry point.
        /// </summary>
        public MainWindow()
        {
            logger.Debug("Main application starting");

            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            InitializeComponent();

            InitializeWidgets();

            ConfigureAutoUpdater();

            DataContext = this;
        }
        #endregion

        #region Initialization        
        /// <summary>
        /// Initialize UI wigets and set default values where necessary.
        /// </summary>
        private void InitializeWidgets()
        {
            Title = ApplicationVersion.Name + " v" + ApplicationVersion.VersionShort;

            // Set default target
            TargetText = DatabaseOption.FindOptionByName("nmapwin.main.defaultTarget")?.Value;

            // Set auto update interval
            AutoUpdateInterval = TimeSpan.Parse(DatabaseOption.FindOptionByName("nmapwin.main.autoUpdate.interval")?.Value);

            // Fill in profile combobox
            ProfileComboBox.Items.Clear();
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileComboBox.ItemsSource = ProfileData;
            logger.Debug(string.Format("Found profiles - count: {0}", ProfileData.Count));

            // Set default profile
            string defaultProfile = DatabaseOption.FindOptionByName("nmapwin.main.defaultProfile")?.Value;
            if (defaultProfile != null)
            {
                SelectedProfile = ProfileData.First(p => p.Name.Equals(defaultProfile));
            }

            // Fill in scan data grid
            ScanData = new ObservableCollection<Scan>(DatabaseScan.FindScans());
            ScanDatagrid.ItemsSource = ScanData;
            logger.Debug(string.Format("Found scans - count: {0}", ScanData.Count));

            UpdateStatusBar();
            Closing += HandleWindowClosing;

            logger.Debug("Widgets initialized");
        }

        /// <summary>
        /// Configure the auto-updater
        /// </summary>
        private void ConfigureAutoUpdater()
        {
            if (IsAutoUpdate)
            {
                AutoUpdateTokenSource = new CancellationTokenSource();
                Task.Run(() => DoAutoUpdateWork(AutoUpdateTokenSource.Token), AutoUpdateTokenSource.Token);
                logger.Debug(string.Format("Auto update task started - interval: {0}s", (int)AutoUpdateInterval.TotalMilliseconds));
            }
            else
            {
                AutoUpdateTokenSource.Cancel();
                logger.Debug(string.Format("Auto update task stopeed"));
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Auto update worker method.
        /// </summary>
        /// <param name="ct">cancellation token</param>
        private void DoAutoUpdateWork(CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
            {
                logger.Info("Auto update cancelled");
                ct.ThrowIfCancellationRequested();
            }
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    logger.Info("Auto update cancelled");
                    break;
                }
                try
                {
                    ScanDatagrid.Dispatcher.Invoke(() => RefreshScanList());
                    Thread.Sleep((int)AutoUpdateInterval.TotalMilliseconds);
                }
                catch (TaskCanceledException ex)
                {
                    logger.InfoFormat("Update task was cancelled - message: {0}", ex.Message);
                    break;
                }
            }
        }

        /// <summary>
        /// Refresh the scan table
        /// </summary>
        private void RefreshScanList()
        {
            stopwatch.Restart();

            // Save current sorting
            List<SortDescription> sortDescriptions = new List<SortDescription>(ScanDatagrid.Items.SortDescriptions);

            ScanData = new ObservableCollection<Scan>(DatabaseScan.FindScans());
            ScanDatagrid.ItemsSource = ScanData;

            // Apply scrolling
            DataGridUtils.ScrollToEnd(ScanDatagrid, IsScrollToEnd);

            // Resort datagrid
            DataGridUtils.SortDataGrid(ScanDatagrid, sortDescriptions);

            OnPropertyChanged("ScanData");
            UpdateStatusBar();
            stopwatch.Stop();
        }

        /// <summary>
        /// Open the scan details dialog.
        /// </summary>
        /// <param name="scan"></param>
        private void OpenScanDetailsDialog(Scan scan)
        {
            if (scan != null)
            {
                ScanDetailsDialog scanDetailsDialog = new ScanDetailsDialog()
                {
                    Owner = this,
                    Scan = scan
                };
                if (scanDetailsDialog.ShowDialog() == true)
                {
                    RefreshScanList();
                }
            }
        }

        /// <summary>
        /// Update profiles
        /// </summary>
        private void UpdateProfiles()
        {
            int index = ProfileData.IndexOf(SelectedProfile);
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileComboBox.ItemsSource = ProfileData;
            OnPropertyChanged("Profiles");

            if (index >= 0 && index < ProfileData.Count)
            {
                SelectedProfile = ProfileData[index];
                TargetText = SelectedProfile.Target;
                CommandText = SelectedProfile.Command;
                OnPropertyChanged("TargetText");
                OnPropertyChanged("CommentText");
                OnPropertyChanged("SelectedProfile");
            }
            else if (ProfileData.Count > 0)
            {
                SelectedProfile = ProfileData[0];
                OnPropertyChanged("SelectedProfile");
            }
            logger.Debug(string.Format("Profiles updated - count: {0}", ProfileData.Count));
        }

        /// <summary>
        /// Returns a scan from the context menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private Scan GetScanFromContextMenu(object sender)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Cancel and delete scan
            return item.SelectedCells.Count > 0 && item.SelectedCells[0] != null ? (Scan)item.SelectedCells[0].Item : null;
        }

        /// <summary>
        /// Delete all hosts of a scan
        /// </summary>
        /// <param name="scan">scan to look for hosts</param>
        private void DeleteHosts(Scan scan)
        {
            DeleteHosts deleteAll = new DeleteHosts
            {
                Owner = this,
                Hosts = DatabaseHost.FindHostsByScanId(scan.Id)
            };
            if (deleteAll.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Delete performace data for the scan.
        /// </summary>
        /// <param name="scan">scan to delete performace for</param>
        private void DeletePerformance(Scan scan)
        {
            DatabasePerformance.Instance.DeletePerformance(scan.Id);
        }

        /// <summary>
        /// Update status bar
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat);
        }
        #endregion

        #region Datagrid handlers
        /// <summary>
        /// Data grid selection changed.
        /// </summary>
        /// <param name="sender">evnt sender</param>
        /// <param name="e">event</param>
        private void OnDataGridSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Scan scan = (Scan)row.Item;
            OpenScanDetailsDialog(scan);
        }

        /// <summary>
        /// Insert a new scan into the database
        /// </summary>
        /// <returns>inserted scan entity</returns>
        private Scan InsertScan()
        {
            Scan scan = new Scan
            {
                Name = NameText,
                Target = TargetText,
                Command = CommandText,
                ProfileId = SelectedProfile != null ? SelectedProfile.Id : -1,
                StartTime = DateTime.Now.Ticks,
                DebugLevel = Command.GetDebugLevel(),
                VerbosityLevel = Command.GetVerbosityLevel(),
                Status = NmapWorkerStatus.STARTED
            };
            scan = DatabaseScan.InsertScan(scan);
            ScanData.Add(scan);
            return scan;
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh scan list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshScanList();
        }

        /// <summary>
        /// Delete all scans.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnClearAllButtonClicked(object sender, RoutedEventArgs e)
        {
            ScanData.Clear();
            DatabaseScan.DeleteAllScans();
            OnPropertyChanged("ScanData");
            UpdateStatusBar();
        }

        /// <summary>
        /// Start scan button click handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanButtonClicked(object sender, RoutedEventArgs e)
        {
            // Choose name
            if (string.IsNullOrEmpty(NameText))
            {
                if (SelectedProfile != null)
                    NameText = SelectedProfile.Name;
                else
                    NameText = "New Scan";
            }

            // Validate command
            string errorMessage = Command.ValidateCommand(CommandText);
            if (errorMessage != string.Empty)
            {
                ValidationError validationError = new ValidationError(new DataErrorValidationRule(), CommandTextBox.GetBindingExpression(TextBox.TextProperty))
                {
                    ErrorContent = FieldValidation.GetError("invalid-options", errorMessage, "Command", "Text box")
                };
                Validation.MarkInvalid(CommandTextBox.GetBindingExpression(TextBox.TextProperty), validationError);
            }
            else
            {
                Scan scan = InsertScan();
                if (scan != null)
                {
                    NmapWorkerManager.AddWorker(scan);
                }
            }
        }

        /// <summary>
        /// Save current command as profile.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnProfileButtonClicked(object sender, RoutedEventArgs e)
        {
            Profile profile = new Profile()
            {
                Command = CommandText,
                Target = TargetText
            };

            SaveProfileDialog SaveProfileDialog = new SaveProfileDialog()
            {
                Owner = this,
                Profile = profile
            };
            if (SaveProfileDialog.ShowDialog() == true)
            {
                UpdateProfiles();
            }
        }

        /// <summary>
        /// Exit button click handler
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnExitButtonClicked(object sender, RoutedEventArgs e)
        {
            // Cancel the asynchronous operation.
            NmapWorkerManager.CancelAll();
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Open scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Scan scan = (Scan)row.Item;
                    OpenScanDetailsDialog(scan);
                    break;
                }
        }

        /// <summary>
        /// Delete selected scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Scan scan = (Scan)row.Item;
                    NmapWorkerManager.CancelScan(scan);
                    DatabaseScan.DeleteScan(scan);
                    ScanData.Remove(scan);
                    break;
                }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/mainwindow/scan-table.htm");
        }
        #endregion

        #region Context menu handlers
        /// <summary>
        /// Context menu loading handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnContextMenuLoading(object sender, RoutedEventArgs e)
        {
            if (sender is ContextMenu contextMenu)
            {
                MenuItem m = LogicalTreeHelper.FindLogicalNode(contextMenu, "NdiffContextMenuItem") as MenuItem;
                m.IsEnabled = false;

                var item = (DataGrid)contextMenu.PlacementTarget;
                if (item.SelectedItems == null || item.SelectedItems.Count == 0)
                {
                    return;
                }
                if (item.SelectedItems.Count == 2)
                {
                    m.IsEnabled = true;
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Open scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanContextDetailsClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Open scan details dialog
            Scan scan = (Scan)item.SelectedCells[0].Item;
            OpenScanDetailsDialog(scan);
        }

        /// <summary>
        /// Restart a previous scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanContextRestartClicked(object sender, RoutedEventArgs e)
        {
            // Get scan
            Scan scan = GetScanFromContextMenu(sender);
            if (scan == null)
            {
                return;
            }

            // Check worker
            NmapWorker nmapWorker = NmapWorkerManager.FindWorkerByScan(scan);
            if (nmapWorker == null || !nmapWorker.IsBusy)
            {
                // Cleanup hosts
                DeleteHosts(scan);
                DeletePerformance(scan);

                //scan = DatabaseScan.FindScanById(scan.Id);
                scan.EndTime = 0;
                scan.Status = NmapWorkerStatus.STARTED;
                scan.Output = scan.OutputXml = null;
                scan = DatabaseScan.UpdateScan(scan);

                // Create new worker
                nmapWorker = NmapWorkerManager.AddWorker(scan);
                nmapWorker.PropertyChanged += HandleWorkerChanges;

                // Refresh list of scans
                RefreshScanList();
            }
            else
            {
                MessageBox.Show("Scan " + scan.Id + " is currently running and cannot be restarted!", "Restart scan (id: " + scan.Id + ")", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Stop a running scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanContextStopClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Get the scan and stop it
            if (item.SelectedCells.Count > 0)
            {
                Scan scan = (Scan)item.SelectedCells[0].Item;
                NmapWorkerManager.CancelScan(scan);
            }
        }

        /// <summary>
        /// Delete selected scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScanContextDeleteClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var dataGrid = (DataGrid)contextMenu.PlacementTarget;

            // Cancel and delete scan
            List<Scan> selectedScans = (List<Scan>)dataGrid.SelectedItems;
            selectedScans.ForEach(s =>
            {
                NmapWorkerManager.CancelScan(s);
                DatabaseScan.DeleteScan(s);
                ScanData.Remove(s);
            });
        }

        /// <summary>
        /// Delete selected scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnContextNdiffClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var dataGrid = (DataGrid)contextMenu.PlacementTarget;

            // Cancel and delete scan
            List<Scan> selectedScans = dataGrid.SelectedItems.Cast<Scan>().ToList();

            // Open show ndiff dialog
            ShowNdiffDialog ndiffDialog = new ShowNdiffDialog
            {
                Owner = this,
                Scans = selectedScans
            };
            ndiffDialog.Show();
        }
        #endregion

        #region Menu item handlers
        /// <summary>
        /// Import XMl file.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnImportXmlMenuItemclicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files\NmapWin",
                Filter = "xml files (*.xml)|*.XML|All files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                string fileName = openFileDialog.FileName;

                XmlReader xmlReaderDialog = new XmlReader()
                {
                    Owner = this,
                    FileName = fileName
                };
                if (xmlReaderDialog.ShowDialog() == true)
                {
                    RefreshScanList();
                }
            }
        }

        /// <summary>
        /// Import data from JSON files
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnImportMenuItemClicked(object sender, RoutedEventArgs e)
        {
            JsonReader jsonReader = new JsonReader
            {
                Owner = this,
                DialogTitle = "Import from JSON"
            };
            if (jsonReader.ShowDialog() == true)
            {
                RefreshScanList();
            }
        }

        /// <summary>
        /// Export data to JSON files
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnExportMenuItemClicked(object sender, RoutedEventArgs e)
        {
            JsonWriter jsonWriter = new JsonWriter
            {
                Owner = this,
                DialogTitle = "Export to JSON"
            };
            if (jsonWriter.ShowDialog() == true)
            {
                RefreshScanList();
            }
        }

        /// <summary>
        /// Exit menu item click handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnExitMenuItemClicked(object sender, RoutedEventArgs e)
        {
            // Cancel the asynchronous operation.
            NmapWorkerManager.CancelAll();

            Application.Current.Shutdown();
        }

        /// <summary>
        /// Option menu item click handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnOptionMenuItemClicked(object sender, RoutedEventArgs e)
        {
            OptionDialog optionDialog = new OptionDialog
            {
                Owner = this
            };
            optionDialog.ShowDialog();
        }

        /// <summary>
        /// Add a job to the service
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnAddJobMenuItemClicked(object sender, RoutedEventArgs e)
        {
            EditJobDialog editJobDialog = new EditJobDialog
            {
                Owner = this,
                IsNew = true,
                Job = new Job()
            };
            if (editJobDialog.ShowDialog() == true)
            {

            }
        }

        /// <summary>
        /// OPens the job list dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobListMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ListJobDialog listJobDialog = new ListJobDialog
            {
                Owner = this
            };
            if (listJobDialog.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Show the help
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHelpMenuItemClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic(null);
        }

        /// <summary>
        /// About menu item clíck handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnAboutMenuItemClicked(object sender, RoutedEventArgs e)
        {
            AboutDialog aboutDialog = new AboutDialog
            {
                Owner = this
            };
            aboutDialog.ShowDialog();
        }

        /// <summary>
        /// About menu item clíck handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnLicenseMenuItemClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/appendix/license.htm");
        }

        /// <summary>
        /// Add profile menu item clíck handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnAddProfileMenuItemClicked(object sender, RoutedEventArgs e)
        {
            EditProfileDialog editProfileDialog = new EditProfileDialog
            {
                Owner = this,
                SelectedProfile = null,
                UpdateMode = false
            };
            if (editProfileDialog.ShowDialog() == true)
            {
                UpdateProfiles();
            }
        }

        /// <summary>
        /// Edit profile menu item clíck handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnEditProfileMenuItemClicked(object sender, RoutedEventArgs e)
        {
            EditProfileDialog editProfileDialog = new EditProfileDialog
            {
                Owner = this,
                SelectedProfile = SelectedProfile,
                UpdateMode = true
            };
            if (editProfileDialog.ShowDialog() == true)
            {
                UpdateProfiles();
            }
        }

        /// <summary>
        /// Open host list dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostListMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ListHostDialog listHostDialog = new ListHostDialog
            {
                Owner = this,
            };
            listHostDialog.Show();
        }

        /// <summary>
        /// Open host list dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnPortListMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ListPortDialog listPortDialog = new ListPortDialog
            {
                Owner = this,
            };
            listPortDialog.Show();
        }

        /// <summary>
        /// Open profile list dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnProfileListMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ListProfileDialog listProfileDialog = new ListProfileDialog
            {
                Owner = this,
            };
            if (listProfileDialog.ShowDialog() == true)
            {
                UpdateProfiles();
            }
        }

        /// <summary>
        /// Delete profile menu item clíck handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnDeleteProfileMenuItemClicked(object sender, RoutedEventArgs e)
        {
            DeleteProfileDialog deleteProfileDialog = new DeleteProfileDialog
            {
                Owner = this,
                SelectedProfile = SelectedProfile
            };
            if (deleteProfileDialog.ShowDialog() == true)
            {
                UpdateProfiles();
            }
        }

        /// <summary>
        /// Opoen the script list dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScriptListMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ListScriptDialog listScriptDialog = new ListScriptDialog
            {
                Owner = this
            };
            if (listScriptDialog.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// New script menu item clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnNewScriptMenuItemClicked(object sender, RoutedEventArgs e)
        {
            EditScriptDialog editScriptDialog = new EditScriptDialog
            {
                Owner = this,
                IsNew = true,
                Title = "New Script"
            };
            if (editScriptDialog.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Edit script menu item clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnEditScriptMenuItemClicked(object sender, RoutedEventArgs e)
        {
            EditScriptDialog editScriptDialog = new EditScriptDialog
            {
                Owner = this,
                IsNew = false,
                Title = "Edit Script"
            };
            if (editScriptDialog.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Edit script menu item clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnNdiffMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ShowNdiffDialog ndiffDialog = new ShowNdiffDialog
            {
                Owner = this
            };
            if (ndiffDialog.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        private void OnWriteHelpFilesMenuItemClicked(object sender, RoutedEventArgs e)
        {
            WriteHelpFiles writeHelpFiles = new WriteHelpFiles
            {
                Owner = this,
                Title = "Writing Help Files"
            };
            if (writeHelpFiles.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        private void OnCreateDatabaseMenuItemClicked(object sender, RoutedEventArgs e)
        {
            RecreateDatabase recreateDatabase = new RecreateDatabase
            {
                Owner = this,
                Recreate = false,
                Title = "Create Database"
            };
            if (recreateDatabase.ShowDialog() == true)
            {
                ScanData.Clear();
                ScanData = new ObservableCollection<Scan>(DatabaseScan.FindScans());
                ScanDatagrid.ItemsSource = ScanData;
                OnPropertyChanged("ScanDataGrid");
                UpdateStatusBar();
            }
        }

        private void OnRecreateDatabaseMenuItemClicked(object sender, RoutedEventArgs e)
        {
            bool hasAutoupdate = IsAutoUpdate;
            IsAutoUpdate = false;
            RecreateDatabase recreateDatabase = new RecreateDatabase
            {
                Owner = this,
                Recreate = true,
                Title = "Recreate Database"
            };
            if (recreateDatabase.ShowDialog() == true)
            {
                ScanDatagrid.ItemsSource = ScanData = new ObservableCollection<Scan>(DatabaseScan.FindScans());
                OnPropertyChanged("ScanDataGrid");
                UpdateStatusBar();
                if (hasAutoupdate)
                    IsAutoUpdate = true;
            }
        }

        private void OnReloadDefaultsMenuItemClicked(object sender, RoutedEventArgs e)
        {
            ReloadDefaults reloadDefaults = new ReloadDefaults
            {
                Owner = this,
                Title = "Reload Defaults"
            };
            if (reloadDefaults.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        private void OnDeleteDatabaseMenuItemClicked(object sender, RoutedEventArgs e)
        {
            DeleteDatabase deleteDatabase = new DeleteDatabase
            {
                Owner = this,
                Title = "Delete database"
            };
            if (deleteDatabase.ShowDialog() == true)
            {
                ScanData.Clear();
                UpdateStatusBar();
            }
        }

        private void OnRestartApplicationMenuItemClicked(object sender, RoutedEventArgs e)
        {
            CheckForUpdates checkForUpdates = new CheckForUpdates
            {
                Owner = this,
                Title = "Check for updates..."
            };
            if (checkForUpdates.ShowDialog() == true)
            {
            }
        }
        #endregion

        #region Taskbar handlers
        private void ThumbButtonInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Clicked");
        }

        private void ThumbButtonInfo_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Clicked");
        }
        #endregion

        #region Shortcut handlers
        private void RefreshCommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RefreshCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            RefreshScanList();
        }
        #endregion

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void HandleWindowClosing(object sender, CancelEventArgs e)
        {
            NmapWorkerManager.CancelAll();

            Application.Current.Shutdown();
        }

        private void HandleWorkerChanges(object sender, PropertyChangedEventArgs e)
        {
            NmapWorker worker = sender as NmapWorker;
            if (worker.CancellationPending)
            {
                return;
            }
            switch (e.PropertyName)
            {
                case "WorkerOutputChanged":
                    ScanDatagrid.Dispatcher.Invoke(() =>
                    {
                        Scan s = ScanData.FirstOrDefault(i => i.Id == worker.Scan.Id);
                        ScanData[ScanData.IndexOf(s)] = worker.Scan;
                        ScanDatagrid.ItemsSource = ScanData;
                        OnPropertyChanged("ScanDatagrid");
                        UpdateStatusBar();
                    });
                    break;
            }
        }
        #endregion
    }
}
