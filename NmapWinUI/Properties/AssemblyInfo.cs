
using System.Reflection;
using System.Resources;

// General Information
[assembly: AssemblyTitle("NmapWin")]
[assembly: AssemblyDescription("Native Windows UI for nmap")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rodion Raskolnikov")]
[assembly: AssemblyProduct("nmapwin")]
[assembly: AssemblyCopyright("Copyright � 2020 - 2021 Rodion Raskolnikov")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version informationr(
[assembly: AssemblyVersion("1.2.78.270")]
[assembly: AssemblyFileVersion("1.2.78.270")]
[assembly: NeutralResourcesLanguageAttribute( "en-US" )]

// Logging
[assembly: log4net.Config.XmlConfigurator]

