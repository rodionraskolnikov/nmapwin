﻿
using QuickGraph;

namespace NmapWinCommon.map
{
    public class NmapEdge : Edge<NmapVertex>
    {
        public string ID
        {
            get;
            private set;
        }

        public NmapEdge(string id, NmapVertex source, NmapVertex target) : base(source, target)
        {
            ID = id;
        }
    }
}
