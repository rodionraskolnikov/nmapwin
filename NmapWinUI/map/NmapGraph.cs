﻿using QuickGraph;

namespace NmapWinCommon.map
{
    public class NmapGraph : BidirectionalGraph<NmapVertex, NmapEdge>
    {
        public NmapGraph() { }

        public NmapGraph(bool allowParallelEdges) : base(allowParallelEdges) { }

        public NmapGraph(bool allowParallelEdges, int vertexCapacity) : base(allowParallelEdges, vertexCapacity) { }
    }
}
