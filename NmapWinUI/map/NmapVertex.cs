﻿using System.ComponentModel;

namespace NmapWinCommon.map
{
    public class NmapVertex : INotifyPropertyChanged
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        private string _osMatch = "unknown";

        public string OsMatch
        {
            get
            {
                return _osMatch;
            }
            set
            {
                if (value != null)
                {
                    _osMatch = value;
                    OnPropertyChanged("OsMatch");
                }
            }
        }

        public NmapVertex()
        {
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}", Id, OsMatch);
        }

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
