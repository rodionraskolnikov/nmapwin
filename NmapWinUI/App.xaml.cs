﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Monitor;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;

namespace NmapWinUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(App));

        private SplashScreen splashScreen = null;

        private readonly DateTime start = DateTime.Now;

        private readonly static int splashTime = 6000;

        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            logger.Debug("Splash screen starting");
            splashScreen = new SplashScreen();
            splashScreen.PropertyChanged += HandleSplashScreen;
            splashScreen.Show();
        }

        private void HandleSplashScreen(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SplashScreenReady":
                    int sleepTime = splashTime - (int)DateTime.Now.Subtract(start).TotalMilliseconds;
                    if (sleepTime > 0)
                    {
                        Thread.Sleep(sleepTime);
                    }
                    splashScreen.Close();
                    ProcessMonitor.Instance.RunWorkerAsync();
                    ProcessMonitorConsolidator.Instance.RunWorkerAsync();
                    break;
            }
        }
    }
}
