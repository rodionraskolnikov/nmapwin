﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Worker;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for ScanDetailsDialog.xaml
    /// </summary>
    public partial class ScanDetailsDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ScanDetailsDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Host database singleton
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Performance database singleton
        /// </summary>
        private readonly DatabasePerformance DatabasePerformance = DatabasePerformance.Instance;

        /// <summary>
        /// Nmap worker manager singleton
        /// </summary>
        private readonly NmapWorkerManager NmapWorkerManager = NmapWorkerManager.Instance;

        /// <summary>
        /// Current nmap worker
        /// </summary>
        private NmapWorker nmapWorker;

        /// <summary>
        /// Collection for the host table
        /// </summary>
        public ObservableCollection<Host> HostData { get; set; }

        /// <summary>
        /// Performance data
        /// </summary>
        public ObservableCollection<Performance> PerformanceData { get; set; }

        /// <summary>
        /// Dialog initialization
        /// </summary>
        private bool DialogInitialized = false;

        /// <summary>
        /// Date time formatter for chart X-axis
        /// </summary>
        public Func<double, string> XFormatter { get; set; }

        /// <summary>
        /// Value formatter for chart Y-axis
        /// </summary>
        public Func<double, string> YFormatter { get; set; }

        /// <summary>
        /// Chart series
        /// </summary>
        public SeriesCollection Series { get; set; }

        /// <summary>
        /// Chart configuration
        /// </summary>
        private CartesianMapper<ChartDateModel> chartConfig;

        /// <summary>
        /// Auto-update cancellation source
        /// </summary>
        private CancellationTokenSource AutoUpdateTokenSource;

        /// <summary>
        /// Auto-update interval
        /// </summary>
        private TimeSpan AutoUpdateInterval = TimeSpan.FromSeconds(5);

        /// <summary>
        /// Current tab item
        /// </summary>
        private TabItem currentTabItem;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scan">currrent scan</param>
        public ScanDetailsDialog()
        {
            InitializeComponent();

            InitializeChart();

            InitializeAutoUpdater();

            DataContext = this;
        }
        #endregion

        #region Initialization
        /// <summary>
        /// Initialize the scan details dialog.
        /// </summary>
        private void InitializeDialog()
        {
            Title = "Scan Details (id: " + Scan.Id + ")";

            Profile = DatabaseProfile.FindProfileById(Scan.ProfileId);

            nmapWorker = NmapWorkerManager.Instance.FindWorkerByScan(Scan);
            if (nmapWorker != null)
            {
                ScanStopButton.IsEnabled = true;
                nmapWorker.PropertyChanged += HandleWorkerChanges;
            }

            XmlPrettyPrintConverter.IsPrettyPrint = IsPrettyPrint;

            UpdatePerformanceData();

            DialogInitialized = true;
            logger.Debug("Dialog initialized");
            UpdateStatusBar();
        }

        /// <summary>
        /// Initialization of the performance chart
        /// </summary>
        private void InitializeChart()
        {
            chartConfig = Mappers.Xy<ChartDateModel>()
                .X(dayModel => dayModel.DateTime.Ticks)
                .Y(dayModel => dayModel.Value);

            XFormatter = value => new DateTime((long)value).ToString("dd/MM/yyyy HH:mm:ss");

            PerformanceChart.DisableAnimations = true;
            PerformanceChart.Pan = PanningOptions.Xy;
            PerformanceChart.Zoom = ZoomingOptions.Y;

            Series = new SeriesCollection(chartConfig)
            {
                new LineSeries
                {
                    Fill = Brushes.Transparent,
                    LineSmoothness = 0,
                    Name = "CPU",
                    Title = "CPU"
                }
            };
        }

        /// <summary>
        /// Configure the auto-updater
        /// </summary>
        private void InitializeAutoUpdater()
        {
            if (IsAutoUpdate)
            {
                AutoUpdateTokenSource = new CancellationTokenSource();
                Task.Run(() => DoAutoUpdateWork(AutoUpdateTokenSource.Token), AutoUpdateTokenSource.Token);
                logger.Debug(string.Format("Auto update task started - interval: {0}s", (int)AutoUpdateInterval.TotalMilliseconds));
            }
            else
            {
                if (AutoUpdateTokenSource != null)
                {
                    AutoUpdateTokenSource.Cancel();
                    logger.Debug(string.Format("Auto update task stopeed"));
                }
            }
        }
        #endregion

        #region Bindings
        /// <summary>
        /// Scan profile
        /// </summary>
        private Profile _profile;
        public Profile Profile
        {
            get
            {
                return _profile;
            }
            set
            {
                _profile = value;
            }
        }

        /// <summary>
        /// Current scan.
        /// </summary>
        private Scan _scan;
        public Scan Scan
        {
            get
            {
                return _scan;
            }
            set
            {
                _scan = value;
                OnPropertyChanged("Scan");
                if (!DialogInitialized)
                {
                    InitializeDialog();
                }
            }
        }

        /// <summary>
        /// XML pretty print flag.
        /// </summary>
        private bool _isPrettyPrint = false;
        public bool IsPrettyPrint
        {
            get
            {
                return _isPrettyPrint;
            }
            set
            {
                _isPrettyPrint = value;
                XmlPrettyPrintConverter.IsPrettyPrint = value;
                OnPropertyChanged("Scan");
            }
        }

        /// <summary>
        /// Scroll to end flag.
        /// </summary>
        private bool _isScrollToEnd = false;
        public bool IsScrollToEnd
        {
            get
            {
                return _isScrollToEnd;
            }
            set
            {
                _isScrollToEnd = value;
                XmlPrettyPrintConverter.IsPrettyPrint = value;
                OnPropertyChanged("Scan");
            }
        }

        /// <summary>
        /// Scroll to host data grid end flag.
        /// </summary>
        private bool _isGridScrollToEnd = false;
        public bool IsGridScrollToEnd
        {
            get
            {
                return _isGridScrollToEnd;
            }
            set
            {
                _isGridScrollToEnd = value;
                OnPropertyChanged("Scan");
            }
        }

        /// <summary>
        /// Is busy índicator.
        /// </summary>
        private bool _isBusy = false;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                OnPropertyChanged("IsBusy");
                OnPropertyChanged("IsBusyVisible");
            }
        }

        /// <summary>
        /// Busy visibility
        /// </summary>
        public Visibility IsBusyVisible
        {
            get
            {
                return IsBusy ? Visibility.Visible : Visibility.Hidden;
            }
        }

        /// <summary>
        /// Reparse button available.
        /// </summary>
        public bool IsXmlOutputAvailable
        {
            get
            {
                return Scan.OutputXml != null;
            }
        }

        /// <summary>
        /// Chart minimum
        /// </summary>
        private DateTime _chartMinimum;
        public DateTime ChartMinimum
        {
            get
            {
                return _chartMinimum;
            }

            set
            {
                if (_chartMinimum == value)
                    return;
                _chartMinimum = value;
                OnPropertyChanged("ChartMinimum");
            }
        }

        /// <summary>
        /// Chart maximum
        /// </summary>
        private DateTime _chartMaximum;
        public DateTime ChartMaximum
        {
            get
            {
                return _chartMaximum;
            }

            set
            {
                if (_chartMaximum == value)
                    return;
                _chartMaximum = value;
                OnPropertyChanged("ChartMaximum");
            }
        }

        /// <summary>
        /// Auto update flag.
        /// </summary>
        private bool _isAutoUpdate = false;
        public bool IsAutoUpdate
        {
            get
            {
                return _isAutoUpdate;
            }
            set
            {
                _isAutoUpdate = value;
                InitializeAutoUpdater();
                OnPropertyChanged("IsAutoUpdate");
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            if (nmapWorker != null)
            {
                Scan = nmapWorker.Scan;
            }
            else
            {
                Scan = DatabaseScan.FindScanById(_scan.Id);
            }
            OnPropertyChanged("Scan");
        }

        /// <summary>
        /// Restart the scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRestartButtonClicked(object sender, RoutedEventArgs e)
        {
            _scan = DatabaseScan.Instance.FindScanById(_scan.Id);
            if (nmapWorker != null && nmapWorker.IsBusy)
            {
                nmapWorker.CancelAsync();
                NmapWorkerManager.RemoveWorker(nmapWorker);
            }

            nmapWorker = NmapWorkerManager.AddWorker(_scan);
            OnPropertyChanged("WorkerStatusChanged");
        }

        /// <summary>
        /// Cancel the asynchronous operation.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnStopButtonClicked(object sender, RoutedEventArgs e)
        {
            if (nmapWorker == null)
            {
                return;
            }
            nmapWorker.Cancel();
            OnPropertyChanged("WorkerStatusChanged");
        }

        /// <summary>
        /// Starts the network map dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnNetworkMapButtonClicked(object sender, RoutedEventArgs e)
        {
            NetworkMapDialog networkMapDialog = new NetworkMapDialog
            {
                Owner = this,
                Scan = Scan
            };
            if (networkMapDialog.ShowDialog() == true)
            {

            }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/scandetails/scan-details.htm");
        }

        /// <summary>
        /// Close the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            if (IsAutoUpdate)
            {
                IsAutoUpdate = false;
            }
            Close();
        }

        /// <summary>
        /// Delete the scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            DatabaseScan.DeleteScan(_scan);
            _scan.Status = NmapWorkerStatus.UNKNOWN;
            Close();
        }

        /// <summary>
        /// Refresh the host list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHostRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            // Refresh host table
            UpdateHostData();

            currentTabItem.Focus();
        }

        /// <summary>
        /// Refresh the performance chart.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnPerformanceRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            UpdatePerformanceData();
            currentTabItem.Focus();
        }

        public void OnPerformanceDefaultViewButtonClicked(object sender, RoutedEventArgs e)
        {
            XAxis.MinValue = double.NaN;
            XAxis.MaxValue = double.NaN;
            YAxis.MinValue = double.NaN;
            YAxis.MaxValue = double.NaN;
        }


        /// <summary>
        /// Button handler for the toolbar button delete-down.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHostDeleteDownButtonClicked(object sender, RoutedEventArgs e)
        {
            List<Host> hosts = DatabaseHost.FindDownHostsByScanId(Scan.Id);
            if (hosts.Count > 0)
            {
                DeleteHosts deleteDown = new DeleteHosts
                {
                    Owner = this,
                    Hosts = hosts
                };
                if (deleteDown.ShowDialog() == true)
                {
                    UpdateHostData();
                }
            }
        }

        /// <summary>
        /// Delete all hosts for a scan.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHostDeleteAllButtonClicked(object sender, RoutedEventArgs e)
        {
            Scan.Hosts.Clear();
            Scan = DatabaseScan.UpdateScan(Scan);
            OnPropertyChanged("Scan");

            DeleteHosts deleteAll = new DeleteHosts
            {
                Owner = this,
                Hosts = DatabaseHost.FindHostsByScanId(Scan.Id)
            };
            if (deleteAll.ShowDialog() == true)
            {
                HostData.Clear();
                OnPropertyChanged("HostData");
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Opens the host details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHostOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Host host = (Host)row.Item;
                    OpenHostDetailsDialog(host);
                    break;
                }
        }

        /// <summary>
        /// Deletes the selected host.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHostDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Host host = (Host)row.Item;
                    DeleteHost(host);
                    break;
                }
        }

        /// <summary>
        /// Opens the host details dialog on data grid row selection.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            OpenHostDetailsDialog((Host)row.Item);
        }

        /// <summary>
        /// Refresh the output windows.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnOutputRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Scan = DatabaseScan.FindScanById(_scan.Id);
            OnPropertyChanged("Scan");
            currentTabItem.Focus();
        }

        /// <summary>
        /// Delete output button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnOutputDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            _scan.Output = null;
            OnPropertyChanged("Scan");
        }

        /// <summary>
        /// Reparse XMl output.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnReparseXmlOutputButtonClicked(object sender, RoutedEventArgs e)
        {
            ReparseXml reparseXml = new ReparseXml(Scan)
            {
                Owner = this
            };
            if (reparseXml.ShowDialog() == true)
            {
                UpdateHostData();
            }
        }

        /// <summary>
        /// Delete output XML button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnOutputXmlDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            _scan.OutputXml = null;
            OnPropertyChanged("Scan");
        }

        /// <summary>
        /// Opens the full output dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnOutputFullButtonClicked(object sender, RoutedEventArgs e)
        {
            FullOutputDialog fullOutputDialog = new FullOutputDialog
            {
                Owner = this,
                Scan = _scan,
                Output = _scan.Output,
                XmlOutput = false
            };
            fullOutputDialog.Show();
        }

        /// <summary>
        /// Opens the full output dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnOutputXmlFullButtonClicked(object sender, RoutedEventArgs e)
        {
            FullOutputDialog fullOutputDialog = new FullOutputDialog
            {
                Owner = this,
                Scan = _scan,
                Output = _scan.OutputXml,
                XmlOutput = true
            };
            fullOutputDialog.Show();
        }

        /// <summary>
        /// Opens the host details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OpenHostDetailsDialog(Host host)
        {
            // Lazy loading of host attributes
            host = DatabaseHost.FindHostById(host.Id);
            HostDetailsDialog hostDetailsDialog = new HostDetailsDialog
            {
                Owner = this,
                Host = host
            };
            hostDetailsDialog.Show();
        }

        /// <summary>
        /// Opens the host details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostContextDetailsClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Get the host and profile
            Host host = (Host)item.SelectedCells[0].Item;
            HostDetailsDialog hostDetailsDialog = new HostDetailsDialog
            {
                Owner = this,
                Host = host
            };
            hostDetailsDialog.Show();
            logger.Debug(string.Format("Open host details started - id: {0}", host.Id));
        }

        /// <summary>
        /// Rescan the host.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostContextScanClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Get the host and profile
            Host host = (Host)item.SelectedCells[0].Item;
            Profile profile = DatabaseProfile.FindProfileById(_scan.ProfileId);

            // Start new scan
            Scan hostScan = new Scan
            {
                Command = _scan.Command,
                Target = host.PrimaryAddress,
                Name = "Host scan " + host.PrimaryAddress,
                ProfileId = profile != null ? profile.Id : -1,
                StartTime = DateTime.Now.Ticks,
                Status = NmapWorkerStatus.STARTED
            };

            // Insert into database and start worker
            hostScan = DatabaseScan.InsertScan(hostScan);
            NmapWorkerManager.AddWorker(hostScan);
            OnPropertyChanged("WorkerStatusChanged");
            logger.Debug(string.Format("Host scan started - id: {0}", host.Id));
        }

        /// <summary>
        /// Deletes the host from zthe context menu.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostContextDeleteClicked(object sender, RoutedEventArgs e)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Open scan details dialog
            Host host = (Host)item.SelectedCells[0].Item;
            DeleteHost(host);
        }

        /// <summary>
        /// Window close callback
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnWindowClose(object sender, CancelEventArgs e)
        {
            if (IsAutoUpdate)
            {
                IsAutoUpdate = false;
            }
        }
        #endregion

        #region Tab handlers
        /// <summary>
        /// Tab selection changed
        /// </summary>
        /// <remarks>uses lazy loading</remarks>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnTabSelectionChanged(object sender, RoutedEventArgs e)
        {
            currentTabItem = (TabItem)(sender as TabControl).SelectedItem;

            IsBusy = true;
            switch (currentTabItem.Name)
            {
                case "HostsTabe":
                    UpdateHostData();
                    break;
                case "PerformanceTab":
                    UpdatePerformanceData();
                    break;
            }
            IsBusy = false;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Update stats bar
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }

        /// <summary>
        ///  Refresh host data.
        /// </summary>
        private void UpdateHostData()
        {
            // Save current sorting
            List<SortDescription> sortDescriptions = new List<SortDescription>(HostDatagrid.Items.SortDescriptions);

            // Refresh data from database
            HostData = new ObservableCollection<Host>(DatabaseHost.FindHostsByScanId(Scan.Id));
            HostDatagrid.ItemsSource = HostData;

            // Apply scrolling
            DataGridUtils.ScrollToEnd(HostDatagrid, IsGridScrollToEnd);

            // Resort datagrid
            DataGridUtils.SortDataGrid(HostDatagrid, sortDescriptions);

            OnPropertyChanged("HostData");
            UpdateStatusBar();
        }

        /// <summary>
        /// Refresh host data.
        /// </summary>
        private void UpdatePerformanceData()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            long min = DatabasePerformance.FindMinimum(Scan.Id);
            long max = DatabasePerformance.FindMaximum(Scan.Id);
            if (min < 0 && max < 0)
            {
                ChartMinimum = DateTime.Now;
                ChartMaximum = DateTime.Now.AddMinutes(1);
            }
            else
            {
                ChartMinimum = new DateTime(min);
                ChartMaximum = new DateTime(max);
            }
            logger.DebugFormat("Initialized x-axis bounds - xmin: {0} xmax: {1}", ChartMinimum, ChartMaximum);

            PerformanceData = new ObservableCollection<Performance>(DatabasePerformance.FindPerformanceDataByScan(Scan.Id));
            logger.DebugFormat("Got performance data - count: {0} [{1}ms]", PerformanceData.Count, stopWatch.ElapsedMilliseconds);

            ChartValues<ChartDateModel> chartValues = new ChartValues<ChartDateModel>();
            foreach (Performance p in PerformanceData)
            {
                chartValues.Add(new ChartDateModel { DateTime = new DateTime(p.Timestamp), Value = p.Cpu });
            }
            logger.DebugFormat("Performance converted - count: {0} [{1}ms]", PerformanceData.Count, stopWatch.ElapsedMilliseconds);

            if (Series[0].Values != null)
            {
                Series[0].Values.Clear();
                Series[0].Values.AddRange(chartValues);
            }
            else
            {
                Series[0].Values = chartValues;
            }

            logger.DebugFormat("Performance chart updated - count: {0} [{1}ms]", PerformanceData.Count, stopWatch.ElapsedMilliseconds);

            OnPropertyChanged("Series");
            UpdateStatusBar();
        }

        /// <summary>
        /// Deletes a host.
        /// </summary>
        /// <param name="host"></param>
        private void DeleteHost(Host host)
        {
            DatabaseHost.DeleteHost(host);
            HostData.Remove(host);
            logger.DebugFormat("Host deleted - host: {0}", host.Identifier);
        }

        /// <summary>
        /// Auto update worker method.
        /// </summary>
        /// <param name="ct">cancellation token</param>
        private void DoAutoUpdateWork(CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
            {
                ct.ThrowIfCancellationRequested();
            }
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                try
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        UpdatePerformanceData();
                    });
                    Thread.Sleep((int)AutoUpdateInterval.TotalMilliseconds);
                }
                catch (TaskCanceledException ex)
                {
                    logger.InfoFormat("Update task was cancelled - message: {0}", ex.Message);
                    break;
                }
            }
        }
        #endregion

        #region Event handling       
        /// <summary>
        /// Property change event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Send property change event.
        /// </summary>
        /// <param name="propertyName">property name, which changed</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatusBar();
        }

        /// <summary>
        /// Handle worker changes.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="args">arguments</param>
        private void HandleWorkerChanges(object sender, PropertyChangedEventArgs args)
        {
            NmapWorker worker = sender as NmapWorker;
            switch (args.PropertyName)
            {
                case "WorkerOutputChanged":
                    Dispatcher.Invoke(() =>
                    {
                        Scan = nmapWorker.Scan;
                        UpdateStatusBar();
                    });
                    break;
            }
        }
        #endregion
    }
}
