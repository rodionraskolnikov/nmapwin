﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Microsoft.Win32;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for EditProfileDialog.xaml
    /// </summary>
    public partial class EditProfileDialog : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Field
        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Scripts database singleton
        /// </summary>
        private readonly DatabaseScript DatabaseScript = DatabaseScript.Instance;

        /// <summary>
        /// Input field validation.
        /// </summary>
        private readonly FieldValidation Validation = FieldValidation.Instance;

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Current command singleton
        /// </summary>
        public Command Command { get; set; } = Command.Instance;

        /// <summary>
        /// Profile list-
        /// </summary>
        public List<Profile> Profiles { get; set; }

        /// <summary>
        /// Update / new modes
        /// </summary>
        public bool UpdateMode { get; set; } = true;

        /// <summary>
        /// List of scripts.
        /// </summary>
        public List<Script> ScriptList { get; set; }

        /// <summary>
        /// Script argument list
        /// </summary>
        public List<ScriptArgument> ScriptArgumentList { get; set; }
        #endregion

        #region Bindings
        /// <summary>
        /// Selected script argument
        /// </summary>
        private Script _scriptListSelectedItem;
        public Script ScriptListSelection
        {
            get
            {
                return _scriptListSelectedItem;
            }
            set
            {
                if (!value.Equals(_scriptListSelectedItem))
                {
                    _scriptListSelectedItem = value;
                    LoadScriptArguments(value);
                    OnPropertyChanged("ScriptArgumentList");
                }
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public EditProfileDialog()
        {
            InitializeComponent();

            InitializeScriptList();

            InitializeComboBoxes();

            DataContext = this;
        }
        #endregion

        #region Initialization           
        private void InitializeCheckBoxes()
        {
            // Scan page
            AdvancedCheckBox.IsChecked = Command.IsChecked("-A");
            OperatingSystemCheckBox.IsChecked = Command.IsChecked("-O");
            VersionDetectionCheckBox.IsChecked = Command.IsChecked("-sV");
            DisableDnsCheckBox.IsChecked = Command.IsChecked("-n");
            Ipv6CheckBox.IsChecked = Command.IsChecked("-6");
            IdleScanCheckBox.IsChecked = IdleScanTextBox.IsEnabled = Command.IsChecked("-sI");
            FtpBounceCheckBox.IsChecked = FtpBounceTextBox.IsEnabled = Command.IsChecked("-b");
            DisableArpPingCheckBox.IsChecked = Command.IsChecked("--disable-arp-ping");
            DiscoveryIgnoreRstCheckBox.IsChecked = Command.IsChecked("--discovery-ignore-rst");

            // Ping page
            DontPingCheckBox.IsChecked = SelectedProfile.CheckOption("-Pn");
            IcmpPingCheckBox.IsChecked = SelectedProfile.CheckOption("-PE");
            IcmpTimestampCheckBox.IsChecked = SelectedProfile.CheckOption("-PP");
            IcmpNetmaskCheckBox.IsChecked = SelectedProfile.CheckOption("-PM");
            AckPingCheckBox.IsChecked = AckPingTextBox.IsEnabled = Command.IsChecked("-PA");
            SynPingCheckBox.IsChecked = SynPingTextBox.IsEnabled = Command.IsChecked("-PS");
            UdpProbesCheckBox.IsChecked = UdpProbesTextBox.IsEnabled = Command.IsChecked("-PU");
            IpProtoProbesCheckBox.IsChecked = IpProtoProbesTextBox.IsEnabled = Command.IsChecked("-PO");
            SctpPingProbesCheckBox.IsChecked = SctpPingProbesTextBox.IsEnabled = Command.IsChecked("-PY");

            // Target page
            ExcludeCheckBox.IsChecked = ExcludeTextBox.IsEnabled = Command.IsChecked("--exclude");
            ExcludeFileCheckBox.IsChecked = ExcludeFileTextBox.IsEnabled = ExcludeFileBrowseButton.IsEnabled = Command.IsChecked("--excludefile");
            TargetListFileCheckBox.IsChecked = TargetListFileTextBox.IsEnabled = TargetListFileBrowseButton.IsEnabled = Command.IsChecked("-iL");
            ScanRandomHostCheckBox.IsChecked = ScanRandomHostTextBox.IsEnabled = Command.IsChecked("-iR");
            PortsToScanCheckBox.IsChecked = PortsToScanTextBox.IsEnabled = Command.IsChecked("-p");
            FastScanCheckBox.IsChecked = SelectedProfile.CheckOption("-F");

            // Source page
            DecoysCheckBox.IsChecked = DecoysTextBox.IsEnabled = Command.IsChecked("-D");
            SourceIPCheckBox.IsChecked = SourceIPTextBox.IsEnabled = Command.IsChecked("-S");
            SourcePortCheckBox.IsChecked = SourcePortTextBox.IsEnabled = Command.IsChecked("--source-port");
            NetworkInterfaceCheckBox.IsChecked = NetworkInterfaceTextBox.IsEnabled = Command.IsChecked("-e");

            // Misc page
            FragmentIpPacketsCheckBox.IsChecked = SelectedProfile.CheckOption("-f");
            PacketTraceCheckBox.IsChecked = SelectedProfile.CheckOption("--packet-trace");
            DisableRandomPortsCheckBox.IsChecked = SelectedProfile.CheckOption("-r");
            TracerouteCheckBox.IsChecked = SelectedProfile.CheckOption("--traceroute");
            IPv4TtlCheckBox.IsChecked = IPv4TtlTextBox.IsEnabled = SelectedProfile.CheckOption("--ttl");
            VerbosityLevelCheckBox.IsChecked = VerbosityLevelTextBox.IsEnabled = VerbosityLevelSlider.IsEnabled = SelectedProfile.CheckOption("-v");
            DebugLevelCheckBox.IsChecked = DebugLevelTextBox.IsEnabled = DebugLevelSlider.IsEnabled = SelectedProfile.CheckOption("-d");
            MaxRetriesCheckBox.IsChecked = MaxRetriesTextBox.IsEnabled = SelectedProfile.CheckOption("--max-retries");

            // Timing page
            MaxScanTimeCheckBox.IsChecked = MaxScanTimeTextBox.IsEnabled = SelectedProfile.CheckOption("--host-timeout");
            MaxProbeTimeoutCheckBox.IsChecked = MaxProbeTimeoutTextBox.IsEnabled = SelectedProfile.CheckOption("--max-rtt-timeout");
            MinProbeTimeoutCheckBox.IsChecked = MinProbeTimeoutTextBox.IsEnabled = SelectedProfile.CheckOption("--min-rtt-timeout");
            InitialProbeTimeoutCheckBox.IsChecked = InitialProbeTimeoutTextBox.IsEnabled = SelectedProfile.CheckOption("--initial-rtt-timeout");
            MaxHostGroupsCheckBox.IsChecked = MaxHostGroupsTextBox.IsEnabled = SelectedProfile.CheckOption("--max-hostgroup");
            MinHostGroupsCheckBox.IsChecked = MinHostGroupsTextBox.IsEnabled = SelectedProfile.CheckOption("--min-hostgroup");
            MaxOutStandingProbesCheckBox.IsChecked = MaxOutStandingProbesTextBox.IsEnabled = SelectedProfile.CheckOption("--max-parallelism");
            MinOutStandingProbesCheckBox.IsChecked = MinOutStandingProbesTextBox.IsEnabled = SelectedProfile.CheckOption("--min-parallelism");
            MaxScanDelayCheckBox.IsChecked = MaxScanDelayTextBox.IsEnabled = SelectedProfile.CheckOption("--max-scan-delay");
            ScanDelayCheckBox.IsChecked = ScanDelayTextBox.IsEnabled = SelectedProfile.CheckOption("--ScanDelayTextBox");

            // Output page
            OutputNormalCheckBox.IsChecked = OutputNormalTextBox.IsEnabled = OutputNormalBrowseButton.IsEnabled = SelectedProfile.CheckOption("-oN");
            OutputXmlCheckBox.IsChecked = OutputXmlTextBox.IsEnabled = OutputXmlBrowseButton.IsEnabled = SelectedProfile.CheckOption("-oX");
            OutputGrepCheckBox.IsChecked = OutputGrepTextBox.IsEnabled = OutputGrepBrowseButton.IsEnabled = SelectedProfile.CheckOption("-oG");
            OutputScriptCheckBox.IsChecked = OutputScriptTextBox.IsEnabled = OutputScriptBrowseButton.IsEnabled = SelectedProfile.CheckOption("-oS");
            OutputBaseCheckBox.IsChecked = OutputBaseTextBox.IsEnabled = OutputBaseBrowseButton.IsEnabled = SelectedProfile.CheckOption("-oA");
            OutputAppendCheckBox.IsChecked = SelectedProfile.CheckOption("--append-output");
            OutputResumeCheckBox.IsChecked = OutputResumeTextBox.IsEnabled = OutputResumeBrowseButton.IsEnabled = SelectedProfile.CheckOption("--resume");
            ReasonCheckBox.IsChecked = SelectedProfile.CheckOption("--reason");
            OpenCheckBox.IsChecked = SelectedProfile.CheckOption("--open");

            // Output page
            DnsServerCheckBox.IsChecked = DnsServerTextBox.IsEnabled = SelectedProfile.CheckOption("--dns-servers");
        }

        private void InitializeSlider()
        {
            if (Command.IsChecked("-v"))
            {
                VerbosityLevelSlider.Value = Command.GetModifier("-v") != null ? Double.Parse(Command.GetModifier("-v")) : 1;
            }
            if (Command.IsChecked("-d"))
            {
                DebugLevelSlider.Value = Command.GetModifier("-d") != string.Empty ? Double.Parse(Command.GetModifier("-d")) : 1;
            }
        }

        private void InitializeComboBoxes()
        {
            TcpScanItems.Add(new ScanOption("None", null));
            TcpScanItems.Add(new ScanOption("ACK scan (-sA)", "-sA"));
            TcpScanItems.Add(new ScanOption("FIN scan (-sF)", "-sF"));
            TcpScanItems.Add(new ScanOption("Maimon scan (-sM)", "-sM"));
            TcpScanItems.Add(new ScanOption("Null scan (-sN)", "-sN"));
            TcpScanItems.Add(new ScanOption("TCP SYN scan (-sS)", "-sS"));
            TcpScanItems.Add(new ScanOption("TCP connect scan (-sT)", "-sT"));
            TcpScanItems.Add(new ScanOption("Window scan (-sW)", "-sW"));
            TcpScanItems.Add(new ScanOption("Xmas tree scan (-sX)", "-sX"));
            _selectedTcpScan = TcpScanItems[0];


            NonTcpScanItems.Add(new ScanOption("None", null));
            NonTcpScanItems.Add(new ScanOption("UDP scan (-sU)", "-sU"));
            NonTcpScanItems.Add(new ScanOption("IP protocol scan (-sO)", "-sO"));
            NonTcpScanItems.Add(new ScanOption("List scan (-sL)", "-sL"));
            NonTcpScanItems.Add(new ScanOption("No port scan (-sn)", "-sn"));
            NonTcpScanItems.Add(new ScanOption("SCTP INIT port scan (-sY)", "-sY"));
            NonTcpScanItems.Add(new ScanOption("SCTP cookie-echo port scan (-sZ)", "-sZ"));
            _selectedNonTcpScan = NonTcpScanItems[0];

            TimingTemplateItems.Add(new TimingTemplate("None", null));
            TimingTemplateItems.Add(new TimingTemplate("Paranoid (-T0)", "-T0"));
            TimingTemplateItems.Add(new TimingTemplate("Sneaky (-T1)", "-T1"));
            TimingTemplateItems.Add(new TimingTemplate("Polite (-T2)", "-T2"));
            TimingTemplateItems.Add(new TimingTemplate("Normal (-T3)", "-T3"));
            TimingTemplateItems.Add(new TimingTemplate("Aggressive (-T4)", "-T4"));
            TimingTemplateItems.Add(new TimingTemplate("Insane (-T5)", "-T5"));
            _selectedTimingTemplate = TimingTemplateItems[0];

            OutputFormatItems.Add(new OutputOption("None", null));
            OutputFormatItems.Add(new OutputOption("Normal (-oN)", "-oN"));
            OutputFormatItems.Add(new OutputOption("XML (-oX)", "-oX"));
            OutputFormatItems.Add(new OutputOption("Script (-oS)", "-oS"));
            OutputFormatItems.Add(new OutputOption("Grepable (-oG)", "-oG"));

            _selectedOutputFormat = OutputFormatItems[0];
        }

        public void InitializeScriptList()
        {
            ScriptList = DatabaseScript.FindScripts();
            Command.CheckScripts(ScriptList);
            ScriptArgumentDatagrid.CellEditEnding += HandleScriptEditEnding;
        }

        private void HandleScriptEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            ScriptArgument tmp = (ScriptArgument)e.EditingElement.DataContext;
            if (!string.IsNullOrEmpty(tmp.Value))
                Command.AddScriptArg(tmp.Name, tmp.Value);
        }

        private void LoadScriptArguments(Script script)
        {
            ScriptArgumentList = script.Arguments;
            ScriptArgumentList.ForEach(a => a.Value = a.DefaultValue);
            Command.CheckScriptArguments(ScriptArgumentList);
            OnPropertyChanged("ScriptArgumentList");
        }
        #endregion

        #region Private functions
        private void UpdateComboBoxes()
        {
            int index = SelectedProfile.CheckOptions("None", "-sA", "-sF", "-sM", "-sN", "-sS", "-sT", "-sW", "-sX");
            if (index > 0)
            {
                _selectedTcpScan = TcpScanItems[index];
            }
            index = SelectedProfile.CheckOptions("None", "-sU", "-sO", "-sL", "-sn", "-sY", "-sZ");
            if (index > 0)
            {
                _selectedNonTcpScan = NonTcpScanItems[index];
            }
            index = SelectedProfile.CheckOptions("None", "-T0", "-T1", "-T2", "-T3", "-T4", "-T5");
            if (index > 0)
            {
                _selectedTimingTemplate = TimingTemplateItems[index];
            }
            index = SelectedProfile.CheckOptions("None", "-oN", "-oX", "-oS", "-oG");
            if (index > 0)
            {
                _selectedOutputFormat = OutputFormatItems[index];
            }
        }

        #endregion

        #region Bindings combo boxes
        private Profile _selectedProfile;
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set
            {
                _selectedProfile = value;
                UpdateWidgets();
            }
        }

        public List<ScanOption> TcpScanItems { get; set; } = new List<ScanOption>();
        private ScanOption _selectedTcpScan;
        public ScanOption SelectedTcpScan
        {
            get { return _selectedTcpScan; }
            set
            {
                _selectedTcpScan = value;
                Command.UnsetScanOptions(TcpScanItems);
                if (value.Name != "None")
                {
                    Command.SetOption(value.Option);
                }
            }
        }

        public List<ScanOption> NonTcpScanItems { get; set; } = new List<ScanOption>();
        private ScanOption _selectedNonTcpScan;
        public ScanOption SelectedNonTcpScan
        {
            get { return _selectedNonTcpScan; }
            set
            {
                _selectedNonTcpScan = value;
                Command.UnsetScanOptions(NonTcpScanItems);
                if (value.Name != "None")
                {
                    Command.SetOption(value.Option);
                }
            }
        }

        public List<TimingTemplate> TimingTemplateItems { get; set; } = new List<TimingTemplate>();
        private TimingTemplate _selectedTimingTemplate;
        public TimingTemplate SelectedTimingTemplate
        {
            get { return _selectedTimingTemplate; }
            set
            {
                _selectedTimingTemplate = value;
                Command.UnsetTimingTemplateOptions(TimingTemplateItems);
                if (value.Name != "None")
                {
                    Command.SetOption(value.Option);
                }
            }
        }

        public List<OutputOption> OutputFormatItems { get; set; } = new List<OutputOption>();
        private OutputOption _selectedOutputFormat;
        public OutputOption SelectedOutputFormat
        {
            get { return _selectedOutputFormat; }
            set
            {
                _selectedOutputFormat = value;
                Command.UnsetOutputOptions(OutputFormatItems);
                if (value.Name != "None")
                {
                    Command.SetOption(value.Option);
                }
            }
        }
        #endregion

        #region Bindings scan page
        private string _target;
        public string Target
        {
            get { return _target; }
            set
            {
                if (_target == null || !_target.Equals(value))
                {
                    _target = value;
                    SelectedProfile.Target = value;
                    OnPropertyChanged("Target");
                }
            }
        }

        private string _idleScanText = Command.Instance.GetModifier("-sI");
        public string IdleScanText
        {
            get { return _idleScanText; }
            set
            {
                if (_idleScanText == string.Empty || !_idleScanText.Equals(value))
                {
                    _idleScanText = value;
                    Command.SetCheckBoxTextOption("-sI", true, value);
                    OnPropertyChanged("IdleScanText");
                }
            }
        }

        private string _ftpBounceText = Command.Instance.GetModifier("-b");
        public string FtpBounceText
        {
            get { return _ftpBounceText; }
            set
            {
                if (_ftpBounceText == string.Empty || !_ftpBounceText.Equals(value))
                {
                    _ftpBounceText = value;
                    Command.SetCheckBoxTextOption("-b", true, value);
                    OnPropertyChanged("FtpBounceText");
                }
            }
        }

        private Visibility _isNewProfile;
        public Visibility IsNewProfile
        {
            get { return _isNewProfile; }
            set
            {
                _isNewProfile = value;
                OnPropertyChanged("IsNewProfile");
            }
        }
        private Visibility _isEditProfile;
        public Visibility IsEditProfile
        {
            get { return _isEditProfile; }
            set
            {
                _isEditProfile = value;
                OnPropertyChanged("IsEditProfile");
            }
        }
        #endregion

        #region Bindings ping page
        private string _ackPingText = Command.Instance.GetModifier("-PA");
        public string AckPingText
        {
            get { return _ackPingText; }
            set
            {
                if (_ackPingText == string.Empty || !_ackPingText.Equals(value))
                {
                    _ackPingText = value;
                    Command.SetCheckBoxTextOption("-PA", true, value);
                    OnPropertyChanged("AckPingText");
                }
            }
        }

        private string _synPingText = Command.Instance.GetModifier("-PS");
        public string SynPingText
        {
            get { return _synPingText; }
            set
            {
                if (_synPingText == string.Empty || !_synPingText.Equals(value))
                {
                    _synPingText = value;
                    Command.SetCheckBoxTextOption("-PS", true, value);
                    OnPropertyChanged("SynPingText");
                }
            }
        }

        private string _udpPingText = Command.Instance.GetModifier("-PU");
        public string UdpPingText
        {
            get { return _udpPingText; }
            set
            {
                if (_udpPingText == string.Empty || !_udpPingText.Equals(value))
                {
                    _udpPingText = value;
                    Command.SetCheckBoxTextOption("-PU", true, value);
                    OnPropertyChanged("UdpPingText");
                }
            }
        }

        private string _ipProtoProbesText = Command.Instance.GetModifier("-PO");
        public string IpProtoProbesText
        {
            get { return _ipProtoProbesText; }
            set
            {
                if (_ipProtoProbesText == string.Empty || !_ipProtoProbesText.Equals(value))
                {
                    _ipProtoProbesText = value;
                    Command.SetCheckBoxTextOption("-PO", true, value);
                    OnPropertyChanged("IpProtoProbesText");
                }
            }
        }

        private string _sctpPingProbesText = Command.Instance.GetModifier("-PY");
        public string SctpPingProbesText
        {
            get { return _sctpPingProbesText; }
            set
            {
                if (_sctpPingProbesText == string.Empty || !_sctpPingProbesText.Equals(value))
                {
                    _sctpPingProbesText = value;
                    Command.SetCheckBoxTextOption("-PY", true, value);
                    OnPropertyChanged("SctpPingProbesText");
                }
            }
        }
        #endregion

        #region Bindings target page
        private string _excludeText = Command.Instance.GetModifier("--exclude");
        public string ExcludeText
        {
            get { return _excludeText; }
            set
            {
                if (_excludeText == string.Empty || !_excludeText.Equals(value))
                {
                    _excludeText = value;
                    Command.SetCheckBoxTextOption("--exclude", ExcludeCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("ExcludeText");
                }
            }
        }

        private string _excludeFileText = Command.Instance.GetModifier("--excludefile");
        public string ExcludeFileText
        {
            get { return _excludeFileText; }
            set
            {
                if (_excludeFileText == string.Empty || !_excludeFileText.Equals(value))
                {
                    _excludeFileText = value;
                    Command.SetCheckBoxTextOption("--excludefile", ExcludeFileCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("ExcludeFileText");
                }
            }
        }

        private string _targetListFileText = Command.Instance.GetModifier("-iL");
        public string TargetListFileText
        {
            get { return _targetListFileText; }
            set
            {
                if (string.IsNullOrEmpty(_targetListFileText) || !_targetListFileText.Equals(value))
                {
                    _targetListFileText = value;
                    Command.SetCheckBoxTextOption("-iL", TargetListFileCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("TargetListFileText");
                }
            }
        }

        private string _scanRandomHostText = Command.Instance.GetModifier("-iR");
        public string ScanRandomHostText
        {
            get { return _scanRandomHostText; }
            set
            {
                if (_scanRandomHostText == null || !_scanRandomHostText.Equals(value))
                {
                    _scanRandomHostText = value;
                    Command.SetCheckBoxTextOption("-iR", true, value);
                    OnPropertyChanged("ScanRandomHostText");
                }
            }
        }

        private string _portsToScanText = Command.Instance.GetModifier("-p");
        public string PortsToScanText
        {
            get { return _portsToScanText; }
            set
            {
                if (_portsToScanText == null || !_portsToScanText.Equals(value))
                {
                    _portsToScanText = value;
                    Command.SetCheckBoxTextOption("-p", true, value);
                    OnPropertyChanged("PortsToScanText");
                }
            }
        }

        private string _outputBaseText = Command.Instance.GetModifier("-oA");
        public string OutputBaseText
        {
            get { return _outputBaseText; }
            set
            {
                if (_outputBaseText == null || !_outputBaseText.Equals(value))
                {
                    _outputBaseText = value;
                    Command.SetCheckBoxTextOption("-oA", true, value);
                    OnPropertyChanged("OutputBaseText");
                }
            }
        }

        #endregion

        #region Bindings source page
        private string _decoysText = Command.Instance.GetModifier("-D");
        public string DecoysText
        {
            get { return _decoysText; }
            set
            {
                if (_decoysText == string.Empty || !_decoysText.Equals(value))
                {
                    _decoysText = value;
                    Command.SetCheckBoxTextOption("-D", DecoysCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("DecoysText");
                }
            }
        }

        private string _sourceIPText = Command.Instance.GetModifier("-S");
        public string SourceIPText
        {
            get { return _sourceIPText; }
            set
            {
                if (_sourceIPText == string.Empty || !_sourceIPText.Equals(value))
                {
                    _sourceIPText = value;
                    Command.SetCheckBoxTextOption("-S", SourceIPCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("SourceIPText");
                }
            }
        }

        private string _sourcePortText = Command.Instance.GetModifier("--source-port");
        public string SourcePortText
        {
            get { return _sourcePortText; }
            set
            {
                if (_sourcePortText == string.Empty || !_sourcePortText.Equals(value))
                {
                    _sourcePortText = value;
                    Command.SetCheckBoxTextOption("--source-port", SourcePortCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("SourcePortText");
                }
            }
        }

        private string _networkInterfaceText = Command.Instance.GetModifier("-e");
        public string NetworkInterfaceText
        {
            get { return _networkInterfaceText; }
            set
            {
                if (_networkInterfaceText == string.Empty || !_networkInterfaceText.Equals(value))
                {
                    _networkInterfaceText = value;
                    Command.SetCheckBoxTextOption("-e", NetworkInterfaceCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("NetworkInterfaceText");
                }
            }
        }
        #endregion

        #region Bindings timing page
        private string _maxScanTimeText = Command.Instance.GetModifier("--host-timeout");
        public string MaxScanTimeText
        {
            get { return _maxScanTimeText; }
            set
            {
                if (_maxScanTimeText == string.Empty || !_maxScanTimeText.Equals(value))
                {
                    _maxScanTimeText = value;
                    Command.SetCheckBoxTextOption("--host-timeout", MaxScanTimeCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MaxScanTimeText");
                }
            }
        }

        private string _maxProbeTimeoutText = Command.Instance.GetModifier("--max-rtt-timeout");
        public string MaxProbeTimeoutText
        {
            get { return _maxProbeTimeoutText; }
            set
            {
                if (_maxProbeTimeoutText == string.Empty || !_maxProbeTimeoutText.Equals(value))
                {
                    _maxProbeTimeoutText = value;
                    Command.SetCheckBoxTextOption("--max-rtt-timeout", MaxProbeTimeoutCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MaxProbeTimeoutText");
                }
            }
        }

        private string _minProbeTimeoutText = Command.Instance.GetModifier("--min-rtt-timeout");
        public string MinProbeTimeoutText
        {
            get { return _minProbeTimeoutText; }
            set
            {
                if (_minProbeTimeoutText == string.Empty || !_minProbeTimeoutText.Equals(value))
                {
                    _minProbeTimeoutText = value;
                    Command.SetCheckBoxTextOption("--min-rtt-timeout", MinProbeTimeoutCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MinProbeTimeoutText");
                }
            }
        }

        private string _initialProbeTimeoutText = Command.Instance.GetModifier("--initial-rtt-timeout");
        public string InitialProbeTimeoutText
        {
            get { return _initialProbeTimeoutText; }
            set
            {
                if (_initialProbeTimeoutText == string.Empty || !_initialProbeTimeoutText.Equals(value))
                {
                    _initialProbeTimeoutText = value;
                    Command.SetCheckBoxTextOption("--initial-rtt-timeout", InitialProbeTimeoutCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("InitialProbeTimeoutText");
                }
            }
        }

        private string _maxHostGroupsText = Command.Instance.GetModifier("--max-hostgroup");
        public string MaxHostGroupsText
        {
            get { return _maxHostGroupsText; }
            set
            {
                if (_maxHostGroupsText == string.Empty || !_maxHostGroupsText.Equals(value))
                {
                    _maxHostGroupsText = value;
                    Command.SetCheckBoxTextOption("--max-hostgroup", MaxHostGroupsCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MaxHostGroupsText");
                }
            }
        }

        private string _minHostGroupsText = Command.Instance.GetModifier("--min-hostgroup");
        public string MinHostGroupsText
        {
            get { return _minHostGroupsText; }
            set
            {
                if (_minHostGroupsText == string.Empty || !_minHostGroupsText.Equals(value))
                {
                    _minHostGroupsText = value;
                    Command.SetCheckBoxTextOption("--min-hostgroup", MinHostGroupsCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MinHostGroupsText");
                }
            }
        }

        private string _maxOutStandingProbesText = Command.Instance.GetModifier("--max-parallelism");
        public string MaxOutStandingProbesText
        {
            get { return _maxOutStandingProbesText; }
            set
            {
                if (_maxOutStandingProbesText == string.Empty || !_maxOutStandingProbesText.Equals(value))
                {
                    _maxOutStandingProbesText = value;
                    Command.SetCheckBoxTextOption("--max-parallelism", MaxOutStandingProbesCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MaxOutStandingProbesText");
                }
            }
        }

        private string _minOutStandingProbesText = Command.Instance.GetModifier("--min-parallelism");
        public string MinOutStandingProbesText
        {
            get { return _minOutStandingProbesText; }
            set
            {
                if (_minOutStandingProbesText == string.Empty || !_minOutStandingProbesText.Equals(value))
                {
                    _minOutStandingProbesText = value;
                    Command.SetCheckBoxTextOption("--min-parallelism", MinOutStandingProbesCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MinOutStandingProbesText");
                }
            }
        }

        private string _maxScanDelayText = Command.Instance.GetModifier("--max-scan-delay");
        public string MaxScanDelayText
        {
            get { return _maxScanDelayText; }
            set
            {
                if (_maxScanDelayText == string.Empty || !_maxScanDelayText.Equals(value))
                {
                    _maxScanDelayText = value;
                    Command.SetCheckBoxTextOption("--max-scan-delay", MaxScanDelayCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("MaxScanDelayText");
                }
            }
        }

        private string _scanDelayText = Command.Instance.GetModifier("--scan-delay");
        public string ScanDelayText
        {
            get { return _scanDelayText; }
            set
            {
                if (_scanDelayText == string.Empty || !_scanDelayText.Equals(value))
                {
                    _scanDelayText = value;
                    Command.SetCheckBoxTextOption("--scan-delay", ScanDelayCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("ScanDelayText");
                }
            }
        }
        #endregion

        #region Bindings output page
        private string _outputNormalText = Command.Instance.GetModifier("-oN");
        public string OutputNormalText
        {
            get { return _outputNormalText; }
            set
            {
                if (_outputNormalText == string.Empty || !_outputNormalText.Equals(value))
                {
                    _outputNormalText = value;
                    Command.SetCheckBoxTextOption("-oN", OutputNormalCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("OutputNormalText");
                }
            }
        }

        private string _outputXmlText = Command.Instance.GetModifier("-oX");
        public string OutputXmlText
        {
            get { return _outputXmlText; }
            set
            {
                if (_outputXmlText == string.Empty || !_outputXmlText.Equals(value))
                {
                    _outputXmlText = value;
                    Command.SetCheckBoxTextOption("-oX", OutputXmlCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("OutputXmlText");
                }
            }
        }

        private string _outputGrepText = Command.Instance.GetModifier("-oG");
        public string OutputGrepText
        {
            get { return _outputGrepText; }
            set
            {
                if (_outputGrepText == string.Empty || !_outputGrepText.Equals(value))
                {
                    _outputGrepText = value;
                    Command.SetCheckBoxTextOption("-oG", OutputGrepCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("OutputGrepText");
                }
            }
        }

        private string _outputScriptText = Command.Instance.GetModifier("-oS");
        public string OutputScriptText
        {
            get { return _outputScriptText; }
            set
            {
                if (_outputScriptText == string.Empty || !_outputScriptText.Equals(value))
                {
                    _outputScriptText = value;
                    Command.SetCheckBoxTextOption("-oS", OutputScriptCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("OutputScriptText");
                }
            }
        }

        private string _outputResumeText = Command.Instance.GetModifier("--resume");
        public string OutputResumeText
        {
            get { return _outputResumeText; }
            set
            {
                if (_outputResumeText == string.Empty || !_outputResumeText.Equals(value))
                {
                    _outputResumeText = value;
                    Command.SetCheckBoxTextOption("--resume", OutputResumeCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("OutputResumeText");
                }
            }
        }
        #endregion

        #region Bindings DNS page
        private string _dnsServerText = Command.Instance.GetModifier("--dns-servers");
        public string DnsServerText
        {
            get { return _dnsServerText; }
            set
            {
                if (_dnsServerText == string.Empty || !_dnsServerText.Equals(value))
                {
                    _dnsServerText = value;
                    Command.SetCheckBoxTextOption("--dns-servers", DnsServerCheckBox.IsChecked.Value, value);
                    OnPropertyChanged("DnsServerText");
                }
            }
        }
        #endregion

        #region Binding misc page
        private void UpdateWidgets()
        {
            Profiles = DatabaseProfile.FindProfiles();
            if (_selectedProfile != null)
            {
                IsNewProfile = Visibility.Collapsed;
                IsEditProfile = Visibility.Visible;
                Command.CommandString = _selectedProfile.Command;
                TargetTextBox.Text = _selectedProfile.Target;
                DescriptionTextBox.Text = _selectedProfile.Description;
            }
            else
            {
                Command.Clear();
                UpdateMode = false;
                Title = "New Profile";
                SelectedProfile = new Profile();
                IsNewProfile = Visibility.Visible;
                IsEditProfile = Visibility.Collapsed;
            }
            UpdateComboBoxes();
            InitializeCheckBoxes();
            InitializeSlider();
        }
        #endregion

        #region Checkbox handler
        /// <summary>
        /// Check box change handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">send eevent</param>
        public void OnCheckBoxChanged(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            Button browseButton = (Button)FindName(checkBox.Name.Replace("CheckBox", "BrowseButton"));
            if (browseButton != null)
            {
                browseButton.IsEnabled = checkBox.IsChecked.Value;
            }
            Slider slider = (Slider)FindName(checkBox.Name.Replace("CheckBox", "Slider"));
            if (slider != null)
            {
                slider.IsEnabled = checkBox.IsChecked.Value;
            }

            CommandOption commandOption = Command.FindByCheckBox(checkBox.Name);
            if (commandOption != null)
            {
                TextBox textBox = (TextBox)FindName(checkBox.Name.Replace("Check", "Text"));
                if (textBox != null)
                {
                    textBox.IsEnabled = checkBox.IsChecked.Value;
                    Command.SetCheckBoxTextOption(commandOption.Qualifier, checkBox.IsChecked.Value, textBox.Text);
                    if (!checkBox.IsChecked.Value && textBox.GetBindingExpression(TextBox.TextProperty) != null)
                    {
                        textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                    }
                }
                else
                {
                    Command.SetCheckBoxOption(commandOption.Qualifier, checkBox.IsChecked.Value);
                }
            }
        }

        /// <summary>
        /// Slider change handler.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">send event</param>
        public void OnSliderChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = (Slider)sender;
            switch (slider.Name)
            {
                case "VerbosityLevelSlider":
                    Command.SetCheckBoxTextOption("-v", VerbosityLevelCheckBox.IsChecked.Value, string.Format("{0:d}", (int)slider.Value));
                    VerbosityLevelTextBox.Text = string.Format("{0:d}", (int)slider.Value);
                    break;
                case "DebugLevelSlider":
                    Command.SetCheckBoxTextOption("-d", DebugLevelCheckBox.IsChecked.Value, string.Format("{0:d}", (int)slider.Value));
                    DebugLevelTextBox.Text = string.Format("{0:d}", (int)slider.Value);
                    break;
            }
        }

        /// <summary>
        /// Script loist has been clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">send event</param>
        public void OnScriptListClicked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            Script script = checkBox.DataContext as Script;
            if (checkBox.IsChecked.Value)
            {
                ScriptArgumentDatagrid.IsReadOnly = false;
                Command.AddScript(script.Name);
                LoadScriptArguments(script);
            }
            else
            {
                ScriptArgumentDatagrid.IsReadOnly = true;
                script.Arguments.ToList().ForEach(a => Command.RemoveScriptArg(a.Name, a.Value));
                Command.RemoveScript(script.Name);
            }
        }
        #endregion

        #region Button handlers
        private void OnExclusionFileButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                ExcludeFileText = openFileDialog.FileName;
            }
        }
        private void OnTargetListFileButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                TargetListFileText = openFileDialog.FileName;
            }
        }

        private void OnOutputNormalBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputNormalText = openFileDialog.FileName;
            }
        }

        private void OnOutputXmlBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputXmlText = openFileDialog.FileName;
            }
        }

        private void OnOutputGrepBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputGrepText = openFileDialog.FileName;
            }
        }

        private void OnOutputScriptBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputScriptText = openFileDialog.FileName;
            }
        }

        private void OnOutputBaseBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputBaseText = openFileDialog.FileName;
            }
        }

        private void OnOutputResumeBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "All files (*.*)|*.*",
                RestoreDirectory = true,
                CheckFileExists = false
            };
            if (openFileDialog.ShowDialog() == true)
            {
                OutputResumeText = openFileDialog.FileName;
            }
        }

        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            SelectedProfile.Command = Command.CommandString;
            if (ValidateProfile(SelectedProfile) && Command.ValidateOptions())
            {
                if (UpdateMode)
                {
                    SelectedProfile.Command = Command.CommandString;
                    DatabaseProfile.UpdateProfile(SelectedProfile);
                }
                else
                {
                    SelectedProfile = DatabaseProfile.InsertProfile(SelectedProfile);
                }
                OnPropertyChanged("Profile");
                DialogResult = true;
                Close();
            }
        }

        public void OnScriptListHelpClicked(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Script script = button.DataContext as Script;
            HelpProvider.ShowHelpTopic("nmap/scripts/" + script.Name + ".html");
        }
        #endregion

        #region List handlers
        public void OnListItemDoubleClick(object sender, RoutedEventArgs e)
        {
            ListBox listBox = (ListBox)sender;
            Script selectedScript = listBox.SelectedItem as Script;
            EditScriptDialog editScriptDialog = new EditScriptDialog
            {
                Owner = this,
                Script = selectedScript
            };
            editScriptDialog.Show();
        }
        #endregion

        #region Validation
        /// <summary>
        /// Trigger a validation even when the text box is left empty. WPF does only trigger
        /// the validation when the text box content has been changes. This will also trigger
        /// the validation when the text hasn't been changed and is still left empty.
        /// </summary>
        /// <param name="sender">text box sender</param>
        /// <param name="e">trigger event</param>
        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            ((Control)sender).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        public string Error { get; set; } = string.Empty;

        public string this[string propertyName]
        {
            get
            {
                return ValidateProperties(propertyName);
            }
        }

        private string ValidateProperties(string propertyName)
        {
            switch (propertyName)
            {
                case "Target":
                    return Validation.ValidateTarget("Target", Target, true);
                case "IdleScanText":
                    if (IdleScanCheckBox.IsChecked.Value)
                        return Validation.ValidateIdleScan(IdleScanText);
                    break;
                case "FtpBounceText":
                    if (FtpBounceCheckBox.IsChecked.Value)
                        return Validation.ValidateFtpBounce(FtpBounceText);
                    break;
                case "AckPingText":
                    if (AckPingCheckBox.IsChecked.Value)
                        return Validation.ValidatePortList("TCP ACK ping", AckPingText);
                    break;
                case "SynPingText":
                    if (SynPingCheckBox.IsChecked.Value)
                        return Validation.ValidatePortList("TCP SYN ping", SynPingText);
                    break;
                case "UdpPingText":
                    if (UdpProbesCheckBox.IsChecked.Value)
                        return Validation.ValidatePortList("UDP Ping", UdpPingText);
                    break;
                case "IpProtoProbesText":
                    if (IpProtoProbesCheckBox.IsChecked.Value)
                        return Validation.ValidateProtocolList("IP Protocol Ping", IpProtoProbesText);
                    break;
                case "SctpPingProbesText":
                    if (SctpPingProbesCheckBox.IsChecked.Value)
                        return Validation.ValidatePortList("SCTP INIT Ping", SctpPingProbesText);
                    break;
                case "ExcludeText":
                    if (ExcludeCheckBox.IsChecked.Value)
                        return Validation.ValidateTarget("Exclude host", ExcludeText, false);
                    break;
                case "ExcludeFileText":
                    if (ExcludeFileCheckBox.IsChecked.Value)
                        return Validation.ValidateFile("Exclude file", ExcludeFileText);
                    break;
                case "TargetListFileText":
                    if (TargetListFileCheckBox.IsChecked.Value)
                        return Validation.ValidateFile("Target list file", TargetListFileText);
                    break;
                case "ScanRandomHostText":
                    if (ScanRandomHostCheckBox.IsChecked.Value)
                    {
                        Target = "Random";
                        return Validation.ValidateRandomCount("Scan random host", ScanRandomHostText);
                    }
                    break;
                case "PortsToScanText":
                    if (PortsToScanCheckBox.IsChecked.Value)
                        return Validation.ValidatePortRanges("Ports to scan", PortsToScanText);
                    break;
                case "DecoysText":
                    if (DecoysCheckBox.IsChecked.Value)
                        return Validation.ValidateDecoyList("Use decoys", DecoysText);
                    break;
                case "SourceIPText":
                    if (SourceIPCheckBox.IsChecked.Value)
                        return Validation.ValidateSourceIp("Source IP address", SourceIPText);
                    break;
                case "SourcePortText":
                    if (SourcePortCheckBox.IsChecked.Value)
                        return Validation.ValidateSourcePort("Source port", SourcePortText);
                    break;
                case "NetworkInterfaceText":
                    if (NetworkInterfaceCheckBox.IsChecked.Value)
                        return Validation.ValidateInterface("Network interface", NetworkInterfaceText);
                    break;
                case "MaxScanTimeText":
                    if (MaxScanTimeCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Max time to scan", MaxScanTimeText);
                    break;
                case "MaxProbeTimeoutText":
                    if (MaxProbeTimeoutCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Max probe timeout", MaxProbeTimeoutText);
                    break;
                case "MinProbeTimeoutText":
                    if (MinProbeTimeoutCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Min probe timeout", MinProbeTimeoutText);
                    break;
                case "InitialProbeTimeoutText":
                    if (InitialProbeTimeoutCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Initial probe timeout", InitialProbeTimeoutText);
                    break;
                case "MaxHostGroupsText":
                    if (MaxHostGroupsCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Max hosts parallel", MaxHostGroupsText);
                    break;
                case "MinHostGroupsText":
                    if (MinHostGroupsCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Min hosts parallel", MinHostGroupsText);
                    break;
                case "MaxOutStandingProbesText":
                    if (MaxOutStandingProbesCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Max outstanding probes", MaxOutStandingProbesText);
                    break;
                case "MinOutStandingProbesText":
                    if (MinOutStandingProbesCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Min outstanding probes", MinOutStandingProbesText);
                    break;
                case "MaxScanDelayText":
                    if (MaxScanDelayCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Max scan delay", MaxScanDelayText);
                    break;
                case "ScanDelayText":
                    if (ScanDelayCheckBox.IsChecked.Value)
                        return Validation.ValidateTimeout("Min scan delay", MaxScanDelayText);
                    break;
                case "OutputBaseText":
                    if (OutputBaseCheckBox.IsChecked.Value)
                        return Validation.ValidateFile("Output base", OutputBaseText);
                    break;
                case "DnsServerText":
                    if (DnsServerCheckBox.IsChecked.Value)
                        return Validation.ValidateTarget("DNS servers", DnsServerText, false);
                    break;
            }
            return string.Empty;
        }

        private bool ValidateProfile(Profile profile)
        {
            if (string.IsNullOrEmpty(profile.Name))
            {
                MessageBox.Show("Name cannot be empty!", "Profiles", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(profile.Command))
            {
                MessageBox.Show("Command cannot be empty!", "Profiles", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Send property change notification.
        /// </summary>
        /// <param name="propertyName">property name</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Handle property change events.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event</param>
        private void HandleScriptChanges(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ScriptChecked":
                    //                    descriptionBrowser.Navigate(new Uri(((Script)sender).Html));
                    //descriptionBrowser.NavigateToString(((Script)sender).Description);
                    break;
                case "ScriptUnchecked":
                    //descriptionBrowser.NavigateToString(" ");
                    break;
            }
        }
        #endregion
    }
}
