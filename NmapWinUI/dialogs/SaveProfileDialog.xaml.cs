﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 

using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using System.ComponentModel;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for EditProfileDialog.xaml
    /// </summary>
    public partial class SaveProfileDialog : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        public Profile Profile { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SaveProfileDialog()
        {
            InitializeComponent();

            DataContext = this;
        }

        #region Button handlers
        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {

            DatabaseProfile.InsertProfile(Profile);

            OnPropertyChanged("Profile");

            DialogResult = true;
            Close();
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
