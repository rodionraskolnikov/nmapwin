﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace NmapWinUI.dialogs
{
    /// <summary>
    /// Interaction logic for PortDetailsDialog.xaml
    /// </summary>
    public partial class PortDetailsDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(PortDetailsDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Host database singleton
        /// </summary>
        private readonly DatabasePort DatabasePort = DatabasePort.Instance;

        /// <summary>
        /// Collection for the port script table
        /// </summary>
        public ObservableCollection<PortScript> PortScriptData { get; set; }
        #endregion

        #region Constructor
        public PortDetailsDialog()
        {
            InitializeComponent();

            DataContext = this;
        }
        #endregion

        #region Initialization
        private void InitializeDialog()
        {
            Title = "Port Details (id: " + Port.Id + " portId: " + Port.PortId + ")";
        }
        #endregion

        #region Bindings
        private Port _port;
        public Port Port
        {
            get
            {
                return _port;
            }
            set
            {
                if (value != null && !value.Equals(_port))
                {
                    _port = value;
                    InitializeDialog();
                    OnPropertyChanged("Port");
                }
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the host dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Port = DatabasePort.FindPortById(_port.Id);
            OnPropertyChanged("Port");
        }

        /// <summary>
        /// Script table refresh clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnScriptTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            PortScriptData = new ObservableCollection<PortScript>(DatabasePort.FindPortScriptsByPort(Port));
            PortScriptDatagrid.ItemsSource = PortScriptData;
            OnPropertyChanged("PortScripts");
        }

        public void OnScriptOpenButtonClicked(object sender, RoutedEventArgs e)
        {

        }

        public void OnScriptDeleteButtonClicked(object sender, RoutedEventArgs e)
        {

        }

        public void OnScriptDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {

        }
        
        /// <summary>
        /// Close button clicked. Close dialog
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Tab handlers
        public void OnTabSelectionChanged(object sender, RoutedEventArgs e)
        {
            if (ScriptsTabItem.IsSelected)
            {
                PortScriptData = new ObservableCollection<PortScript>(DatabasePort.FindPortScriptsByPort(Port));
                PortScriptDatagrid.ItemsSource = PortScriptData;
                OnPropertyChanged("PortScripts");
            }
        }
        #endregion

        #region Private methdos
        private void UpdateStatus()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        #region Shortcut handlers
        private void RefreshCommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RefreshCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Port = DatabasePort.FindPortById(_port.Id);
            OnPropertyChanged("Port");
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Property changed notifications
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            UpdateStatus();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
