﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for EditProfileDialog.xaml
    /// </summary>
    public partial class DeleteProfileDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Profile list-
        /// </summary>
        public List<Profile> Profiles { get; set; }
        #endregion

        #region Constrcutor
        /// <summary>
        /// Constructor
        /// </summary>
        public DeleteProfileDialog()
        {
            InitializeComponent();

            InitializeProfiles();

            DataContext = this;
        }
        #endregion

        #region Initialization
        private void InitializeProfiles()
        {
            Profiles = DatabaseProfile.FindProfiles();
        }
        #endregion

        #region Button handlers
        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public void OnDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            DatabaseProfile.DeleteProfile(SelectedProfile);

            OnPropertyChanged("Profiles");

            DialogResult = true;
            Close();
        }
        #endregion

        #region Bindings
        private Profile _selectedProfile;

        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set
            {
                _selectedProfile = value;
                OnPropertyChanged("SelectedProfile");
            }
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
