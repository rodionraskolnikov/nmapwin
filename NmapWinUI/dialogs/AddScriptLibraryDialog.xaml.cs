﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for AddLibraryDialog.xaml
    /// </summary>
    public partial class AddScriptLibraryDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(AddScriptLibraryDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Script argument
        /// </summary>
        private Script _script;
        public Script Script
        {
            get
            {
                return _script;
            }
            set
            {
                _script = value;
                SetSelection();
            }
        }

        /// <summary>
        /// Argument group list
        /// </summary>
        public List<ArgumentGroup> ArgumentGroups { get; set; } = new List<ArgumentGroup>();

        /// <summary>
        /// Argument group list
        /// </summary>
        public List<ArgumentGroup> SelectedArgumentGroups { get; set; } = new List<ArgumentGroup>();

        /// <summary>
        /// Final list of script arguments
        /// </summary>
        public List<ScriptArgument> SelectedScriptArguments { get; set; } = new List<ScriptArgument>();
        #endregion

        #region Constructor
        public AddScriptLibraryDialog()
        {
            InitializeComponent();

            InitializeGroups();

            DataContext = this;
        }

        /// <summary>
        /// Initialize the argument groups.
        /// </summary>
        private void InitializeGroups()
        {
            ArgumentGroups.Add(new ArgumentGroup("AFP Library", new ScriptArgument[] {
                new ScriptArgument("afp.password", "", string.Format("See the documentation for the <a href=\"{0}\">afp</a> library.", "nmap/libraries/afp.html")),
                new ScriptArgument("afp.username", "", string.Format("See the documentation for the <a href=\"{0}\">afp</a> library.", "nmap/libraries/afp.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("ANYCONNECT Library", new ScriptArgument[] {
                new ScriptArgument("anyconnect.group", "", string.Format("See the documentation for the <a href=\"{0}\">anyconnect</a> library.", "nmap/libraries/anyconnect.html")),
                new ScriptArgument("anyconnect.mac", "", string.Format("See the documentation for the <a href=\"{0}\">anyconnect</a> library.", "nmap/libraries/anyconnect.html")),
                new ScriptArgument("anyconnect.version", "", string.Format("See the documentation for the <a href=\"{0}\">anyconnect</a> library.", "nmap/libraries/anyconnect.html")),
                new ScriptArgument("anyconnect.ua", "", string.Format("See the documentation for the <a href=\"{0}\">anyconnect</a> library.", "nmap/libraries/anyconnect.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("BRUTE Library", new ScriptArgument[] {
                new ScriptArgument("brute.credfile", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.delay", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.emptypass", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.firstonly", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.guesses", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.mode", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.passonly", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.retries", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.start", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.threads", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.unique", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html")),
                new ScriptArgument("brute.useraspass", "", string.Format("See the documentation for the <a href=\"{0}\">brute</a> library.", "nmap/libraries/brute.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("CREDS Library", new ScriptArgument[] {
                new ScriptArgument("creds.[service]", "", string.Format("See the documentation for the <a href=\"{0}\">creds</a> library.", "nmap/libraries/creds.html")),
                new ScriptArgument("creds.global", "", string.Format("See the documentation for the <a href=\"{0}\">creds</a> library.", "nmap/libraries/creds.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("HTTP Library", new ScriptArgument[] {
                new ScriptArgument("http.host", "", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html")),
                new ScriptArgument("http.max-body-size", "2097152", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html")),
                new ScriptArgument("http.max-cache-size", "", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html")),
                new ScriptArgument("http.pipeline", "", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html")),
                new ScriptArgument("http.truncated-ok", "", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html")),
                new ScriptArgument("http.useragent", "Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)", string.Format("See the documentation for the <a href=\"{0}\">http</a> library.", "nmap/libraries/http.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("HTTPSPIDER Library", new ScriptArgument[] {
                new ScriptArgument("httpspider.doscraping", "", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.url", "/", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.maxdepth", "3", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.maxpagecount", "20", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.useheadfornonwebfiles", "", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.withindomain", "false", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html")),
                new ScriptArgument("httpspider.withinhost", "true", string.Format("See the documentation for the <a href=\"{0}\">httpspider</a> library.", "nmap/libraries/httpspider.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("LS Library", new ScriptArgument[] {
                new ScriptArgument("ls.checksum", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/ls.html")),
                new ScriptArgument("ls.empty", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/ls.html")),
                new ScriptArgument("ls.errors", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/prlsoxy.html")),
                new ScriptArgument("ls.human", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/ls.html")),
                new ScriptArgument("ls.maxdepth", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/ls.html")),
                new ScriptArgument("ls.maxfiles", "", string.Format("See the documentation for the <a href=\"{0}\">ls</a> library.", "nmap/libraries/ls.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("MSSQL Library", new ScriptArgument[] {
                new ScriptArgument("mssql.domain", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.instance-all", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.instance-name", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.instance-port", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.password", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.protocol", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.scanned-ports-only", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.timeout", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
                new ScriptArgument("mssql.username", "", string.Format("See the documentation for the <a href=\"{0}\">mssql</a> library.", "nmap/libraries/mssql.html")),
            }));
            ArgumentGroups.Add(new ArgumentGroup("OMP2 Library", new ScriptArgument[] {
                new ScriptArgument("omp2.username", "", string.Format("See the documentation for the <a href=\"{0}\">omp2</a> library.", "nmap/libraries/omp2.html")),
                new ScriptArgument("omp2.password", "", string.Format("See the documentation for the <a href=\"{0}\">omp2</a> library.", "nmap/libraries/omp2.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("PROXY Library", new ScriptArgument[] {
                new ScriptArgument("proxy.pattern", "", string.Format("See the documentation for the <a href=\"{0}\">proxy</a> library.", "nmap/libraries/proxy.html")),
                new ScriptArgument("max-proxy.url", "", string.Format("See the documentation for the <a href=\"{0}\">proxy</a> library.", "nmap/libraries/proxy.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("RPC Library", new ScriptArgument[] {
                new ScriptArgument("mount.version", "", string.Format("See the documentation for the <a href=\"{0}\">rpc</a> library.", "nmap/libraries/rpc.html")),
                new ScriptArgument("nfs.version", "", string.Format("See the documentation for the <a href=\"{0}\">rpc</a> library.", "nmap/libraries/rpc.html")),
                new ScriptArgument("rpc.protocol", "", string.Format("See the documentation for the <a href=\"{0}\">rpc</a> library.", "nmap/libraries/rpc.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("SLAX Library", new ScriptArgument[] {
                new ScriptArgument("slaxml.debug", "3", string.Format("See the documentation for the <a href=\"{0}\">slaxml</a> library.", "nmap/libraries/slaxml.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("SMB Library", new ScriptArgument[] {
                new ScriptArgument("randomseed", "", string.Format("See the documentation for the <a href=\"{0}\">smb</a> library.", "nmap/libraries/smb.html")),
                new ScriptArgument("smbbasic", "", string.Format("See the documentation for the <a href=\"{0}\">smb</a> library.", "nmap/libraries/smb.html")),
                new ScriptArgument("smbport", "", string.Format("See the documentation for the <a href=\"{0}\">smb</a> library.", "nmap/libraries/smb.html")),
                new ScriptArgument("smbsign", "", string.Format("See the documentation for the <a href=\"{0}\">smb</a> library.", "nmap/libraries/smb.html")),
            }));
            ArgumentGroups.Add(new ArgumentGroup("SMBAUTH Library", new ScriptArgument[] {
                new ScriptArgument("smbdomain", "", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html")),
                new ScriptArgument("smbhash", "", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html")),
                new ScriptArgument("smbnoguest", "", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html")),
                new ScriptArgument("smbpassword", "", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html")),
                new ScriptArgument("smbtype", "NTLMv1", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html")),
                new ScriptArgument("smbusername", "", string.Format("See the documentation for the <a href=\"{0}\">smbauth</a> library.", "nmap/libraries/smbauth.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("SMTP Library", new ScriptArgument[] {
                new ScriptArgument("smtp.domain", "3", string.Format("See the documentation for the <a href=\"{0}\">smtp</a> library.", "nmap/libraries/smtp.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("SNMP Library", new ScriptArgument[] {
                new ScriptArgument("snmp.version", "0", string.Format("See the documentation for the <a href=\"{0}\">snmp</a> library.", "nmap/libraries/snmp.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("TARGET Library", new ScriptArgument[] {
                new ScriptArgument("newtargets", "", string.Format("See the documentation for the <a href=\"{0}\">target</a> library.", "nmap/libraries/target.html")),
                new ScriptArgument("max-newtargets", "", string.Format("See the documentation for the <a href=\"{0}\">target</a> library.", "nmap/libraries/target.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("TLS Library", new ScriptArgument[] {
                new ScriptArgument("tls.servername", "", string.Format("See the documentation for the <a href=\"{0}\">tls</a> library.", "nmap/libraries/tls.html")),
            }));
            ArgumentGroups.Add(new ArgumentGroup("TNS Library", new ScriptArgument[] {
                new ScriptArgument("tns.sid", "", string.Format("See the documentation for the <a href=\"{0}\">tns</a> library.", "nmap/libraries/tns.html")),
            }));
            ArgumentGroups.Add(new ArgumentGroup("UNPWDB", new ScriptArgument[] {
                new ScriptArgument("passdb", "", string.Format("See the documentation for the <a href=\"{0}\">unpwdb</a> library.", "nmap/libraries/unpwdb.html")),
                new ScriptArgument("unpwdb.passlimit", "", string.Format("See the documentation for the <a href=\"{0}\">unpwdb</a> library.", "nmap/libraries/unpwdb.html")),
                new ScriptArgument("unpwdb.timelimit", "", string.Format("See the documentation for the <a href=\"{0}\">unpwdb</a> library.", "nmap/libraries/unpwdb.html")),
                new ScriptArgument("unpwdb.userlimit", "", string.Format("See the documentation for the <a href=\"{0}\">unpwdb</a> library.", "nmap/libraries/unpwdb.html")),
                new ScriptArgument("userdb", "", string.Format("See the documentation for the <a href=\"{0}\">unpwdb</a> library.", "nmap/libraries/unpwdb.html"))
            }));
            ArgumentGroups.Add(new ArgumentGroup("VULNS Library", new ScriptArgument[] {
                new ScriptArgument("vulns.short", "", string.Format("See the documentation for the <a href=\"{0}\">vulns</a> library.", "nmap/libraries/vulns.html")),
                new ScriptArgument("vulns.showall", "", string.Format("See the documentation for the <a href=\"{0}\">vulns</a> library.", "nmap/libraries/vulns.html"))
            }));
            OnPropertyChanged("ArgumentGroups");
        }

        private void SetSelection()
        {
            ArgumentGroups.ForEach(ag =>
            {
                ag.IsSelected = ag.Arguments.Intersect(Script.Arguments).Any();
            });
        }
        #endregion

        #region List handlers
        /// <summary>
        /// Script category list check box has been clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">send event</param>
        public void OnScriptArgumentListClicked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            ArgumentGroup scriptArgumentGroup = checkBox.DataContext as ArgumentGroup;
            if (checkBox.IsChecked.Value)
            {
                SelectedArgumentGroups.Add(scriptArgumentGroup);
                logger.Debug(string.Format("Script argument group added - name: {0}", scriptArgumentGroup.GroupName));
            }
            else
            {
                SelectedArgumentGroups.Add(scriptArgumentGroup);
                logger.Debug(string.Format("Script argument group removed - name: {0}", scriptArgumentGroup.GroupName));
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Close the script library dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            logger.Debug(string.Format("Add script library dialog closed"));
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// Save button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedArgumentGroups != null && SelectedArgumentGroups.Count > 0)
            {
                logger.Debug(string.Format("Script argument groups selected - size: {0}", SelectedArgumentGroups.Count));
                SelectedArgumentGroups.ForEach(g =>
                {
                    g.Arguments.ForEach(a => SelectedScriptArguments.Add(a));
                });
                logger.Debug(string.Format("Script argument selected - size: {0}", SelectedScriptArguments.Count));
                DialogResult = true;
            }
            else
            {
                DialogResult = false;
            }
            Close();
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property changed handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Property changed notification.
        /// </summary>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    #region Argument group class
    /// <summary>
    /// Argument group class.
    /// </summary>
    public class ArgumentGroup
    {
        public string GroupName { get; set; }

        public bool IsSelected { get; set; }

        public List<ScriptArgument> Arguments = new List<ScriptArgument>();

        public ArgumentGroup(string groupName, params ScriptArgument[] arguments)
        {
            GroupName = groupName;
            Arguments = arguments.ToList();
        }
    }
    #endregion
}
