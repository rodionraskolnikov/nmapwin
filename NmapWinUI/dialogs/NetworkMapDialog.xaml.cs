﻿using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.map;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for NetworkMapDialog.xaml
    /// </summary>
    public partial class NetworkMapDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Host database singleton.
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Trace route database singleton.
        /// </summary>
        private readonly DatabaseTraceRoute DatabaseTraceRoute = DatabaseTraceRoute.Instance;

        /// <summary>
        /// Hop database singleton.
        /// </summary>
        private readonly DatabaseHop DatabaseHop = DatabaseHop.Instance;
        #endregion

        private Scan _scan;
        public Scan Scan
        {
            get
            {
                return _scan;
            }
            set
            {
                if (value != null)
                {
                    _scan = value;
                    InitializeNetworkGraph(value);
                }
            }
        }

        #region Constructor
        public NetworkMapDialog()
        {
            InitializeComponent();

            DataContext = this;
        }

        /// <summary>
        /// Initialize the network graph
        /// </summary>
        /// <param name="scan">scan to use for the initialization</param>
        private void InitializeNetworkGraph(Scan scan)
        {
            NmapGraph nmapGraph = new NmapGraph(true);

            // Add localhost
            NmapVertex rootVertex = new NmapVertex
            {
                Id = 0,
                Name = "localhost",
                Address = "127.0.0.1"
            };
            nmapGraph.AddVertex(rootVertex);

            NmapVertex currentVertex;
            List<Host> hosts = DatabaseHost.FindHostsByScanId(scan.Id);
            hosts.ForEach(host =>
            {
                currentVertex = rootVertex;
                List<TraceRoute> traceroutes = DatabaseTraceRoute.FindTraceRoutesByHost(host.Id);
                if (traceroutes != null && traceroutes.Count > 0)
                {
                    traceroutes.ForEach(t =>
                    {
                        List<Hop> hops = DatabaseHop.FindHopsByTraceRoute(t.Id);
                        hops.ForEach(hop =>
                        {
                            NmapVertex newVertex = AddVertex(nmapGraph, hop, currentVertex);
                            currentVertex = newVertex;
                        });
                    });
                }
            });

            //Add Layout Algorithm Types
            _layoutAlgorithmTypes.Add("BoundedFR");
            _layoutAlgorithmTypes.Add("Circular");
            _layoutAlgorithmTypes.Add("CompoundFDP");
            _layoutAlgorithmTypes.Add("EfficientSugiyama");
            _layoutAlgorithmTypes.Add("FR");
            _layoutAlgorithmTypes.Add("ISOM");
            _layoutAlgorithmTypes.Add("KK");
            _layoutAlgorithmTypes.Add("LinLog");
            _layoutAlgorithmTypes.Add("Tree");

            //Pick a default Layout Algorithm Type
            LayoutAlgorithmType = "BoundedFR";

            Graph = nmapGraph;
        }
        #endregion

        #region Private Methods
        private NmapEdge AddNewGraphEdge(NmapGraph graph, NmapVertex from, NmapVertex to)
        {
            string edgeString = string.Format("{0}-{1} Connected", from.Id, to.Id);

            NmapEdge newEdge = new NmapEdge(edgeString, from, to);
            graph.AddEdge(newEdge);
            return newEdge;
        }

        private NmapVertex AddVertex(NmapGraph nmapGraph, Hop hop, NmapVertex currentVertex)
        {
            NmapVertex newVertex = new NmapVertex
            {
                Id = hop.Id,
                Name = hop.HostName,
                Address = hop.IpAddr,
            };
            Host hopHost = DatabaseHost.FindHostByAddress(hop.IpAddr);
            if (hopHost?.PrimaryOs != null)
            {
                List<Os> operatingSystems = hopHost.OperatingSystems;
                operatingSystems.ForEach(o =>
                {
                    OsMatch osMatch = o.OsMatches.OrderByDescending(item => item.Accuracy).First();
                    if (osMatch.Name.Contains("Linux"))
                    {
                        newVertex.OsMatch = "linux";
                    }
                    else if (osMatch.Name.Contains("Windows"))
                    {
                        newVertex.OsMatch = "windows";
                    }
                    else if (osMatch.Name.Contains("Android"))
                    {
                        newVertex.OsMatch = "android";
                    }
                    else if (osMatch.Name.Contains("iOS"))
                    {
                        newVertex.OsMatch = "iOs";
                    }
                    else
                    {
                        newVertex.OsMatch = "unknown";
                    }
                });
            }
            else
            {
                newVertex.OsMatch = "unknown";
            }

            // Add vertex
            nmapGraph.AddVertex(newVertex);
            AddNewGraphEdge(nmapGraph, currentVertex, newVertex);
            return newVertex;
        }
        #endregion

        #region Public Properties
        private List<string> _layoutAlgorithmTypes = new List<string>();
        public List<string> LayoutAlgorithmTypes
        {
            get { return _layoutAlgorithmTypes; }
        }

        private string _layoutAlgorithmType;
        public string LayoutAlgorithmType
        {
            get { return _layoutAlgorithmType; }
            set
            {
                _layoutAlgorithmType = value;
                NotifyPropertyChanged("LayoutAlgorithmType");
            }
        }

        private NmapGraph _graph;
        public NmapGraph Graph
        {
            get { return _graph; }
            set
            {
                _graph = value;
                NotifyPropertyChanged("Graph");
            }
        }
        #endregion

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        #endregion
    }
}
