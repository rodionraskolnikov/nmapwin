﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Worker;
using NmapWinUI.util;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace NmapWinUI.dialogs
{
    /// <summary>
    /// Interaction logic for ShowNdiffDIalog.xaml
    /// </summary>
    public partial class ShowNdiffDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ShowNdiffDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;
        /// <summary>
        /// Collection for the scan table
        /// </summary>
        public ObservableCollection<Scan> ScanData { get; set; }

        /// <summary>
        /// Ndiff worker process
        /// </summary>
        private NdiffWorker ndiffWorker;
        #endregion

        #region Bindings
        /// <summary>
        /// Scan list
        /// </summary>
        private List<Scan> _scans = new List<Scan>();
        public List<Scan> Scans
        {
            get
            {
                return _scans;
            }
            set
            {
                if (value != null && value.Count == 2)
                {
                    _scans = value;
                    SetDefined(true);
                    RunNdiff(value);
                }
            }
        }

        /// <summary>
        /// Output text
        /// </summary>
        private string _outputText;
        public string OutputText
        {
            get
            {
                return _outputText;
            }
            set
            {
                if (value != null)
                {
                    _outputText = value;
                    OnPropertyChanged("OutputText");
                }
            }
        }

        /// <summary>
        /// Scan 1 name
        /// </summary>
        public string Name1Text { get; set; }

        /// <summary>
        /// Scan 2 name
        /// </summary>
        public string Name2Text { get; set; }

        /// <summary>
        /// Name 1 text box
        /// </summary>
        private TextBox Name1TextBox;

        /// <summary>
        /// Name 1 combo box
        /// </summary>
        private ComboBox Name1ComboBox;

        /// <summary>
        /// Name 2 text box
        /// </summary>
        private TextBox Name2TextBox;

        /// <summary>
        /// Name 2 combo box
        /// </summary>
        private ComboBox Name2ComboBox;

        /// <summary>
        /// Defined flag
        /// </summary>
        private bool _isDefined = false;
        public bool IsDefined
        {
            get
            {
                return _isDefined;
            }
            set
            {
                SetDefined(value);
            }
        }

        public Scan SelectedScan1 { get; set; }
        public Scan SelectedScan2 { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constrcutor
        /// </summary>
        public ShowNdiffDialog()
        {
            InitializeComponent();

            DataContext = this;

            IsDefined = false;
        }
        #endregion

        public void RunNdiff(List<Scan> scans)
        {
            // Set name text boxes
            Name1Text = scans[0].Name;
            Name2Text = scans[1].Name;

            // Start worker
            ndiffWorker = new NdiffWorker();
            ndiffWorker.InitializeWorker();
            ndiffWorker.PropertyChanged += HandleWorkerChanges;
            ndiffWorker.RunWorkerAsync(scans);
            UpdateStatusBar();
        }

        #region Private methods
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }

        private void SetDefined(bool isDefined)
        {
            if (isDefined)
            {
                WidgetTools.RemoveWidget(Name1StackPanel, Name1TextBox);
                WidgetTools.RemoveWidget(Name1StackPanel, Name1ComboBox);
                Name1TextBox = new TextBox
                {
                    Name = "Name1TextBox",
                    DataContext = this,
                    IsReadOnly = true,
                    Height = 23,
                    Text = Scans[0].Name
                };
                WidgetTools.AddWidget(Name1StackPanel, Name1TextBox);

                WidgetTools.RemoveWidget(Name2StackPanel, Name2TextBox);
                WidgetTools.RemoveWidget(Name2StackPanel, Name2ComboBox);
                Name2TextBox = new TextBox
                {
                    Name = "Name2TextBox",
                    DataContext = this,
                    IsReadOnly = true,
                    Height = 23,
                    Text = Scans[1].Name
                };
                WidgetTools.AddWidget(Name2StackPanel, Name2TextBox);
            }
            else
            {
                ScanData = new ObservableCollection<Scan>(DatabaseScan.FindScans());
                WidgetTools.RemoveWidget(Name2StackPanel, Name2TextBox);
                WidgetTools.RemoveWidget(Name2StackPanel, Name2ComboBox);
                Name1ComboBox = new ComboBox
                {
                    Name = "Name1ComboBox",
                    DataContext = this,
                    Height = 23,
                    DisplayMemberPath = "Name",
                    ItemsSource = ScanData,
                    IsEditable = true,
                    IsReadOnly = true,
                    Text = "Select first scan..."
                };
                WidgetTools.AddWidget(Name1StackPanel, Name1ComboBox);
                Binding binding1 = new Binding
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    Path = new PropertyPath("SelectedScan1"),
                };
                Name1ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding1);

                WidgetTools.RemoveWidget(Name2StackPanel, Name2TextBox);
                WidgetTools.RemoveWidget(Name2StackPanel, Name2ComboBox);
                Name2ComboBox = new ComboBox
                {
                    Name = "Name2ComboBox",
                    DataContext = this,
                    DisplayMemberPath = "Name",
                    ItemsSource = ScanData,
                    IsEditable = true,
                    IsReadOnly = true,
                    Text = "Select second scan..."
                };
                WidgetTools.AddWidget(Name2StackPanel, Name2ComboBox);
                Binding binding2 = new Binding
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    Path = new PropertyPath("SelectedScan2"),
                };
                Name2ComboBox.SetBinding(ComboBox.SelectedItemProperty, binding2);
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            if (Scans != null && Scans.Count == 2)
            {
                RunNdiff(Scans);
            }
            else if (SelectedScan1 != null && SelectedScan2 != null)
            {

                Scans.Add(SelectedScan1);
                Scans.Add(SelectedScan2);
                RunNdiff(Scans);
            }
            UpdateStatusBar();
        }

        /// <summary>
        /// Close button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            logger.DebugFormat("Ndiff dialog closed");
            Close();
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Ndiff output changed
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void HandleWorkerChanges(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("NdiffOutputText"))
            {
                OutputText = ndiffWorker.NdiffOutputText;
            }
        }

        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
