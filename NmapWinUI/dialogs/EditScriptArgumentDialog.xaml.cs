﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System.ComponentModel;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for AddScriptArgument.xaml
    /// </summary>
    public partial class EditScriptArgumentDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(EditScriptArgumentDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Script database singleton
        /// </summary>
        private DatabaseScript DatabaseScript = DatabaseScript.Instance;

        /// <summary>
        /// Script argument
        /// </summary>
        public ScriptArgument ScriptArgument { get; set; } = new ScriptArgument();
        #endregion

        #region Constructor
        public EditScriptArgumentDialog()
        {
            InitializeComponent();

            DataContext = this;
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Close the script edit dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            logger.Debug(string.Format("Add script argument dialog closed - name: {0} id: {1}", ScriptArgument.Name, ScriptArgument.Id));
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// Save button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            ScriptArgument = DatabaseScript.UpdateScriptArgument(ScriptArgument);
            DialogResult = true;
            Close();
        }
        #endregion

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
