﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 

using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for ListScriptDialog.xaml
    /// </summary>
    public partial class ListScriptDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(EditScriptDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Script database singleton
        /// </summary>
        private readonly DatabaseScript DatabaseScript = DatabaseScript.Instance;

        /// <summary>
        /// Collection for the scripts table
        /// </summary>
        public ObservableCollection<Script> ScriptData { get; set; }

        /// <summary>
        /// Categories for the selection combo box.
        /// </summary>
        public List<CategoryClass> Categories { get; set; } = new List<CategoryClass>();

        /// <summary>
        /// Selected category
        /// </summary>
        private CategoryClass _selectedCategory;
        public CategoryClass SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                _selectedCategory = value;
                FilterCategory();
            }
        }
        #endregion

        #region Constructor
        public ListScriptDialog()
        {
            InitializeComponent();

            DataContext = this;

            InitializeDialog();
        }

        private void InitializeDialog()
        {
            // Fill in scan data grid
            ScriptDatagrid.Items.Clear();
            ScriptData = new ObservableCollection<Script>(DatabaseScript.FindScripts());
            ScriptDatagrid.ItemsSource = ScriptData;

            // Set categories
            Enum.GetNames(typeof(ScriptCategory.ScriptCategories))
                .ToList()
                .ForEach(t =>
                {
                    ScriptCategory.ScriptCategories category = (ScriptCategory.ScriptCategories)Enum.Parse(typeof(ScriptCategory.ScriptCategories), t);
                    Categories.Add(new CategoryClass(t));
                });
            OnPropertyChanged("Categories");

            UpdateStatusBar();
            logger.Debug(string.Format("Found scripts - count: {0}", ScriptData.Count));
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Refresh the script list.
        /// </summary>
        private void RefreshScriptList()
        {
            // Save current sorting
            List<SortDescription> sortDescriptions = new List<SortDescription>(ScriptDatagrid.Items.SortDescriptions);

            // Refresh data from database
            ScriptData = new ObservableCollection<Script>(DatabaseScript.FindScripts());
            ScriptDatagrid.ItemsSource = ScriptData;

            // Apply scrolling
            DataGridUtils.ScrollToEnd(ScriptDatagrid, IsScrollToEnd);

            // Resort datagrid
            DataGridUtils.SortDataGrid(ScriptDatagrid, sortDescriptions);

            OnPropertyChanged("ScriptData");
        }

        /// <summary>
        /// Open script edit dialog.
        /// </summary>
        private void OpenEditDialog(Script script)
        {
            EditScriptDialog editScriptDialog = new EditScriptDialog
            {
                Owner = this,
                IsNew = false,
                Script = script
            };
            if (editScriptDialog.ShowDialog() == true)
            {
                RefreshScriptList();
            }
        }

        /// <summary>
        /// Update status bar.
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }        
        #endregion

        #region Datagrid handlers
        /// <summary>
        /// Data grid selection changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnDataGridSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Script script = (Script)row.Item;
            OpenEditDialog(script);
        }

        /// <summary>
        /// Scroll to end flag.
        /// </summary>
        private bool _isScrollToEnd = false;
        public bool IsScrollToEnd
        {
            get
            {
                return _isScrollToEnd;
            }
            set
            {
                _isScrollToEnd = value;
                if (_isScrollToEnd)
                {
                    ScriptDatagrid.ScrollIntoView(ScriptData[ScriptData.Count - 1]);
                }
                OnPropertyChanged("ScriptData");
            }
        }
        #endregion

        #region Search handlers
        /// <summary>
        /// Filter the scripts by category.
        /// </summary>
        private void FilterCategory()
        {
            if (SelectedCategory != null)
            {
                ScriptCategory.ScriptCategories category = (ScriptCategory.ScriptCategories)Enum.Parse(typeof(ScriptCategory.ScriptCategories), SelectedCategory.Name);
                ScriptDatagrid.ItemsSource = new ObservableCollection<Script>(DatabaseScript.FindScriptsByCategory(category));
            }
        }

        /// <summary>
        /// Category filter clear button clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCategoryClearButtonClicked(object sender, RoutedEventArgs e)
        {
            ScriptData = new ObservableCollection<Script>(DatabaseScript.FindScripts());
            ScriptDatagrid.ItemsSource = new ObservableCollection<Script>(DatabaseScript.FindScripts());
            CategoryComboBox.SelectedIndex = -1;
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refreshes the script list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshScriptList();
            if (IsScrollToEnd)
            {
                ScriptDatagrid.ScrollIntoView(ScriptData[ScriptData.Count - 1]);
            }
        }

        /// <summary>
        /// Opens the new script dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnNewScriptButtonClicked(object sender, RoutedEventArgs e)
        {
            EditScriptDialog editScriptDialog = new EditScriptDialog
            {
                Owner = this,
                IsNew = true,
                Title = "New Script"
            };
            if (editScriptDialog.ShowDialog() == true)
            {
                RefreshScriptList();
            }
        }

        /// <summary>
        /// Opoen new script dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnWriteHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            WriteHelpFiles writeHelpFiles = new WriteHelpFiles
            {
                Owner = this,
                Title = "Writing Help Files"
            };
            if (writeHelpFiles.ShowDialog() == true)
            {
                UpdateStatusBar();
            }
        }

        /// <summary>
        /// Open script details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScriptOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Script script = (Script)row.Item;
                    OpenEditDialog(script);
                    break;
                }
        }

        /// <summary>
        /// Delete selected script.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnScriptDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Script script = (Script)row.Item;
                    DatabaseScript.DeleteScript(script);
                    ScriptData.Remove(script);
                    break;
                }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/scriptlist/script-list.htm");
        }

        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatusBar();
        }
        #endregion
    }

    public class CategoryClass
    {
        public string Name { get; set; }

        public CategoryClass(string name)
        {
            Name = name;
        }
    }
}
