﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.dialogs;
using NmapWinUI.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for HostDetails.xaml
    /// </summary>
    public partial class HostDetailsDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(HostDetailsDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Host database singleton
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Address database singleton
        /// </summary>
        private readonly DatabaseAddress DatabaseAddress = DatabaseAddress.Instance;

        /// <summary>
        /// Host name database singleton
        /// </summary>
        private readonly DatabaseHostName DatabaseHostName = DatabaseHostName.Instance;

        /// <summary>
        /// OS match database singleton
        /// </summary>
        private readonly DatabaseOsMatch DatabaseOsMatch = DatabaseOsMatch.Instance;

        /// <summary>
        /// Port database singleton
        /// </summary>
        private readonly DatabasePort DatabasePort = DatabasePort.Instance;

        /// <summary>
        /// Service database singleton
        /// </summary>
        private readonly DatabaseService DatabaseService = DatabaseService.Instance;

        /// <summary>
        /// Hop database singleton
        /// </summary>
        private readonly DatabaseHop DatabaseHop = DatabaseHop.Instance;

        /// <summary>
        /// Collection for the host names table
        /// </summary>
        public ObservableCollection<HostName> HostNameData { get; set; }

        /// <summary>
        /// Collection for the address table
        /// </summary>
        public ObservableCollection<Address> AddressData { get; set; }

        /// <summary>
        /// Collection for the port table
        /// </summary>
        public ObservableCollection<Port> PortData { get; set; }

        /// <summary>
        /// Collection for the port service table
        /// </summary>
        public ObservableCollection<PortService> ServiceData { get; set; }

        /// <summary>
        /// Collection for the OS match table
        /// </summary>
        public ObservableCollection<OsMatch> OsMatchData { get; set; }

        /// <summary>
        /// Collection for the hop table
        /// </summary>
        public ObservableCollection<Hop> HopData { get; set; }

        /// <summary>
        /// Collection for the host script table
        /// </summary>
        public ObservableCollection<HostScript> HostScriptData { get; set; }

        /// <summary>
        /// Service details
        /// </summary>
        public string ServiceDetails { get; set; }
        #endregion

        #region Bindings
        private Host _host;
        public Host Host
        {
            get
            {
                return _host;
            }
            set
            {
                if (!value.Equals(_host))
                {
                    _host = value;
                    InitializeDialog();
                }
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public HostDetailsDialog()
        {
            InitializeComponent();

            DataContext = this;
        }
        #endregion

        #region Initialization
        /// <summary>
        /// Initialization
        /// </summary>
        private void InitializeDialog()
        {
            Title = "Host Details (id: " + Host.Id + ")";

            // Get host from database
            _host = DatabaseHost.FindHostByIdFull(Host.Id);

            // Initialize host name table
            HostNameData = new ObservableCollection<HostName>(DatabaseHostName.FindHostNamesByHost(Host.Id));
            HostNameDatagrid.ItemsSource = HostNameData;

            // Initialize host script table
            HostScriptData = new ObservableCollection<HostScript>(DatabaseHost.FindHostScriptsByHost(Host.Id));
            HostScriptDatagrid.ItemsSource = HostScriptData;

            // Initialize port table
            AddressData = new ObservableCollection<Address>(DatabaseAddress.FindAddressByHost(Host.Id));
            AddressDatagrid.ItemsSource = AddressData;

            // Initialize port table
            PortData = new ObservableCollection<Port>(DatabasePort.FindPortsByHost(Host.Id));
            PortDatagrid.ItemsSource = PortData;

            // Initializing the service table
            ServiceData = new ObservableCollection<PortService>(DatabaseService.FindServicesByHost(Host.Id));
            ServiceDatagrid.ItemsSource = ServiceData;

            // Initialize OS match table
            OsMatchData = new ObservableCollection<OsMatch>(DatabaseOsMatch.FindOsMatchesByHost(Host.Id));
            OsMatchDatagrid.ItemsSource = OsMatchData;

            // Initialize hops table
            HopData = new ObservableCollection<Hop>(DatabaseHop.FindHopsByHost(Host.Id));
            HopDatagrid.ItemsSource = HopData;

            OnPropertyChanged("Host");
            UpdateStatus();

            logger.DebugFormat("Host details dialog initialized - host: {0}", _host.Identifier);
        }
        #endregion

        #region Private methdos
        /// <summary>
        /// Refresh the host name table.
        /// </summary>
        private void RefreshNameTable()
        {
            HostNameData = new ObservableCollection<HostName>(DatabaseHostName.FindHostNamesByHost(Host.Id));
            HostNameDatagrid.ItemsSource = HostNameData;
            OnPropertyChanged("HostNameDatagrid");
        }

        /// <summary>
        /// Refresh the address table.
        /// </summary>
        private void RefreshAddressTable()
        {
            AddressData = new ObservableCollection<Address>(DatabaseAddress.FindAddressByHost(Host.Id));
            AddressDatagrid.ItemsSource = AddressData;
            OnPropertyChanged("AddressDatagrid");
        }

        /// <summary>
        /// Refresh the port table.
        /// </summary>
        private void RefreshPortTable()
        {
            PortData = new ObservableCollection<Port>(DatabasePort.FindPortsByHost(Host.Id));
            PortDatagrid.ItemsSource = PortData;
            OnPropertyChanged("PortDatagrid");
        }

        /// <summary>
        /// Refresh the service table.
        /// </summary>
        private void RefreshServiceTable()
        {
            ServiceData = new ObservableCollection<PortService>(DatabaseService.FindServicesByHost(Host.Id));
            ServiceDatagrid.ItemsSource = ServiceData;
            OnPropertyChanged("PortDatagrid");
        }

        /// <summary>
        /// Refresh the hop table.
        /// </summary>
        private void RefreshHopTable()
        {
            HopData = new ObservableCollection<Hop>(DatabaseHop.FindHopsByHost(Host.Id));
            HopDatagrid.ItemsSource = HopData;
            OnPropertyChanged("HopDatagrid");
        }

        /// <summary>
        /// Opens the port details dialog.
        /// </summary>
        /// <param name="port">port to openr</param>
        private void OpenPortDetailsDialog(Port port)
        {
            // Lazy loading of host attributes
            port = DatabasePort.FindPortById(port.Id);
            PortDetailsDialog portDetailsDialog = new PortDetailsDialog
            {
                Owner = this,
                Port = port
            };
            portDetailsDialog.Show();
        }

        /// <summary>
        /// Update status bar label.
        /// </summary>
        private void UpdateStatus()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        #region Table selection
        public void OnAddressDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            //Address address = (Address)row.Item;
            /*if (address.Service != null)
            {
                Service service = DatabaseService.FindServiceById(port.Service.Id);
                service.Cpes.ForEach(c =>
                {
                    ServiceDetails = c.Content + Environment.NewLine;
                });
            }*/
        }

        public void OnPortDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Port port = (Port)row.Item;
            if (port != null)
            {
                PortDetailsDialog portDetailsDialog = new PortDetailsDialog
                {
                    Owner = this,
                    Port = port
                };
                portDetailsDialog.Show();
            }
        }

        public void OnServiceDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            /*Port port = (Port)row.Item;
            if (port.Service != null)
            {
                Service service = DatabaseService.FindServiceById(port.Service.Id);
                service.Cpes.ForEach(c =>
                {
                    ServiceDetails = c.Content + Environment.NewLine;
                });
            }*/
        }

        public void OnOsMatchDataGridSelectionChanged(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            /*Port port = (Port)row.Item;
            if (port.Service != null)
            {
                Service service = DatabaseService.FindServiceById(port.Service.Id);
                service.Cpes.ForEach(c =>
                {
                    ServiceDetails = c.Content + Environment.NewLine;
                });
            }*/
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the host dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Host = DatabaseHost.FindHostByIdFull(_host.Id);
            OnPropertyChanged("Host");
        }

        /// <summary>
        /// Delete the host
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            Host = DatabaseHost.FindHostById(Host.Id);
            DatabaseHost.DeleteHost(Host);
            logger.InfoFormat("Host deleted - host: {0}", Host.Identifier);
            Close();
        }

        /// <summary>
        /// Help button clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/hostdetails/host-details.htm");
        }

        /// <summary>
        /// Name table refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnNameTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshNameTable();
        }

        /// <summary>
        /// Address table refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnAddressTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshAddressTable();
        }

        /// <summary>
        /// Delete all addresses button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnAddressTableDeleteAllButtonClicked(object sender, RoutedEventArgs e)
        {
            PortDatagrid.Dispatcher.Invoke(() =>
            {
                DatabaseAddress.DeleteAddresses(Host.Addresses);
                Host = DatabaseHost.UpdateHost(Host);
                RefreshAddressTable();
            });
        }

        public void OnAddressOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not yet implemented!", "Host details host (id: " + Host.Id + ")", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// Address delete button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnAddressDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Address address = (Address)row.Item;
                    DatabaseAddress.DeleteAddress(address);
                    RefreshAddressTable();
                    break;
                }
        }

        /// <summary>
        /// Port table refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnPortTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshPortTable();
        }

        public void OnPortTableDeleteAllButtonClicked(object sender, RoutedEventArgs e)
        {
            PortDatagrid.Dispatcher.Invoke(() =>
            {
                DatabasePort.DeletePorts(Host.Ports);
                Host = DatabaseHost.UpdateHost(Host);
                PortData.Clear();
            });
        }

        public void OnPortOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Port port = (Port)row.Item;
                    OpenPortDetailsDialog(port);
                    break;
                }
        }

        /// <summary>
        /// Port delete button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnPortDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Port port = (Port)row.Item;
                    DatabasePort.DeletePort(port);
                    RefreshPortTable();
                    break;
                }
        }

        /// <summary>
        /// Service table refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnServiceTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshServiceTable();
        }

        /// <summary>
        /// Service delete all button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnServiceTableDeleteAllButtonClicked(object sender, RoutedEventArgs e)
        {
            ServiceDatagrid.Dispatcher.Invoke(() =>
            {
                DatabaseService.DeleteServices(DatabaseService.FindServicesByHost(Host.Id));
                RefreshServiceTable();
            });
        }

        public void OnServiceOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not yet implemented!", "Host details host (id: " + Host.Id + ")", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// Service delete button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnServiceDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    PortService service = (PortService)row.Item;
                    DatabaseService.DeleteService(service);
                    RefreshServiceTable();
                    break;
                }
        }

        /// <summary>
        /// Hop table refresh button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHopTableRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshHopTable();
        }

        /// <summary>
        /// Hop table delete all button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHopTableDeleteAllButtonClicked(object sender, RoutedEventArgs e)
        {
            HopDatagrid.Dispatcher.Invoke(() =>
            {
                DatabaseHop.DeleteHops(DatabaseHop.FindHopsByHost(Host.Id));
                RefreshHopTable();
            });
        }


        /// <summary>
        /// Hop table details button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnHopOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not yet implemented!", "Host details host (id: " + Host.Id + ")", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public void OnHopDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Hop hop = (Hop)row.Item;
                    DatabaseHop.DeleteHop(hop);
                    RefreshHopTable();
                    break;
                }
        }

        /// <summary>
        /// Dialog close button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Property changed notifications
        /// </summary>
        /// <param name="propertyName">property name</param>
        private void OnPropertyChanged(string propertyName)
        {
            UpdateStatus();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
