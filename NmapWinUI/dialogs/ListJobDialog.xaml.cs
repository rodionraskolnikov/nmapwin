﻿using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Wcf;
using NmapWinUI.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NmapWinUI.dialogs
{
    /// <summary>
    /// Interaction logic for ListJobDialog.xaml
    /// </summary>
    public partial class ListJobDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ListJobDialog));

        /// <summary>
        /// Name for the error/warning dialogs.
        /// </summary>
        private static string ErrorDialogTitle = "NmapWin Service";

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Script database singleton
        /// </summary>
        private DatabaseJob DatabaseJob = DatabaseJob.Instance;

        /// <summary>
        /// Collection for the job table
        /// </summary>
        public ObservableCollection<Job> JobData { get; set; }

        /// <summary>
        /// HTTP Proxy for the communication with the service
        /// </summary>
        private ICommandListener httpProxy;
        #endregion

        #region Contructor
        public ListJobDialog()
        {
            InitializeComponent();

            DataContext = this;

            InitializeDialog();

            InitializeCommandListener();
        }

        private void InitializeDialog()
        {
            // Fill in scan data grid
            JobDatagrid.Items.Clear();
            JobData = new ObservableCollection<Job>(DatabaseJob.FindJobs());
            JobDatagrid.ItemsSource = JobData;

            UpdateStatusBar();
            logger.Debug(string.Format("Found Jobs - count: {0}", JobData.Count));
        }

        /// <summary>
        /// Initialize service communication.
        /// </summary>
        private void InitializeCommandListener()
        {
            ChannelFactory<ICommandListener> httpFactory = new ChannelFactory<ICommandListener>(new BasicHttpBinding(), new EndpointAddress("http://localhost:8000/Command"));
            httpProxy = httpFactory.CreateChannel();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Refresh the job list.
        /// </summary>
        private void RefreshJobList()
        {
            JobData = new ObservableCollection<Job>(DatabaseJob.FindJobs());
            JobDatagrid.ItemsSource = JobData;
            OnPropertyChanged("JobData");
            UpdateStatusBar();
            logger.Debug(string.Format("Scripts refreshed - count: {0}", JobData.Count));
        }

        /// <summary>
        /// Open job edit dialog.
        /// </summary>
        private void OpenEditDialog(Job job)
        {
            EditJobDialog editScriptDialog = new EditJobDialog
            {
                Owner = this,
                IsNew = false,
                Job = job
            };
            if (editScriptDialog.ShowDialog() == true)
            {
                RefreshJobList();
            }
        }

        /// <summary>
        /// Remove job from service scheduler.
        /// </summary>
        /// <param name="job"></param>
        private void RemoveJobFromScheduler(Job job)
        {
            try
            {
                httpProxy.RemoveJob(job);
                logger.Info(string.Format("Job removed from scheduler - id: {0} name: {1}", job.Id, job.Name));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not remove job from NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, ErrorDialogTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Pause a job in the service scheduler.
        /// </summary>
        /// <param name="job"></param>
        /// <return>updated job</param>
        private Job PauseJobInScheduler(Job job)
        {
            try
            {
                job = httpProxy.PauseJob(job);
                logger.Info(string.Format("Job paused in scheduler - id: {0} name: {1}", job.Id, job.Name));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not pause job in NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, ErrorDialogTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return job;
        }

        /// <summary>
        /// Restart a job in the service scheduler.
        /// </summary>
        /// <param name="job"></param>
        /// <return>updated job</param>
        private Job ResumeJobInScheduler(Job job)
        {
            try
            {
                job = httpProxy.ResumeJob(job);
                logger.Info(string.Format("Job restarted in scheduler - id: {0} name: {1}", job.Id, job.Name));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not restart job in NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, ErrorDialogTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return job;
        }

        /// <summary>
        /// Returns a job item from the context menu
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <returns>job item</returns>
        private Job GetJobFromContextMenu(object sender)
        {
            // Get the clicked MenuItem
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var item = (DataGrid)contextMenu.PlacementTarget;

            // Cancel and delete scan
            return item.SelectedCells != null && item.SelectedCells.Count > 0 && item.SelectedCells[0] != null ? (Job)item.SelectedCells[0].Item : null;
        }

        /// <summary>
        /// Update status bar.
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        #region Datagrid handlers
        /// <summary>
        /// Data grid delection changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnDataGridSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Job job = (Job)row.Item;
            OpenEditDialog(job);
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the job list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshJobList();
        }

        /// <summary>
        /// Opens the new job dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnNewJobButtonClicked(object sender, RoutedEventArgs e)
        {
            EditJobDialog editJobDialog = new EditJobDialog
            {
                Owner = this,
                IsNew = true,
                Title = "New Job",
                Job = new Job()
            };
            if (editJobDialog.ShowDialog() == true)
            {
                RefreshJobList();
            }
        }

        /// <summary>
        /// Open script details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Job job = (Job)row.Item;
                    OpenEditDialog(job);
                    break;
                }
        }

        /// <summary>
        /// Delete selected job.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Job job = (Job)row.Item;
                    RemoveJobFromScheduler(job);
                    JobData.Remove(job);
                    DatabaseJob.DeleteJob(job);
                    break;
                }
        }

        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Context menu handlers
        /// <summary>
        /// COntext menu loading handler.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnContextMenuLoading(object sender, RoutedEventArgs e)
        {
            ContextMenu contextMenu = sender as ContextMenu;
            if (contextMenu != null)
            {
                var item = (DataGrid)contextMenu.PlacementTarget;
                if (item.SelectedCells != null && item.SelectedCells.Count > 0)
                {
                    Job job = (Job)item.SelectedCells[0].Item;
                    switch (job.State)
                    {
                        case JobState.Normal:
                            ((MenuItem)contextMenu.Items[2]).IsEnabled = true;
                            ((MenuItem)contextMenu.Items[3]).IsEnabled = false;
                            break;
                        case JobState.Paused:
                            ((MenuItem)contextMenu.Items[2]).IsEnabled = false;
                            ((MenuItem)contextMenu.Items[3]).IsEnabled = true;
                            break;
                    }
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Open job details
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobDetailsContextMenuClicked(object sender, RoutedEventArgs e)
        {
            Job job = GetJobFromContextMenu(sender);
            if (job != null)
            {
                OpenEditDialog(job);
            }
        }

        /// <summary>
        /// Resume a paused job
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobResumeContextMenuClicked(object sender, RoutedEventArgs e)
        {
            Job job = GetJobFromContextMenu(sender);
            if (job != null)
            {
                if (job.State != JobState.Paused)
                {
                    MessageBox.Show("Job '" + job.Group + "." + job.Name + "' not paused. Cannot resume job!", ErrorDialogTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                job = ResumeJobInScheduler(job);
                DatabaseJob.UpdateJob(job);
                RefreshJobList();
            }
        }

        /// <summary>
        /// Open scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobPauseContextMenuClicked(object sender, RoutedEventArgs e)
        {
            Job job = GetJobFromContextMenu(sender);
            if (job != null)
            {
                if (job.State != JobState.Normal)
                {
                    MessageBox.Show("Job '" + job.Group + "." + job.Name + "' is currently not in the correct state. Cannot pause job!", ErrorDialogTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                job = PauseJobInScheduler(job);
                DatabaseJob.UpdateJob(job);
                RefreshJobList();
            }
        }

        /// <summary>
        /// Deletes a job from the context menu
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnJobDeleteContextMenuClicked(object sender, RoutedEventArgs e)
        {
            Job job = GetJobFromContextMenu(sender);
            if (job != null)
            {
                RemoveJobFromScheduler(job);
                DatabaseJob.DeleteJob(job);
                RefreshJobList();
            }
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatusBar();
        }
        #endregion
    }
}
