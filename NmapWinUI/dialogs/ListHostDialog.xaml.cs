﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 

using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Dialogs;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NmapWinUI.dialogs
{
    /// <summary>
    /// Interaction logic for ListHostDialog.xaml
    /// </summary>
    public partial class ListHostDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(EditScriptDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Host database singleton
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Collection for the scripts table
        /// </summary>
        public ObservableCollection<Host> HostData { get; set; }

        /// <summary>
        /// Auto-update cancellation source
        /// </summary>
        private CancellationTokenSource AutoUpdateTokenSource;

        /// <summary>
        /// Auto-update interval
        /// </summary>
        private TimeSpan AutoUpdateInterval = TimeSpan.FromSeconds(5);
        #endregion

        #region Bindings
        /// <summary>
        /// Scroll to end flag.
        /// </summary>
        private bool _isScrollToEnd = false;
        public bool IsScrollToEnd
        {
            get
            {
                return _isScrollToEnd;
            }
            set
            {
                _isScrollToEnd = value;
                RefreshHostList();
            }
        }

        /// <summary>
        /// Auto update flag.
        /// </summary>
        private bool _isAutoUpdate = true;
        public bool IsAutoUpdate
        {
            get
            {
                return _isAutoUpdate;
            }
            set
            {
                _isAutoUpdate = value;
                InitializeAutoUpdater();
                OnPropertyChanged("IsAutoUpdate");
            }
        }
        #endregion

        #region Constructor
        public ListHostDialog()
        {
            InitializeComponent();

            InitializeWidgets();

            DataContext = this;
        }
        #endregion

        #region Initialization
        /// <summary>
        /// Initialize all widgets
        /// </summary>
        private void InitializeWidgets()
        {
            // Set auto update interval
            AutoUpdateInterval = TimeSpan.Parse(DatabaseOption.Instance.FindOptionByName("nmapwin.main.autoUpdate.interval")?.Value);

            // Initialize auto-updater
            InitializeAutoUpdater();

            // Refresh host list
            RefreshHostList();
            logger.DebugFormat("Host list dialog initialized");
        }

        /// <summary>
        /// Configure the auto-updater
        /// </summary>
        private void InitializeAutoUpdater()
        {
            if (IsAutoUpdate)
            {
                AutoUpdateTokenSource = new CancellationTokenSource();
                Task.Run(() => DoAutoUpdateWork(AutoUpdateTokenSource.Token), AutoUpdateTokenSource.Token);
                logger.Debug(string.Format("Auto update task started - interval: {0}s", (int)AutoUpdateInterval.TotalMilliseconds));
            }
            else
            {
                AutoUpdateTokenSource.Cancel();
                logger.Debug(string.Format("Auto update task stopeed"));
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refreshes the host list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshHostList();
        }

        /// <summary>
        /// Open host details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Host host = (Host)row.Item;
                    OpenEditDialog(host);
                    break;
                }
        }

        /// <summary>
        /// Delete selected host.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnHostDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Host host = (Host)row.Item;
                    DatabaseHost.DeleteHost(host);
                    HostData.Remove(host);
                    break;
                }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/hostlist/host-list.htm");
        }

        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Datagrid handlers
        /// <summary>
        /// Data grid selection changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnDataGridSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Host host = (Host)row.Item;
            OpenEditDialog(host);
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Refresh the script list.
        /// </summary>
        private void RefreshHostList()
        {
            // Save current sorting
            List<SortDescription> sortDescriptions = new List<SortDescription>(HostDatagrid.Items.SortDescriptions);

            // Refresh data from database
            HostData = new ObservableCollection<Host>(DatabaseHost.FindHostsFull());
            HostDatagrid.ItemsSource = HostData;

            // Apply scrolling
            DataGridUtils.ScrollToEnd(HostDatagrid, IsScrollToEnd);

            // Resort datagrid
            DataGridUtils.SortDataGrid(HostDatagrid, sortDescriptions);

            OnPropertyChanged("HostData");
        }

        /// <summary>
        /// Auto update worker method.
        /// </summary>
        /// <param name="ct">cancellation token</param>
        private void DoAutoUpdateWork(CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
            {
                ct.ThrowIfCancellationRequested();
            }
            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                try
                {
                    HostDatagrid.Dispatcher.Invoke(() => RefreshHostList());
                    Thread.Sleep((int)AutoUpdateInterval.TotalMilliseconds);
                }
                catch (TaskCanceledException ex)
                {
                    logger.InfoFormat("Update task was cancelled - message: {0}", ex.Message);
                    break;
                }
            }
        }

        /// <summary>
        /// Open script edit dialog.
        /// </summary>
        private void OpenEditDialog(Host host)
        {
            HostDetailsDialog hostDetailsDialog = new HostDetailsDialog
            {
                Owner = this,
                Host = host
            };
            if (hostDetailsDialog.ShowDialog() == true)
            {
                RefreshHostList();
            }
        }

        /// <summary>
        /// Update status bar.
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        #region Shortcut handlers
        private void RefreshCommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RefreshCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            RefreshHostList();
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Propetrty change notifications
        /// </summary>
        /// <param name="propertyName">property name</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatusBar();
        }
        #endregion
    }
}
