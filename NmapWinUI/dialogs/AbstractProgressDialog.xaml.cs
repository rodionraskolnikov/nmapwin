﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for RecreateDatabaseDialog.xaml
    /// </summary>
    public abstract partial class AbstractProgressDialog : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Dialog title.
        /// </summary>
        public string DialogTitle { get; set; }

        #region Bindings
        private int _mainProgressBarMaximum;
        public int MainProgressBarMaximum
        {
            get
            {
                return _mainProgressBarMaximum;
            }
            set
            {
                _mainProgressBarMaximum = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainProgressBarMaximum"));
            }
        }

        private int _mainProgressBarValue = 0;
        public int MainProgressBarValue
        {
            get
            {
                return _mainProgressBarValue;
            }
            set
            {
                _mainProgressBarValue = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainProgressBarValue"));
            }
        }

        private string _mainStatusMessage;
        public string MainStatusMessage
        {
            get
            {
                return _mainStatusMessage;
            }
            set
            {
                _mainStatusMessage = "Task: " + value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainStatusMessage"));
            }
        }

        private int _detailedProgressBarMaximum;

        public int DetailedProgressBarMaximum
        {
            get
            {
                return _detailedProgressBarMaximum;
            }
            set
            {
                _detailedProgressBarMaximum = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DetailedProgressBarMaximum"));
            }
        }

        private int _detailedProgressBarValue = 0;
        public int DetailedProgressBarValue
        {
            get
            {
                return _detailedProgressBarValue;
            }
            set
            {
                _detailedProgressBarValue = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DetailedProgressBarValue"));
            }
        }

        private string _detailedStatusMessage;
        public string DetailedStatusMessage
        {
            get
            {
                return _detailedStatusMessage;
            }
            set
            {
                _detailedStatusMessage = "Status: " + value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DetailedStatusMessage"));
            }
        }

        private string _counterMessage;
        public string CounterMessage
        {
            get
            {
                return _counterMessage;
            }
            set
            {
                _counterMessage = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CounterMessage"));
            }
        }
        #endregion

        #region Constructor
        public AbstractProgressDialog()
        {
            InitializeComponent();

            DataContext = this;
            Loaded += WindowLoaded;
        }
        #endregion;

        #region Progress bars handlers
        public void SetMainValues(string status)
        {
            MainStatusLabel.Dispatcher.Invoke(() =>
            {
                MainStatusLabel.Content = status;
            });
        }

        public void SetMainValues(string status, int value)
        {
            MainStatusLabel.Dispatcher.Invoke(() =>
            {
                MainStatusLabel.Content = status;
                MainProgressBar.Value = value;
            });
        }

        public void SetMainValues(string status, int max, int value)
        {
            MainStatusLabel.Dispatcher.Invoke(() =>
            {
                MainStatusLabel.Content = status;
                MainProgressBar.Value = value;
                MainProgressBar.Maximum = max;
            });
        }

        public void IncMainValues(string status)
        {
            DetailedStatusLabel.Dispatcher.Invoke(() =>
            {
                MainProgressBar.Value++;
                MainStatusLabel.Content = status;
            });
        }

        public void SetDetailedValues(string status)
        {
            DetailedStatusLabel.Dispatcher.Invoke(() =>
            {
                DetailedStatusLabel.Content = status;
            });
        }

        public void SetDetailedValues(string status, int max, int value)
        {
            DetailedStatusLabel.Dispatcher.Invoke(() =>
            {
                DetailedStatusLabel.Content = status;
                DetailedProgressBar.Value = value;
                DetailedProgressBar.Maximum = max;
            });
        }

        public void IncDetailedValues(string status)
        {
            DetailedStatusLabel.Dispatcher.Invoke(() =>
            {
                DetailedProgressBar.Value++;
                DetailedStatusLabel.Content = status;
                CounterMessage = " (" + DetailedProgressBar.Value + "/" + DetailedProgressBar.Maximum + ")";
            });
        }
        #endregion

        public abstract void AsyncTasks();

        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                AsyncTasks();
            });
            DialogResult = true;
            Close();
        }

        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
