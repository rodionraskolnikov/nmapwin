﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using log4net.Core;
using MySql.Data.MySqlClient;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for OptionDialog.xaml
    /// </summary>
    public partial class OptionDialog : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(OptionDialog));

        /// <summary>
        /// Option database singleton
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Input field validation.
        /// </summary>
        private readonly FieldValidation Validation = FieldValidation.Instance;

        /// <summary>
        /// Collection for the scan table
        /// </summary>
        public ObservableCollection<Profile> ProfileData { get; set; }

        public Visibility IsFilePathVisible { get; set; } = Visibility.Hidden;

        public Visibility IsDatabaseServerVisible { get; set; } = Visibility.Hidden;

        public Visibility IsDatabaseNameVisible { get; set; } = Visibility.Hidden;

        public Visibility IsDatabasePortVisible { get; set; } = Visibility.Hidden;

        public Visibility IsDatabaseUserVisible { get; set; } = Visibility.Hidden;

        public Visibility IsDatabasePasswordVisible { get; set; } = Visibility.Hidden;
        #endregion

        #region Bindings
        /// <summary>
        /// Selected log level.
        /// </summary>
        private LogLevelClass _selectedLogLevel;
        public LogLevelClass SelectedLogLevel
        {
            get
            {
                return _selectedLogLevel;
            }
            set
            {
                _selectedLogLevel = value;
                ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).Root.Level = _selectedLogLevel.LogLevel;
                ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).RaiseConfigurationChanged(EventArgs.Empty);
                logger.Info(string.Format("Logging level changed - level: {0}", _selectedLogLevel.Name));
                OnPropertyChanged("SelectedLogLevel");
            }
        }

        /// <summary>
        /// Log levels
        /// </summary>
        public List<LogLevelClass> LogLevels { get; set; } = new List<LogLevelClass>();

        /// <summary>
        /// Binding for SQL logging check box
        /// </summary>
        private bool _isSqlLogging;
        public bool IsSqlLogging
        {
            get
            {
                return _isSqlLogging;
            }
            set
            {
                _isSqlLogging = value;
                logger.InfoFormat("SQL logging changed - logging: {0}", _isSqlLogging);
                DatabaseOption.UpdateOptionValue("nmapwin.logging.sql", _isSqlLogging ? "true" : "false"); 
                OnPropertyChanged("IsSqlLogging");
            }
        }

        /// <summary>
        /// Selected log level.
        /// </summary>
        private DatabaseTypeClass CurrentDatabaseType;
        private DatabaseTypeClass _selectedDatabaseType;
        public DatabaseTypeClass SelectedDatabaseType
        {
            get
            {
                return _selectedDatabaseType;
            }
            set
            {
                _selectedDatabaseType = value;
                SetVisibility(value);
                SetDatabaseFields(value);
                OnPropertyChanged("SelectedDatabaseType");
            }
        }

        /// <summary>
        /// Log levels
        /// </summary>
        public List<DatabaseTypeClass> DatabaseTypes { get; set; } = new List<DatabaseTypeClass>();

        /// <summary>
        /// Selected default profile
        /// </summary>
        private Profile _selectedProfile;
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set
            {
                _selectedProfile = value;
                if (_selectedProfile.Name.Equals("None"))
                {
                    DatabaseOption.UpdateOptionValue("nmapwin.main.defaultProfile", null);
                }
                else if (_selectedProfile != null)
                {
                    DatabaseOption.UpdateOptionValue("nmapwin.main.defaultProfile", _selectedProfile.Name);
                    OnPropertyChanged("SelectedProfile");
                    if (!string.IsNullOrEmpty(_selectedProfile.Target))
                    {
                        TargetText = _selectedProfile.Target;
                        OnPropertyChanged("TargetTextBox");
                    }
                }
            }
        }

        /// <summary>
        /// Default target
        /// </summary>
        private string _targetText = "127.0.0.1";
        public string TargetText
        {
            get { return _targetText; }
            set
            {
                if (_targetText == null || !_targetText.Equals(value))
                {
                    _targetText = value;
                    DatabaseOption.UpdateOptionValue("nmapwin.main.defaultTarget", _targetText);
                    OnPropertyChanged("Target");
                }
            }
        }

        /// <summary>
        /// Autp-update interval
        /// </summary>
        private TimeSpan _autoUpdateInterval = new TimeSpan(0, 0, 5);
        public TimeSpan AutoUpdateInterval
        {
            get { return _autoUpdateInterval; }
            set
            {
                if (value != null && !_autoUpdateInterval.Equals(value))
                {
                    _autoUpdateInterval = value;
                    DatabaseOption.UpdateOptionValue("nmapwin.main.autoUpdate.interval", _autoUpdateInterval.ToString());
                    OnPropertyChanged("AutoUpdateInterval");
                }
            }
        }

        /// <summary>
        /// Nmap base path
        /// </summary>
        private string _nmapBasePathText;
        public string NmapBasePathText
        {
            get { return _nmapBasePathText; }
            set
            {
                if (_nmapBasePathText == null || !_nmapBasePathText.Equals(value))
                {
                    _nmapBasePathText = value;
                    DatabaseOption.UpdateOptionValue("nmap.base.path", _nmapBasePathText);
                    OnPropertyChanged("NmapBasePathText");
                }
            }
        }

        /// <summary>
        /// Nmap executable path
        /// </summary>
        private string _nmapExecutableText;
        public string NmapExecutableText
        {
            get { return _nmapExecutableText; }
            set
            {
                if (_nmapExecutableText == null || !_nmapExecutableText.Equals(value))
                {
                    _nmapExecutableText = value;
                    DatabaseOption.UpdateOptionValue("nmap.executable.path", _nmapExecutableText);
                    OnPropertyChanged("NmapExecutableText");
                }
            }
        }

        /// <summary>
        /// Ncat executable path
        /// </summary>
        private string _ncatExecutableText;
        public string NcatExecutableText
        {
            get { return _ncatExecutableText; }
            set
            {
                if (_ncatExecutableText == null || !_ncatExecutableText.Equals(value))
                {
                    _ncatExecutableText = value;
                    DatabaseOption.UpdateOptionValue("ncat.executable.path", _ncatExecutableText);
                    OnPropertyChanged("NcatExecutableText");
                }
            }
        }

        /// <summary>
        /// Nping executable path
        /// </summary>
        private string _npingExecutableText;
        public string NpingExecutableText
        {
            get { return _npingExecutableText; }
            set
            {
                if (_npingExecutableText == null || !_npingExecutableText.Equals(value))
                {
                    _npingExecutableText = value;
                    DatabaseOption.UpdateOptionValue("nping.executable.path", _npingExecutableText);
                    OnPropertyChanged("NpingExecutableText");
                }
            }
        }

        /// <summary>
        /// Ndiff executable path
        /// </summary>
        private string _ndiffExecutableText;
        public string NdiffExecutableText
        {
            get { return _ndiffExecutableText; }
            set
            {
                if (_ndiffExecutableText == null || !_ndiffExecutableText.Equals(value))
                {
                    _ndiffExecutableText = value;
                    DatabaseOption.UpdateOptionValue("ndiff.executable.path", _ndiffExecutableText);
                    OnPropertyChanged("NdiffExecutableText");
                }
            }
        }

        /// <summary>
        /// Directory path for backups.
        /// </summary>
        private string _backupPathText;
        public string BackupPathText
        {
            get { return _backupPathText; }
            set
            {
                if (_backupPathText == null || !_backupPathText.Equals(value))
                {
                    _backupPathText = value;
                    DatabaseOption.UpdateOptionValue("nmapwin.backup.path", _backupPathText);
                    OnPropertyChanged("BackupPathText");
                }
            }
        }

        /// <summary>
        /// Database file path for a SQL Server CE database.
        /// </summary>
        private string _databaseFilePath;
        public string DatabaseFilePath
        {
            get { return _databaseFilePath; }
            set
            {
                if (_databaseFilePath == null || !_databaseFilePath.Equals(value))
                {
                    _databaseFilePath = value;
                    logger.Info(string.Format("Database file path changed - name: {0}", _databaseFilePath));
                    OnPropertyChanged("DatabaseFilePath");
                }
            }
        }

        /// <summary>
        /// Database server name for MySQL database.
        /// </summary>
        private string _databaseServer;
        public string DatabaseServer
        {
            get { return _databaseServer; }
            set
            {
                if (_databaseServer == null || !_databaseServer.Equals(value))
                {
                    _databaseServer = value;
                    logger.Info(string.Format("Database server changed - name: {0}", _databaseName));
                    OnPropertyChanged("DatabaseServer");
                }
            }
        }

        /// <summary>
        /// Database name for MySQL database.
        /// </summary>
        private string _databaseName;
        public string DatabaseName
        {
            get { return _databaseName; }
            set
            {
                if (_databaseName == null || !_databaseName.Equals(value))
                {
                    _databaseName = value;
                    logger.Info(string.Format("Database name changed - name: {0}", _databaseName));
                    OnPropertyChanged("DatabaseName");
                }
            }
        }

        /// <summary>
        /// Database server port for MySQL database.
        /// </summary>
        private int _databasePort;
        public int DatabasePort
        {
            get { return _databasePort; }
            set
            {
                if (_databasePort != value)
                {
                    _databasePort = value;
                    logger.Info(string.Format("Database port changed - port: {0}", _databasePort));
                    OnPropertyChanged("DatabasePort");
                }
            }
        }

        /// <summary>
        /// Database user for MySQL database.
        /// </summary>
        private string _databaseUser;
        public string DatabaseUser
        {
            get { return _databaseUser; }
            set
            {
                if (_databaseUser == null || !_databaseUser.Equals(value))
                {
                    _databaseUser = value;
                    logger.Info(string.Format("Database user changed - name: {0}", _databaseUser));
                    OnPropertyChanged("DatabaseUser");
                }
            }
        }

        /// <summary>
        /// Database pasword for MySQL database.
        /// </summary>
        private string _databasePassword;
        public string DatabasePassword
        {
            get { return _databasePassword; }
            set
            {
                if (_databasePassword == null || !_databasePassword.Equals(value))
                {
                    _databasePassword = value;
                    logger.Info(string.Format("Database password changed - name: {0}", _databasePassword));
                    OnPropertyChanged("DatabasePassword");
                }
            }
        }

        /// <summary>
        /// Skip host which are down
        /// </summary>
        private bool _isSkipDownHost;
        public bool IsSkipDownHost
        {
            get { return _isSkipDownHost; }
            set
            {
                if (!_isSkipDownHost.Equals(value))
                {
                    _isSkipDownHost = value;
                    logger.Info(string.Format("Skip down hosts flag set - value: {0}", _isSkipDownHost));
                    DatabaseOption.UpdateOptionValue("nmapwin.output.skipDown", _isSkipDownHost ? "true" : "false");
                    OnPropertyChanged("IsSkipDownHost");
                }
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Set input fields visibility.
        /// </summary>
        /// <param name="databaseType"></param>
        private void SetVisibility(DatabaseTypeClass databaseType)
        {
            switch (databaseType.Name)
            {
                case "SQL Server CE":
                    IsFilePathVisible = Visibility.Visible;
                    IsDatabaseServerVisible = Visibility.Hidden;
                    IsDatabaseNameVisible = Visibility.Hidden;
                    IsDatabasePortVisible = Visibility.Hidden;
                    IsDatabaseUserVisible = Visibility.Hidden;
                    IsDatabasePasswordVisible = Visibility.Hidden;
                    break;
                case "SQL Server":
                    IsFilePathVisible = Visibility.Hidden;
                    IsDatabaseServerVisible = Visibility.Visible;
                    IsDatabaseNameVisible = Visibility.Visible;
                    IsDatabasePortVisible = Visibility.Visible;
                    IsDatabaseUserVisible = Visibility.Visible;
                    IsDatabasePasswordVisible = Visibility.Visible;
                    break;
                case "MySQL Server":
                    IsFilePathVisible = Visibility.Hidden;
                    IsDatabaseServerVisible = Visibility.Visible;
                    IsDatabaseNameVisible = Visibility.Visible;
                    IsDatabasePortVisible = Visibility.Visible;
                    IsDatabaseUserVisible = Visibility.Visible;
                    IsDatabasePasswordVisible = Visibility.Visible;
                    break;
                case "SQLite":
                    IsFilePathVisible = Visibility.Visible;
                    IsDatabaseServerVisible = Visibility.Hidden;
                    IsDatabaseNameVisible = Visibility.Hidden;
                    IsDatabasePortVisible = Visibility.Hidden;
                    IsDatabaseUserVisible = Visibility.Hidden;
                    IsDatabasePasswordVisible = Visibility.Hidden;
                    break;
            }
            OnPropertyChanged("IsFilePathVisible");
            OnPropertyChanged("IsDatabaseServerVisible");
            OnPropertyChanged("IsDatabaseNameVisible");
            OnPropertyChanged("IsDatabasePortVisible");
            OnPropertyChanged("IsDatabaseUserVisible");
            OnPropertyChanged("IsDatabasePasswordVisible");
        }

        private void SetDatabaseFields(DatabaseTypeClass databaseType)
        {
            DatabaseServer = GetCurrentDatabaseServer(databaseType);
            DatabasePort = GetCurrentDatabasePort(databaseType);
            DatabaseName = GetCurrentDatabaseName(databaseType);
            DatabaseUser = GetCurrentDatabaseUser(databaseType);
            DatabasePassword = GetCurrentDatabasePwd(databaseType);
            DatabaseFilePath = GetCurrentFilePath(databaseType);
        }

        /// <summary>
        /// Return the file path for a SQL Server CE database.
        /// </summary>
        /// <returns>current file path</returns>
        private string GetCurrentFilePath(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            switch (databaseType.Name)
            {
                case "SQL Server CE":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return @"..\data\nmapwin.sdf";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[0].Split('=')[1];
                    }
                    break;
                case "SQLite":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return @"..\data\nmapwin.db";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[0].Split('=')[1];
                    }
                    break;
            }
            return null;
        }

        /// <summary>
        /// Return the server host for a NySQL server data source.
        /// </summary>
        /// <returns>server name</returns>
        private string GetCurrentDatabaseServer(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            switch (databaseType.Name)
            {
                case "SQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "localhost";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[0].Split('=')[1].Split(',')[0];
                    }
                    break;
                case "MySQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "localhost";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[0].Split('=')[1];
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Return the server port for a NySQL server data source.
        /// </summary>
        /// <returns>server port</returns>
        private int GetCurrentDatabasePort(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            switch (databaseType.Name)
            {
                case "SQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return 1433;
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return int.Parse(tmp[0].Split(',')[1]);
                    }
                    break;
                case "MySQL Server":
                    if(CurrentDatabaseType != databaseType)
                    {
                        return 3306;
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return int.Parse(tmp[1].Split('=')[1]);
                    }
                    break;
                default:
                    break;
            }
            return -1;
        }

        /// <summary>
        /// Return the server name for a NySQL server data source.
        /// </summary>
        /// <returns>server name</returns>
        private string GetCurrentDatabaseName(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            switch (databaseType.Name)
            {
                case "SQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[1].Split('=')[1];
                    }
                    break;
                case "MySQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[2].Split('=')[1];
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Return the user name for a NySQL server data source.
        /// </summary>
        /// <returns>server name</returns>
        private string GetCurrentDatabaseUser(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            switch (databaseType.Name)
            {
                case "SQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[2].Split('=')[1];
                    }
                    break;
                case "MySQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[3].Split('=')[1];
                    }
                    break;
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Return the password name for a NySQL server data source.
        /// </summary>
        /// <returns>server name</returns>
        private string GetCurrentDatabasePwd(DatabaseTypeClass databaseType)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            switch (databaseType.Name)
            {
                case "SQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[3].Split('=')[1];
                    }
                    break;
                case "MySQL Server":
                    if (CurrentDatabaseType != databaseType)
                    {
                        return "nmapwin";
                    }
                    if (!string.IsNullOrEmpty(connectionstring))
                    {
                        string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                        return tmp[4].Split('=')[1];
                    }
                    break;
                default:
                    break;
            }            
            return null;
        }

        /// <summary>
        /// Define new connection string.
        /// </summary>
        /// <param name="databaseType">database type</param>
        /// <returns></returns>
        private string GetConnectionString(DatabaseTypeClass databaseType)
        {
            switch (databaseType.Name)
            {
                case "SQL Server CE":
                    return @"Data Source=" + DatabaseFilePath + ";Persist Security Info=False;Max Database Size=4091;";
                case "SQL Server":
                    return @"Server=" + DatabaseServer + "," + DatabasePort + ";Database=" + DatabaseName + ";User Id=" + DatabaseUser + ";Password=" + DatabasePassword + ";Persist Security Info=True;";
                case "MySQL Server":
                    return @"server=" + DatabaseServer + ";port=" + DatabasePort + ";database=" + DatabaseName + ";uid=" + DatabaseUser + ";password=" + DatabasePassword + ";";
                case "SQLite":
                    return @"Data Source=" + DatabaseFilePath + ";Version=3;";
                default:
                    break;
            }
            return string.Empty;
        }

        /// <summary>
        /// Set new connection string
        /// </summary>
        /// <param name="databaseType">database type</param>
        private void SetConnectionString(DatabaseTypeClass databaseType)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            DoSetConnectionString(databaseType, config);
            ConfigurationManager.RefreshSection("connectionStrings");

            string serviceConfigFile = @".\nmapserv.exe.config";
            if (File.Exists(serviceConfigFile))
            {
                var configMap = new ExeConfigurationFileMap { ExeConfigFilename = serviceConfigFile };
                var serviceConfig = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                DoSetConnectionString(databaseType, serviceConfig);
                System.Windows.MessageBox.Show("Please restart the NmapWin Service in order to apply current changes!", "NmapWin", MessageBoxButton.OK);
            }
        }

        private void DoSetConnectionString(DatabaseTypeClass databaseType, Configuration config)
        {
            var parent = config.ConnectionStrings;
            var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(parent, false);
            parent.ConnectionStrings.Clear();

            ConnectionStringSettings settings = new ConnectionStringSettings();
            switch (databaseType.Name)
            {
                case "SQL Server CE":
                    SetVisibility(databaseType);
                    settings.Name = "ConnectionString";
                    settings.ProviderName = databaseType.Provider;
                    settings.ConnectionString = GetConnectionString(databaseType);
                    break;
                case "SQL Server":
                    SetVisibility(databaseType);
                    settings.Name = "ConnectionString";
                    settings.ProviderName = databaseType.Provider;
                    settings.ConnectionString = GetConnectionString(databaseType);
                    break;
                case "MySQL Server":
                    SetVisibility(databaseType);
                    settings.Name = "ConnectionString";
                    settings.ProviderName = databaseType.Provider;
                    settings.ConnectionString = GetConnectionString(databaseType);
                    break;
                case "SQLite":
                    SetVisibility(databaseType);
                    settings.Name = "ConnectionString";
                    settings.ProviderName = databaseType.Provider;
                    settings.ConnectionString = GetConnectionString(databaseType);
                    break;
            }

            // Set field to read/write
            parent.ConnectionStrings.Add(settings);
            config.Save();
        }
        #endregion

        #region Validation
        /// <summary>
        /// Trigger a validation even when the text box is left empty. WPF does only trigger
        /// the validation when the text box content has been changes. This will also trigger
        /// the validation when the text hasn't been changed and is still left empty.
        /// </summary>
        /// <param name="sender">text box sender</param>
        /// <param name="e">trigger event</param>
        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            ((System.Windows.Controls.Control)sender).GetBindingExpression(System.Windows.Controls.TextBox.TextProperty).UpdateSource();
        }

        public string Error { get; set; } = string.Empty;

        public string this[string propertyName]
        {
            get
            {
                return ValidateProperties(propertyName);
            }
        }

        private string ValidateProperties(string propertyName)
        {
            switch (propertyName)
            {
                case "NmapBasePathText":
                    return Validation.ValidateDirectory("NmapBasePathText", NmapBasePathText);
                case "NmapExecutableText":
                    return Validation.ValidateFile("NmapExecutableText", NmapExecutableText);
                case "NcatExecutableText":
                    return Validation.ValidateFile("NcatExecutableText", NcatExecutableText);
                case "NpingExecutableText":
                    return Validation.ValidateFile("NpingExecutableText", NpingExecutableText);
                case "NdiffExecutableText":
                    return Validation.ValidateFile("NdiffExecutableText", NdiffExecutableText);
                case "BackupPathText":
                    return Validation.ValidateDirectory("BackupPathText", BackupPathText);
            }
            return string.Empty;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public OptionDialog()
        {
            InitializeComponent();

            InitializeWidgets();

            DataContext = this;
        }

        /// <summary>
        /// Initialize widgets.
        /// </summary>
        private void InitializeWidgets()
        {
            // Main window options
            TargetText = DatabaseOption.FindOptionByName("nmapwin.main.defaultTarget").Value;

            // Load profiles
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileData.Insert(0, new Profile(-1, "None", "", ""));
            ProfileComboBox.ItemsSource = ProfileData;
            string defaultProfileName = DatabaseOption.FindOptionByName("nmapwin.main.defaultProfile").Value;
            SelectedProfile = ProfileData.Where(p => p.Name == defaultProfileName).FirstOrDefault();
            OnPropertyChanged("Profiles");

            // Auto update
            string defaultAutoUpdateInterval = DatabaseOption.FindOptionByName("nmapwin.main.autoUpdate.interval").Value;
            AutoUpdateInterval = TimeSpan.Parse(defaultAutoUpdateInterval);
            IntervalPicker.Step = 5;
            IntervalPicker.Maximum = TimeSpan.FromHours(1);
            IntervalPicker.Minimum = TimeSpan.FromSeconds(5);
            IntervalPicker.ShowDays = false;

            // Is skip host
            IsSkipDownHost = DatabaseOption.FindOptionByName("nmapwin.output.skipDown").GetBool(false);

            // Nmap options
            NmapBasePathText = DatabaseOption.FindOptionByName("nmap.base.path").Value;
            NmapExecutableText = DatabaseOption.FindOptionByName("nmap.executable.path").Value;
            NcatExecutableText = DatabaseOption.FindOptionByName("ncat.executable.path").Value;
            NpingExecutableText = DatabaseOption.FindOptionByName("nping.executable.path").Value;
            NdiffExecutableText = DatabaseOption.FindOptionByName("ndiff.executable.path").Value;

            // Backup options
            BackupPathText = DatabaseOption.FindOptionByName("nmapwin.backup.path").Value;

            // Database type combo box values
            DatabaseTypes.Add(new DatabaseTypeClass("SQL Server CE", "System.Data.SqlServerCe.4.0"));
            DatabaseTypes.Add(new DatabaseTypeClass("SQL Server", "System.Data.SqlClient"));
            DatabaseTypes.Add(new DatabaseTypeClass("MySQL Server", "MySql.Data.MySqlClient"));
            DatabaseTypes.Add(new DatabaseTypeClass("SQLite", "System.Data.SQLite"));

            // Selected database type
            string providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            SelectedDatabaseType = CurrentDatabaseType = DatabaseTypes.Find(x => x.Provider == providerName);

            // Log level combo box values
            LogLevels.Add(new LogLevelClass("All", Level.All));
            LogLevels.Add(new LogLevelClass("Debug", Level.Debug));
            LogLevels.Add(new LogLevelClass("Info", Level.Info));
            LogLevels.Add(new LogLevelClass("Warn", Level.Warn));
            LogLevels.Add(new LogLevelClass("Error", Level.Error));
            LogLevels.Add(new LogLevelClass("Fatal", Level.Fatal));

            // Selected log level
            Level currentLevel = ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).Root.Level;
            SelectedLogLevel = LogLevels.Find(x => x.LogLevel == currentLevel);

            // SQL logging options
            IsSqlLogging = bool.Parse(DatabaseOption.FindOptionByName("nmapwin.logging.sql").Value);
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Cancel button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Set database connectivity and ask user to change connection string.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnTestButtonClicked(object sender, RoutedEventArgs e)
        {
            switch (SelectedDatabaseType.Name)
            {
                case "SQL Server CE":
                    if (File.Exists(DatabaseFilePath))
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Database connection successful! Would you like to set the database connection string?", "NmapWin", MessageBoxButton.YesNoCancel);
                        if (result == MessageBoxResult.Yes)
                        {
                            SetConnectionString(SelectedDatabaseType);
                        }
                        else
                        {
                            SetConnectionString(CurrentDatabaseType);
                            SelectedDatabaseType = CurrentDatabaseType;
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Cannot connect to database!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                case "MySQL Server":
                    if (CheckMySql())
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Database connection successful! Would you like to set the database connection string?", "My App", MessageBoxButton.YesNoCancel);
                        if (result == MessageBoxResult.Yes)
                        {
                            SetConnectionString(SelectedDatabaseType);
                        }
                        else
                        {
                            SetConnectionString(CurrentDatabaseType);
                            SelectedDatabaseType = CurrentDatabaseType;
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Cannot connect to database!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                case "SQL Server":
                    if (CheckSqlServer())
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Database connection successful! Would you like to set the database connection string?", "My App", MessageBoxButton.YesNoCancel);
                        if (result == MessageBoxResult.Yes)
                        {
                            SetConnectionString(SelectedDatabaseType);
                        }
                        else
                        {
                            SetConnectionString(CurrentDatabaseType);
                            SelectedDatabaseType = CurrentDatabaseType;
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Cannot connect to database!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Check MySQL database connectivity.
        /// </summary>
        /// <returns>true if successful, otherwise false</returns>
        private bool CheckMySql()
        {
            string connectionString = GetConnectionString(SelectedDatabaseType);
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check MySQL database connectivity.
        /// </summary>
        /// <returns>true if successful, otherwise false</returns>
        private bool CheckSqlServer()
        {
            string connectionString = GetConnectionString(SelectedDatabaseType);
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Save button clicked.
        /// </summary>
        /// <<remarks>Save changes an close dialog.</remarks>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            SetConnectionString(SelectedDatabaseType);
            Close();
        }

        public void OnBasePathBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new FolderBrowserDialog
            {
                SelectedPath = DatabaseOption.FindOptionByName("nmap.base.path").Value
            };
            DialogResult result = openFileDialog.ShowDialog(this.GetIWin32Window());
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                NmapBasePathText = openFileDialog.SelectedPath;
                OnPropertyChanged("Options");
            }
        }

        public void OnNmapBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\Nmap",
                Filter = "exe files (*.exe)|*.EXE|All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                NmapExecutableText = openFileDialog.FileName;
                OnPropertyChanged("Options");
            }
        }

        public void OnNcatBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\Nmap",
                Filter = "exe files (*.exe)|*.EXE|All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                NcatExecutableText = openFileDialog.FileName;
                OnPropertyChanged("Options");
            }
        }

        public void OnNpingBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\Nmap",
                Filter = "exe files (*.exe)|*.EXE|All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                NpingExecutableText = openFileDialog.FileName;
                OnPropertyChanged("Options");
            }
        }

        public void OnNdiffBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\Nmap",
                Filter = "exe files (*.exe)|*.EXE|All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                NdiffExecutableText = openFileDialog.FileName;
                OnPropertyChanged("Options");
            }
        }
        public void OnBackupPathBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new FolderBrowserDialog
            {
                SelectedPath = DatabaseOption.FindOptionByName("nmapwin.backup.path").Value
            };
            DialogResult result = openFileDialog.ShowDialog(this.GetIWin32Window());
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                BackupPathText = openFileDialog.SelectedPath;
                OnPropertyChanged("Options");
            }
        }

        public void OnDatabasePathBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = @"C:\Program Files (x86)\NmapWin",
                Filter = "sdf files (*.sdf)|*.sdf|All files (*.*)|*.*",
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == true)
            {
                DatabaseFilePath = openFileDialog.FileName;
                OnPropertyChanged("DatabaseFilePath");
            }
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class LogLevelClass
    {
        public string Name { get; set; }

        public Level LogLevel { get; set; }

        public LogLevelClass(string name, Level level)
        {
            Name = name;
            LogLevel = level;
        }
    }

    public class DatabaseTypeClass
    {
        public string Name { get; set; }

        public string Provider { get; set; }

        public DatabaseTypeClass(string name, string provider)
        {
            Name = name;
            Provider = provider;
        }
    }

    public static class MyWpfExtensions
    {
        public static IWin32Window GetIWin32Window(this System.Windows.Media.Visual visual)
        {
            var source = PresentationSource.FromVisual(visual) as System.Windows.Interop.HwndSource;
            IWin32Window win = new OldWindow(source.Handle);
            return win;
        }

        private class OldWindow : IWin32Window
        {
            private readonly System.IntPtr _handle;
            public OldWindow(System.IntPtr handle)
            {
                _handle = handle;
            }

            #region IWin32Window Members
            System.IntPtr IWin32Window.Handle
            {
                get { return _handle; }
            }
            #endregion
        }
    }
}
