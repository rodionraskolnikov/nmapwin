﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for RecreateDatabaseDialog.xaml
    /// </summary>
    public abstract partial class AbstractSingleProgressDialog : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Dialog title.
        /// </summary>
        public string DialogTitle { get; set; }

        private int _mainProgressBarMaximum;

        public int MainProgressBarMaximum
        {
            get
            {
                return _mainProgressBarMaximum;
            }
            set
            {
                _mainProgressBarMaximum = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainProgressBarMaximum"));
            }
        }

        private int _mainProgressBarValue = 0;
        public int MainProgressBarValue
        {
            get
            {
                return _mainProgressBarValue;
            }
            set
            {
                _mainProgressBarValue = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainProgressBarValue"));
            }
        }

        private string _mainStatusMessage;
        public string MainStatusMessage
        {
            get
            {
                return _mainStatusMessage;
            }
            set
            {
                _mainStatusMessage = "Task: " + value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MainStatusMessage"));
            }
        }

        private string _counterMessage;
        public string CounterMessage
        {
            get
            {
                return _counterMessage;
            }
            set
            {
                _counterMessage = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CounterMessage"));
            }
        }

        public AbstractSingleProgressDialog()
        {
            InitializeComponent();

            DataContext = this;
            Loaded += WindowLoaded;
        }

        public void SetMainValues(string status, int max, int value)
        {
            MainProgressBar.Dispatcher.Invoke(() =>
            {
                MainStatusMessage = status;
                MainProgressBarMaximum = max;
                MainProgressBarValue = value;
            });
        }

        public void IncProgress()
        {
            MainProgressBar.Dispatcher.Invoke(() => MainProgressBarValue++);
            CounterMessage = " (" + MainProgressBar.Value + "/" + MainProgressBar.Maximum + ")";
        }

        public void IncProgress(string status)
        {
            MainProgressBar.Dispatcher.Invoke(() =>
            {
                MainStatusLabel.Content = status;
                MainProgressBarValue++;
                CounterMessage = " (" + MainProgressBar.Value + "/" + MainProgressBar.Maximum + ")";
            });
        }

        public abstract void AsyncTasks();

        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                AsyncTasks();
            });
            DialogResult = true;
            Close();
        }

        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
