﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Database;
using NmapWinUI.Utils;
using System.Configuration;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class AboutDialog : Window
    {
        #region Fields
        /// <summary>
        /// Database history singleton.
        /// </summary>
        public static DatabaseHistory DatabaseHistory = DatabaseHistory.Instance;

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Application version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Running architecture
        /// </summary>
        public string Architecture { get; set; }

        /// <summary>
        /// Application author
        /// </summary>
        public string Author { get; set; } = "Rodion Raskolnikov";

        /// <summary>
        /// Date/time of link step
        /// </summary>
        public long LinkerTime { get; set; }

        /// <summary>
        /// Database version
        /// </summary>
        public string DatabaseVersion { get; set; }

        /// <summary>
        /// Database type
        /// </summary>
        public string DatabaseType { get; set; }
        #endregion

        public AboutDialog()
        {
            InitializeComponent();

            DataContext = this;

            Version = ApplicationVersion.Version;
            Architecture = ApplicationVersion.GetArchitecture();
            Author = ApplicationVersion.Author;
            LinkerTime = ApplicationVersion.LinkerTime;
            DatabaseVersion = DatabaseHistory.FindLatestVersion();
            DatabaseType = GetDatabaseType();
        }

        private void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private string GetDatabaseType()
        {
            string providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            return providerName switch
            {
                "System.Data.SQLite" => "SQLite",
                "MySql.Data.MySqlClient" => "MySQL",
                "System.Data.SqlClient" => "MSSQL",
                "System.Data.SqlServerCe.4.0" => "SQLServer Compact",
                _ => "Unkown",
            };
        }
    }
}
