﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Worker;
using NmapWinUI.Utils;
using System;
using System.ComponentModel;
using System.Windows;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for FullOutputDialog.xaml
    /// </summary>
    public partial class FullOutputDialog : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(FullOutputDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Nmap worker manager singleton
        /// </summary>
        private readonly NmapWorkerManager NmapWorkerManager = NmapWorkerManager.Instance;

        /// <summary>
        /// Current nmap worker
        /// </summary>
        private NmapWorker nmapWorker;

        #region Bindings
        private Scan _scan;
        public Scan Scan
        {
            get
            {
                return _scan;
            }
            set
            {
                _scan = value;
                Title = "Output (scanId: " + _scan.Id + ")";
                InitializeDialog();
                OnPropertyChanged("Scan");
                UpdateStatusBar();
            }
        }

        private string _output;
        public string Output
        {
            get
            {
                return _output;
            }
            set
            {
                _output = value;
                OnPropertyChanged("Output");
                UpdateStatusBar();
            }
        }

        private string _statusText;
        public string StatusText
        {
            get
            {
                return _statusText;
            }
            set
            {
                _statusText = value;
                OnPropertyChanged("StatusText");
            }
        }
        
        /// <summary>
        /// XML pretty print flag.
        /// </summary>
        private bool _isPrettyPrint = false;
        public bool IsPrettyPrint
        {
            get
            {
                return _isPrettyPrint;
            }
            set
            {
                _isPrettyPrint = value;
                XmlPrettyPrintConverter.IsPrettyPrint = value;
                OnPropertyChanged("Output");
            }
        }

        /// <summary>
        ///  XML available flag
        /// </summary>
        public bool XmlOutput { get; set; } = false;
        #endregion

        #region Constructor
        public FullOutputDialog()
        {
            InitializeComponent();

            DataContext = this;

            logger.Debug("Output dialog opened");
        }

        private void InitializeDialog()
        {
            nmapWorker = NmapWorkerManager.FindWorkerByScan(Scan);
            if (nmapWorker != null)
            {
                nmapWorker.PropertyChanged += HandleWorkerChanges;
            }
        }
        #endregion

        #region Status text handling
        private void UpdateStatusBar()
        {
            statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat);
        }
        #endregion

        #region Button handlers
        public void OnOutputRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Scan = DatabaseScan.FindScanById(_scan.Id);
            Output = XmlOutput ? _scan.OutputXml : _scan.Output;
        }

        public void OnOutputDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            if (XmlOutput)
                Scan.OutputXml = null;
            else
                Scan.Output = null;
            Scan = DatabaseScan.UpdateScan(Scan);
            Output = XmlOutput ? _scan.OutputXml : _scan.Output;
        }

        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion

        #region Event handling
        private void HandleWorkerChanges(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "WorkerOutputChanged":
                    ScanOutput.Dispatcher.Invoke(() =>
                    {
                        Scan = nmapWorker.Scan;
                        Output = XmlOutput ? _scan.OutputXml : _scan.Output;
                    });
                    break;
                case "WorkerStatusChanged":
                    ScanOutput.Dispatcher.Invoke(() =>
                    {
                        Scan = nmapWorker.Scan;
                        Output = XmlOutput ? _scan.OutputXml : _scan.Output;
                    });
                    break;
            }
        }

        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
