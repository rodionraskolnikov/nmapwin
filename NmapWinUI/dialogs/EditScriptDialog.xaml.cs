﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for ScriptDetailsDialog.xaml
    /// </summary>
    public partial class EditScriptDialog : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(EditScriptDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Input field validation.
        /// </summary>
        private readonly FieldValidation Validation = FieldValidation.Instance;

        /// <summary>
        /// Script database singleton
        /// </summary>
        private DatabaseScript DatabaseScript = DatabaseScript.Instance;

        /// <summary>
        /// Current list index
        /// </summary>
        private int CurrentIndex = 0;

        /// <summary>
        /// Full list of scrtips
        /// </summary>
        private List<Script> ScriptList;

        /// <summary>
        /// Script argument list
        /// </summary>
        /// // TODO: Remove???
        public ObservableCollection<ScriptArgument> ScriptArgumentList = new ObservableCollection<ScriptArgument>();
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public EditScriptDialog()
        {
            InitializeComponent();

            DataContext = this;

            InitializeScriptList();
        }

        /// <summary>
        /// Initialize script list.
        /// </summary>
        public void InitializeScriptList()
        {
            ScriptList = DatabaseScript.FindScripts();
            Script = ScriptList[CurrentIndex];
        }

        /// <summary>
        /// Initialize the current script.
        /// </summary>
        private void InitializeScript()
        {
            ScriptTypesList = new List<ScriptTypeClass>();
            Enum.GetNames(typeof(ScriptType.ScriptTypes))
                .ToList()
                .ForEach(t =>
                {
                    ScriptType.ScriptTypes sss = (ScriptType.ScriptTypes)Enum.Parse(typeof(ScriptType.ScriptTypes), t);
                    bool isSelected = _script.Types.Contains(new ScriptType(sss));
                    ScriptTypesList.Add(new ScriptTypeClass(t, isSelected));
                });
            OnPropertyChanged("ScriptTypesList");

            ScriptCategoriesList = new List<ScriptCategoryClass>();
            Enum.GetNames(typeof(ScriptCategory.ScriptCategories))
                .ToList()
                .ForEach(t =>
                {
                    ScriptCategory.ScriptCategories category = (ScriptCategory.ScriptCategories)Enum.Parse(typeof(ScriptCategory.ScriptCategories), t);
                    bool isSelected = _script.Categories.Contains(new ScriptCategory(category));
                    ScriptCategoriesList.Add(new ScriptCategoryClass(t, isSelected));
                });
            OnPropertyChanged("ScriptCategoriesList");

            //_script = DatabaseScript.FindScriptById(Script.Id);
            ScriptArgumentList = new ObservableCollection<ScriptArgument>(Script.Arguments.OrderBy(a => a.Name));
            ScriptArgumentDatagrid.ItemsSource = ScriptArgumentList;
            OnPropertyChanged("ScriptArgumentDatagrid");

            int index = ScriptList.FindIndex(a => a.Id == Script.Id);
            if (index > 0)
            {
                CurrentIndex = index;
            }
            ScriptName = Script.Name;
            ScriptAuthor = Script.Author;
            ScriptLicense = Script.License;
            ScriptHtml = Script.Html;

            logger.Debug(string.Format("Script edit dialog initialized - name: {0} id: {1} isNew: {2}", Script.Name, Script.Id, IsNew));
        }
        #endregion

        #region Bindings scripts
        /// <summary>
        /// Script type list.
        /// </summary>
        public List<ScriptTypeClass> ScriptTypesList { get; set; }

        /// <summary>
        /// Script categories list.
        /// </summary>
        public List<ScriptCategoryClass> ScriptCategoriesList { get; set; }

        /// <summary>
        /// Script binding
        /// </summary>
        private Script _script;
        public Script Script
        {
            get
            {
                return _script;
            }
            set
            {
                if (value != null)
                {
                    _script = value;
                    InitializeScript();
                    OnPropertyChanged("Script");
                }
            }
        }

        /// <summary>
        /// New script flag.
        /// </summary>
        private bool _isNew = false;
        public bool IsNew
        {
            get
            {
                return _isNew;
            }
            set
            {
                _isNew = value;
                if (_isNew)
                {
                    IsNavigationVisible = Visibility.Hidden;
                    Script = new Script();
                }
            }
        }

        private string _scriptName;
        public string ScriptName
        {
            get { return _scriptName; }
            set
            {
                if (_scriptName == null || !_scriptName.Equals(value))
                {
                    _scriptName = value;
                    Script.Name = value;
                    //DatabaseScript.UpdateScript(Script);
                    OnPropertyChanged("ScriptName");
                }
            }
        }

        private string _scriptAuthor;
        public string ScriptAuthor
        {
            get { return _scriptAuthor; }
            set
            {
                if (_scriptAuthor == null || !_scriptAuthor.Equals(value))
                {
                    _scriptAuthor = value;
                    Script.Author = value;
                    DatabaseScript.UpdateScript(Script);
                    OnPropertyChanged("ScriptAuthor");
                }
            }
        }

        private string _scriptLicense = "https://nmap.org/book/man-legal.html";
        public string ScriptLicense
        {
            get { return _scriptLicense; }
            set
            {
                if (_scriptLicense == null || !_scriptLicense.Equals(value))
                {
                    _scriptLicense = value;
                    Script.License = value;
                    DatabaseScript.UpdateScript(Script);
                    OnPropertyChanged("ScriptLicense");
                }
            }
        }

        private string _scriptHtml;
        public string ScriptHtml
        {
            get { return _scriptHtml; }
            set
            {
                if (_scriptHtml == null || !_scriptHtml.Equals(value))
                {
                    _scriptHtml = value;
                    Script.Html = value;
                    DatabaseScript.UpdateScript(Script);
                    OnPropertyChanged("ScriptLicense");
                }
            }
        }

        /// <summary>
        /// Navigation visible flag.
        /// </summary>
        public Visibility IsNavigationVisible { get; set; } = Visibility.Visible;
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Script = DatabaseScript.FindScriptById(Script.Id);
        }

        /// <summary>
        /// Delete the script.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            DatabaseScript.DeleteScript(Script);
            ScriptList.Remove(Script);
            if (CurrentIndex > ScriptList.Count - 1)
            {
                CurrentIndex = ScriptList.Count - 1;
            }
            Script = ScriptList[CurrentIndex];
        }

        /// <summary>
        /// First button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnFirstButtonClicked(object sender, RoutedEventArgs e)
        {
            CurrentIndex = 0;
            Script = ScriptList[CurrentIndex];
        }

        /// <summary>
        /// Backward button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnBackwardButtonClicked(object sender, RoutedEventArgs e)
        {
            CurrentIndex--;
            if (CurrentIndex < 0)
            {
                CurrentIndex = 0;
            }
            Script = ScriptList[CurrentIndex];
        }

        /// <summary>
        /// Forward button clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnForwardButtonClicked(object sender, RoutedEventArgs e)
        {
            CurrentIndex++;
            if (CurrentIndex > ScriptList.Count - 1)
            {
                CurrentIndex = ScriptList.Count - 1;
            }
            Script = ScriptList[CurrentIndex];
        }

        /// <summary>
        /// Last button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnLastButtonClicked(object sender, RoutedEventArgs e)
        {
            Script = ScriptList.Last();
            CurrentIndex = ScriptList.Count - 1;
        }

        /// <summary>
        /// Search button clicked
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSearchButtonClicked(object sender, RoutedEventArgs e)
        {
            string seachName = SearchTextBox.Text;
            int index = ScriptList.FindIndex(a => a.Name.Contains(seachName));
            if (index > 0)
            {
                CurrentIndex = index;
                Script = ScriptList[CurrentIndex];
            }
            else
            {
                MessageBox.Show("No script with name '" + seachName + "' found!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/scriptdetails/script-details.htm");
        }

        /// <summary>
        /// Close the script edit dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            logger.Debug(string.Format("Script edit dialog closed - name: {0} id: {1} isNew: {2}", Script.Name, Script.Id, IsNew));
            Close();
        }

        /// <summary>
        /// Save button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            if (Script.Id == 0)
            {
                Script = DatabaseScript.InsertScript(Script);
                ScriptList.Add(Script);
                logger.Debug(string.Format("Script inserted - name: {0} id: {1}", Script.Name, Script.Id));
            }
            else
            {
                Script = DatabaseScript.UpdateScript(Script);
                logger.Debug(string.Format("Script updated - name: {0} id: {1}", Script.Name, Script.Id));
            }
            DialogResult = true;
            Close();
        }
        #endregion

        #region List handlers
        /// <summary>
        /// Script type list check box has been clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">send event</param>
        public void OnScriptTypeListClicked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            ScriptTypeClass scriptTypeClass = checkBox.DataContext as ScriptTypeClass;
            ScriptType scriptType = new ScriptType((ScriptType.ScriptTypes)Enum.Parse(typeof(ScriptType.ScriptTypes), scriptTypeClass.Name));
            if (checkBox.IsChecked.Value)
            {
                if (Script.Id == 0)
                {
                    Script.Types.Add(scriptType);
                    logger.Debug(string.Format("Script type added - name: {0}", scriptType.Type));
                }
                else
                {
                    Script = DatabaseScript.AddScriptType(Script, scriptType);
                }
            }
            else
            {
                scriptType = Script.Types.FirstOrDefault(s => s.Type == scriptType.Type);
                if (scriptType != null)
                {
                    Script.Types.Remove(scriptType);
                    logger.Debug(string.Format("Script type removed - name: {0}", scriptType.Type));
                }
            }
        }

        /// <summary>
        /// Script category list check box has been clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">send event</param>
        public void OnScriptCategoryListClicked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            ScriptCategoryClass scriptCategoryClass = checkBox.DataContext as ScriptCategoryClass;
            ScriptCategory scriptCategory = new ScriptCategory((ScriptCategory.ScriptCategories)Enum.Parse(typeof(ScriptCategory.ScriptCategories), scriptCategoryClass.Name));
            if (checkBox.IsChecked.Value)
            {
                if (Script.Id == 0)
                {
                    Script.Categories.Add(scriptCategory);
                }
                else
                {
                    Script = DatabaseScript.AddScriptCategory(Script, scriptCategory);
                }
                logger.Debug(string.Format("Script category added - name: {0}", scriptCategory.Category));
            }
            else
            {
                scriptCategory = Script.Categories.FirstOrDefault(s => s.Category == scriptCategory.Category);
                if (scriptCategory != null)
                {
                    Script.Categories.Remove(scriptCategory);
                    logger.Debug(string.Format("Script category removed - name: {0}", scriptCategory.Category));
                }
            }
        }

        /// <summary>
        /// Add argument button clicked.
        /// </summary>
        /// <remarks>The script is updated before the dialog is opened, so the dialog gets the latest script from the database.</remarks>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnAddArgumentButtonClicked(object sender, RoutedEventArgs e)
        {
            AddScriptArgumentDialog addScriptArgumentDialog = new AddScriptArgumentDialog
            {
                Owner = this,
                Script = Script
            };
            if (addScriptArgumentDialog.ShowDialog() == true)
            {
                if (Script.Id != 0 && !Script.Arguments.Contains(addScriptArgumentDialog.ScriptArgument))
                {
                    DatabaseScript.AddScriptArgument(Script, addScriptArgumentDialog.ScriptArgument);
                }
                Script.Arguments.Add(addScriptArgumentDialog.ScriptArgument);
                ScriptArgumentDatagrid.ItemsSource = new ObservableCollection<ScriptArgument>(_script.Arguments.OrderBy(a => a.Name));
                OnPropertyChanged("ScriptArgumentDatagrid");
            }
        }

        /// <summary>
        /// Add library button clicked.
        /// </summary>
        /// <remarks>The script is updated before the dialog is opened, so the dialog gets the latest script from the database.</remarks>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnAddLibraryButtonClicked(object sender, RoutedEventArgs e)
        {
            AddScriptLibraryDialog addScriptLibraryDialog = new AddScriptLibraryDialog
            {
                Owner = this,
                Script = Script
            };
            if (addScriptLibraryDialog.ShowDialog() == true)
            {
                if (Script.Id != 0)
                {
                    addScriptLibraryDialog.SelectedScriptArguments.ForEach(a =>
                    {
                        if (!Script.Arguments.Contains(a))
                        {
                            DatabaseScript.AddScriptArgument(Script, a);
                        }
                    });
                }
                Script.Arguments.AddRange(addScriptLibraryDialog.SelectedScriptArguments);
                ScriptArgumentDatagrid.ItemsSource = new ObservableCollection<ScriptArgument>(Script.Arguments.OrderBy(a => a.Name));
                OnPropertyChanged("ScriptArgumentDatagrid");
            }
        }

        /// <summary>
        /// Script argument edit button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnScriptArgumentEditButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow row)
                {
                    ScriptArgument scriptArgument = (ScriptArgument)row.Item;
                    if (scriptArgument != null)
                    {
                        EditScriptArgumentDialog editScriptArgumentDialog = new EditScriptArgumentDialog
                        {
                            Owner = this,
                            ScriptArgument = scriptArgument
                        };
                        if (editScriptArgumentDialog.ShowDialog() == true)
                        {
                            logger.Debug(string.Format("Script argument edited - name: {0}", scriptArgument.Name));
                            OnPropertyChanged("ScriptArgumentList");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Script argument delete button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnScriptArgumentDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow row)
                {
                    ScriptArgument scriptArgument = (ScriptArgument)row.Item;
                    if (scriptArgument != null)
                    {
                        Script.RemoveArgument(scriptArgument);
                        DatabaseScript.DeleteScriptArgument(scriptArgument);
                        ScriptArgumentList.Remove(scriptArgument);
                        logger.Debug(string.Format("Script argument removed - name: {0}", scriptArgument.Name));
                        OnPropertyChanged("ScriptArgumentList");
                    }
                    break;
                }
            }
        }
        #endregion

        #region Validation
        /// <summary>
        /// Trigger a validation even when the text box is left empty. WPF does only trigger
        /// the validation when the text box content has been changes. This will also trigger
        /// the validation when the text hasn't been changed and is still left empty.
        /// </summary>
        /// <param name="sender">text box sender</param>
        /// <param name="e">trigger event</param>
        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            ((Control)sender).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        public string Error { get; set; } = string.Empty;

        public string this[string propertyName]
        {
            get
            {
                return ValidateProperties(propertyName);
            }
        }

        private string ValidateProperties(string propertyName)
        {
            switch (propertyName)
            {
                case "ScriptName":
                    return Validation.ValidateScriptName("ScriptName", ScriptName, false);
                case "ScriptAuthor":
                    return Validation.ValidateScriptAuthor("ScriptAuthor", ScriptAuthor);
                case "ScriptLicense":
                    return Validation.ValidateUrl("ScriptLicense", ScriptLicense);
                case "ScriptHtml":
                    return Validation.ValidateUrl("ScriptHtml", ScriptHtml);
            }
            return string.Empty;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Update the status bar.
        /// </summary>
        private void UpdateStatus()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change notification
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatus();
        }
        #endregion
    }

    /// <summary>
    /// Private class fo the type list box.
    /// </summary>
    public class ScriptTypeClass
    {
        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public ScriptTypeClass(string name, bool isChecked)
        {
            Name = name;
            IsSelected = isChecked;
        }
    }

    /// <summary>
    /// Private class fo the categories list box.
    /// </summary>
    public class ScriptCategoryClass
    {
        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public ScriptCategoryClass(string name, bool isChecked)
        {
            Name = name;
            IsSelected = isChecked;
        }
    }
}
