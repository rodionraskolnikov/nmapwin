﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NmapWinUI.Dialogs
{
    /// <summary>
    /// Interaction logic for ListProfileDialog.xaml
    /// </summary>
    public partial class ListProfileDialog : Window, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ListProfileDialog));

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Collection for the scripts table
        /// </summary>
        public ObservableCollection<Profile> ProfileData { get; set; }
        #endregion

        #region Bindings
        /// <summary>
        /// Scroll to end flag.
        /// </summary>
        private bool _isScrollToEnd = false;
        public bool IsScrollToEnd
        {
            get
            {
                return _isScrollToEnd;
            }
            set
            {
                _isScrollToEnd = value;
                RefreshProfileList();
            }
        }
        #endregion

        #region Constructor
        public ListProfileDialog()
        {
            InitializeComponent();

            DataContext = this;

            InitializeDialog();
        }

        /// <summary>
        /// Initialize dialog
        /// </summary>
        private void InitializeDialog()
        {
            // Fill in scan data grid
            ProfileDatagrid.Items.Clear();
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileDatagrid.ItemsSource = ProfileData;
            UpdateStatusBar();
            logger.Debug(string.Format("Found profiles - count: {0}", ProfileData.Count));
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Refresh the profile list.
        /// </summary>
        private void RefreshProfileList()
        {
            // Save current sorting
            List<SortDescription> sortDescriptions = new List<SortDescription>(ProfileDatagrid.Items.SortDescriptions);

            // Refresh porfile data
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileDatagrid.ItemsSource = ProfileData;

            // Resort datagrid
            DataGridUtils.SortDataGrid(ProfileDatagrid, sortDescriptions);

            // Apply scrolling
            DataGridUtils.ScrollToEnd(ProfileDatagrid, IsScrollToEnd);

            OnPropertyChanged("ProfileData");
            logger.DebugFormat("Profiles refreshed - count: {0}", ProfileData.Count);
        }

        /// <summary>
        /// Update status bar.
        /// </summary>
        private void UpdateStatusBar()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }

        /// <summary>
        /// Open profile edit dialog.
        /// </summary>
        private void OpenProfileEditDialog(Profile profile)
        {
            EditProfileDialog editProfileDialog = new EditProfileDialog
            {
                Owner = this,
                SelectedProfile = profile,
                UpdateMode = true
            };
            if (editProfileDialog.ShowDialog() == true)
            {
                RefreshProfileList();
            }
        }

        /// <summary>
        /// Open profile new dialog.
        /// </summary>
        private void OpenProfileNewDialog()
        {
            EditProfileDialog editProfileDialog = new EditProfileDialog
            {
                Owner = this,
                SelectedProfile = null,
                UpdateMode = false
            };
            if (editProfileDialog.ShowDialog() == true)
            {
                RefreshProfileList();
            }
        }
        #endregion

        #region Datagrid handlers
        /// <summary>
        /// Data grid delection changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnDataGridSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            Profile profile = (Profile)row.Item;
            OpenProfileEditDialog(profile);
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the profile list.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            RefreshProfileList();
        }

        /// <summary>
        /// Opoen new profile dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnNewProfileButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenProfileNewDialog();
        }

        /// <summary>
        /// Open profile details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnProfileOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Profile profile = (Profile)row.Item;
                    OpenProfileEditDialog(profile);
                    break;
                }
        }

        /// <summary>
        /// Open profile copy dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnProfileCopyButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Profile copyProfile = (Profile)row.Item;
                    copyProfile.Id = 0;
                    copyProfile.Name += " (copy)";
                    DatabaseProfile.InsertProfile(copyProfile);
                    OpenProfileEditDialog(copyProfile);
                    break;
                }
        }

        /// <summary>
        /// Delete selected profile.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void OnProfileDeleteButtonClicked(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow row)
                {
                    Profile profile = (Profile)row.Item;
                    DatabaseProfile.DeleteProfile(profile);
                    ProfileData.Remove(profile);
                    break;
                }
        }

        /// <summary>
        /// Open the help dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnDialogHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelpTopic("nmapwin/profiles/profile-list.htm");
        }

        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
        #endregion

        #region Event handling        
        /// <summary>
        /// Property change event handler
        /// </summary>
        ///
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Property changed notifications
        /// </summary>
        /// <param name="propertyName">property name</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatusBar();
        }
        #endregion
    }
}
