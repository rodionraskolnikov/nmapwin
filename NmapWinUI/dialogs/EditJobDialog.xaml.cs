﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinCommon.Wcf;
using NmapWinUI.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;

namespace NmapWinUI.dialogs
{
    /// <summary>
    /// Interaction logic for EditJobDialog.xaml
    /// </summary>
    public partial class EditJobDialog : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(EditJobDialog));


        /// <summary>
        /// Granularity for the simple trigger.
        /// </summary>
        public static TimeSpan Granularity { get; set; } = TimeSpan.FromMinutes(5);

        /// <summary>
        /// Provider for the HTML tooltips
        /// </summary>
        public TooltipProvider TooltipProvider { get; } = TooltipProvider.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Job database singleton
        /// </summary>
        private readonly DatabaseJob DatabaseJob = DatabaseJob.Instance;

        /// <summary>
        /// Input field validation.
        /// </summary>
        private readonly FieldValidation Validation = FieldValidation.Instance;

        /// <summary>
        /// Collection for the profile combo box
        /// </summary>
        public ObservableCollection<Profile> ProfileData { get; set; }

        /// <summary>
        /// Collection for the job tyoe combo box
        /// </summary>
        public ObservableCollection<JobType> JobTypeData { get; set; }

        /// <summary>
        /// HTTP Proxy for the communication with the service
        /// </summary>
        private ICommandListener httpProxy;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public EditJobDialog()
        {
            InitializeComponent();

            DataContext = this;

            InitializeDialog();

            InitializeCommandListener();
        }

        /// <summary>
        /// Initialize the dialog.
        /// </summary>
        private void InitializeDialog()
        {
            // Fill in profile combo box
            ProfileComboBox.Items.Clear();
            ProfileData = new ObservableCollection<Profile>(DatabaseProfile.FindProfiles());
            ProfileComboBox.ItemsSource = ProfileData;
            SelectedProfile = ProfileData[0];
            logger.Debug(string.Format("Found profiles - count: {0}", ProfileData.Count));

            // Fill in job type combo box
            JobTypeComboBox.ItemsSource = Enum.GetValues(typeof(JobType)).Cast<JobType>(); ;
            SelectedJobType = JobType.Simple;

            // Mininum / maximum for start time
            StartDateTimePicker.Kind = DateTimeKind.Local;
            StartDateTimePicker.Value = DateTimeUtils.LastFullHour();
            StartDateTimePicker.Minimum = DateTimeUtils.LastFullDay();
            StartDateTimePicker.Maximum = DateTime.MaxValue;

            // Mininum / maximum for end time
            EndDateTimePicker.Kind = DateTimeKind.Local;
            EndDateTimePicker.Value = DateTime.MaxValue;
            EndDateTimePicker.Minimum = DateTimeUtils.NextFullDay();
            EndDateTimePicker.Maximum = DateTime.MaxValue;
        }

        /// <summary>
        /// Initialize service communication.
        /// </summary>
        private void InitializeCommandListener()
        {
            ChannelFactory<ICommandListener> httpFactory = new ChannelFactory<ICommandListener>(new BasicHttpBinding(), new EndpointAddress("http://localhost:8000/Command"));
            httpProxy = httpFactory.CreateChannel();
        }

        private void InitializeJob()
        {
            JobNameText = Job.Name;
            JobGroupText = Job.Group;
            TargetText = Job.Target;
            foreach (Profile p in ProfileData)
            {
                if (p.Id == Job.ProfileId)
                {
                    SelectedProfile = p;
                }
            }
            SelectedJobType = Job.Type;
            CronText = Job.Cron;
            StartDateTimePicker.Value = Job.StartTime == 0 ? DateTime.Now : new DateTime(Job.StartTime);
            EndDateTimePicker.Value = Job.EndTime == 0 ? DateTime.MaxValue : new DateTime(Job.EndTime);
            IntervalPicker.Value = TimeSpan.FromMinutes(Job.Interval);
        }
        #endregion

        #region Bindings
        /// <summary>
        /// Script binding
        /// </summary>
        private Job _job;
        public Job Job
        {
            get
            {
                return _job;
            }
            set
            {
                if (value != null)
                {
                    _job = value;
                    if (_isNew)
                    {
                        _job = new Job
                        {
                            Name = "Job1",
                            Group = "NmapWin",
                            Target = "127.0.0.1",
                            Type = JobType.Simple,
                            ProfileId = SelectedProfile.Id,
                            StartTime = StartTime.Ticks,
                            Interval = (int)Interval.TotalMinutes
                        };
                        Title = "New job";
                    }
                    InitializeJob();
                    OnPropertyChanged("Job");
                }
            }
        }

        /// <summary>
        /// New script flag.
        /// </summary>
        private bool _isNew = false;
        public bool IsNew
        {
            get
            {
                return _isNew;
            }
            set
            {
                _isNew = value;
            }
        }

        /// <summary>
        /// Job name text
        /// </summary>
        private string _jobNameText = "Job1";
        public string JobNameText
        {
            get { return _jobNameText; }
            set
            {
                if (_jobNameText == null || !_jobNameText.Equals(value))
                {
                    _jobNameText = value;
                    if (Job != null)
                        Job.Name = value;
                    OnPropertyChanged("JobNameText");
                }
            }
        }

        /// <summary>
        /// Group name text
        /// </summary>
        private string _jobGroupText = "NmapWin";
        public string JobGroupText
        {
            get { return _jobGroupText; }
            set
            {
                if (_jobGroupText == null || !_jobGroupText.Equals(value))
                {
                    _jobGroupText = value;
                    if (Job != null)
                        Job.Group = value;
                    OnPropertyChanged("JobGroupText");
                }
            }
        }

        /// <summary>
        /// Target text
        /// </summary>
        private string _targetText = "127.0.0.1";
        public string TargetText
        {
            get { return _targetText; }
            set
            {
                if (_targetText == null || !_targetText.Equals(value))
                {
                    _targetText = value;
                    if (Job != null)
                        Job.Target = value;
                    OnPropertyChanged("TargetText");
                }
            }
        }

        /// <summary>
        /// Selected profile
        /// </summary>
        private Profile _selectedProfile;
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set
            {
                if (value != null)
                {
                    _selectedProfile = value;
                    if (Job != null)
                        Job.ProfileId = _selectedProfile.Id;
                    if (!string.IsNullOrEmpty(_selectedProfile.Target))
                    {
                        TargetText = _selectedProfile.Target;
                    }
                    OnPropertyChanged("SelectedProfile");
                }
            }
        }

        /// <summary>
        /// Selected job type
        /// </summary>
        private JobType _selectedJobType;
        public JobType SelectedJobType
        {
            get { return _selectedJobType; }
            set
            {
                _selectedJobType = value;
                if (Job != null)
                {
                    Job.Type = _selectedJobType;
                }
                CronText = "0 0 * * * ?";
                OnPropertyChanged("SelectedJobType");
                IsCron = _selectedJobType == JobType.Cron ? Visibility.Visible : Visibility.Hidden;
                IsSimple = _selectedJobType == JobType.Simple ? Visibility.Visible : Visibility.Hidden;
                OnPropertyChanged("IsSimple");
                OnPropertyChanged("IsCron");
            }
        }

        public Visibility IsCron { get; set; }
        public Visibility IsSimple { get; set; }
        public bool IsKeyEditable
        {
            get { return !IsNew; }
        }

        /// <summary>
        /// Cron text
        /// </summary>
        private string _cronText;
        public string CronText
        {
            get { return _cronText; }
            set
            {
                if (value != null && !value.Equals(_cronText))
                {
                    _cronText = value;
                    if (Job != null)
                        Job.Cron = _cronText;
                    OnPropertyChanged("CronText");
                }
            }
        }

        /// <summary>
        /// Job start date/time
        /// </summary>
        private DateTime _startDateTime = DateTimeUtils.NextFullHour();
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set
            {
                if (value != null)
                {
                    _startDateTime = value;
                    if (Job != null)
                        Job.StartTime = _startDateTime.Ticks;
                    OnPropertyChanged("StartDateTime");
                }
            }
        }

        /// <summary>
        /// Job end date/time
        /// </summary>
        private DateTime _endDateTime = DateTime.MaxValue;
        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set
            {
                if (value != null)
                {
                    _endDateTime = value;
                    if (Job != null)
                        Job.EndTime = _endDateTime.Ticks;
                    OnPropertyChanged("EndDateTime");
                }
            }
        }

        /// <summary>
        /// Job start time
        /// </summary>
        private DateTime _startTime = DateTimeUtils.NextFullHour();
        public DateTime StartTime
        {
            get { return _startTime; }
            set
            {
                if (value != null)
                {
                    _startTime = value;
                    OnPropertyChanged("StartTime");
                }
            }
        }

        /// <summary>
        /// Job interval in minutes
        /// </summary>
        private TimeSpan _interval = new TimeSpan(1, 0, 0);
        public TimeSpan Interval
        {
            get { return _interval; }
            set
            {
                if (value != null)
                {
                    _interval = value;
                    if (_interval.TotalHours < 1)
                        _interval = new TimeSpan(1, 0, 0);
                    if (_interval.TotalDays > 1)
                        _interval = new TimeSpan(24, 0, 0);
                }
                OnPropertyChanged("Interval");
            }
        }

        /// <summary>
        /// Can start scan flag.
        /// </summary>
        private bool _canSave = true;
        public bool CanSave
        {
            get
            {
                return _canSave;
            }
            set
            {
                _canSave = value;
                OnPropertyChanged("CanSave");
            }
        }
        #endregion

        #region Button handlers
        /// <summary>
        /// Refresh the scan details dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnRefreshButtonClicked(object sender, RoutedEventArgs e)
        {
            Job = DatabaseJob.FindJobById(Job.Id);
        }

        /// <summary>
        /// Close the job edit dialog.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnCloseButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            logger.Debug(string.Format("Job edit dialog closed - name: {0} id: {1} isNew: {2}", Job.Name, Job.Id, IsNew));
            Close();
        }

        /// <summary>
        /// Save button clicked.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        public void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            if (Job.StartTime < DateTime.Now.Ticks)
            {
                Job.StartTime = DateTimeUtils.RoundUp(new DateTime(Job.StartTime), TimeSpan.FromMinutes(15)).Ticks;
            }
            if (Job.EndTime > 0 && Job.EndTime <= Job.StartTime)
            {
                MessageBox.Show("End time cannnot be before start time!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Job.Cron = CronText;
            if (Job.Id == 0)
            {
                if (DatabaseJob.FindJobByName(Job.Name, Job.Group) != null)
                {
                    MessageBox.Show("Job with this name/group exists already!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                Job j = DatabaseJob.InsertJob(Job);
                AddJobToScheduler(j);
                logger.Debug(string.Format("Job inserted - name: {0} id: {1}", Job.Name, Job.Id));
            }
            else
            {
                Job j = DatabaseJob.UpdateJob(Job);
                UpdateJobFromScheduler(j);
                logger.Debug(string.Format("Script updated - name: {0} id: {1}", Job.Name, Job.Id));
            }
            DialogResult = true;
            Close();
        }
        #endregion

        #region Validation
        /// <summary>
        /// Trigger a validation even when the text box is left empty. WPF does only trigger
        /// the validation when the text box content has been changes. This will also trigger
        /// the validation when the text hasn't been changed and is still left empty.
        /// </summary>
        /// <param name="sender">text box sender</param>
        /// <param name="e">trigger event</param>
        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            ((Control)sender).GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        public string Error { get; set; } = string.Empty;

        public string this[string propertyName]
        {
            get
            {
                string errorString = ValidateProperties(propertyName);
                CanSave = string.IsNullOrEmpty(errorString);
                return errorString;
            }
        }

        private string ValidateProperties(string propertyName)
        {
            switch (propertyName)
            {
                case "JobNameText":
                    return Validation.ValidateJobName("JobNameText", JobNameText, false);
                case "TargetText":
                    return Validation.ValidateTarget("TargetText", TargetText, true);
            }
            return string.Empty;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Adds a job to the service scheduler.
        /// </summary>
        /// <param name="job">job to add</param>
        private void AddJobToScheduler(Job job)
        {
            try
            {
                Job = httpProxy.AddJob(job);
                logger.Debug(string.Format("Job added to scheduler - rc: {0}", job.State));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not add job to NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Updates a job in the service scheduler.
        /// </summary>
        /// <param name="job">job to update</param>
        private void UpdateJobFromScheduler(Job job)
        {
            try
            {
                Job = httpProxy.UpdateJob(job);
                logger.Debug(string.Format("Job updated - rc: {0}", job.State));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not update job in NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Removes a job from the service scheduler.
        /// </summary>
        /// <param name="job">job to remove</param>
        private void RemoveJobFromScheduler(Job job)
        {
            try
            {
                Job = httpProxy.RemoveJob(job);
                logger.Debug(string.Format("Job removed from scheduler - rc: {0}", job.State));
            }
            catch (CommunicationException ex)
            {
                MessageBox.Show("Could not remove job from NmapWinService!" + Environment.NewLine + "Error: " + ex.Message, "NmapWin", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change notification
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #region Private methods        
        /// <summary>
        /// Update the status bar.
        /// </summary>
        private void UpdateStatus()
        {
            statusText.Dispatcher.Invoke(() => statusText.Content = "Last update: " + DateTime.Now.ToString(Constants.TimeFormat));
        }
        #endregion

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            UpdateStatus();
        }
        #endregion
    }
}
