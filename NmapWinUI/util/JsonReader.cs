﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;

namespace NmapWinUI.Utils
{
    public class JsonReader : AbstractProgressDialog
    {
        private static readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        private static readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        private static readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        private static readonly DatabaseScript DatabaseScript = DatabaseScript.Instance;

        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            PreserveReferencesHandling = PreserveReferencesHandling.Objects
        };

        /// <summary>
        /// Read JSON files asynchronously.
        /// </summary>
        public override void AsyncTasks()
        {
            MainProgressBarValue = 0;
            MainProgressBarMaximum = 5;
            DetailedProgressBarValue = 0;
            DetailedProgressBarMaximum = 1;

            MainStatusMessage = "Importing profiles...";
            MainProgressBarValue = 2;
            ReadProfilesFile();

            MainStatusMessage = "Importing scripts...";
            MainProgressBarValue = 2;
            ReadScriptsFile();

            //MainStatusMessage = "Export host file...";
            //MainProgressBarValue = 2;
            //WriteHostFile();

            //MainStatusMessage = "Export options file...";
            //MainProgressBarValue = 3;
            //WriteOptionsFile();

            //MainStatusMessage = "Export profiles file...";
            //MainProgressBarValue = 3;
            //WriteProfilesFile();

            //MainStatusMessage = "Export profiles file...";
            //MainProgressBarValue = 4;
            //WriteScriptsFile();

            MainProgressBarValue = 5;
        }

        /// <summary>
        /// Creates the backup directory
        /// </summary>
        private void EnsureDirectory()
        {
            DetailedStatusMessage = "Creating directory...";
            Directory.CreateDirectory(Constants.BackupDirectory);
            DetailedProgressBarValue = 1;
        }

        /// <summary>
        /// Writes the scan file
        /// </summary>
        private void WriteScanFile()
        {
            DetailedStatusMessage = "Exporting scans...";
            string filename = Path.Combine(Constants.BackupDirectory, "scans-" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".json");

            List<Scan> scans = DatabaseScan.FindScans();
            DetailedProgressBarMaximum = scans.Count;
            string json = JsonConvert.SerializeObject(scans.ToArray(), Formatting.None, jsonSerializerSettings);
            File.WriteAllText(filename, json);
            DetailedProgressBarValue = scans.Count;
        }

        /// <summary>
        /// Writes the host file.
        /// </summary>
        private void WriteHostFile()
        {
            DetailedStatusMessage = "Exporting hosts...";
            string filename = Path.Combine(Constants.BackupDirectory, "hosts-" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".json");

            List<Host> hosts = DatabaseHost.FindHostsFull();
            DetailedProgressBarMaximum = hosts.Count;
            string json = JsonConvert.SerializeObject(hosts.ToArray(), Formatting.None, jsonSerializerSettings);
            File.WriteAllText(filename, json);
            DetailedProgressBarValue = hosts.Count;
        }

        /// <summary>
        /// Writes the options file.
        /// </summary>
        private void WriteOptionsFile()
        {
            DetailedStatusMessage = "Exporting options...";
            string filename = Path.Combine(Constants.BackupDirectory, "options-" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".json");

            List<Option> options = DatabaseOption.Instance.FindOptions();
            DetailedProgressBarMaximum = options.Count;
            string json = JsonConvert.SerializeObject(options.ToArray(), Formatting.None, jsonSerializerSettings);
            File.WriteAllText(filename, json);
            DetailedProgressBarValue = options.Count;
        }

        /// <summary>
        /// Read the profiles file.
        /// </summary>
        private void ReadProfilesFile()
        {
            DetailedProgressBarValue = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "profiles.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Profile> profiles = JsonConvert.DeserializeObject<List<Profile>>(JSONtxt);
                DetailedProgressBarMaximum = profiles.Count;
                profiles.ForEach(p =>
                {
                    p = DatabaseProfile.UpdateProfile(p);
                    DetailedStatusMessage = "Importing profile '" + p.Name + "'";
                    DetailedProgressBarValue++;
                });
            }
        }

        /// <summary>
        /// Read scripts file.
        /// </summary>
        private void ReadScriptsFile()
        {
            DetailedProgressBarValue = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "scripts.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Script> scripts = JsonConvert.DeserializeObject<List<Script>>(JSONtxt);
                DetailedProgressBarMaximum = scripts.Count;
                scripts.ForEach(s =>
                {
                    s = DatabaseScript.UpdateScript(s);
                    DetailedStatusMessage = "Importing script '" + s.Name + "'";
                    DetailedProgressBarValue++;
                });
            }
        }
    }
}
