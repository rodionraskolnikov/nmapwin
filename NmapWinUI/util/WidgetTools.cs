﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace NmapWinUI.util
{
    public static class WidgetTools
    {
        public static void ReplaceWith(this FrameworkElement elementToReplace, FrameworkElement newControl)
        {
            newControl.Width = elementToReplace.Width;
            newControl.Height = elementToReplace.Height;
            newControl.Margin = elementToReplace.Margin;

            // get parent of control
            var parent = elementToReplace.Parent;

            if (parent is Panel)
            {
                var panel = (Panel)parent;

                for (var i = 0; i < panel.Children.Count; i++)
                {
                    if (panel.Children[i] == elementToReplace)
                    {
                        panel.Children.RemoveAt(i);
                        panel.Children.Insert(i, newControl);
                        break;
                    }
                }
            }
            else if (parent is Decorator)
            {
                ((Decorator)parent).Child = newControl;
            }
            else if (parent is ContentControl)
            {
                ((ContentControl)parent).Content = newControl;
            }
            else
            {
                if (Debugger.IsAttached)
                    Debugger.Break();

                throw new NotImplementedException("Missing other possibilities to implement");
            }
        }

        public static void AddWidget(FrameworkElement parent, FrameworkElement newControl)
        {
            if (parent is Panel)
            {
                var panel = (Panel)parent;
                panel.Children.Add(newControl);
            }
            else
            {
                if (Debugger.IsAttached)
                    Debugger.Break();

                throw new NotImplementedException("Missing other possibilities to implement");
            }
        }

        public static void RemoveWidget(FrameworkElement parent, FrameworkElement oldControl)
        {
            if (parent is Panel)
            {
                var panel = (Panel)parent;

                for (var i = 0; i < panel.Children.Count; i++)
                {
                    if (panel.Children[i] == oldControl)
                    {
                        panel.Children.RemoveAt(i);
                        break;
                    }
                }
            }
            else
            {
                if (Debugger.IsAttached)
                    Debugger.Break();

                throw new NotImplementedException("Missing other possibilities to implement");
            }
        }
    }
}
