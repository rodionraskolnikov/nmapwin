﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinUI.Dialogs;
using System.Xml;

namespace NmapWinUI.Utils
{
    public class ReparseXml : AbstractSingleProgressDialog
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ReparseXml));

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Scan to process
        /// </summary>
        private Scan Scan = null;

        /// <summary>
        /// XML output parser.
        /// </summary>
        private readonly XmlParser XmlParser = XmlParser.Instance;
        #endregion

        #region Constructor
        public ReparseXml(Scan scan)
        {
            Scan = scan;
            Title = "Reparse XML output";
        }
        #endregion

        #region Asynchronous task
        /// <summary>
        /// Asynchronous task
        /// </summary>
        public override void AsyncTasks()
        {
            if (Scan.OutputXml == null)
            {
                logger.ErrorFormat("XML output missing - scanId: {0}", Scan.Id);
                return;
            }

            logger.DebugFormat("Starting offline XML parsing - scanId: {0}", Scan.Id);

            // Load XML document
            XmlDocument document = new XmlDocument();
            document.LoadXml(Scan.OutputXml);

            // Get nmap run
            XmlNode xmlNmapRun = document.SelectSingleNode("nmaprun");
            Scan.Version = xmlNmapRun.Attributes.GetNamedItem("version").Value;
            Scan.XmlOutputVersion = xmlNmapRun.Attributes.GetNamedItem("xmloutputversion").Value;

            // Get verbose/debug level
            XmlNode xmlVerbose = document.SelectSingleNode("/nmaprun/verbose");
            Scan.VerbosityLevel = int.Parse(xmlVerbose.Attributes.GetNamedItem("level").Value);
            XmlNode xmlDebug = document.SelectSingleNode("/nmaprun/debugging");
            Scan.VerbosityLevel = int.Parse(xmlDebug.Attributes.GetNamedItem("level").Value);

            // Update scan in database
            Scan = DatabaseScan.UpdateScan(Scan);
            logger.DebugFormat("Converted XML scan - scan: {0}", Scan.Id);

            XmlNodeList hostList = document.SelectNodes("descendant::host");
            SetMainValues("Parsing hosts....", hostList.Count, 0);
            logger.DebugFormat("Found hosts - count: {0}", Scan.Id, hostList.Count);

            foreach (XmlNode host in hostList)
            {
                Host h = XmlParser.AddHost(Scan, host);
                if (h != null)
                {
                    IncProgress("Host '" + h.Identifier + "' added...");
                }
                else
                {
                    IncProgress("Host skipped...");
                }
            }
            logger.DebugFormat("Finished offline XML parsing - scanId: {0} hosts: {1}", Scan.Id, hostList.Count);
        }
        #endregion
    }
}
