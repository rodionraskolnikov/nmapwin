﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using Newtonsoft.Json;
using NmapWinUI.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;

namespace NmapWinUI.Utils
{
    /// <summary>
    /// Checks for new updates and installs them.
    /// </summary>
    public class CheckForUpdates : AbstractProgressDialog
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(CheckForUpdates));

        /// <summary>
        /// Gitlab base URL
        /// </summary>
        private static readonly string URL = "https://gitlab.com/";
        private static readonly string TOKEN = "zJJ9QkxWULA_5Sihjvzo";

        /// <summary>
        /// JSON deserializer settings
        /// </summary>
        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            PreserveReferencesHandling = PreserveReferencesHandling.Objects
        };

        /// <summary>
        /// Package list
        /// </summary>
        private static List<Package> packages;

        /// <summary>
        /// Package file list
        /// </summary>
        private static List<PackageFile> packageFiles;
        #endregion

        /// <summary>
        /// Asynchronous task, which checks for a new version and starts the installation of the
        /// new version.
        /// </summary>
        public override void AsyncTasks()
        {
            SetMainValues("Checking for updates...", 4, 1);
            SetMainValues("", 3, 0);

            // Download package list
            GetPackageList().Wait();

            IncMainValues("Checking versions...");

            // Check versions
            if (packages.Count > 0)
            {
                string architecture = ApplicationVersion.GetArchitecture().ToLower();
                Package latest = GetLatestPackage(architecture);
                if (latest == null || !ApplicationVersion.CheckVersion(latest.version))
                {
                    logger.InfoFormat("Current version is up to date - version: {0}", ApplicationVersion.Version);
                    MessageBox.Show("NmapWin is up to date!", "NmapWin", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else
                {
                    logger.InfoFormat("New package version available - version: {0} architecture: {1}", latest.version, architecture);
                    MessageBoxResult result = MessageBox.Show("A new version is available (v" + latest.version + "-" + architecture + "). Shall I install the new version?", "NmapWin", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            UpdateVersion(latest);
                            break;
                        case MessageBoxResult.No:
                            return;
                        case MessageBoxResult.Cancel:
                            return;
                    }
                }
            }
        }

        /// <summary>
        /// Get the latest package for the current architecture.
        /// </summary>
        /// <returns>latest package or null</returns>
        private Package GetLatestPackage(string architecture)
        {
            return packages.Where(x => x.name.Contains(architecture)).Where(x => x.id == packages.Max(y => y.id)).FirstOrDefault();
        }

        /// <summary>
        /// Get the current package list.
        /// </summary>
        /// <returns>list of available packages.</returns>
        static async Task GetPackageList()
        {
            using var client = new HttpClient
            {
                BaseAddress = new Uri(URL)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", TOKEN);

            // HTTP GET
            HttpResponseMessage response = await client.GetAsync("api/v4/projects/22461593/packages");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                packages = JsonConvert.DeserializeObject<List<Package>>(content, jsonSerializerSettings);
                logger.InfoFormat("Got package list - count: {0}", packages.Count);
                packages.ForEach(p => logger.DebugFormat("Found package - name: {0} version: {1}", p.name, p.version));
            }
        }

        /// <summary>
        /// A new versino is available, so install it.
        /// </summary>
        /// <param name="latest">latest package</param>
        private void UpdateVersion(Package latest)
        {
            IncMainValues("Installing new version...");
            IncDetailedValues("Downloading file...");

            string fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".zip";
            DownloadVersion(latest, fileName).Wait();
            logger.DebugFormat("New version downloaded - name: {0} version: {1} temp: {2}", latest.name, latest.version, fileName);

            //IncDetailedValues("Extracting file...");
            //fileName = UnzipFile(fileName, Path.GetTempPath());
            //logger.DebugFormat("Downloaded file unzipped - path: {0}", fileName);

            IncDetailedValues("Installing package...");
            InstallPackage(fileName);
        }

        /// <summary>
        /// Download the latest versino of the nmapwin package.
        /// </summary>
        /// <param name="latest"></param>
        /// <param name="fileName"></param>
        /// <returns>download task</returns>
        static async Task DownloadVersion(Package latest, string fileName)
        {
            WebClient webClient = new WebClient
            {
                BaseAddress = URL
            };
            webClient.Headers.Add("PRIVATE-TOKEN: " + TOKEN);

            // Download package file list
            GetPackageFileList(latest).Wait();
            if (packageFiles.Count > 0)
            {
                PackageFile latestFile = packageFiles.Where(x => x.id == packageFiles.Max(y => y.id)).First();
                string downloadUrl = URL + "api/v4/projects/22461593/packages/generic/" + latest.name + "/" + latest.version + "/" + latestFile.file_name;
                logger.InfoFormat("Got download URL - path: {0}", downloadUrl);

                // HTTP GET
                await webClient.DownloadFileTaskAsync(new Uri(downloadUrl), fileName);
                logger.InfoFormat("Package downloaded - file: {0}", fileName);
            }
        }

        /// <summary>
        /// Get the file list of the latest package.
        /// </summary>
        /// <param name="package">package</param>
        /// <returns>package list task</returns>
        static async Task GetPackageFileList(Package package)
        {
            using var client = new HttpClient
            {
                BaseAddress = new Uri(URL)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", TOKEN);

            // HTTP GET
            HttpResponseMessage response = await client.GetAsync("api/v4/projects/22461593/packages/" + package.id + "/package_files");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                packageFiles = JsonConvert.DeserializeObject<List<PackageFile>>(content, jsonSerializerSettings);
                packageFiles.ForEach(p => logger.InfoFormat("Found package - name: {0}", p.file_name));
            }
        }

        /// <summary>
        /// Returns the installation executable.
        /// </summary>
        /// <param name="directoryName">extraction directory</param>
        /// <returns></returns>
        private static string FindFile(string directoryName)
        {
            return Directory.GetFiles(directoryName, "nmapwin-*.exe").First();
        }

        /// <summary>
        /// Delete all previoud installation files.
        /// </summary>
        /// <param name="directoryName">name of the extraction directory</param>
        private static void DeleteFiles(string directoryName)
        {
            string[] files = Directory.GetFiles(directoryName, "nmapwin-*.exe");
            foreach (string file in files)
            {
                File.Delete(file);
                logger.DebugFormat("File deleted - file: {0}", file);
            }
            logger.InfoFormat("Previous files deleted - count: {0}", files.Length);
        }

        /// <summary>
        /// Install the new package.
        /// </summary>
        /// <remarks>the msiexec will restart the application</remarks>
        /// <param name="fileName"></param>
        private void InstallPackage(string fileName)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("msiexec")
            {
                Arguments = "/passive /i " + fileName,
                WorkingDirectory = Path.GetTempPath(),
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            // Start the process.
            using Process installProcess = Process.Start(startInfo);
            logger.InfoFormat("Installation started - file: {0}", fileName);

            installProcess.WaitForExit();
        }

        /// <summary>
        /// Gitlab package structure.
        /// </summary>
        private class Package
        {
            public int id { get; set; }

            public string name { get; set; }

            public string version { get; set; }

            public Link _links { get; set; }
        }

        /// <summary>
        /// Gitlab link structure.
        /// </summary>
        private class Link
        {
            public string web_path { get; set; }
        }

        /// <summary>
        /// Gitlab package file structure.
        /// </summary>
        private class PackageFile
        {
            public int id { get; set; }

            public int package_id { get; set; }

            public string file_name { get; set; }
        }
    }
}
