﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using NmapWinCommon.Worker;
using NmapWinUI.Dialogs;
using System.IO;
using System.Xml;

namespace NmapWinUI.Utils
{
    public class XmlReader : AbstractSingleProgressDialog
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ReparseXml));

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// XML output parser.
        /// </summary>
        private readonly XmlParser XmlParser = XmlParser.Instance;

        /// <summary>
        /// XML file name.
        /// </summary>
        public string FileName { get; set; }
        #endregion

        /// <summary>
        /// Asynchronous reading of XML file.
        /// </summary>
        public override void AsyncTasks()
        {
            if (File.Exists(FileName))
            {
                logger.Debug(string.Format("Starting import of XML file - path: {0}", FileName));

                string xmlText = File.ReadAllText(@FileName);

                Scan scan = new Scan();

                XmlDocument document = new XmlDocument();
                document.LoadXml(xmlText);

                scan = GetHeader(document, scan);

                XmlNodeList hostList = document.SelectNodes("descendant::host");
                SetMainValues("Parsing hosts....", hostList.Count, 0);
                logger.Debug(string.Format("Found hosts - count: {0}", scan.Id, hostList.Count));

                foreach (XmlNode xmlHost in hostList)
                {
                    Host host = XmlParser.AddHost(scan, xmlHost);
                    if (host != null)
                    {
                        IncProgress("Host '" + host.Identifier + "' added...");
                    }
                }
                logger.Debug(string.Format("Finished offline XML parsing - scanId: {0} hosts: {1}", scan.Id, hostList.Count));
            }
        }

        private Scan GetHeader(XmlDocument document, Scan scan)
        {
            // From header
            XmlNode xmlNmapRun = document.SelectSingleNode("nmaprun");
            scan.StartTime = DateTimeUtils.UnixTimeToTicks(int.Parse(xmlNmapRun.Attributes.GetNamedItem("start").Value));
            scan.Version = xmlNmapRun.Attributes.GetNamedItem("version").Value;
            scan.ProcessName = xmlNmapRun.Attributes.GetNamedItem("scanner").Value;
            scan.XmlOutputVersion = xmlNmapRun.Attributes.GetNamedItem("xmloutputversion").Value;
            string args = xmlNmapRun.Attributes.GetNamedItem("args").Value;

            // From footer
            XmlNode xmlNmapStats = document.SelectSingleNode("//runstats/finished");
            scan.Name = "Imported from XML";
            scan.ProfileId = -1;
            scan.EndTime = DateTimeUtils.UnixTimeToTicks(int.Parse(xmlNmapStats.Attributes.GetNamedItem("time").Value));
            scan.LastUpdate = scan.EndTime;
            scan.Uptime = scan.EndTime - scan.StartTime;
            scan.Status = NmapWorkerStatus.COMPLETED;

            // Debug / verbosity level
            XmlNode xmlDebugLevel = document.SelectSingleNode("debugging");
            scan.DebugLevel = int.Parse(xmlDebugLevel.Attributes.GetNamedItem("level").Value);
            XmlNode xmlVerboseLevel = document.SelectSingleNode("verbose");
            scan.VerbosityLevel = int.Parse(xmlVerboseLevel.Attributes.GetNamedItem("level").Value);

            scan = DatabaseScan.InsertScan(scan);
            return scan;
        }
    }
}
