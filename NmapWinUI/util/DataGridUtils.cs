﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;

namespace NmapWinUI.Utils
{
    public class DataGridUtils
    {
        /// <summary>
        /// Keep the sort order during updates.
        /// </summary>
        /// <param name="dataGrid">data grid to sort</param>
        /// <param name="column">column definition</param>
        /// <param name="sortDirection">column sort direction</param>
        public static void SortDataGrid(DataGrid dataGrid, List<SortDescription> sortDescriptions)
        {
            sortDescriptions.ForEach(s =>
            {
                DataGridColumn column = dataGrid.Columns.FirstOrDefault(c => c.SortMemberPath == s.PropertyName);

                // Clear current sort descriptions
                dataGrid.Items.SortDescriptions.Clear();

                // Add the new sort description
                dataGrid.Items.SortDescriptions.Add(new SortDescription(column.SortMemberPath, s.Direction));

                // Apply sort
                foreach (var col in dataGrid.Columns)
                {
                    col.SortDirection = null;
                }
                column.SortDirection = s.Direction;

                // Refresh items to display sort
                dataGrid.Items.Refresh();
            });
        }

        /// <summary>
        /// Scrolls to the end of the list
        /// </summary>
        /// <param name="dataGrid">data grid to scroll</param>
        /// <param name="isScrollToEnd">scroll to end flag</param>
        public static void ScrollToEnd(DataGrid dataGrid, bool isScrollToEnd)
        {
            if (dataGrid.Items.Count < 2) { 
                return;
            }

            if (isScrollToEnd)
            {
                dataGrid.ScrollIntoView(dataGrid.Items[dataGrid.Items.Count - 1]);
            }
        }
    }
}
