﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NmapWinUI.Utils
{
    public class WriteHelpFiles : AbstractProgressDialog
    {
        private DatabaseScript DatabaseScript = DatabaseScript.Instance;

        private static readonly string dirName = Path.Combine(Constants.NmapHelpDirectory, "scripts");

        private static readonly string Header = "<html><head><style>body {font-family: verdana;font-size: 0.7em;} tr {font-family: verdana;font-size: 0.7em;}</style></head><body>";

        public override void AsyncTasks()
        {
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }

            SetMainValues("Writing Help Files...", 4, 1);

            List<Script> scripts = DatabaseScript.FindScripts();
            SetDetailedValues("Writing Help Files...", scripts.Count, 0);

            scripts.ForEach(s =>
            {
                IncDetailedValues("Writing File '" + s.Name + "'...");
                WriteHelpFile(s);
            });

            // Write content file
            SetMainValues("Writing Content Files...", 2);
            WriteContentTableEntry();

            // Write scripts backup file
            SetMainValues("Writing Backup...", 3);
            JsonWriter.WriteScriptsFileData();
        }

        private void WriteHelpFile(Script script)
        {
            string fileName = Path.Combine(dirName, script.Name + ".html");
            using (StreamWriter sw = File.CreateText(fileName))
            {
                sw.WriteLine(Header);
                sw.WriteLine("<h3>File " + script.Name + "</h3>");
                sw.WriteLine("<b>Script types:</b> " + string.Join(",", script.Types.Select(x => x.Type.ToString().ToLower())) + "<br/>");
                sw.WriteLine("<b>Categories:</b> " + string.Join(",", script.Categories.Select(x => x.Category.ToString().ToLower())) + "<br/>");
                sw.WriteLine("<b>Author:</b> " + script.Author + "<br/>");
                sw.WriteLine("<b>License:</b> <a href=\"" + script.License + "\" target=_blank>" + script.License + "</a><br/>");
                sw.WriteLine("<b>Download:</b> <a href=\"" + script.Html + "\">" + script.Html + "</a><br/>");
                sw.WriteLine("<h3>Summary</h3>");
                sw.WriteLine(script.Description);
                sw.WriteLine("<h3>Arguments</h3>");
                if (script.Arguments.Count > 0)
                {
                    sw.WriteLine("<table>");
                    script.Arguments.FindAll(a => !string.IsNullOrEmpty(a.Name)).ForEach(a => sw.WriteLine("<tr><td>" + a.Name + ":</td><td>" + a.Description + "</td></tr>"));
                    sw.WriteLine("</table>");
                }
                else
                {
                    sw.WriteLine("None");
                }
                sw.WriteLine("<h3>Usage</h3>");
                sw.WriteLine("<pre>" + HttpUtility.HtmlEncode(script.Example) + "</pre>");
                sw.WriteLine("<h3>Output</h3>");
                sw.WriteLine("<pre>" + HttpUtility.HtmlEncode(script.Output) + "</pre>");
                sw.WriteLine("<h3>Requirements</h3>");
                if (script.Requires != null)
                {
                    sw.WriteLine("<ul>");
                    if (script.Requires.Contains(','))
                    {
                        string[] requires = script.Requires.Split(',');
                        foreach (string require in requires)
                        {
                            sw.WriteLine("<li><a href=\"nmap/libraries/" + require + ".htm\">" + require + "</a></li>");
                        }
                    }
                    else
                    {
                        string[] requires = script.Requires.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                        foreach (string require in requires)
                        {
                            sw.WriteLine("<li><a href=\"nmap/libraries/" + require + ".htm\">" + require + "</a></li>");
                        }
                        script.Requires = string.Join(",", requires);
                        DatabaseScript.UpdateScript(script);
                    }
                }
                sw.WriteLine("</ul>");
                sw.WriteLine("<h3>Code</h3>");
                sw.WriteLine("<pre>" + HttpUtility.HtmlEncode(script.Code) + "</pre>");
                sw.Close();
            }
        }

        private void WriteContentTableEntry()
        {
            string fileName = Path.Combine(dirName, "_aaa_content.xml");
            string[] files = Directory.GetFiles(dirName, "*.htm*");
            using StreamWriter sw = File.CreateText(fileName);
            List<Script> scripts = DatabaseScript.FindScripts();
            scripts.ForEach(s =>
            {
                IncDetailedValues("Writing content entry '" + s.Name + "'...");
                sw.WriteLine("				<LI><OBJECT type=\"text/sitemap\">");
                sw.WriteLine("				    <param name=\"Name\" value=\"File " + s.Name + ".html\">");
                sw.WriteLine("				    <param name=\"Local\" value=\"nmap/scripts/" + s.Name + ".html\">");
                sw.WriteLine("				    </OBJECT>");
            });
            sw.Close();
        }
    }
}
