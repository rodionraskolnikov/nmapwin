﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Database;
using NmapWinCommon.Utils;
using NmapWinUI.Dialogs;
using System;
using System.ComponentModel;

namespace NmapWinUI.Utils
{
    public class ReloadDefaults : AbstractProgressDialog
    {
        public override void AsyncTasks()
        {
            DefaultObjects defaultObjects = new DefaultObjects();
            defaultObjects.DatabaseEvent += HandleDatabaseChanges;
            defaultObjects.ReloadDefaults();
        }

        #region Event handling
        private void HandleDatabaseChanges(object sender, EventArgs e)
        {
            ProgressEventArgs args = e as ProgressEventArgs;
            MainStatusLabel.Dispatcher.Invoke(() =>
            {

                MainStatusLabel.Content = args.MainMessage ?? MainStatusLabel.Content;
                MainProgressBar.Maximum = args.MainMaximum > 0 ? args.MainMaximum : MainProgressBar.Maximum;
                MainProgressBar.Value = args.MainProgress > 0 ? args.MainProgress : MainProgressBar.Value;
                DetailedStatusLabel.Content = args.DetailedMessage ?? DetailedStatusLabel.Content;
                DetailedProgressBar.Maximum = args.DetailedMaximum > 0 ? args.DetailedMaximum : DetailedProgressBar.Maximum;
                DetailedProgressBar.Value = args.DetailedProgress > 0 ? args.DetailedProgress : DetailedProgressBar.Value;
                CounterLabel.Content = args.DetailedProgress > 0 ? "(" + args.DetailedProgress + "/" + args.DetailedMaximum + ")" : "";
            });
        }
        #endregion
    }
}
