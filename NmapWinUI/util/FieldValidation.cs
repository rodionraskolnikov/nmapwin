﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace NmapWinCommon.Utils
{
    public class FieldValidation
    {
        private static readonly string NetworkPattern = @"(\d{1,3}(?:\.\d{1,3}(-\d{1,3})?){2}\.(\d{1,3}(-\d{1,3})?))(?:(?:-|\s+to\s+)(\d{1,3}(?![\d\.]))|(?:-|\s*to\s+)(\d{1,3}(?:\.\d{1,3}){3})|\s+(25\d(?:\.\d{1,3}){3})|\s*/(\d{1,3}))?";

        private static readonly string HostnamePattern = @"^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]?)$";

        private static readonly string PortListPattern = @"((\d+(-\d+)?),?)+";

        private static readonly string PortRangePattern = @"([UT:]?(\d+(-\d+)?)+)";

        private static readonly string ProtocolListPattern = @"(\d+,?)+";

        private static readonly string InterfacePattern = @"^[a-zA-Z0-9-_]+$";

        private static readonly string IntegerPattern = @"^\d+$";

        private static readonly string ScriptNamePattern = @"^[a-zA-Z0-9-_]+$";

        private static readonly string JobNamePattern = @"^[a-zA-Z0-9-_]+$";

        //private static readonly string ScriptAuthorPattern = @"^[a-zA-Z0-9-_@<>\,\ ]+$";

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<FieldValidation> lazy = new Lazy<FieldValidation>(() => new FieldValidation());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static FieldValidation Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public FieldValidation()
        {
        }
        #endregion

        /// <summary>
        /// Get the error file name or name of the not found page.
        /// </summary>
        /// <param name="target">target file name</param>
        /// <returns>file name of the error file or not-found.html</returns>
        private string GetError(string target)
        {
            return header + errors[target] + footer;
        }

        /// <summary>
        /// Get the error file name or name of the not found page.
        /// </summary>
        /// <param name="target">target file name</param>
        /// <returns>file name of the error file or not-found.html</returns>
        public string GetError(string target, string extra, string fieldName, string type)
        {
            return GetError(target).Replace("$field", fieldName).Replace("$type", type).Replace("$extra", extra);
        }

        /// <summary>
        /// Validate a file name input field
        /// </summary>
        /// <param name="fieldName">name of the input field</param>
        /// <param name="fileName">file name input field text</param>
        /// <returns>error string</returns>
        public string ValidateFile(string fieldName, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "File name field");
            }
            if (fileName.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                return GetError("file-name").Replace("$field", fieldName).Replace("$type", "File name field");
            }
            return string.Empty;
        }

        /// <summary>
        /// Validate a directory name input field
        /// </summary>
        /// <param name="fieldName">name of the input field</param>
        /// <param name="dirName">directory name input field text</param>
        /// <returns>error string</returns>
        public string ValidateDirectory(string fieldName, string dirName)
        {
            if (string.IsNullOrEmpty(dirName))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Directory name field");
            }
            if (dirName.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                return GetError("directory-name").Replace("$field", fieldName).Replace("$type", "Directory name field");
            }
            return string.Empty;
        }

        /// <summary>
        /// Validate a target input field.
        /// </summary>
        /// <param name="target">target input field text</param>
        /// <param name="canBeNull">target can be empty or null</param>
        /// <returns>error string</returns>
        public string ValidateTarget(string fieldName, string target, bool canBeNull)
        {
            if (target.Equals("Random"))
            {
                return string.Empty;
            }
            if (canBeNull && string.IsNullOrEmpty(target))
            {
                return string.Empty;
            }
            else if (string.IsNullOrEmpty(target))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Network field");
            }
            else
            {
                if (!ValidateNetworkFields(target))
                {
                    return GetError("target").Replace("$field", fieldName).Replace("$type", "Network field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a command input field.
        /// </summary>
        /// <param name="command">command input field text</param>
        /// <returns>error string</returns>
        public string ValidateCommand(string fieldName, string command)
        {
            if (string.IsNullOrEmpty(command))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Text field");
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a source IP input field.
        /// </summary>
        /// <param name="sourceIp">source IP input field text</param>
        /// <returns>error string</returns>
        public string ValidateSourceIp(string fieldName, string sourceIp)
        {
            if (string.IsNullOrEmpty(sourceIp))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Network field");
            }
            else
            {
                if (!ValidateNetworkField(sourceIp))
                {
                    return GetError("target").Replace("$field", fieldName).Replace("$type", "Network field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a source port input field.
        /// </summary>
        /// <param name="sourceIp">source port input field text</param>
        /// <returns>error string</returns>
        public string ValidateSourcePort(string fieldName, string sourceIp)
        {
            if (string.IsNullOrEmpty(sourceIp))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Network field");
            }
            else
            {
                if (!ValidateInteger(sourceIp))
                {
                    return GetError("port").Replace("$field", fieldName).Replace("$type", "Network field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a random count input field.
        /// </summary>
        /// <param name="randomCount">random count input field text</param>
        /// <returns>error string</returns>
        public string ValidateRandomCount(string fieldName, string randomCount)
        {
            if (string.IsNullOrEmpty(randomCount))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Network field");
            }
            else
            {
                if (!ValidateInteger(randomCount))
                {
                    return GetError("port").Replace("$field", fieldName).Replace("$type", "Network field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a source port input field.
        /// </summary>
        /// <param name="sourceIp">source port input field text</param>
        /// <returns>error string</returns>
        public string ValidateInterface(string fieldName, string sourceIp)
        {
            if (string.IsNullOrEmpty(sourceIp))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Interface field");
            }
            else
            {
                if (!ValidateInterface(sourceIp))
                {
                    return GetError("interface").Replace("$field", fieldName).Replace("$type", "Interface field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a timeout input field.
        /// </summary>
        /// <param name="timeout">timeout input field text</param>
        /// <returns>error string</returns>
        public string ValidateTimeout(string fieldName, string timeout)
        {
            if (string.IsNullOrEmpty(timeout))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Timeout field");
            }
            else
            {
                if (!ValidateInteger(timeout))
                {
                    return GetError("timeout").Replace("$field", fieldName).Replace("$type", "Timeout field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate a target input field.
        /// </summary>
        /// <param name="target">target input field text</param>
        /// <param name="canBeNull">target can be empty or null</param>
        /// <returns>error string</returns>
        public string ValidateDecoyList(string fieldName, string target)
        {
            if (string.IsNullOrEmpty(target))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Network field");
            }
            else
            {
                if (!ValidateDecoys(target))
                {
                    return GetError("decoys").Replace("$field", fieldName).Replace("$type", "Network field"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Validate the idle scan field (-sI <zombie host[:probeport]>: Idle scan).
        /// </summary>
        /// <param name="value">idle scan input field text</param>
        /// <returns>error string</returns>
        public string ValidateIdleScan(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return GetError("empty").Replace("$field", "Idle Scan").Replace("$type", "Text field");
            }

            string[] parts = value.Split(':');
            string host = parts[0];
            string port = parts.Length == 2 ? parts[1] : null;

            if (!ValidateNetworkField(host))
            {
                return GetError("idle-scan");
            }
            if (port != null)
            {
                if (!ValidateInteger(port))
                {
                    return GetError("idle-scan");
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Validate the ftp bounce (-b <FTP relay host>: FTP bounce scan).
        /// </summary>
        /// <param name="value">idle scan input field text</param>
        /// <returns>error string</returns>
        public string ValidateFtpBounce(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return GetError("empty").Replace("$field", "FTP Bounce").Replace("$type", "Text field");
            }

            Regex regex = new Regex(NetworkPattern);
            if (!regex.IsMatch(value))
            {
                return GetError("ftp-bounce");
            }
            return String.Empty;
        }

        /// <summary>
        /// Validate the port list
        /// </summary>
        /// Example: 22-25,80,8080,9000-9800
        /// <param name="value">port list to validate</param>
        /// <returns>error string</returns>
        public string ValidatePortList(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Text field");
            }

            Regex regex = new Regex(PortListPattern);
            if (!regex.IsMatch(value))
            {
                return GetError("port-list").Replace("$field", fieldName).Replace("$type", "Text field");
            }
            return String.Empty;
        }

        /// <summary>
        /// Validate the port range
        /// </summary>
        /// Example: 22-25,80,8080,9000-9800
        /// <param name="value">port list to validate</param>
        /// <returns>error string</returns>
        public string ValidatePortRanges(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Text field");
            }
            bool result = true;
            if (value.Contains(","))
            {
                List<string> parts = value.Split(',').ToList();
                parts.ForEach(p => result &= ValidatePortRange(p));
            }
            else
            {
                result = ValidatePortRange(value);
            }
            if (!result)
            {
                return GetError("port-range").Replace("$field", fieldName).Replace("$type", "Text field");
            }
            return string.Empty;
        }

        /// <summary>
        /// Validates a single port range.
        /// </summary>
        /// <param name="value">port range value to validate</param>
        /// <returns>validation result</returns>
        private bool ValidatePortRange(string value)
        {
            Regex regex = new Regex(PortRangePattern);
            if (!regex.IsMatch(value))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validates a single port range.
        /// </summary>
        /// <param name="value">port range value to validate</param>
        /// <returns>validation result</returns>
        private bool ValidateInterface(string value)
        {
            Regex regex = new Regex(InterfacePattern);
            if (!regex.IsMatch(value))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validate the protocol list
        /// </summary>
        /// Example: 2,17,4
        /// <param name="value">protocol list to validate</param>
        /// <returns>error string</returns>
        public string ValidateProtocolList(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Text field");
            }

            Regex regex = new Regex(ProtocolListPattern);
            if (!regex.IsMatch(value))
            {
                return GetError("protocol-list").Replace("$field", fieldName).Replace("$type", "Text field");
            }
            return String.Empty;
        }

        /// <summary>
        /// Network field validation.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>error string</returns>
        private bool ValidateNetworkFields(string value)
        {
            bool result = true;
            if (value.Contains(","))
            {
                List<string> parts = value.Split(',').ToList();
                parts.ForEach(p => result &= ValidateNetworkField(p));
                return result;
            }
            return ValidateNetworkField(value);
        }

        /// <summary>
        /// Validate a single network address / host name
        /// </summary>
        /// <param name="value">value to validate</param>
        /// <returns>validation success</returns>
        private bool ValidateNetworkField(string value)
        {
            Regex regex1 = new Regex(NetworkPattern);
            Regex regex2 = new Regex(HostnamePattern);
            if (!regex1.IsMatch(value) && !regex2.IsMatch(value))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Network field validation.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>error string</returns>
        private bool ValidateDecoys(string value)
        {
            bool result = true;
            if (value.Contains(","))
            {
                List<string> parts = value.Split(',').ToList();
                parts.ForEach(p => result &= ValidateDecoy(p));
                return result;
            }
            return ValidateDecoy(value);
        }

        /// <summary>
        /// Validate a single network address / host name
        /// </summary>
        /// <param name="value">value to validate</param>
        /// <returns>validation success</returns>
        private bool ValidateDecoy(string value)
        {
            Regex regex1 = new Regex(NetworkPattern);
            Regex regex2 = new Regex(HostnamePattern);
            if (!regex1.IsMatch(value) && !regex2.IsMatch(value))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Generic integer field validation.
        /// </summary>
        /// <param name="value">value of the integer input field</param>
        /// <returns>error string</returns>
        private bool ValidateInteger(string value)
        {
            Regex regex = new Regex(IntegerPattern);
            if (!regex.IsMatch(value))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Script name validation
        /// </summary>
        /// <param name="value">value of the script name</param>
        /// <returns>error string</returns>
        public string ValidateScriptName(string fieldName, string target, bool canBeNull)
        {
            if (canBeNull && String.IsNullOrEmpty(target))
            {
                return string.Empty;
            }
            else if (string.IsNullOrEmpty(target))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Script Name");
            }
            else
            {
                Regex regex = new Regex(ScriptNamePattern);
                if (!regex.IsMatch(target))
                {
                    return GetError("script-name").Replace("$field", fieldName).Replace("$type", "Script Name"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Script author validation
        /// </summary>
        /// <param name="value">value of the script author</param>
        /// <returns>error string</returns>
        public string ValidateScriptAuthor(string fieldName, string target)
        {
            if (string.IsNullOrEmpty(target))
            {
                return string.Empty;
            }
            else
            {
                /*   Regex regex = new Regex(ScriptAuthorPattern);
                   if (!regex.IsMatch(target))
                   {
                       return GetFileName("script-author").Replace("$field", fieldName).Replace("$type", "Script Name"); ;
                   }*/
                return string.Empty;
            }
        }

        /// <summary>
        /// Script author validation
        /// </summary>
        /// <param name="value">value of the script author</param>
        /// <returns>error string</returns>
        public string ValidateUrl(string fieldName, string target)
        {
            if (string.IsNullOrEmpty(target))
            {
                return string.Empty;
            }
            else
            {
                if (!Uri.IsWellFormedUriString(target, UriKind.RelativeOrAbsolute))
                {
                    return GetError("script-license").Replace("$field", fieldName).Replace("$type", "Script License"); ;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Job name validation
        /// </summary>
        /// <param name="value">value of the job name</param>
        /// <returns>error string</returns>
        public string ValidateJobName(string fieldName, string target, bool canBeNull)
        {
            if (canBeNull && String.IsNullOrEmpty(target))
            {
                return string.Empty;
            }
            else if (string.IsNullOrEmpty(target))
            {
                return GetError("empty").Replace("$field", fieldName).Replace("$type", "Job Name");
            }
            else
            {
                Regex regex = new Regex(JobNamePattern);
                if (!regex.IsMatch(target))
                {
                    return GetError("script-name").Replace("$field", fieldName).Replace("$type", "Job Name"); ;
                }
                return string.Empty;
            }
        }

        private static string header = "<html><head><style>body {font-family: verdana;font-size: 0.7em;} .errorDiv {width:400px;}</style></head><body>";

        private static string footer = "<p>For further information press &lt;F1&gt;.</p></div></body> ";

        /// <summary>
        /// Error dictionary
        /// </summary>
        private static readonly Dictionary<string, string> errors = new Dictionary<string, string>
        {
            { "empty", "<p>The input field '$field' cannot be left empty!</p>"},
            { "decoys", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid decoy specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Network field<br><b>Description</b><div class=\"errorDiv\"><p>The decoy specification for the input field " +
                "'$field' is invalid. You can use single host names, list of host names or 'ME' for your real IP address.</p><p>Valid network specification are:<p><ul><li>192.168.0.1, 192.168.0.10, ME</li><li>host1@example.com, host2@example.com</li></ul>"},
            { "directory-name", "<b style=\"color:red\">Error: </b><span style=\"color:red\">Invalid directory name specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> $type<br><h3>Description</h3><div class=\"errorDiv\"><p>Invalid directory name specification. " +
                "The input field '$field' need to have a value for a valid Windows directory name.</p><p>Valid file names are:<p><ul><li>C:\\Program Files\\NmapWin\\</li><li>.\\</li><li>..\\data\\</li></ul>"},
            { "file-name", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid file name specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> $type<br><h3>Description</h3><div class=\"errorDiv\"><p>Invalid file name specification. The input field " +
                "'$field' need to have a value for a valid Windows file name.</p><p>Valid file names are:<p><ul><li>C:\\Program Files\\NmapWin\\FileList.txt</li><li>.\\FileList.txt</li><li>..\\data\\FileList.txt</li></ul>" },
            { "ftp-bounce", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid FTP bounce specification.</span><br><b>InputField:</b> FTP bounce<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The ftp bounce field specification is " +
                "invalid. The input field need to have a value in the format &lt; hostname&lt;[:port].</p><p>Valid ftp bounce specifications are:<p><ul><li>192.168.0.1</li><li>192.168.0.1:101</li><li>host1 @example.com:101</li></ul>"},
            { "idle-scan", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid idle scan specification.</span><br><b>InputField:</b> Idle scan<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The idle scan field specification is invalid. " +
                "The input field need to have a value in the format &lt; hostname&lt;[:port].</p><p>Valid idle field specifications are:<p><ul><li>192.168.0.1</li><li>192.168.0.1:101</li><li>host1 @example.com:101</li></ul>"},
            { "interface", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid network interface specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The network interface specification " +
                "in input field '$field' is invalid.The input field need to have a value in the format of a valid interface name.</p><p>Valid specifications are:<p><ul><li>eth0</li><li>ens6</li><li>loopback-1</li></ul>"},
            { "invalid-options", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid nmap command line options</span><br><b>InputField:</b> $field<br><b>Type:</b> $type<br><h4>Description</h4><div class=\"errorDiv\"><p>Unknown command line options found: $extra</p>" },
            { "port", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid port specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The port specification in input field '$field' is invalid. " +
                "The input field need to have a value in the format of a simple integer.</p><p>Valid specifications are:</p><ul><li>123</li></ul>"},
            { "port-list", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid port list specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The input field '$field' specification is " +
                "invalid. The input field need to have a value in the format port1, port2, port3.</p><p>Valid specifications are:<p><ul><li>1,2,3,4</li></ul>"},
            { "port-range", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid port range specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The port range specification in input field " +
                "'$field' is invalid. The input field need to have a value in the format [UT:]port1-port2[,] where U stands for UDP and T for TCP</p><p>Valid specifications are:<p><ul><li>U:53,111,137,T:21-25,80,139,8080</li></ul>"},
            { "protocol-list", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid IP protocol list specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Text field<br><h3>Description</h3><div class=\"errorDiv\"><p>The input field '$field' specification " +
                "is invalid. The input field need to have a value in the format protocol1, protocol2, protocol3.</p><p>Valid specifications are:<p><ul><li>17,29,2,4</li></ul>"},
            { "script-name", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid script name.</span><br><b>InputField:</b> $field<br><b>Type:</b> $type<br><h3>Description</h3><div class=\"errorDiv\"><p>Invalid script name specification. The input field '$field' need to have a value for a valid Windows file name.</p>" +
                "<p>Valid script names are:<p><ul><li>script-name</li><li>scriptname</li><li>script-name-123</li></ul>"},
            { "target", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid network specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Network field<br><h3>Description</h3><div class=\"errorDiv\"><p>The network specification for the input field '$field' is invalid. You can use single host names, list " +
                "of host names, IP addresses list of IP addreses or listsof CIDR notation. Additionally, you can use ranges of IP addresses.</p><p>Valid network specification are:<p><ul><li>192.168.0.1, 192.168.0.0/24</li><li>192.168.1-245.2-100</li><li>host1@example.com, host2@example.com</li></ul>"},
            { "timeout", "<b style=\"color:red\">Error:</b><span style=\"color:red\"> Invalid timeout specification.</span><br><b>InputField:</b> $field<br><b>Type:</b> Timeout field<br><h3>Description</h3><div class=\"errorDiv\"><p>The timeout specification in input field '$field' is invalid. The input field "+
                "need to have a valid integer value in seconds.</p><p>Valid specifications are:<p><ul><li>5</li></ul>" },
        };
    }
}
