﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Dialogs;
using System.Collections.Generic;

namespace NmapWinUI.Utils
{
    public class DeleteHosts : AbstractSingleProgressDialog
    {
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        public List<Host> Hosts { get; set; }

        public DeleteHosts()
        {
            Title = "Deleting hosts";
        }

        public DeleteHosts(List<Host> hosts)
        {
            Title = "Deleting hosts";
            Hosts = hosts;
        }

        /// <summary>
        /// Asynchronous task
        /// </summary>
        public override void AsyncTasks()
        {
            SetMainValues("Deleting hosts...", Hosts.Count, 0);
            Hosts.ForEach(h =>
            {
                IncProgress("Host with ID '" + h.Id + "' deleted");
                DatabaseHost.DeleteHost(h);
            });
        }
    }
}
