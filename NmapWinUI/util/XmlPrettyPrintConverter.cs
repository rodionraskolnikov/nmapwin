﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using System;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Linq;

namespace NmapWinUI.Utils
{
    /// <summary>
    /// XML pretty print converter
    /// </summary>
    /// <remarks>
    /// If the input text is not XML formatted, just return the current value, otherwise
    /// return the indented XML string. XML text is indented with 4 spaces.
    /// </remarks>
    [ValueConversion(typeof(string), typeof(string))]
    public class XmlPrettyPrintConverter : IValueConverter
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(XmlPrettyPrintConverter));

        public static bool IsPrettyPrint { get; set; } = false;

        /// <summary>
        /// Convert a input XML string to an indended XMl string.
        /// </summary>
        /// <param name="value">value to convert</param>
        /// <param name="targetType">target tpe</param>
        /// <param name="parameter">converter parameter</param>
        /// <param name="culture">localization</param>
        /// <returns>pretty print XML string</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !IsPrettyPrint)
            {
                return value;
            }

            string xml = value as string;
            if (!xml.TrimStart().StartsWith("<"))
                return value;

            var stringBuilder = new StringBuilder();
            logger.Debug(string.Format("Starting pretty print XML conversion - length: {0}", xml.Length));

            try
            {
                var element = XElement.Parse(xml);
                var settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = true,
                    Indent = true,
                    NewLineOnAttributes = false,
                    IndentChars = "    "
                };

                using var xmlWriter = XmlWriter.Create(stringBuilder, settings);
                element.Save(xmlWriter);
            }
            catch (XmlException e)
            {
                logger.Error(string.Format("XML pretty print conversion failed - error: {0}", e.Message));
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Convert a input XML string to an non-indended XMl string.
        /// </summary>
        /// <param name="value">value to convert</param>
        /// <param name="targetType">target tpe</param>
        /// <param name="parameter">converter parameter</param>
        /// <param name="culture">localization</param>
        /// <returns>converted XMl string</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;
            DateTime resultDateTime;
            if (DateTime.TryParse(strValue, out resultDateTime))
            {
                return resultDateTime;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
