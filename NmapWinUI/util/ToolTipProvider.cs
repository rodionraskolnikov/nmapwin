﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using System;
using System.Collections.Generic;

namespace NmapWinUI.Utils
{
    public class TooltipProvider
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(TooltipProvider));

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<TooltipProvider> lazy = new Lazy<TooltipProvider>(() => new TooltipProvider());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static TooltipProvider Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        public TooltipProvider()
        {
        }
        #endregion

        #region Methods
        public string LoadTooltip(string fieldName)
        {
            if (fieldName == null)
                return null;
            string tooltip;
            if (tootips.ContainsKey(fieldName))
            {
                tooltip = header + tootips[fieldName] + footer;
            }
            else
            {
                tooltip = header + tootips["not-found"] + footer;
                logger.Error(string.Format("Missing tooltip - fieldName: {0}", fieldName));
            }
            return tooltip;
        }

        public string LoadTooltip(string page, string fieldName)
        {
            if (page == null || fieldName == null)
            {
                return header + tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName) + footer;
            }

            Dictionary<string, string> tooltips;
            switch (page)
            {
                case "General":
                    tooltips = tootipsGeneral;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "PortDetails":
                    tooltips = tootipsPortDetails;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "PortList":
                    tooltips = tootipsPortList;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "HostDetails":
                    tooltips = tootipsHostDetails;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "HostList":
                    tooltips = tootipsHostList;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "ProfileList":
                    tooltips = tootipsProfileList;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "ScanDetails":
                    tooltips = tootipsScanDetails;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
                case "ScriptList":
                    tooltips = tootipsScriptList;
                    return header + (tooltips.ContainsKey(fieldName) ? tooltips[fieldName] : tootipsGeneral["not-found"].Replace("$page", page).Replace("$field", fieldName)) + footer;
            }
            return null;
        }
        #endregion

        #region Tooltip values
        private static readonly string header = "<html><head><style>body {font-family: verdana;font-size: 0.7em;} .tooltipDiv {width:400px;}</style></head><body>";

        private static readonly string footer = "<p>For further information press &lt;F1&gt;.</p></div></body>";

        private static readonly Dictionary<string, string> tootipsGeneral = new Dictionary<string, string>
        {
            { "auto-update", "<b>InputField:</b> Auto update<br><b>Type:</b> Toolbar button<h3>Auto-update the table</h3><div class=\"tooltipDiv\"><p>Starts/stops the auto-update feature. If switched on the table will be updated according to the auto-update interval option.</p>"},
            { "close", "<b>InputField:</b> Dialog close<br><b>Type:</b> Button<h3>Closes the Dialog</h3><div class=\"tooltipDiv\"><p>Closes the current dialog, without saving any data.</p>"},
            { "delete", "<b>InputField:</b> Delete<br><b>Type:</b> Toolbar button<h3>Delete object</h3><div class=\"tooltipDiv\"><p>Deletes the currently displayed item from the database.</p>"},
            { "delete-all", "<b>InputField:</b> Delete all<br><b>Type:</b> Toolbar button<h3>Delete all</h3><div class=\"tooltipDiv\"><p>Deletes all items currently listed in the table.</p>"},
            { "exit", "<b>InputField:</b> Exit Button<br><b>Type:</b> Button<h3>Exit Button</h3><div class=\"tooltipDiv\"><p>The exit button stops the nmapwin application. All currently running threads will be stopped prior to leaving the application.</p>"},
            { "help", "<b>InputField:</b> Dialog help<br><b>Type:</b> Toolbar Button<h3>Opens help</h3><div class=\"tooltipDiv\"><p>Opens the help screen for the current dialog.</p>"},
            { "last-update", "<b>InputField:</b> Last update<br><b>Type:</b> Label<h3>Last update</h3><div class=\"tooltipDiv\"><p>Time of the last update of the dialog.</p>"},
            { "not-found", "<h3>Not found</h3><p>The requested tooltip could not be found!</p><ul><li>Page: $page</li><li>Field: $field</li></ul>"},
            { "refresh", "<b>InputField:</b> Dialog refresh<br><b>Type:</b> Toolbar button<h3>Refresh the whole Dialog</h3><div class=\"tooltipDiv\"><p>Refreshes the whole dialog. All data field will be updated with the latest values stored in the embedded database. Also all lists " +
              "will be updated.</p><p>A refresh can also be triggered using the &lt;F5&gt; keyboard shortcut</p>"},
            { "save", "<b>InputField:</b> Save data<br><b>Type:</b> Button<h3>Save data</h3><div class=\"tooltipDiv\"><p>Saves the current object displayed in the dialog to the database.</p>"},
            { "scroll", "<b>InputField:</b> Scroll to end<br><b>Type:</b> Toolbar button<h3>Scroll to end</h3><div class=\"tooltipDiv\"><p>Scrolls to the end of the table, so that the last row is visible. This is also applied whenever the table gets updated.</p>"},
        };

        private static readonly Dictionary<string, string> tootipsHostDetails = new Dictionary<string, string>
        {
            { "address-table", "<b>InputField:</b> Address table<br><b>Type:</b> Data grid<h3>Address table</h3><div class=\"tooltipDiv\"><p>All addresses of the host, which are discovered during a nmap scan. This table shows all the gathered addresses of a particular scan/host.</p>" },
            { "hop-table", "<b>InputField:</b> Hop table<br><b>Type:</b> Data grid<h3>Hop table</h3><div class=\"tooltipDiv\"><p>All hops, which are discovered during a nmap scan using the traceroute options. This table shows all the gathered hops of a particular scan/host.</p>" },
            { "name-table", "<b>InputField:</b> Names table<br><b>Type:</b> Data grid<h3>Names table</h3><div class=\"tooltipDiv\"><p>All host names of the host, which are discovered during a nmap scan. This table shows all the gathered host names of a particular scan/host.</p>" },
            { "osmatch-table", "<b>InputField:</b> OS match table<br><b>Type:</b> Data grid<h3>OS match table</h3><div class=\"tooltipDiv\"><p>All OS matches, which are discovered during a nmap scan are saved to the database. This table shows all the gathered OS matches of a particular scan/host.</p>" },
            { "port-table", "<b>InputField:</b> Port table<br><b>Type:</b> Data grid<h3>Port table</h3><div class=\"tooltipDiv\"><p>All ports, which are discovered during a nmap scan are saved to the database. This table shows all the gathered ports of a particular scan.</p>" },
            { "service-table", "<b>InputField:</b> Service table<br><b>Type:</b> Data grid<h3>Service table</h3><div class=\"tooltipDiv\"><p>All services, attached to a port, which are discovered during a nmap scan are saved to the database. This table shows all the gathered services of a particular scan.</p>" },
        };

        private static readonly Dictionary<string, string> tootipsPortDetails = new Dictionary<string, string>
        {
            { "id", "<b>InputField:</b> ID<br><b>Type:</b> Text box (readonly)<h3>ID of the port</h3><div class=\"tooltipDiv\"><p>ID of the port, which is also the primary key in the database."},
            { "portId", "<b>InputField:</b> Port ID<br><b>Type:</b> Text box (readonly)<h3>Port ID of the port</h3><div class=\"tooltipDiv\"><p>Shows the port ID of the corresponding port."},
            { "protocol", "<b>InputField:</b> Protocol<br><b>Type:</b> Text box (readonly)<h3>Protocol</h3><div class=\"tooltipDiv\">Protocol of the port, can be one of 'tcp' or 'udp'."},
            { "reason", "<b>InputField:</b> Reason<br><b>Type:</b> Text box (readonly)<h3>Reason for the port status</h3><div class=\"tooltipDiv\">By default, an Nmap output indicates whether a host is up or not, but does not describe the discovery tests that the host responded to. " +
                "It can be useful to understand the reason why a port is marked as open, closed, or filtered and why the host is marked as alive."},
            { "reasonttl", "<b>InputField:</b> Reason<br><b>Type:</b> Text box (readonly)<h3>TTL to get the status reason</h3><div class=\"tooltipDiv\">TTL for the reason discovery of the port state."},
            { "state", "<b>InputField:</b> Status<br><b>Type:</b> Text box (readonly)<h3>Status of the port</h3><div class=\"tooltipDiv\">These states are not intrinsic properties of the port itself, but describe how Nmap sees them. For example, an Nmap scan from the same network " +
                "as the target may show port 135/tcp as open, while a scan at the same time with the same options from across the Internet might show that port as filtered."},
            { "script-table", "<b>InputField:</b> Script Table<br><b>Type:</b> Data Grid<h3>Script Table</h3><div class=\"tooltipDiv\">Table of all executed NSE scripts on the corresponding port."},
            { "script-table-refresh", "<b>InputField:</b> Script Table Refresh<br><b>Type:</b> Toolbar button<h3>Script Table Refresh</h3><div class=\"tooltipDiv\">Reloads the scripts table from the database for the corresponding port."},
            { "script-table-open", "<b>InputField:</b> Script Open<br><b>Type:</b> Data grid button<h3>Script Open Details</h3><div class=\"tooltipDiv\">Open the details dialog for the corresponding script."},
            { "script-table-delete", "<b>InputField:</b> Script Delete<br><b>Type:</b> Data grid button<h3>Script delete</h3><div class=\"tooltipDiv\">Deletes the selected script from the list of port scripts."},
        };

        private static readonly Dictionary<string, string> tootipsPortList = new Dictionary<string, string>
        {
            { "list", "<b>InputField:</b> Port table<br><b>Type:</b> Data grid<h3>Port list</h3><div class=\"tooltipDiv\"><p>List of all discovered ports, independent of the scan."},
            { "details", "<b>InputField:</b> Port details<br><b>Type:</b> Button<h3>Open port details</h3><div class=\"tooltipDiv\"><p>Opens the port details dialog for the selected port."},
            { "delete", "<b>InputField:</b> Delete port<br><b>Type:</b> Button<h3>Delete port</h3><div class=\"tooltipDiv\"><p>Deletes the selected port from the database."},
        };

        private static readonly Dictionary<string, string> tootipsHostList = new Dictionary<string, string>
        {
            { "list", "<b>InputField:</b> Host table<br><b>Type:</b> Data grid<h3>Host list</h3><div class=\"tooltipDiv\"><p>List of all discovered hosts, independent of the scan."},
            { "details", "<b>InputField:</b> Host details<br><b>Type:</b> Button<h3>Open host details</h3><div class=\"tooltipDiv\"><p>Opens the host details dialog for the selected host."},
            { "delete", "<b>InputField:</b> Delete host<br><b>Type:</b> Button<h3>Delete host</h3><div class=\"tooltipDiv\"><p>Deletes the selected host from the database."},
        };

        private static readonly Dictionary<string, string> tootipsProfileList = new Dictionary<string, string>
        {
            { "copy", "<b>InputField:</b> Copy profile<br><b>Type:</b> Button<h3>Copy profile</h3><div class=\"tooltipDiv\"><p>Copy the selected profile. The new profile will be a clone of the selected profile."},
            { "list", "<b>InputField:</b> Profile table<br><b>Type:</b> Data grid<h3>Profile list</h3><div class=\"tooltipDiv\"><p>Lists all available profile. The profiles are per default ordered by ID. To change the ordering, click on one of the table column headers."},
            { "details", "<b>InputField:</b> Profile details<br><b>Type:</b> Button<h3>Open profile details</h3><div class=\"tooltipDiv\"><p>Opens the profile details dialog for the selected profile."},
            { "delete", "<b>InputField:</b> Delete profile<br><b>Type:</b> Button<h3>Delete profile</h3><div class=\"tooltipDiv\"><p>Deletes the selected profile from the database."},
            { "new", "<b>InputField:</b> Create new profile<br><b>Type:</b> Toolbar Button<h3>Create new profile</h3><div class=\"tooltipDiv\"><p>Create a new profile. A new profile dialog opens where you can set the profile parameters."},
        };

        private static readonly Dictionary<string, string> tootipsScriptList = new Dictionary<string, string>
        {
            { "copy", "<b>InputField:</b> Copy script<br><b>Type:</b> Button<h3>Copy script</h3><div class=\"tooltipDiv\"><p>Copy the selected script. The new script will be a clone of the selected script."},
            { "list", "<b>InputField:</b> Script Table<br><b>Type:</b> Data grid<h3>Script list</h3><div class=\"tooltipDiv\"><p>Lists all available script. The scripts are per default ordered by ID. To change the ordering, click on one of the table column headers."},
            { "details", "<b>InputField:</b> Script Details<br><b>Type:</b> Button<h3>Open script details</h3><div class=\"tooltipDiv\"><p>Opens the script details dialog for the selected script."},
            { "delete", "<b>InputField:</b> Delete Script<br><b>Type:</b> Button<h3>Delete script</h3><div class=\"tooltipDiv\"><p>Deletes the selected script from the database."},
            { "new", "<b>InputField:</b> Create new script<br><b>Type:</b> Toolbar Button<h3>Create new script</h3><div class=\"tooltipDiv\"><p>Create a new script. A new script dialog opens, in which you can change the script parameters."},
            { "write", "<b>InputField:</b> Writes help files<br><b>Type:</b> Toolbar Button<h3>Write help files</h3><div class=\"tooltipDiv\"><p>Writes help files generated from the script data. The files can be found in $NMAPWIN_HOME\\help\\nmap\\scripts."},
        };

        private static readonly Dictionary<string, string> tootipsScanDetails = new Dictionary<string, string>
        {
            { "chart", "<b>InputField:</b> Chart<br><b>Type:</b> Chart<h3>Chart</h3><div class=\"tooltipDiv\"><p>Show the CPU utilization of the current scan. The CPU utilization is the avarage over 5 min. of the nmap process. The CPU utilization is taken from the" +
                "Windows WMI performance counters.<p>" },
            { "chart-defaults", "<b>InputField:</b> Chart defaults<br><b>Type:</b> Toolbar Button<h3>Reset chart</h3><div class=\"tooltipDiv\"><p>Reset the charts and restore the dfault values. All Zooming and panning will be reset to the default view.</p>" },
            { "chart-refresh", "<b>InputField:</b> Chart refresh<br><b>Type:</b> Toolbar Button<h3>CHart refresh</h3><div class=\"tooltipDiv\"><p>Reloads all available performance data for the current scan. The chart will be redrawn with the new values.</p>" },
            { "command", "<b>InputField:</b> Command<br><b>Type:</b> Text Box<h3>Command</h3><div class=\"tooltipDiv\"><p>The command field shows the current nmap command to be executed, as soon as the 'Scan' button is clicked. The command is the resulting nmap command " +
              "line, which is saved in the selected profile, plus additional editing.</p><p>If you select a profile, the stored nmap command line is shown in the command field. It can edited on demand and started using the 'Scan' button.<p><p>Edited commands are not saved " +
              "to the profile.Use the 'Edit profile' menu item to change the corresponding profile.</p><p>Create a new profile by using the &lt;Profile&gt; button.This will open a small dialog with target, name command and description. Using the &lt;Save&gt; will create a " +
              "new profile with the given name.</p>"},
            { "debug-level", "<b>InputField:</b> Debug level<br><b>Type:</b> Text Box<h3>Debug level</h3><div class=\"tooltipDiv\"><p>Debug level of the nmap scan.</p>" },
            { "delete", "<b>InputField:</b> Scan delete<br><b>Type:</b> Button<h3>Delete a scan</h3><div class=\"tooltipDiv\"><p>Deletes the selected scan from the embedded database.</p>"},
            { "end", "<b>InputField:</b> Scan end<br><b>Type:</b> Text Box<h3>Scan end</h3><div class=\"tooltipDiv\"><p>Local date/time of the end of the scan process.</p>" },
            { "exit-code", "<b>InputField:</b> Scan Exit Code<br><b>Type:</b> Text Box<h3>Scan Exit Code</h3><div class=\"tooltipDiv\"><p>The exit code of the nmap background process. Normal execution end has an exit code of 0. In case the scan has been aborted, the exit code will be -1.</p>" },
            { "host-delete", "<b>InputField:</b> Host delete<br><b>Type:</b> Button<h3>Delete a Host</h3><div class=\"tooltipDiv\"><p>Deletes the selected host. The host will be also deleted from the embedded database.</p>" },
            { "host-details", "<b>InputField:</b> Host details<br><b>Type:</b> Button<h3>Open Host Details</h3><div class=\"tooltipDiv\"><p>Opens the host details dialog.</p>" },
            { "host-table", "<b>InputField:</b> Host table<br><b>Type:</b> Data grid<h3>Host table</h3><div class=\"tooltipDiv\"><p>All hosts, which are dicovered during an nmap scan are saved to the embedded database. This table shows all the gathered hosts of a particular scan. The table " +
              "shows the following columns:</p><ul><li><b>ID:</b> Primary key of the host in the embedded database.</li><li><b>Name:</b> Name of the host.</li><li><b>Status:</b> Host status.</li><li><b>Reason:</b> Status reason.</li><li><b>ReasonTtl:</b> Status reason TTL.</li>" +
              "<li><b>Buttons:</b> Command buttons, currently, 'Open details' and 'Delete host'.</li></ul><p>Using the 'Open Details' button on the right most column, will open the details dialog for the selected host.</p><p>Using the 'Delete host' button will delete the host and all related "+
              "addresses, ports etc. from the embedded database.</p>" },
            { "host-table-deleteall", "<b>InputField:</b> Delete all<br><b>Type:</b> Toolbar button<h3>Delete all hosts</h3><div class=\"tooltipDiv\"><p>Deletes all currently listed hosts in the host table from the embedded database. Only hosts from the current scan will be deleted. " +
              "This can't be undone, so be careful with this command.</p>"},
            { "host-table-deletedown", "<b>InputField:</b> Delete host in state 'down'<br><b>Type:</b> Toolbar button<h3>Delete all hosts in state 'down'</h3><div class=\"tooltipDiv\"><p>Deletes all currently listed hosts in the host table from the embedded database, "+
              "which are in state 'down'. Only hosts from the current scan will be deleted. This can't be undone, so be carefull with deleting the hosts.</p>"},
            { "host-table-refresh", "<b>InputField:</b> Host table refresh<br><b>Type:</b> Toolbar button<h3>Refresh the host table</h3><div class=\"tooltipDiv\"><p>All discovered host are represented by a row in the host table. Refreshing the host list can be done using the refresh button in the " +
              "toolbar. This will reload all data from the embedded database host table.</p>"},
            { "id", "<b>InputField:</b> Scan ID<br><b>Type:</b> Text Box<h3>Scan ID</h3><div class=\"tooltipDiv\"><p>Primry key of the scan entity.</p>" },
            { "last-update", "<b>InputField:</b> Scan last update<br><b>Type:</b> Text Box<h3>Scan Last Update</h3><div class=\"tooltipDiv\"><p>Date/time of the last update to the scan entity. This is usually the date/time of the last entry in the nmap scan output.</p>" },
            { "name", "<b>InputField:</b> Scan name<br><b>Type:</b> Text Box<h3>Scan Name</h3><div class=\"tooltipDiv\"><p>Name of the scan. If the scan was started with a profile, this is the name of the profile, otherwise a random name is chosen.</p>" },
            { "network-map", "<b>InputField:</b> Network map<br><b>Type:</b> Button<h3>Network map</h3><div class=\"tooltipDiv\"><p>Shows a graphical representaion of the trace route result (use --traceroute in your nmap scan). The map is zoomable and show all network nodes between your " +
              "machine and the target host.</p><p>In case a OS could be discovered, the OS is shown as icon on the left side of the host label. Additionally the IP address and the host name are displayed, if possible.<p>" },
            { "output", "<b>InputField:</b> Scan output<br><b>Type:</b> Text Box<h3>Scan output</h3><div class=\"tooltipDiv\"><p>This text box show the output from the nmap command. Stderr and Stdout are captured and displayed here. The output is automaically appended as soon as the nmap "+
              "process, which runs as background thread produces new lines of output.</p><p>The output is always scrolled to the end of the window. To stop automatic scolling switch off the automatic scrolling using the toggle button in the toolbar.</p>" },
            { "output-full", "<b>InputField:</b> Open output dialog<br><b>Type:</b> Toolbar button<h3>Open output dialog</h3><div class=\"tooltipDiv\"><p>Opens the output dialog.</p>"},
            { "output-scroll", "<b>InputField:</b> Scroll output window<br><b>Type:</b> Toggle button<h3>Scroll Output Window</h3><div class=\"tooltipDiv\"><p>If the button is checked the output windows will be scrolled to the end of the output window.</p>"},
            { "output-refresh", "<b>InputField:</b> Refresh output<br><b>Type:</b> Button<h3>Refresh Output</h3><div class=\"tooltipDiv\"><p>Refreshes the output window. If the nmap background task is still running, the currebtly running task is asked for latest output lines. Otherwise the output " +
              "is refreshed from the embedded datatbase.</p>"},
            { "output-delete", "<b>InputField:</b> Output delete<br><b>Type:</b> Button<h3>Delete the Scan Ouput</h3><div class=\"tooltipDiv\"><p>Deletes the current scan output. Also the output in the database will be deleted.</p>"},
            { "output-pretty", "<b>InputField:</b> Pretty print<br><b>Type:</b> Toobar Button<h3>Pretty print of the XML output</h3><div class=\"tooltipDiv\"><p>Converts the XML output to a pretty print XML document, with 4 spaces indentation.</p>"},
            { "output-reparse", "<b>InputField:</b> Reparse output<br><b>Type:</b> Toobar Button<h3>Reparse Ouput</h3><div class=\"tooltipDiv\"><p>Reparses the XMl output and recovers lost hosts. This is usefull in case you accidentally deleted some or all hosts.</p>"},
            { "pid", "<b>InputField:</b> Scan PID<br><b>Type:</b> Text Box<h3>Scan PID</h3><div class=\"tooltipDiv\"><p>Process ID fo the nmap process.</p>" },
            { "process-name", "<b>InputField:</b> Scan Process name<br><b>Type:</b> Text Box<h3>Scan process name</h3><div class=\"tooltipDiv\"><p>Name of the nmap process.</p>" },
            { "profile", "<b>InputField:</b> Scan profile<br><b>Type:</b> Text Box<h3>Scan Profile</h3><div class=\"tooltipDiv\"><p>Name of the profile, which was used for the scan. If the scan was started without profile, this text box will be empty.</p>" },
            { "restart", "<b>InputField:</b> Scan restart<br><b>Type:</b> Button<h3>Restart a scan</h3><div class=\"tooltipDiv\"><p>Restarts the current scan. All values are taken from the previously saved scan and reapplied to a newly started background task.</p>" },
            { "start", "<b>InputField:</b> Scan start<br><b>Type:</b> Text Box<h3>Scan start</h3><div class=\"tooltipDiv\"><p>Local date/time of the start of the scan process.</p>" },
            { "status", "<b>InputField:</b> Scan status<br><b>Type:</b> Text Box<h3>Scan Status</h3><div class=\"tooltipDiv\"><p>Status of the scan. The following statuses are valid: UNKNOWN,STARTED,RUNNING,COMPLETED,ABORTED,ERROR.</p>" },
            { "stop", "<b>InputField:</b> Scan stop<br><b>Type:</b> Button<h3>Stopping a scan</h3><div class=\"tooltipDiv\"><p>If the scan is currently running as background task, the task will be stopped and the scan will be aborted.</p>" },
            { "target", "<b>InputField:</b> Scan target<br><b>Type:</b> Text Box.<h3>Scan target</h3><div class=\"tooltipDiv\"><p>The current target of the scan. In case the scan was started by a profile and the profile contains a target, this target specified in the profile is taken, " +
              "otherwise the target is taken from the target field on the main sceen.</p>" },
            { "uptime", "<b>InputField:</b> Scan uptime<br><b>Type:</b> Text Box<h3>Scan Uptime</h3><div class=\"tooltipDiv\"><p>Running time of the scan process in days.hours:minutes:seconds.</p>" },
            { "verbosity-level", "<b>InputField:</b> Verbosity level<br><b>Type:</b> Text Box<h3>Verbosity level</h3><div class=\"tooltipDiv\"><p>Verbosity level of the nmap scan.</p>" },
            { "version", "<b>InputField:</b> Scan version<br><b>Type:</b> Text Box<h3>Scan Version</h3><div class=\"tooltipDiv\"><p>Version of the nmap executable, which was used for the scan. The version is taken from the XML output of the nmap scan, therefore it will be available as soon as " +
                "scan has finished and the XML output is parsed.</p>" },
        };

        private static readonly Dictionary<string, string> tootips = new Dictionary<string, string>
        {
            { "add-profile-menu-item", "<b>InputField:</b> New Profile<br><b>Type:</b> Menu Item<h3>Add a new profile</h3><div class=\"tooltipDiv\"><p>Adds a new profile to the embedded database. It will open a dialog box, in which you can specify the features of the new profile."},
            { "command", "<b>InputField:</b> Command<br><b>Type:</b> Text Box<h3>Command</h3><div class=\"tooltipDiv\"><p>The command field shows the current nmap command to be executed, as soon as the 'Scan' button is clicked. The command is the resulting nmap command "+
              "line, which is saved in the selected profile, plus additional editing.</p><p>If you select a profile, the stored nmap command line is shown in the command field. It can edited on demand and started using the 'Scan' button.<p><p>Edited commands are not saved "+
              "to the profile.Use the 'Edit profile' menu item to change the corresponding profile.</p><p>Create a new profile by using the &lt;Profile&gt; button.This will open a small dialog with target, name command and description. Using the &lt;Save&gt; will create a " +
              "new profile with the given name.</p>"},
            { "database-browse", "<b>InputField:</b> Database file path<br><b>Type:</b> Button<h3>Browse for database file</h3><div class=\"tooltipDiv\"><p>Opens a file search dialog to browse for the database file in case of a SQL Server CE.</p>"},
            { "database-file", "<b>InputField:</b> Database file path<br><b>Type:</b> Text box<h3>Database file</h3><div class=\"tooltipDiv\"><p>Name of the database file in case of a SQL Server CE.</p>"},
            { "database-name", "<b>InputField:</b> Database name<br><b>Type:</b> Text box<h3>Database name</h3><div class=\"tooltipDiv\"><p>Name of the database in case of a MySQL database.</p>"},
            { "database-password", "<b>InputField:</b> Database password<br><b>Type:</b> Text box<h3>Database password</h3><div class=\"tooltipDiv\"><p>Database password in case of a MySQL database.</p>"},
            { "database-port", "<b>InputField:</b> Database port<br><b>Type:</b> Text box<h3>Database port</h3><div class=\"tooltipDiv\"><p>Database port in case of a MySQL database.</p>"},
            { "database-type", "<b>InputField:</b> Database type<br><b>Type:</b> Combo box<h3>Database type</h3><div class=\"tooltipDiv\"><p>Choose the database type. Currently supported are SQLServer CE or MySQL</p>"},
            { "database-test", "<b>InputField:</b> Database test<br><b>Type:</b> Button<h3>Test database connectivity</h3><div class=\"tooltipDiv\"><p>Tests the database connectivity. On success you can choose to keep the current connection string.</p>"},
            { "database-user", "<b>InputField:</b> Database user<br><b>Type:</b> Text box<h3>Database user</h3><div class=\"tooltipDiv\"><p>Username for the database in case of a MySQL database.</p>"},
            { "delete-profile-menu-item", "<b>InputField:</b> Delete Profile<br><b>Type:</b> Menu Item<h3>Delete a profile</h3><div class=\"tooltipDiv\"><p>Deletes a profile from the embedded database. If the main screen profile select box contains a valid profile, this profile will " +
              "be deleted, otherwise you can select profile to edit in the dialog."},
            { "edit-profile-menu-item", "<b>InputField:</b> Edit Profile<br><b>Type:</b> Menu Item<h3>Edits a profile</h3><div class=\"tooltipDiv\"><p>Edits an existing profile. If the main screen profile select box contains a valid profile, this profile will be edited, otherwise " +
              "you can select profile to edit in the dialog."},
            { "exit-menu-item", "<b>InputField:</b> Exit<br><b>Type:</b> Menu Item<h3>Close the application</h3><div class=\"tooltipDiv\"><p>The exit menu item stops the nmapwin application. All currently running threads will be stopped prior to leaving the application. Unsaved command lines "+
              "are not saved to the profile database.</p>"},
            { "export-menu-item", "<b>InputField:</b> Export<br><b>Type:</b> Menu Item<h3>Starts an export</h3><div class=\"tooltipDiv\"><p>Starts a export of all data in the embedded database to a JSON file. The export task runs asynchronously.</p>"},
            { "host-table", "<b>InputField:</b> Host table<br><b>Type:</b> Data grid<h3>Host table</h3><div class=\"tooltipDiv\"><p>All hosts, which are dicovered during an nmap scan are saved to the embedded database. This table shows all the gathered hosts of a particular scan. The table " +
              "shows the following columns:</p><ul><li><b>ID:</b> Primary key of the host in the embedded database.</li><li><b>Name:</b> Name of the host.</li><li><b>Status:</b> Host status.</li><li><b>Reason:</b> Status reason.</li><li><b>ReasonTtl:</b> Status reason TTL.</li>" +
              "<li><b>Buttons:</b> Command buttons, currently, 'Open details' and 'Delete host'.</li></ul><p>Using the 'Open Details' button on the right most column, will open the details dialog for the selected host.</p><p>Using the 'Delete host' button will delete the host and all related "+
              "addresses, ports etc. from the embedded database.</p>" },
            { "host-table-refresh", "<b>InputField:</b> Host table refresh<br><b>Type:</b> Toolbar button<h3>Refresh the host table</h3><div class=\"tooltipDiv\"><p>All discovered host are represented by a row in the host table. Refreshing the host list can be done using the refresh button in the " +
              "toolbar. This will reload all data from the embedded database host table.</p>"},
            { "host-table-deleteall", "<b>InputField:</b> Delete all<br><b>Type:</b> Toolbar button<h3>Delete all hosts</h3><div class=\"tooltipDiv\"><p>Deletes all currently listed hosts in the host table from the embedded database. Only hosts from the current scan will be deleted. " +
              "This can't be undone, so be careful with this command.</p>"},
            { "host-table-deletedown", "<b>InputField:</b> Delete host in state 'down'<br><b>Type:</b> Toolbar button<h3>Delete all hosts in state 'down'</h3><div class=\"tooltipDiv\"><p>Deletes all currently listed hosts in the host table from the embedded database, "+
              "which are in state 'down'. Only hosts from the current scan will be deleted. This can't be undone, so be carefull with deleting the hosts.</p>"},
            { "import-menu-item", "<b>InputField:</b> Import<br><b>Type:</b> Menu Item<h3>Starts an import</h3><div class=\"tooltipDiv\"><p>Starts an import of all data from JSON files and updates the database. The import task runs asynchronously.</p>"},
            { "import-xml-menu-item", "<b>InputField:</b> Import XML<br><b>Type:</b> Menu Item<h3>Starts a XML import</h3><div class=\"tooltipDiv\"><p>Starts an import of an XML output from the nmap process and updates the database. The import task runs asynchronously.</p>"},
            { "job-cron", "<b>InputField:</b> Cron expression<br><b>Type:</b> Text Box<h3>Cron expression</h3><div class=\"tooltipDiv\"><p>Cron expression of a job type of 'Cron'.</p>"},
            { "job-delete", "<b>InputField:</b> Delete Job<br><b>Type:</b> Button<h3>Delete the job</h3><div class=\"tooltipDiv\"><p>Deletes the job. The job is unscheduled before deletion.</p>"},
            { "job-details", "<b>InputField:</b> Job Details<br><b>Type:</b> Button<h3>Opens the job details dialog</h3><div class=\"tooltipDiv\"><p>Opens the job details dialog.</p>"},
            { "job-group", "<b>InputField:</b> Job Group Name<br><b>Type:</b> Text Box<h3>Name of the job group</h3><div class=\"tooltipDiv\"><p>Name of the job group. If the job is new the input field will editable, otherwise relad-only.</p>"},
            { "job-interval", "<b>InputField:</b> Interval<br><b>Type:</b> Time span picker<h3>Interval</h3><div class=\"tooltipDiv\"><p>Interval in minutes for a 'Simple' schedule in minutes.</p>"},
            { "job-list", "<b>InputField:</b> Job List<br><b>Type:</b> Grid Data<h3>List of all jobs</h3><div class=\"tooltipDiv\"><p>Lists all currently defined jobs. The job list taken from the scheduler service.</p>"},
            { "job-name", "<b>InputField:</b> Job Name<br><b>Type:</b> Text Box<h3>Name of the job</h3><div class=\"tooltipDiv\"><p>Name of the job. If the job is new the input field will editable, otherwise relad-only.</p>"},
            { "job-new", "<b>InputField:</b> Add new Job<br><b>Type:</b> Button<h3>Opens the job add dialog</h3><div class=\"tooltipDiv\"><p>Opens the job add dialog and adds a newly defined job to the scheduler.</p>"},
            { "job-save", "<b>InputField:</b> Save the job<br><b>Type:</b> Button<h3>Save the job</h3><div class=\"tooltipDiv\"><p>Saves the job. If the job is new it will be inserted into the database otherwise the job will be updated.</p>"},
            { "job-type", "<b>InputField:</b> Job Type<br><b>Type:</b> Combo Box<h3>Job type</h3><div class=\"tooltipDiv\"><p>Type of the job. Can be one of 'Simple' or 'Cron'.</p>"},
            { "job-starttime", "<b>InputField:</b> Start Time<br><b>Type:</b> Time picker<h3>Start time</h3><div class=\"tooltipDiv\"><p>Start time for the 'Simple' schedule. Granularity is 30min. Can be changed from 00:00 until 24:00 today.</p>"},
            { "main-profile", "<b>InputField:</b> Profile<br><b>Type:</b> Combo box<h3>Profile Selection</h3><div class=\"tooltipDiv\"><p>Select a profile for the scan. Profiles are only templates, you can modify the nmap command line afterwards in the command field, if no profile " +
                "fits your needs. Otherwise create a new profile.</p>" },
            { "not-found", "<h3>Not found</h3>The requested tooltip could not be found!"},
            { "options-updateinterval", "<b>InputField:</b> Update interval<br><b>Type:</b> Text box<h3>Scroll Process update interval</h3><div class=\"tooltipDiv\"><p>Update interval for the nmap background process in milliseconds.</p>"},
            { "profile", "<b>InputField:</b> Profile<br><b>Type:</b> Combo box<h3>Profile Selection</h3><div class=\"tooltipDiv\"><p>All available profiles are listed here. Select a profile to modify of create a new profile.</p>" },
            { "profile-ackping", "<b>InputField:</b> ACK Ping<br><b>Type:</b> Combo box/Text box<h3>ACK Ping</h3><div class=\"tooltipDiv\"><p>The TCP ACK ping is quite similar to the just-discussed SYN ping. The difference, as you could likely guess, is that the TCP ACK flag is set instead of the SYN flag. " +
                "Such an ACK packet purports to be acknowledging data over an established TCP connection, but no such connection exists. So remote hosts should always respond with a RST packet, disclosing their existence in the process.</p>" },
            { "profile-advancedscan", "<b>InputField:</b> Advanced Scan<br><b>Type:</b> Check box<h3>Aggressive Scan</h3><div class=\"tooltipDiv\"><p>This option enables additional advanced and aggressive options. Presently this enables OS detection (-O), version scanning (-sV), script scanning " +
                "(-sC) and traceroute (--traceroute). More features may be added in the future. The point is to enable a comprehensive set of scan options without people having to remember a large set of flags. However, because script scanning with the default set is considered intrusive, you " +
                "should not use -A against target networks without permission. This option only enables features, and not timing options (such as -T4) or verbosity options (-v) that you might want as well. Options which require privileges (e.g. root access) such as OS detection and traceroute will " +
                "only be enabled if those privileges are available.</p>"},
            { "profile-button", "<b>InputField:</b> Profile<br><b>Type:</b> Button<h3>Save as profile</h3><div class=\"tooltipDiv\"><p>Saves the current command displayed in the command box as new profile. The target host will be also set in case the target field it not empty.</p> <p>It will " +
              "open a new dialog, in which you can provide the name, the target, the command and the description of the new profile.</p>"},
            { "profile-debuglevel", "<b>InputField:</b> Debug level<br><b>Type:</b>Check box/Slider<h3>Debug level</h3><div class=\"tooltipDiv\"><p>When even verbose mode doesn't provide sufficient data for you, debugging is available to flood you with much more! As with the verbosity option (-v), " +
                "debugging is enabled with a command-line flag (-d) and the debug level can be increased by specifying it multiple times, as in -dd, or by setting a level directly. For example, -d9 sets level nine. That is the highest effective level and will produce thousands of lines unless you " +
                "run a very simple scan with very few ports and targets.</p>" },
            { "profile-decoys", "<b>InputField:</b> Decoys<br><b>Type:</b>Check box/Text box<h3>Decoys</h3><div class=\"tooltipDiv\"><p>Causes a decoy scan to be performed, which makes it appear to the remote host that the host(s) you specify as decoys are scanning the target network too. Thus " +
                "their IDS might report 5–10 port scans from unique IP addresses, but they won't know which IP was scanning them and which were innocent decoys. While this can be defeated through router path tracing, response-dropping, and other active mechanisms, it is generally an effective " +
                "technique for hiding your IP address.</p>" },
            { "profile-description", "<b>InputField:</b> Description<br><b>Type:</b>Text box<h3>Profile Description</h3><div class=\"tooltipDiv\"><p>Description of the profile up to a maximum length of 500MB. The description is stored in the profile table of the embedded database. "+
              "You can give any explaination of the profile here.</p>" },
            { "profile-disablearpping", "<b>InputField:</b> Disable ARP ping<br><b>Type:</b> Check Box<h3>Disable ARP Ping</h3><div class=\"tooltipDiv\"><p>Nmap normally does ARP or IPv6 Neighbor Discovery (ND) discovery of locally connected ethernet hosts, even if other host discovery options " +
                "such as -Pn or -PE are used. To disable this implicit behavior, use the --disable-arp-ping option. The default behavior is normally faster, but this option is useful on networks using proxy ARP, in which a router speculatively replies to all ARP requests, making every target appear " +
                "to be up according to ARP scan.</p>"},
            { "profile-dnsserver", "<b>InputField:</b> DNS servers<br><b>Type:</b> Check box/Text box<h3>DNS Server Selection</h3><div class=\"tooltipDiv\"><p>Comma separated list of DNS servers to use. Using multiple DNS servers is often faster, especially if you choose authoritative servers " +
                "for your target IP space. This option can also improve stealth, as your requests can be bounced off just about any recursive DNS server on the Internet</p>"},
            { "profile-fastscan", "<b>InputField:</b> Fast Scan<br><b>Type:</b> Check box<h3>Fast scan</h3><div class=\"tooltipDiv\"><p>Specifies that you wish to scan fewer ports than the default. Normally Nmap scans the most common 1,000 ports for each scanned protocol. With -F, this is reduced to 100.</p>" },
            { "profile-fragmentip", "<b>InputField:</b> Fragment packets<br><b>Type:</b> Check box<h3>Fragment packets</h3><div class=\"tooltipDiv\"><p>The -f option causes the requested scan (including host discovery scans) to use tiny fragmented IP packets. The idea is to split up the TCP header " +
                "over several packets to make it harder for packet filters, intrusion detection systems, and other annoyances to detect what you are doing. Be careful with this! Some programs have trouble handling these tiny packets.</p>" },
            { "profile-ftpbounce", "<b>InputField:</b> FTP Bounce<br><b>Type:</b> Combo box / Text Box<h3>FTP Bounce</h3><div class=\"tooltipDiv\"><p>Nmap supports FTP bounce scan with the -b option. It takes an argument of the form <username>:<password>@<server>:<port>. <Server> is the name or IP " +
                "address of a vulnerable FTP server. As with a normal URL, you may omit <username>:<password>, in which case anonymous login credentials (user: anonymous password:-wwwuser@) are used. The port number (and preceding colon) may be omitted as well, in which case the default FTP port (21) " +
                "on <server> is used.</p>" },
            { "profile-hostexclude", "<b>InputField:</b> Exclude hosts/networks<br><b>Type:</b> Combo box/Text Box<h3>Exclude hosts/networks</h3><div class=\"tooltipDiv\"><p>Specifies a comma-separated list of targets to be excluded from the scan even if they are part of the overall network range you " +
                "specify. The list you pass in uses normal Nmap syntax, so it can include hostnames, CIDR netblocks, octet ranges, etc. This can be useful when the network you wish to scan includes untouchable mission-critical servers, systems that are known to react adversely to port scans, or subnets " +
                "administered by other people.</p>" },
            { "profile-hostexcludefile", "<b>InputField:</b> Exclude file<br><b>Type:</b> Combo box/Text Box<h3>Exclude file</h3><div class=\"tooltipDiv\"><p>This offers the same functionality as the --exclude option, except that the excluded targets are provided in a newline-, space-, or tab-delimited " +
                "&lt;exclude_file&gt; rather than on the command line. The exclude file may contain comments that start with # and extend to the end of the line.</p>" },
            { "profile-hostgroup", "<b>InputField:</b> Host group<br><b>Type:</b> Combo box/Text Box<h3>Host group</h3><div class=\"tooltipDiv\"><p>Nmap has the ability to port scan or version scan multiple hosts in parallel. Nmap does this by dividing the target IP space into groups and then scanning one " +
                "group at a time. In general, larger groups are more efficient. The downside is that host results can't be provided until the whole group is finished. So if Nmap started out with a group size of 50, the user would not receive any reports (except for the updates offered in verbose mode) until " +
                "the first 50 hosts are completed.</p>" },
            { "profile-hosttimeout", "<b>InputField:</b> Host timeout<br><b>Type:</b> Combo box/Text Box<h3>Host timeout</h3><div class=\"tooltipDiv\"><p>Some hosts simply take a long time to scan. This may be due to poorly performing or unreliable networking hardware or software, packet rate limiting, or " +
                "a restrictive firewall. The slowest few percent of the scanned hosts can eat up a majority of the scan time. Sometimes it is best to cut your losses and skip those hosts initially. Specify --host-timeout with the maximum amount of time you are willing to wait. For example, specify 30m to " +
                "ensure that Nmap doesn't waste more than half an hour on a single host. Note that Nmap may be scanning other hosts at the same time during that half an hour, so it isn't a complete loss. A host that times out is skipped. No port table, OS detection, or version detection results are printed " +
                "for that host.</p>" },
            { "profile-icmpping", "<b>InputField:</b> ICMP Ping<br><b>Type:</b> Combo box<h3>ICMP Ping</h3><div class=\"tooltipDiv\"><p>In addition to the unusual TCP, UDP and SCTP host discovery types discussed previously, Nmap can send the standard packets sent by the ubiquitous ping program. Nmap " +
                "sends an ICMP type 8 (echo request) packet to the target IP addresses, expecting a type 0 (echo reply) in return from available hosts. Unfortunately for network explorers, many hosts and firewalls now block these packets, rather than responding as required by RFC 1122. For this reason, " +
                "ICMP-only scans are rarely reliable enough against unknown targets over the Internet. But for system administrators monitoring an internal network, they can be a practical and efficient approach. Use the -PE option to enable this echo request behavior.</p>" },
            { "profile-idlescan", "<b>InputField:</b> Idle Scan<br><b>Type:</b> Combo box/Text Box<h3>Idle Scan</h3><div class=\"tooltipDiv\"><p>This advanced scan method allows for a truly blind TCP port scan of the target (meaning no packets are sent to the target from your real IP address). " +
                "Instead, a unique side-channel attack exploits predictable IP fragmentation ID sequence generation on the zombie host to glean information about the open ports on the target. IDS systems will display the scan as coming from the zombie machine you specify (which must be up and meet " +
                "certain criteria). Full details of this fascinating scan type are in the section called “TCP Idle Scan (-sI)”.</p>" },
            { "profile-ignorerst", "<b>InputField:</b> Ignore RST<br><b>Type:</b> Check Box<h3>Ignore RST</h3><div class=\"tooltipDiv\"><p>In some cases, firewalls may spoof TCP reset (RST) replies in response to probes to unoccupied or disallowed addresses. Since Nmap ordinarily considers RST replies " +
                "to be proof that the target is up, this can lead to wasted time scanning targets that aren't there. Using the --discovery-ignore-rst will prevent Nmap from considering these replies during host discovery. You may need to select extra host discovery options to ensure you don't miss " +
                "targets in this case.</p>"},
            { "profile-interface", "<b>InputField:</b> Initerface<br><b>Type:</b> Check Box/Text box<h3>Interface</h3><div class=\"tooltipDiv\"><p>Tells Nmap what interface to send and receive packets on. Nmap should be able to detect this automatically, but it will tell you if it cannot.</p>"},
            { "profile-ipprotoping", "<b>InputField:</b> IP Protocol Ping<br><b>Type:</b> Combo box/Text Box<h3>IP Protocol Ping</h3><div class=\"tooltipDiv\"><p>One of the newer host discovery options is the IP protocol ping, which sends IP packets with the specified protocol number set in their IP " +
                "header. The protocol list takes the same format as do port lists in the previously discussed TCP, UDP and SCTP host discovery options. If no protocols are specified, the default is to send multiple IP packets for ICMP (protocol 1), IGMP (protocol 2), and IP-in-IP (protocol 4). The " +
                "default protocols can be configured at compile-time by changing DEFAULT_PROTO_PROBE_PORT_SPEC in nmap.h. Note that for the ICMP, IGMP, TCP (protocol 6), UDP (protocol 17) and SCTP (protocol 132), the packets are sent with the proper protocol headers while other protocols are sent with " +
                "no additional data beyond the IP header (unless any of --data, --data-string, or --data-length options are specified).</p>" },
            { "profile-ipv4ttl", "<b>InputField:</b> IPv4 TTL<br><b>Type:</b> Check Box/Text box<h3>Set IPv4 TTL</h3><div class=\"tooltipDiv\"><p>Sets the IPv4 time-to-live field in sent packets to the given value.</p>"},
            { "profile-ipv6", "<b>InputField:</b> IPv6 Support<br><b>Type:</b> Combo box<h3>IPv6 Support</h3><div class=\"tooltipDiv\"><p>Nmap has IPv6 support for its most popular features. Ping scanning, port scanning, version detection, and the Nmap Scripting Engine all support IPv6. The command " +
                "syntax is the same as usual except that you also add the -6 option. Of course, you must use IPv6 syntax if you specify an address rather than a hostname. An address might look like 3ffe:7501:4819:2000:210:f3ff:fe03:14d0, so hostnames are recommended. The output looks the same as usual, " +
                "with the IPv6 address on the “interesting ports” line being the only IPv6 giveaway.</p>" },
            { "profile-nodns", "<b>InputField:</b> No DNS resolution<br><b>Type:</b> Combo box<h3>No DNS resolution</h3><div class=\"tooltipDiv\"><p>Tells Nmap to always do reverse DNS resolution on the target IP addresses. Normally reverse DNS is only performed against responsive (online) hosts.</p>" },
            { "profile-noping", "<b>InputField:</b> No Ping<br><b>Type:</b> Combo box<h3>No Ping</h3><div class=\"tooltipDiv\"><p>This option skips the host discovery stage altogether. Normally, Nmap uses this stage to determine active machines for heavier scanning and to gauge the speed of the network. " +
                "By default, Nmap only performs heavy probing such as port scans, version detection, or OS detection against hosts that are found to be up. Disabling host discovery with -Pn causes Nmap to attempt the requested scanning functions against every target IP address specified. So if a /16 " +
                "sized network is specified on the command line, all 65,536 IP addresses are scanned. Proper host discovery is skipped as with the list scan, but instead of stopping and printing the target list, Nmap continues to perform requested functions as if each target IP is active. Default timing " +
                "parameters are used, which may result in slower scans. To skip host discovery and port scan, while still allowing NSE to run, use the two options -Pn -sn together.</p>" },
            { "profile-maxretries", "<b>InputField:</b> Max retries<br><b>Type:</b> Check box/Text ox<h3>Max. Retries</h3><div class=\"tooltipDiv\"><p>When Nmap receives no response to a port scan probe, it could mean the port is filtered. Or maybe the probe or response was simply lost on the network. It " +
                "is also possible that the target host has rate limiting enabled that temporarily blocked the response. So Nmap tries again by retransmitting the initial probe.</p>" },
            { "profile-operating-system", "<b>InputField:</b> Operating System<br><b>Type:</b> Check box<h3>Operating System</h3><div class=\"tooltipDiv\"><p>Enables OS detection, as discussed above. Alternatively, you can use -A to enable OS detection along with other things.</p>" },
            { "profile-outputappend", "<b>InputField:</b> Append output<br><b>Type:</b> Check box<h3>Append output</h3><div class=\"tooltipDiv\"><p>When you specify a filename to an output format flag such as -oX or -oN, that file is overwritten by default. If you prefer to keep the existing content of the " +
                "file and append the new results, specify the --append-output option. All output filenames specified in that Nmap execution will then be appended to rather than clobbered. This doesn't work well for XML (-oX) scan data as the resultant file generally won't parse properly until you fix it up " +
                "by hand.</p>" },
            { "profile-outputbase", "<b>InputField:</b> Output base<br><b>Type:</b> Check box/Text box<h3>Output base</h3><div class=\"tooltipDiv\"><p>As a convenience, you may specify -oA <basename> to store scan results in normal, XML, and grepable formats at once. They are stored in &lt;basename&gt;.nmap, " +
                "&lt;basename&gt;.xml, and &lt;basename&gt;.gnmap, respectively. As with most programs, you can prefix the filenames with a directory path, such as ~/nmaplogs/foocorp/ on Unix or c:\\hacking\\sco on Windows.</p>" },
            { "profile-outputgrep", "<b>InputField:</b> Grepable output<br><b>Type:</b> Check box/Text box<h3>Grepable output</h3><div class=\"tooltipDiv\"><p>This output format is covered last because it is deprecated. The XML output format is far more powerful, and is nearly as convenient for experienced " +
                "users. XML is a standard for which dozens of excellent parsers are available, while grepable output is my own simple hack. XML is extensible to support new Nmap features as they are released, while I often must omit those features from grepable output for lack of a place to put them.</p>" },
            { "profile-outputnormal", "<b>InputField:</b> Normal output<br><b>Type:</b> Check box/Text box<h3>Normal output</h3><div class=\"tooltipDiv\"><p>Requests that normal output be directed to the given filename. As discussed above, this differs slightly from interactive output.</p>" },
            { "profile-outputopenport", "<b>InputField:</b> Open ports<br><b>Type:</b> Check box<h3>Open ports</h3><div class=\"tooltipDiv\"><p>Sometimes you only care about ports you can actually connect to (open ones), and don't want results cluttered with closed, filtered, and closed|filtered ports. Output " +
                "customization is normally done after the scan using tools such as grep, awk, and Perl, but this feature was added due to overwhelming requests.</p>" },
            { "profile-outputreason", "<b>InputField:</b> Reason<br><b>Type:</b> Check box<h3>Reason</h3><div class=\"tooltipDiv\"><p>Shows the reason each port is set to a specific state and the reason each host is up or down. This option displays the type of the packet that determined a port or hosts state. "+
                "For example, A RST packet from a closed port or an echo reply from an alive host. The information Nmap can provide is determined by the type of scan or ping.</p>" },
            { "profile-outputresume", "<b>InputField:</b> Output resume<br><b>Type:</b> Check box/Text box<h3>Output resume</h3><div class=\"tooltipDiv\"><p>Some extensive Nmap runs take a very long time—on the order of days. Such scans don't always run to completion. Restrictions may prevent Nmap from being " +
                "run during working hours, the network could go down, the machine Nmap is running on might suffer a planned or unplanned reboot, or Nmap itself could crash. The administrator running Nmap could cancel it for any other reason as well, by pressing ctrl-C.</p>" },
            { "profile-outputscript", "<b>InputField:</b> Script output<br><b>Type:</b> Check box/Text box<h3>Script output</h3><div class=\"tooltipDiv\"><p>Script kiddie output is like interactive output, except that it is post-processed to better suit the l33t HaXXorZ who previously looked down on Nmap " +
                "due to its consistent capitalization and spelling. Humor impaired people should note that this option is making fun of the script kiddies before flaming me for supposedly 'helping them'.</p>" },
            { "profile-outputxml", "<b>InputField:</b> XML output<br><b>Type:</b> Check box/Text box<h3>XML output</h3><div class=\"tooltipDiv\"><p>Requests that XML output be directed to the given filename. Nmap includes a document type definition (DTD) which allows XML parsers to validate Nmap XML output. "+
                "While it is primarily intended for programmatic use, it can also help humans interpret Nmap XML output.</p>" },
            { "profile-packettrace", "<b>InputField:</b> Packet trace<br><b>Type:</b> Check box<h3>Packet trace</h3><div class=\"tooltipDiv\"><p>Causes Nmap to print a summary of every packet sent or received. This is often used for debugging, but is also a valuable way for new users to understand exactly what " +
                "Nmap is doing under the covers. To avoid printing thousands of lines, you may want to specify a limited number of ports to scan, such as -p20-30. If you only care about the goings on of the version detection subsystem, use --version-trace instead. If you only care about script tracing, specify " +
                "--script-trace. With --packet-trace, you get all of the above.</p>" },            
            { "profile-parallelism", "<b>InputField:</b> Parallelism<br><b>Type:</b> Combo box/Text Box<h3>Parallelism</h3><div class=\"tooltipDiv\"><p>These options control the total number of probes that may be outstanding for a host group. They are used for port scanning and host discovery. By default, " +
                "Nmap calculates an ever-changing ideal parallelism based on network performance. If packets are being dropped, Nmap slows down and allows fewer outstanding probes. The ideal probe number slowly rises as the network proves itself worthy. These options place minimum or maximum bounds on that " +
                "variable. By default, the ideal parallelism can drop to one if the network proves unreliable and rise to several hundred in perfect conditions.</p>" },
            { "profile-portstoscan", "<b>InputField:</b> Ports to scan<br><b>Type:</b> Combo box/Text Box<h3>Ports to scan</h3><div class=\"tooltipDiv\"><p>This option specifies which ports you want to scan and overrides the default. Individual port numbers are OK, as are ranges separated by a hyphen " +
                "(e.g. 1-1023). The beginning and/or end values of a range may be omitted, causing Nmap to use 1 and 65535, respectively. So you can specify -p- to scan ports from 1 through 65535. Scanning port zero is allowed if you specify it explicitly. For IP protocol scanning (-sO), this option " +
                "specifies the protocol numbers you wish to scan for (0–255).</p>" },
            { "profile-randomport", "<b>InputField:</b> Disable randomize port<br><b>Type:</b> Check box<h3>Disable randomize ports</h3><div class=\"tooltipDiv\"><p>By default, Nmap randomizes the scanned port order (except that certain commonly accessible ports are moved near the beginning for efficiency " +
                "reasons). This randomization is normally desirable, but you can specify -r for sequential (sorted from lowest to highest) port scanning instead.</p>" },
            { "profile-resolveall", "<b>InputField:</b> SCTP Ping<br><b>Type:</b> Combo box/Text box<h3>SCTP Ping</h3><div class=\"tooltipDiv\"><p>If a hostname target resolves to more than one address, scan all of them. The default behavior is to only scan the first resolved address. " +
                "Regardless, only addresses in the appropriate address family will be scanned: IPv4 by default, IPv6 with -6.</p>" },
            { "profile-rtttimeout", "<b>InputField:</b> RTT timeout<br><b>Type:</b> Combo box/Text Box<h3>RTT timeout</h3><div class=\"tooltipDiv\"><p>Nmap maintains a running timeout value for determining how long it will wait for a probe response before giving up or retransmitting the probe. This is " +
                "calculated based on the response times of previous probes. The exact formula is given in the section called “Scan Code and Algorithms”. If the network latency shows itself to be significant and variable, this timeout can grow to several seconds. It also starts at a conservative (high) " +
                "level and may stay that way for a while when Nmap scans unresponsive hosts.</p>" },
            { "profile-scandelay", "<b>InputField:</b> Scan Delay<br><b>Type:</b> Combo box/Text Box<h3>Scan delay</h3><div class=\"tooltipDiv\"><p>This option causes Nmap to wait at least the given amount of time between each probe it sends to a given host. This is particularly useful in the case of rate " +
                "limiting. Solaris machines (among many others) will usually respond to UDP scan probe packets with only one ICMP message per second. Any more than that sent by Nmap will be wasteful. A --scan-delay of 1s will keep Nmap at that slow rate. Nmap tries to detect rate limiting and adjust the " +
                "scan delay accordingly, but it doesn't hurt to specify it explicitly if you already know what rate works best.</p>" },
            { "profile-scanrandom", "<b>InputField:</b> Scan Random Hosts<br><b>Type:</b> Combo box/Text Box<h3>Scan random hosts</h3><div class=\"tooltipDiv\"><p>Reads target specifications from <inputfilename>. Passing a huge list of hosts is often awkward on the command line, yet it is a common desire. " +
                "For example, your DHCP server might export a list of 10,000 current leases that you wish to scan. Or maybe you want to scan all IP addresses except for those to locate hosts using unauthorized static IP addresses. Simply generate the list of hosts to scan and pass that filename to Nmap as " +
                "an argument to the -iL option. Entries can be in any of the formats accepted by Nmap on the command line (IP address, hostname, CIDR, IPv6, or octet ranges). Each entry must be separated by one or more spaces, tabs, or newlines. You can specify a hyphen (-) as the filename if you want Nmap " +
                "to read hosts from standard input rather than an actual file.</p>" },
            { "profile-sctpping", "<b>InputField:</b> SYN Ping<br><b>Type:</b> Combo box/Text box<h3>SYN Ping</h3><div class=\"tooltipDiv\"><p>This option sends an SCTP packet containing a minimal INIT chunk. The default destination port is 80 (configurable at compile time by changing " +
                "DEFAULT_SCTP_PROBE_PORT_SPEC in nmap.h). Alternate ports can be specified as a parameter. The </p>" },
            { "profile-scriptlist", "<b>InputField:</b> Script list<br><b>Type:</b> List box<h3>Script list</h3><div class=\"tooltipDiv\"><p>List of NSE scripts to apply to the scan. Several scripts can be selected. For a detailed description of the particular NSE script click on the info button on the right side.</p>" },
            { "profile-scriptargumentlist", "<b>InputField:</b> Script argument list<br><b>Type:</b> Data grid<h3>Script argument list</h3><div class=\"tooltipDiv\"><p>List of NSE script arguments to apply to the script. If the value is not changed the default value as deinfed by the script is taken.</p>" },
            { "profile-sourceip", "<b>InputField:</b> Source IP<br><b>Type:</b> Check box/Tex box<h3>Source IP</h3><div class=\"tooltipDiv\"><p>In some circumstances, Nmap may not be able to determine your source address (Nmap will tell you if this is the case). In this situation, use -S with the IP address " +
                "of the interface you wish to send packets through.</p>" },
            { "profile-sourceport", "<b>InputField:</b> Source port<br><b>Type:</b> Check box/Tex box<h3>Source port</h3><div class=\"tooltipDiv\"><p>One surprisingly common misconfiguration is to trust traffic based only on the source port number. It is easy to understand how this comes about. An administrator " +
                "will set up a shiny new firewall, only to be flooded with complaints from ungrateful users whose applications stopped working. In particular, DNS may be broken because the UDP DNS replies from external servers can no longer enter the network. FTP is another common example. In active FTP transfers, " +
                "the remote server tries to establish a connection back to the client to transfer the requested file.</p>" },
            { "profile-synping", "<b>InputField:</b> SYN Ping<br><b>Type:</b> Combo box/Text box<h3>SYN Ping</h3><div class=\"tooltipDiv\"><p>This option sends an empty TCP packet with the SYN flag set. The default destination port is 80 (configurable at compile time by changing " +
                "DEFAULT_TCP_PROBE_PORT_SPEC in nmap.h). Alternate ports can be specified as a parameter. The syntax is the same as for the -p except that port type specifiers like T: are not allowed. Examples are -PS22 and -PS22-25,80,113,1050,35000. Note that there can be no space between -PS and " +
                "the port list. If multiple probes are specified they will be sent in parallel.</p>" },
            { "profile-systemdns", "<b>InputField:</b> System DNS<br><b>Type:</b> Check box<h3>System DNS Selection</h3><div class=\"tooltipDiv\"><p>By default, Nmap reverse-resolves IP addresses by sending queries directly to the name servers configured on your host and then listening " +
                "for responses. Many requests (often dozens) are performed in parallel to improve performance. Specify this option to use your system resolver instead (one IP at a time via the getnameinfo call). This is slower and rarely useful unless you find a bug in the Nmap parallel " +
                "resolver (please let us know if you do). The system resolver is always used for forward lookups (getting an IP address from a hostname).</p>"},
            { "profile-targetlistfile", "<b>InputField:</b> Target list file<br><b>Type:</b> Check box/Text box<h3>Target list file</h3><div class=\"tooltipDiv\"><p>Reads target specifications from <inputfilename>. Passing a huge list of hosts is often awkward on the command line, yet it is a common " +
                "desire. For example, your DHCP server might export a list of 10,000 current leases that you wish to scan. Or maybe you want to scan all IP addresses except for those to locate hosts using unauthorized static IP addresses.Simply generate the list of hosts to scan and pass that filename " +
                "to Nmap as an argument to the -iL option. </p>"},
            { "profile-tcpscans", "<b>InputField:</b> TCP Scans<br><b>Type:</b> Combo box<h3>TCP Scans Selection</h3><div class=\"tooltipDiv\"><p>Because host discovery needs are so diverse, Nmap offers a wide variety of options for customizing the techniques used. Host discovery is sometimes " +
                "called ping scan, but it goes well beyond the simple ICMP echo request packets associated with the ubiquitous ping tool. Users can skip the discovery step entirely with a list scan (-sL) or by disabling host discovery (-Pn), or engage the network with arbitrary combinations of " +
                "multi-port TCP SYN/ACK, UDP, SCTP INIT and ICMP probes. The goal of these probes is to solicit responses which demonstrate that an IP address is actually active (is being used by a host or network device). On many networks, only a small percentage of IP addresses are active at any " +
                "given time. This is particularly common with private address space such as 10.0.0.0/8. That network has 16 million IPs, but I have seen it used by companies with less than a thousand machines. Host discovery can find those machines in a sparsely allocated sea of IP addresses.</p>" },
            { "profile-timing", "<b>InputField:</b> Timing<br><b>Type:</b> Combo box<h3>Timing and Performance</h3><div class=\"tooltipDiv\"><p>One of my highest Nmap development priorities has always been performance. A default scan (nmap <hostname>) of a host on my local network takes a fifth of " +
                "a second. That is barely enough time to blink, but adds up when you are scanning hundreds or thousands of hosts. Moreover, certain scan options such as UDP scanning and version detection can increase scan times substantially. So can certain firewall configurations, particularly " +
                "response rate limiting. While Nmap utilizes parallelism and many advanced algorithms to accelerate these scans, the user has ultimate control over how Nmap runs. Expert users carefully craft Nmap commands to obtain only the information they care about while meeting their time constraints.</p>" },
            { "profile-traceroute", "<b>InputField:</b> Traceroute<br><b>Type:</b> Check box<h3>Traceroute</h3><div class=\"tooltipDiv\"><p>Traceroutes are performed post-scan using information from the scan results to determine the port and protocol most likely to reach the target. It works with all " +
                "scan types except connect scans (-sT) and idle scans (-sI). All traces use Nmap's dynamic timing model and are performed in parallel.</p>" },
            { "profile-udpping", "<b>InputField:</b> UDP Ping<br><b>Type:</b> Combo box/Text Box<h3>UDP Ping</h3><div class=\"tooltipDiv\"><p>Another host discovery option is the UDP ping, which sends a UDP packet to the given ports. For most ports, the packet will be empty, though some use a " +
                "protocol-specific payload that is more likely to elicit a response. See the section called “UDP payloads: nmap-payloads” for a description of the database of payloads. Packet content can also be affected with the --data, --data-string, and --data-length options.</p>" },
            { "profile-verbositylevel", "<b>InputField:</b> Verbosity level<br><b>Type:</b>Check box/Slider<h3>Verbosity level</h3><div class=\"tooltipDiv\"><p>Increases the verbosity level, causing Nmap to print more information about the scan in progress. Open ports are shown as they are found and " +
                "completion time estimates are provided when Nmap thinks a scan will take more than a few minutes. Use it twice or more for even greater verbosity: -vv, or give a verbosity level directly, for example -v3.</p>" },
            { "profile-versiondetection", "<b>InputField:</b> Version Detection<br><b>Type:</b> Combo box<h3>Version detection</h3><div class=\"tooltipDiv\"><p>Aktiviert die Versionserkennung von Services. Alternativ dazu können Sie -A benutzen, was unter anderem auch die Versionserkennung aktiviert.</p>" },
            { "target", "<b>InputField:</b> Target<br><b>Type:</b> Network field<h3>Target Selection</h3><div class=\"tooltipDiv\"><p>Everything on the Nmap command-line that isn't an option (or option argument) is treated as a target host specification. The simplest " +
              "case is to specify a target IP address or hostname for scanning.</p><p>When a hostname is given as a target, it is resolved via the Domain Name System (DNS) to determine the IP address to scan. If the name resolves to more than one IP address, only the first one will be scanned. " +
              "To make Nmap scan all the resolved addresses instead of only the first one, use the --resolve-all option.</p>" },
            { "scan-button", "<b>InputField:</b> Scan<br><b>Type:</b> Button.<h3>Start a scan</h3><div class=\"tooltipDiv\"><p>The scan button starts a new nmap process. The process runs as background thread and its output can be seen by opening the corresponding row in the scan table.</p>"+
              "<p>In the scan table are currently running background scans, as well as all previously started nmap scans.</p>" },
            { "scan-delete", "<b>InputField:</b> Scan delete<br><b>Type:</b> Button<h3>Delete a scan</h3><div class=\"tooltipDiv\"><p>Deletes the selected scan from the scan list and removes it from the database.</p>"},
            { "scan-details", "<b>InputField:</b> Scan details<br><b>Type:</b> Button.<h3>Open scan details</h3><div class=\"tooltipDiv\"><p>Options the scan details dialog. The scan details dialog is modal and shows all details of the selected scan.</p><p>This is equivalent to double clicking " +
              "on the scan table row.</p>"},
            { "scan-output-full", "<b>InputField:</b> Scan output full<br><b>Type:</b> Button<h3>Full output dialog</h3><div class=\"tooltipDiv\"><p>Opens the output dialog, which shows the output in a bigger independent window.</p>" },
            { "scan-output-xml", "<b>InputField:</b> XML output<br><b>Type:</b> Text Box<h3>XML output</h3><div class=\"tooltipDiv\"><p>This text box show the output in XML format from the nmap command. Per default the XML output options will be appended to the resulting command, in order to " +
              "parse is more easily.</p><p>The output is always scrolled to the end of the window. To stop automatic scolling switch off the automatic scrolling using the toggle button in the toolbar.</p>" },
            { "scan-table", "<b>InputField:</b> Scan table<br><b>Type:</b> Data grid<h3>Scan Table</h3><div class=\"tooltipDiv\"><p>Each currently running and previously executed nmap command is represented by a row in the scan table. The table shows the following columns:</p> "+
              "<ul> <li><b>ID:</b> Primary key in the embedded database.</li><li><b>Name:</b> Name of the scan or profile.</li><li><b>Target:</b> Target specification.</li><li><b>Start:</b> Start date/time of the background task.</li><li><b>End:</b> End date/time of the background task.</li> "+
              "<li><b>Uptime:</b> Total running time background task in format days.hours:minutes:seconds.millis.</li><li><b>Status:</b> Current status of the nmap background task.</li><li><b>Buttons:</b> Command buttons, currently, 'Open details' and 'Delete scan'.</li></ul> "+
              "<p>Using the 'Open Details' button on the right most column, will open the details dialog for the selected scan. If the scan is still running the output of the nmap command line will be displayed in the output field. It will be updated a s soon as new output lines are produces "+
              "by nmap.</p><p>Using the 'Delete scan' button will stop the current running scan (in case it is still running) and will delete the scan from the embedded database.</p>" },
            { "scan-table-autoUpdate", "<b>InputField:</b> Scan table auto-update<br><b>Type:</b> Toolbar toggle button.<h3>Configure auto-update</h3><div class=\"tooltipDiv\"><p>If set the table will be automatically updated after a period of time. The update interval can be set in the settings dialog." },
            { "scan-table-deleteall", "<b>InputField:</b> Scan table delete all.<br><b>Type:</b> Toolbar button.<h3>Delete all scans</h3><div class=\"tooltipDiv\"><p>Deletes all currently listed scan in the embedded database scan table. This can't be undone.</p>" },
            { "scan-table-refresh", "<b>InputField:</b> Scan table refresh.<br><b>Type:</b> Toolbar button.<h3>Scan table refresh</h3><div class=\"tooltipDiv\"><p>Each currently running and previously executed nmap command is represented by a row in the scan table. Refreshing the scan list can "+
              "be done using the refresh button in the toolbar.This will reload all data from the embedded database scan table.</p>" },
            { "scan-table-scroll", "<b>InputField:</b> Scan table scroll<br><b>Type:</b> Toolbar toggle button.<h3>Scroll scan table</h3><div class=\"tooltipDiv\"><p>If set, the scroll table will be automatically scrolled to the end of the table." },
            { "script-addargument", "<b>InputField:</b> Add argument<br><b>Type:</b> Button<h3>Add argument dialog</h3><div class=\"tooltipDiv\"><p>Opens the add argument dialog, in which you can define and add a script argument.</p>" },
            { "script-addlibrary", "<b>InputField:</b> Add library<br><b>Type:</b> Button<h3>Add library dialog</h3><div class=\"tooltipDiv\"><p>Opens the add library dialog, in which you can define and add a script library.</p>" },
            { "script-arguments", "<b>InputField:</b> Arguments<br><b>Type:</b> DataGrid<h3>List of script arguments</h3><div class=\"tooltipDiv\"><p>Shows the list of all script arguments.</p>" },
            { "script-argumentgroup", "<b>InputField:</b> Argument group<br><b>Type:</b> Combo box<h3>Name of the argument group</h3><div class=\"tooltipDiv\"><p>Argument groups collect several argument, which will be applied to the script as a whole.</p>" },
            { "script-argumentname", "<b>InputField:</b> Argument name<br><b>Type:</b> Text box<h3>Name of the argument</h3><div class=\"tooltipDiv\"><p>The name of the argument.</p>" },
            { "script-argumentdescription", "<b>InputField:</b> Argument description<br><b>Type:</b> Text box<h3>Description of the argument</h3><div class=\"tooltipDiv\"><p>The description of the argument, sometimes together with the default value.</p>" },
            { "script-author", "<b>InputField:</b> Script author<br><b>Type:</b> Text box<h3>Script author</h3><div class=\"tooltipDiv\"><p>The author of the script.</p>" },
            { "script-category", "<b>InputField:</b> Categories<br><b>Type:</b> List<h3>List of script categories</h3><div class=\"tooltipDiv\"><p>Shows the list of all categories. A script can have several categories.</p>" },
            { "script-code", "<b>InputField:</b> Script source code<br><b>Type:</b> Text box<h3>Script source code</h3><div class=\"tooltipDiv\"><p>Source code of the script.</p>" },
            { "script-defaultvalue", "<b>InputField:</b> Default value<br><b>Type:</b> Text box<h3>Default value</h3><div class=\"tooltipDiv\"><p>Default value of the argument, which will be applied if the argument is not listed as script argument.</p>" },
            { "script-delete", "<b>InputField:</b> Delete script<br><b>Type:</b> Toolbar button<h3>Deletes the script</h3><div class=\"tooltipDiv\"><p>Deletes the script from the embedded database.</p>" },
            { "script-deleteargument", "<b>InputField:</b> Delete argument<br><b>Type:</b> Button<h3>Delete an argument dialog</h3><div class=\"tooltipDiv\"><p>Deletes the argument from the list of all script script arguments.</p>" },
            { "script-details", "<b>InputField:</b> Script details<br><b>Type:</b> Button<h3>Open the script details</h3><div class=\"tooltipDiv\"><p>Opens the script details dialog.</p>" },
            { "script-description", "<b>InputField:</b> Script description<br><b>Type:</b> Text box<h3>Script description</h3><div class=\"tooltipDiv\"><p>Description of the script up to a length of 4000 characters.</p>" },
            { "script-editargument", "<b>InputField:</b> Edit argument<br><b>Type:</b> Button<h3>Edit argument dialog</h3><div class=\"tooltipDiv\"><p>Opens the edit argument dialog, in which you can define and add a script argument.</p>" },
            { "script-first", "<b>InputField:</b> First<br><b>Type:</b> Toolbar button<h3>Goto the first script</h3><div class=\"tooltipDiv\"><p>Goes to the first script in the list of all scripts. The scripts are alphabetically ordered.</p>" },
            { "script-html", "<b>InputField:</b> Script code<br><b>Type:</b> Text Box<h3>Code of the LUA script</h3><div class=\"tooltipDiv\"><p>HTTP link of the LUA code of the script. This will show the source code of the script on the nmap scripting engine site.</p>" },
            { "script-id", "<b>InputField:</b> Script ID<br><b>Type:</b> Text box<h3>ID of the script</h3><div class=\"tooltipDiv\"><p>ID of the script, which is also the primary key in the embedded database.</p>" },
            { "script-last", "<b>InputField:</b> Last<br><b>Type:</b> Toolbar button<h3>Goto the last script</h3><div class=\"tooltipDiv\"><p>Goes to the last script in the list of all scripts. The scripts are alphabetically ordered.</p>" },
            { "script-license", "<b>InputField:</b> Script license<br><b>Type:</b> Text box<h3>Script license</h3><div class=\"tooltipDiv\"><p>The license of the script.</p>" },
            { "script-list", "<b>InputField:</b> Script list<br><b>Type:</b> Datagrid<h3>Scripts list</h3><div class=\"tooltipDiv\"><p>Lists all scripts currently installed. The scripts are alphabetically ordered.</p>" },
            { "script-name", "<b>InputField:</b> Script name<br><b>Type:</b> Text Box<h3>Name of the script</h3><div class=\"tooltipDiv\"><p>Name of the script. This can any name up to a length of 255 characters.</p>" },
            { "script-new", "<b>InputField:</b> New script<br><b>Type:</b> Toolbar button<h3>Opens the add script dialog</h3><div class=\"tooltipDiv\"><p>Opens the add script dialog.</p>" },
            { "script-next", "<b>InputField:</b> Next<br><b>Type:</b> Toolbar button<h3>Goto the next script</h3><div class=\"tooltipDiv\"><p>Goes to the next script in the list of all scripts. The scripts are alphabetically ordered.</p>" },
            { "script-output", "<b>InputField:</b> Script output<br><b>Type:</b> Text box<h3>Example output</h3><div class=\"tooltipDiv\"><p>Example output of the script.</p>" },
            { "script-previous", "<b>InputField:</b> Previous<br><b>Type:</b> Toolbar button<h3>Goto the previous script</h3><div class=\"tooltipDiv\"><p>Goes to the previous script in the list of all scripts. The scripts are alphabetically ordered.</p>" },
            { "script-requires", "<b>InputField:</b> Script requirements<br><b>Type:</b> Text box<h3>Script requirements</h3><div class=\"tooltipDiv\"><p>The list of requirements in form of libraries of the script.</p>" },
            { "script-save", "<b>InputField:</b> Save<br><b>Type:</b> Button<h3>Save the script</h3><div class=\"tooltipDiv\"><p>Saves the script in the embedded database and closes the script details dialog.</p>" },
            { "script-type", "<b>InputField:</b> Types<br><b>Type:</b> List<h3>List of types</h3><div class=\"tooltipDiv\"><p>Shows the list of all script types. A script can have several types.</p>" },
            { "script-usage", "<b>InputField:</b> Script usage<br><b>Type:</b> Text box<h3>Usage of the script</h3><div class=\"tooltipDiv\"><p>Example usage of the script.</p>" },
            { "script-write", "<b>InputField:</b> Write scripts<br><b>Type:</b> Toolbar button<h3>Writes new help files</h3><div class=\"tooltipDiv\"><p>Write new help files to the nmap help directory.</p>" },
            { "settings-menu-item", "<b>InputField:</b> Settings <br><b>Type:</b> Menu Item<h3>Open the settings dialog</h3><div class=\"tooltipDiv\"><p>Opens the settings dialog. In the settings you can change some of the settings for NmapWin, like default values, localization, etc."},
        };
        #endregion
    }
}
