﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NmapWinUI.Utils
{
    public class JsonWriter : AbstractProgressDialog
    {
        #region Fields
        private static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CustomResolver(),
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
        };
        /// <summary>
        /// JSON serializer
        /// </summary>
        private readonly JsonSerializer JsonSerializer;
        #endregion

        #region Constructor
        public JsonWriter()
        {
            JsonSerializer = new JsonSerializer
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CustomResolver()
            };
        }
        #endregion

        /// <summary>
        /// Write JSON file asynchronously.
        /// </summary>
        public override void AsyncTasks()
        {
            SetMainValues("Exporting to JSON...", 17, 0);
            IncMainValues("Creating directory...");
            EnsureDirectory();

            IncMainValues("Exporting scans...");
            WriteBackupFile("scans", DatabaseScan.Instance.FindScans());

            IncMainValues("Exporting hosts...");
            WriteBackupFile("hosts", DatabaseHost.Instance.FindHostsFull());

            IncMainValues("Exporting hostnames...");
            WriteBackupFile("hostNames", DatabaseHostName.Instance.FindHostNames());

            IncMainValues("Exporting adddresses...");
            WriteBackupFile("addresses", DatabaseAddress.Instance.FindAddresses());

            IncMainValues("Exporting OSs...");
            WriteBackupFile("osses", DatabaseOs.Instance.FindOperatingSystems());

            IncMainValues("Exporting OSMatches...");
            WriteBackupFile("osMatches", DatabaseOsMatch.Instance.FindOsMatches());

            IncMainValues("Exporting OSClasses...");
            WriteBackupFile("osClasses", DatabaseOsClass.Instance.FindOsClasses());

            IncMainValues("Exporting CPEs...");
            WriteBackupFile("cpes", DatabaseCpe.Instance.FindCpes());

            IncMainValues("Exporting Ports...");
            WriteBackupFile("ports", DatabasePort.Instance.FindPorts());

            IncMainValues("Exporting Hops...");
            WriteBackupFile("hops", DatabaseHop.Instance.FindHops());

            IncMainValues("Exporting Services...");
            WriteBackupFile("services", DatabaseService.Instance.FindServices());

            IncMainValues("Exporting Traceroutes...");
            WriteBackupFile("traceroutes", DatabaseTraceRoute.Instance.FindTraceRoutes());

            IncMainValues("Exporting Jobs...");
            WriteBackupFile("jobs", DatabaseJob.Instance.FindJobs());

            IncMainValues("Exporting scripts...");
            WriteBackupFile1("scripts", DatabaseScript.Instance.FindScripts());

            IncMainValues("Exporting options...");
            WriteBackupFile("options", DatabaseOption.Instance.FindOptions());

            IncMainValues("Exporting profiles...");
            WriteBackupFile("profiles", DatabaseProfile.Instance.FindProfiles());
        }

        /// <summary>
        /// Creates the backup directory
        /// </summary>
        private void EnsureDirectory()
        {
            Directory.CreateDirectory(Constants.BackupDirectory);
        }

        public static void WriteScriptsFileData()
        {
            string filename = Path.Combine(Constants.BaseDirectory, "data", "scripts.json");
            List<Script> scripts = DatabaseScript.Instance.FindScripts();
            string json = JsonConvert.SerializeObject(scripts.ToArray(), Formatting.None, JsonSerializerSettings);
            File.WriteAllText(filename, json);
        }

        private void WriteBackupFile<T>(string file, List<T> list)
        {
            string filename = Path.Combine(Constants.BackupDirectory, file + "-" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".json");
            JsonTextWriter jsonWriter = new JsonTextWriter(new StreamWriter(filename));

            SetDetailedValues("Exporting " + file + "...", list.Count, 0);

            jsonWriter.WriteStartArray();
            list.ForEach(i =>
            {
                JsonSerializer.Serialize(jsonWriter, i);
                IncDetailedValues("Exporting item...");
            });
            jsonWriter.WriteEndArray();
            jsonWriter.Close();

        }

        private void WriteBackupFile1<T>(string file, List<T> list) where T : IIdentifier
        {
            string filename = Path.Combine(Constants.BackupDirectory, file + "-" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".json");
            JsonTextWriter jsonWriter = new JsonTextWriter(new StreamWriter(filename));

            SetDetailedValues("Exporting " + file + "...", list.Count, 0);

            jsonWriter.WriteStartArray();
            list.ForEach(i =>
            {
                JsonSerializer.Serialize(jsonWriter, i);
                IncDetailedValues("Exporting item '" + i.Identifier + "'...");
            });
            jsonWriter.WriteEndArray();
            jsonWriter.Close();

        }
    }

    class CustomResolver : DefaultContractResolver
    {
        private readonly List<string> _namesOfVirtualPropsToKeep = new List<string>(new String[] { });

        public CustomResolver() { }

        public CustomResolver(IEnumerable<string> namesOfVirtualPropsToKeep)
        {
            this._namesOfVirtualPropsToKeep = namesOfVirtualPropsToKeep.Select(x => x.ToLower()).ToList();
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);
            var propInfo = member as PropertyInfo;
            if (propInfo != null)
            {
                if (propInfo.GetMethod.IsVirtual && !propInfo.GetMethod.IsFinal && !_namesOfVirtualPropsToKeep.Contains(propInfo.Name.ToLower()))
                {
                    prop.ShouldSerialize = obj => false;
                }
            }
            return prop;
        }
    }
}
