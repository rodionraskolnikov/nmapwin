﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.Windows;
using System.Windows.Controls;

namespace NmapWinUI.Utils
{
    public class ScrollProperty
    {
        private static bool MustAutoscroll = true;
        public static bool GetAutoScrollToEnd(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoScrollToEndProperty);
        }

        public static void SetAutoScrollToEnd(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoScrollToEndProperty, value);
        }

        // Using a DependencyProperty as the backing store for AutoScrollToEnd.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoScrollToEndProperty =
        DependencyProperty.RegisterAttached("AutoScrollToEnd", typeof(bool), typeof(ScrollProperty), new PropertyMetadata(false, AutoScrollToEndPropertyChanged));

        private static void AutoScrollToEndPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox textbox)
            {
                MustAutoscroll = (bool)e.NewValue;
                if (e.NewValue is bool mustAutoScroll && mustAutoScroll)
                {
                    textbox.TextChanged += (s, ee) => AutoScrollToEnd(s, ee, textbox);
                }
                if (MustAutoscroll)
                {
                    textbox.ScrollToEnd();
                }
                else
                {
                    textbox.ScrollToHome();
                }
            }
        }

        private static void AutoScrollToEnd(object sender, TextChangedEventArgs e, TextBox textbox)
        {
            if (MustAutoscroll)
                textbox.ScrollToEnd();
        }
    }
}
