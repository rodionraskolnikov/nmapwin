﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace NmapWinUI.Utils
{
    public enum Architectures
    {
        X86 = 0,
        X64 = 1,
        Arm = 2,
        Arm64 = 3,
        Wasm = 4
    }

    public class ApplicationVersion
    {
        public static string Version
        {
            get
            {
                return FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion;
            }
        }

        public static string VersionShort
        {
            get
            {
                string version = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion;
                int index = GetNthIndex(version, '.', 2);
                return version.Substring(0, index);
            }
        }

        public static string Name
        {
            get
            {
                return FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductName;
            }
        }

        public static string Author
        {
            get
            {
                return FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).CompanyName;
            }
        }

        public static long LinkerTime
        {
            get
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                string format = "dd/MM/yyyy H:mm:ss,ff";
                string dateTimeString = Properties.Resources.BuildDate.Trim();
                DateTime dateTime = DateTime.ParseExact(dateTimeString, format, provider);
                return dateTime.Ticks;
            }
        }

        /// <summary>
        /// Get currently running architecture.
        /// </summary>
        /// <returns>current architecture as string.</returns>
        public static string GetArchitecture() => RuntimeInformation.ProcessArchitecture.ToString();

        /// <summary>
        /// Check the currrent version against the supplied version.
        /// </summary>
        /// <param name="version">version to compare to</param>
        /// <returns>0 if equals, -1 if supplied version < current version and 1 if current version > supplied version.</current></returns>
        public static bool CheckVersion(string version)
        {
            string oldVersionString = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion;
            Version oldVersion = System.Version.Parse(oldVersionString);
            Version newVersion = System.Version.Parse(version);
            return newVersion.CompareTo(oldVersion) > 0;
        }

        #region Private methods
        private static int GetNthIndex(string s, char t, int n)
        {
            return s.TakeWhile(c => (n -= (c == t ? 1 : 0)) > 0).Count();
        }
        #endregion
    }
}
