﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinUI.Utils;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace NmapWinUI
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(SplashScreen));

        public string AppTitle { get; set; } = "NmapWin v" + ApplicationVersion.VersionShort;

        public int SplashWidth { get; set; } = 480;

        public SplashScreen()
        {
            InitializeComponent();

            DataContext = this;

            Loaded += WindowLoaded;
        }        

        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            ProgressGrid.Visibility = Visibility.Visible;
            await Task.Run(() =>
            {
                MainStatusLabel.Dispatcher.Invoke(() => ProgressGrid.Visibility = Visibility.Visible);

                DatabaseTools databaseTools = new DatabaseTools();
                databaseTools.DatabaseEvent += HandleDatabaseChanges;
                if(!databaseTools.UpgradeDatabase())
                    Dispatcher.Invoke((Action)(() => Application.Current.Shutdown()));
            });

            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SplashScreenReady"));
        }

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;

        private void HandleDatabaseChanges(object sender, EventArgs e)
        {
            ProgressEventArgs args = e as ProgressEventArgs;
            MainStatusLabel.Dispatcher.Invoke(() =>
            {

                MainStatusLabel.Content = args.MainMessage ?? MainStatusLabel.Content;
                MainProgressBar.Maximum = args.MainMaximum > 0 ? args.MainMaximum : MainProgressBar.Maximum;
                MainProgressBar.Value = args.MainProgress > 0 ? args.MainProgress : MainProgressBar.Value;
                DetailedStatusLabel.Content = args.DetailedMessage ?? DetailedStatusLabel.Content;
                DetailedProgressBar.Maximum = args.DetailedMaximum > 0 ? args.DetailedMaximum : DetailedProgressBar.Maximum;
                DetailedProgressBar.Value = args.DetailedProgress > 0 ? args.DetailedProgress : DetailedProgressBar.Value;
                CounterLabel.Content = args.DetailedProgress > 0 ? "(" + args.DetailedProgress + "/" + args.DetailedMaximum + ")" : "";
            });
        }
        #endregion
    }
}
