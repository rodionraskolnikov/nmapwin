﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator;

namespace NmapWinCommon.Migrations
{
    /// <summary>
    /// Add port scripts and the related columns.
    /// </summary>
    [Migration(202012281247073)]
    public class AddPortScript : Migration
    {
        public override void Up()
        {
            Create.Table("PortScript")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("PortId").AsInt64().Indexed().ForeignKey("FK_PortScript_Port", "Port", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Output").AsString(4000).Nullable();

            Create.Table("PortScriptElement")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("PortScriptId").AsInt64().Indexed().ForeignKey("FK_PortScriptElement_PortScript", "PortScript", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Key").AsString(255).Nullable();

            Create.Table("PortScriptTable")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("PortScriptId").AsInt64().Indexed().ForeignKey("FK_PortScriptTable_PortScript", "PortScript", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Key").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_PortScriptTable_PortScript").OnTable("PortScriptTable");
            Delete.ForeignKey("FK_PortScriptElement_PortScript").OnTable("PortScriptElement");
            Delete.ForeignKey("FK_PortScript_Port").OnTable("PortScriptTable");

            Delete.Table("PortScriptTable");
            Delete.Table("PortScriptElement");
            Delete.Table("PortScript");
        }
    }
}
