﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator;

namespace NmapWinCommon.Migrations
{
    /// <summary>
    /// Add the host tcp/ip sequences structure from the XML output.
    /// </summary>
    [Migration(202101070901561)]
    public class AddHostSequences : Migration
    {
        public override void Up()
        {
            Alter.Table("Host")
                .AddColumn("tcpseqindex").AsInt32().WithDefaultValue(0)
                .AddColumn("tcpseqdifficulty").AsString(255).Nullable()
                .AddColumn("tcpseqvalues").AsString(255).Nullable()
                .AddColumn("ipidseqclass").AsString(255).Nullable()
                .AddColumn("ipidseqvalues").AsString(255).Nullable()
                .AddColumn("tcptsseqclass").AsString(255).Nullable()
                .AddColumn("tcptsseqvalues").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.Column("tcpseqindex").FromTable("Host");
            Delete.Column("tcpseqdifficulty").FromTable("Host");
            Delete.Column("tcpseqvalues").FromTable("Host");
            Delete.Column("ipidseqclass").FromTable("Host");
            Delete.Column("ipidseqvalues").FromTable("Host");
            Delete.Column("tcptsseqclass").FromTable("Host");
            Delete.Column("tcptsseqvalues").FromTable("Host");
        }
    }
}
