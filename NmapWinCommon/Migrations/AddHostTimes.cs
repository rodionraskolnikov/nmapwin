﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator;

namespace NmapWinCommon.Migrations
{
    /// <summary>
    /// Add the host times structure from the XML output.
    /// </summary>
    [Migration(202101061234342)]
    public class AddHostTimes : Migration
    {
        public override void Up()
        {
            Alter.Table("Host")
                .AddColumn("srtt").AsInt64().WithDefaultValue(0)
                .AddColumn("rttvar").AsInt64().WithDefaultValue(0)
                .AddColumn("to").AsInt64().WithDefaultValue(0);
        }

        public override void Down()
        {
            Delete.Column("srtt").FromTable("Host");
            Delete.Column("rttvar").FromTable("Host");
            Delete.Column("to").FromTable("Host");
        }
    }
}
