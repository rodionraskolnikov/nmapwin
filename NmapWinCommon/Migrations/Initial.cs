﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator;
using System.ComponentModel;

namespace NmapWinCommon.Migrations
{
    /// <summary>
    /// Initial database schema.
    /// </summary>
    [Migration(202012270843227)]
    public class Initial : Migration, INotifyPropertyChanged
    {
        #region Upgrade database
        public override void Up()
        {
            Create.Table("Option")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString(255)
                .WithColumn("Value").AsString(255)
                .WithColumn("Description").AsString(255).Nullable();

            Create.Table("Profile")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString(255).NotNullable().Indexed()
                .WithColumn("Command").AsString(4000).NotNullable()
                .WithColumn("Target").AsString(255).Nullable()
                .WithColumn("Description").AsString(4000).Nullable();

            Create.Table("Script")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString(255).NotNullable().Indexed()
                .WithColumn("Html").AsString(255).Nullable()
                .WithColumn("Description").AsString(int.MaxValue).Nullable()
                .WithColumn("Example").AsString(4000).Nullable()
                .WithColumn("Output").AsString(int.MaxValue).Nullable()
                .WithColumn("Code").AsString(int.MaxValue).Nullable()
                .WithColumn("Requires").AsString(255).Nullable()
                .WithColumn("Author").AsString(255).Nullable()
                .WithColumn("License").AsString(255).Nullable();

            Create.Table("Host")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Comment").AsString(4000).Nullable()
                .WithColumn("StartTime").AsInt64().Nullable()
                .WithColumn("EndTime").AsInt64().Nullable()
                .WithColumn("StatusState").AsInt32().NotNullable()
                .WithColumn("StatusReason").AsString(255).Nullable()
                .WithColumn("StatusReasonTtl").AsInt32().Nullable()
                .WithColumn("DistanceValue").AsInt32().Nullable()
                .WithColumn("UptimeSeconds").AsInt64().Nullable()
                .WithColumn("UptimeLastBoot").AsString().Nullable();

            Create.Table("Address")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_Address_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Addr").AsString(32).NotNullable()
                .WithColumn("AddrType").AsInt32().NotNullable()
                .WithColumn("Vendor").AsString(128).Nullable();

            Create.Table("HostName")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_HostName_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Type").AsInt32().NotNullable();

            Create.Table("Port")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_Port_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("PortId").AsInt32().Indexed()
                .WithColumn("Protocol").AsInt32().Indexed()
                .WithColumn("StatusState").AsInt32().NotNullable()
                .WithColumn("StatusReason").AsString(255).Nullable()
                .WithColumn("StatusReasonTtl").AsInt32().Nullable();

            Create.Table("Os")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_Os_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade);

            Create.Table("PortUsed")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("OsId").AsInt64().Indexed().ForeignKey("FK_PortUsed_Os", "Os", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("PortId").AsInt32().NotNullable()
                .WithColumn("Protocol").AsInt32().NotNullable()
                .WithColumn("State").AsInt32().Nullable();

            Create.Table("OsMatch")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("OsId").AsInt64().Indexed().ForeignKey("FK_OsMatch_Os", "Os", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Accuracy").AsInt32()
                .WithColumn("Line").AsInt32();

            Create.Table("OsClass")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("OsMatchId").AsInt64().Indexed().ForeignKey("FK_OsClass_OsMatch", "OsMatch", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Type").AsString(255).Nullable()
                .WithColumn("Vendor").AsString(255).Nullable()
                .WithColumn("Family").AsString(255).Nullable()
                .WithColumn("Gen").AsString(255).Nullable()
                .WithColumn("Accuracy").AsInt32();

            Create.Table("HostCpe")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("OsClassId").AsInt64().Indexed().ForeignKey("FK_HostCpe_OsClass", "OsClass", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Content").AsString(4000).Nullable();

            Create.Table("PortService")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("PortId").AsInt64().Indexed().ForeignKey("FK_PortService_Port", "Port", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Method").AsString(255).Nullable()
                .WithColumn("Version").AsString(255).Nullable()
                .WithColumn("Product").AsString(255).Nullable()
                .WithColumn("Conf").AsInt32()
                .WithColumn("ExtraInfo").AsString(255).Nullable();

            Create.Table("PortCpe")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ServiceId").AsInt64().Indexed().ForeignKey("FK_PortCpe_PortService", "PortService", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Content").AsString(255).Nullable();

            Create.Table("Scan")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString(255).NotNullable().WithDefaultValue("New Scan")
                .WithColumn("Target").AsString(255).Nullable()
                .WithColumn("ProfileId").AsInt64()
                .WithColumn("Command").AsString(4000).NotNullable()
                .WithColumn("Version").AsString(32).Nullable()
                .WithColumn("XmlOutputVersion").AsString(32).Nullable()
                .WithColumn("Output").AsString(int.MaxValue).Nullable()
                .WithColumn("OutputXml").AsString(int.MaxValue).Nullable()
                .WithColumn("StartTime").AsInt64()
                .WithColumn("EndTime").AsInt64()
                .WithColumn("Uptime").AsInt64()
                .WithColumn("LastUpdate").AsInt64()
                .WithColumn("Status").AsInt32()
                .WithColumn("Pid").AsInt32()
                .WithColumn("ExitCode").AsInt32()
                .WithColumn("ProcessName").AsString(64).Nullable();

            Create.Table("TraceRoute")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_TraceRoute_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Proto").AsString(255).Nullable()
                .WithColumn("Port").AsInt32();

            Create.Table("Hop")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("TraceRouteId").AsInt64().Indexed().ForeignKey("FK_Hop_TraceRoute", "TraceRoute", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Ttl").AsInt32().Nullable()
                .WithColumn("Rtt").AsDouble().Nullable()
                .WithColumn("IpAddr").AsString(32).NotNullable()
                .WithColumn("HostName").AsString(255).Nullable();

            Create.Table("ScriptArgument")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ScriptId").AsInt64().Indexed().ForeignKey("FK_ScriptArgument_Script", "Script", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Value").AsString(255).Nullable()
                .WithColumn("DefaultValue").AsString(255).Nullable()
                .WithColumn("Description").AsString(4000).Nullable();

            Create.Table("ScriptCategory")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ScriptId").AsInt64().Indexed().ForeignKey("FK_ScriptCategory_Script", "Script", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Category").AsInt32().NotNullable();

            Create.Table("ScriptType")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ScriptId").AsInt64().Indexed().ForeignKey("FK_ScriptType_Script", "Script", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Type").AsInt32().NotNullable();

            Create.Table("ScanHost")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("ScanId").AsInt64().Indexed().ForeignKey("FK_ScanHost_Scan", "Scan", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_ScanHost_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade);

            Create.Table("Job")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Group").AsString(255).NotNullable()
                .WithColumn("ProfileId").AsInt32().NotNullable()
                .WithColumn("Target").AsString(255)
                .WithColumn("Type").AsInt32()
                .WithColumn("Cron").AsString(16).Nullable()
                .WithColumn("StartTime").AsInt64()
                .WithColumn("Interval").AsInt32()
                .WithColumn("Next").AsInt64()
                .WithColumn("Last").AsInt64()
                .WithColumn("State").AsInt32().NotNullable()
                .WithColumn("InstanceId").AsInt32().NotNullable();
        }
        #endregion

        #region Downgrade database
        public override void Down()
        {
            Delete.ForeignKey("FK_Address_Host").OnTable("Address");
            Delete.ForeignKey("FK_HostName_Host").OnTable("HostName");
            Delete.ForeignKey("FK_PortUsed_Host").OnTable("PortUsed");
            Delete.ForeignKey("FK_PortUsed_Port").OnTable("PortUsed");
            Delete.ForeignKey("FK_Os_Host").OnTable("Os");
            Delete.ForeignKey("FK_Os_PortUsed").OnTable("Os");
            Delete.ForeignKey("FK_OsMatch_Os").OnTable("OsMatch");
            Delete.ForeignKey("FK_OsClass_OsMatch").OnTable("OsClass");
            Delete.ForeignKey("FK_HostCpe_OsClass").OnTable("HostCpe");
            Delete.ForeignKey("FK_Port_Host").OnTable("Port");
            Delete.ForeignKey("FK_Service_Port").OnTable("Service");
            Delete.ForeignKey("FK_PortCpe_Service").OnTable("PortCpe");
            Delete.ForeignKey("FK_TraceRoute_Host").OnTable("TraceRoute");
            Delete.ForeignKey("FK_Hop_TraceRoute").OnTable("Hop");
            Delete.ForeignKey("FK_ScriptArgument_Script").OnTable("ScriptArgument");
            Delete.ForeignKey("FK_ScriptCategory_Script").OnTable("ScriptCategory");
            Delete.ForeignKey("FK_ScriptType_Script").OnTable("ScriptType");

            Delete.Table("Option");
            Delete.Table("Profile");
            Delete.Table("Host");
            Delete.Table("Address");
            Delete.Table("HostName");
            Delete.Table("PortUsed");
            Delete.Table("Os");
            Delete.Table("OsMatch");
            Delete.Table("OsClass");
            Delete.Table("HostCpe");
            Delete.Table("Port");
            Delete.Table("PortService");
            Delete.Table("PortCpe");
            Delete.Table("Scan");
            Delete.Table("TraceRoute");
            Delete.Table("Hop");
            Delete.Table("ScriptArgument");
            Delete.Table("ScriptCategory");
            Delete.Table("ScriptType");
            Delete.Table("Script");
            Delete.Table("ScanHost");
            Delete.Table("Job");
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Property change handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notify on property change
        /// </summary>
        /// <param name="propertyName">property name</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
