﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator;

namespace NmapWinCommon.Migrations
{
    /// <summary>
    /// Add host scripts and the related columns.
    /// </summary>
    [Migration(202101161014765)]
    public class AddHostScript : Migration
    {
        public override void Up()
        {
            Create.Table("HostScript")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostId").AsInt64().Indexed().ForeignKey("FK_HostScript_Host", "Host", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Output").AsString(4000).Nullable();

            Create.Table("HostScriptElement")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostScriptId").AsInt64().Indexed().ForeignKey("FK_HostScriptElement_HostScript", "HostScript", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Key").AsString(255).Nullable();

            Create.Table("HostScriptTable")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("HostScriptId").AsInt64().Indexed().ForeignKey("FK_HostScriptTable_HostScript", "HostScript", "Id").OnDelete(System.Data.Rule.Cascade)
                .WithColumn("Key").AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_HostScriptTable_HostScript").OnTable("HostScriptTable");
            Delete.ForeignKey("FK_HostScriptElement_HostScript").OnTable("HostScriptElement");
            Delete.ForeignKey("FK_HostScript_Host").OnTable("PortScriptTable");

            Delete.Table("HostScriptTable");
            Delete.Table("HostScriptElement");
            Delete.Table("HostScript");
        }
    }
}
