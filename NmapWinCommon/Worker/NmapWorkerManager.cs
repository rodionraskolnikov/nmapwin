﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NmapWinCommon.Worker
{
    [DesignerCategory("Code")]
    public class NmapWorkerManager : BackgroundWorker
    {
        public List<NmapWorker> workerList = new List<NmapWorker>();

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        private static readonly Lazy<NmapWorkerManager> lazy = new Lazy<NmapWorkerManager>(() => new NmapWorkerManager());

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static NmapWorkerManager Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public NmapWorkerManager()
        {
        }
        #endregion

        public NmapWorker AddWorker(Scan scan)
        {
            NmapWorker nmapWorker = new NmapWorker();
            nmapWorker.InitializeWorker();
            nmapWorker.RunWorkerAsync(scan);
            workerList.Add(nmapWorker);
            return nmapWorker;
        }

        public void RemoveWorker(NmapWorker nmapWorker)
        {
            workerList.Remove(nmapWorker);
        }

        public NmapWorker FindWorkerByScan(Scan scan)
        {
            return workerList.Find(w => w.Scan.Id == scan.Id);
        }

        public void CancelAll()
        {
            workerList.ForEach(w => w.Cancel());
            workerList.Clear();
        }

        public void CancelScan(Scan scan)
        {
            NmapWorker worker = workerList.Find(w => w.Scan.Id == scan.Id);
            if (worker != null)
            {
                if (worker.IsBusy)
                {
                    worker.Cancel();
                }
                workerList.Remove(worker);
            }
        }
    }
}
