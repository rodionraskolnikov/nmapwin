﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Monitor;
using NmapWinCommon.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace NmapWinCommon.Worker
{
    [DesignerCategory("Code")]
    public class NmapWorker : BackgroundWorker, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(NmapWorker));

        /// <summary>
        /// Nmap executable
        /// </summary>
        private string NmapExecutable = @"C:\Program Files (x86)\Nmap\nmap.exe";

        /// <summary>
        /// Nmap working directory.
        /// </summary>
        private string NmapWorkingDir = @"C:\Program Files (x86)\Nmap";

        /// <summary>
        /// Options database singleton.
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// XML output parser.
        /// </summary>
        private readonly XmlParser XmlParser = XmlParser.Instance;

        /// <summary>
        /// Nmap command
        /// </summary>
        private readonly Command Command = Command.Instance;

        /// <summary>
        /// Nmap worker manager
        /// </summary>
        private readonly NmapWorkerManager NmapWorkerManager = NmapWorkerManager.Instance;

        /// <summary>
        /// XML temporary file name.
        /// </summary>
        private string xmlTempFile;

        /// <summary>
        /// Current scan
        /// </summary>
        public Scan Scan { get; set; }
        #endregion

        #region Initilization        
        /// <summary>
        /// Initialize the worker.
        /// </summary>
        public void InitializeWorker()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(NmapWorkerDoWork);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(NmapWorkerRunWorkerCompleted);

            InitializeNmapProcess();
        }

        /// <summary>
        /// Initialize the nmap process
        /// </summary>
        public void InitializeNmapProcess()
        {
            // Get nmap executable path
            Option NmapExecutablePathOption = DatabaseOption.FindOptionByName("nmap.executable.path");
            if (NmapExecutablePathOption != null)
            {
                NmapExecutable = NmapExecutablePathOption.Value;
            }

            // Get nmap base path
            Option NmapBasePathOption = DatabaseOption.FindOptionByName("nmap.base.path");
            if (NmapBasePathOption != null)
            {
                NmapWorkingDir = NmapBasePathOption.Value;
            }
            logger.Info(string.Format("Worker initialized - executable: {0} workingDir: {1}", NmapExecutable, NmapWorkingDir));
        }
        #endregion

        /// <summary>
        /// This event handler is where the actual, potentially time-consuming work is done.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void NmapWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Scan = (Scan)e.Argument;
            RunNmapProcess();
        }

        /// <summary>
        /// Actually run the nmap process.
        /// </summary>
        public void RunNmapProcess()
        {
            string fullcommand = Scan.Command.Replace("nmap", NmapExecutable) + (Scan.Target.Equals("Random") ? "" : " " + Scan.Target);
            Scan.Output += "Command: " + fullcommand + Environment.NewLine;
            logger.Info(string.Format("Scan worker defined - scanId: {0} fullCommand: {1}", Scan.Id, fullcommand));

            ProcessStartInfo startInfo = new ProcessStartInfo(NmapExecutable)
            {
                Arguments = AddTempXmlFile(fullcommand),
                WorkingDirectory = NmapWorkingDir,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            // Start the process.
            using Process nmapProcess = Process.Start(startInfo);
            Scan.Pid = nmapProcess.Id;
            Scan.ProcessName = nmapProcess.ProcessName;
            Scan.StartTime = DateTime.Now.Ticks;
            Scan.Status = NmapWorkerStatus.RUNNING;
            logger.Info(string.Format("Worker started - name: {0} pid: {1} command: {2}", nmapProcess.ProcessName, nmapProcess.Id, fullcommand));
            UpdateScan();

            // Add monitor
            ProcessMonitor.Instance.AddProcess(Scan.Id, Scan.Pid);            

            nmapProcess.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                // Add the line to the scan output
                if (!string.IsNullOrEmpty(e.Data))
                {
                    Scan.Output += e.Data + Environment.NewLine;
                    Scan.Uptime = DateTime.Now.Ticks - Scan.StartTime;
                    Scan.LastUpdate = DateTime.Now.Ticks;
                    UpdateScan();
                }
            });

            // Get process output
            nmapProcess.BeginOutputReadLine();
            nmapProcess.WaitForExit();

            // Handle exit
            Scan.ExitCode = nmapProcess.ExitCode;
            Scan.Output += $"Process exit code: {nmapProcess.ExitCode}" + Environment.NewLine;
            logger.Info(string.Format("Worker finished - exitCode: {0}", nmapProcess.ExitCode));

            // Wait for output
            nmapProcess.WaitForExit();
            nmapProcess.Close();
            logger.Info(string.Format("Worker closed - exitCode: {0}", Scan.ExitCode));

            // Complete job
            if (!CancellationPending && Scan.ExitCode == 0)
            {
                ProcessJobCompleted();
            }

            // Finished.            
            UpdateScan();
        }

        /// <summary>
        /// This event handler deals with the results of the background operation.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void NmapWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Info(string.Format("Worker completed - id: {0}", Scan.Id));
            ProcessMonitor.Instance.RemoveProcess(Scan.Id, Scan.Pid);

            // Set end time
            Scan.LastUpdate = DateTime.Now.Ticks;
            Scan.EndTime = DateTime.Now.Ticks;
            Scan.Uptime = DateTime.Now.Ticks - Scan.StartTime;

            // Handle the case where an exception was thrown.
            if (e.Cancelled || Scan.ExitCode < 0)
            {
                // Save output
                Scan.Output += "Cancelled by user" + Environment.NewLine;
                Scan.Status = NmapWorkerStatus.ABORTED;
                logger.Warn(string.Format("Worker aborted - id: {0}", Scan.Id));
            }
            else if (e.Error != null)
            {
                // Save output
                Scan.Output += e.Error.Message + Environment.NewLine;
                Scan.Status = NmapWorkerStatus.ERROR;
                logger.Error(string.Format("Worker failed - id:{0} error: {1}", Scan.Id, e.Error.Message));
            }
            UpdateScan();
            NmapWorkerManager.RemoveWorker(this);
        }

        public void ProcessJobCompleted()
        {
            // Set end time / save XML output
            Scan.LastUpdate = DateTime.Now.Ticks;
            Scan.Uptime = DateTime.Now.Ticks - Scan.StartTime;

            // Set status
            Scan.Output += "Process exited" + Environment.NewLine;
            Scan.Status = NmapWorkerStatus.FINALIZING;
            UpdateScan();

            // Read temp file
            Scan.OutputXml = ReadXmlFile(xmlTempFile);
            logger.Debug(string.Format("XML temp output file read - id: {0}", Scan.Id));

            // Parse XML output
            XmlParser.Scan = Scan;
            XmlParser.XmlString = Scan.OutputXml;
            logger.Debug(string.Format("XML output parsed - id: {0}", Scan.Id));

            Scan.EndTime = DateTime.Now.Ticks;
            Scan.Uptime = DateTime.Now.Ticks - Scan.StartTime;
            Scan.Status = NmapWorkerStatus.COMPLETED;
            UpdateScan();

            logger.Info(string.Format("Worker completed - scan: {0} uptime: {1}s", Scan.Id, new TimeSpan(Scan.Uptime).TotalSeconds));
        }

        /// <summary>
        /// Cancel a running task.
        /// </summary>
        public void Cancel()
        {
            if (CancellationPending)
            {
                return;
            }
            if (IsBusy)
            {
                Scan.Status = NmapWorkerStatus.STOPPING;
                Scan.Uptime = DateTime.Now.Ticks - Scan.StartTime;
                Scan.LastUpdate = DateTime.Now.Ticks;
                UpdateScan();
                CancelAsync();

                // Kill nmap process
                Process p = Process.GetProcessById(Scan.Pid);
                if (p != null)
                {
                    p.Kill();
                    logger.Info(string.Format("Nmap process killed - pid: {0}", p.Id));
                }
                logger.Info(string.Format("Worker cancelled"));
            }
        }

        /// <summary>
        /// Update the database and send notifications.
        /// </summary>
        private void UpdateScan()
        {
            Scan = DatabaseScan.UpdateScan(Scan);
            if (Scan != null)
            {
                OnPropertyChanged("WorkerOutputChanged", null, Scan.Output);
            }
        }

        /// <summary>
        /// Return the XML output file.
        /// </summary>
        /// <remarks>If the user has given the -oX option, we will use that file. Otherwise use a
        /// temporary file.</remarks>
        /// <param name="command"></param>
        /// <returns></returns>
        private string AddTempXmlFile(string command)
        {
            Command.CommandString = command;
            CommandOption xmlOutput = Command.FindByOption("-oX");
            if (xmlOutput != null)
            {
                xmlTempFile = xmlOutput.Modifier;
                return command;
            }
            xmlTempFile = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xml";
            logger.Debug(string.Format("Got temporary file - path: {0}", xmlTempFile));
            return command + " -oX " + xmlTempFile;
        }

        /// <summary>
        /// Try to concurrently read the XML output file.
        /// </summary>
        /// <param name="fileName">name of the file to read</param>
        /// <returns>file content</returns>
        private string ReadXmlFile(string fileName)
        {
            logger.Debug(string.Format("Start reading XML file - path: {0}", fileName));

            FileUtils.WaitForFile(fileName, 10);
            logger.Debug(string.Format("File not locked - path: {0}", fileName));

            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                using FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                using var reader = new StreamReader(fileStream);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    stringBuilder.Append(line);
                }
                logger.Debug(string.Format("Finished reading XML output temp file file - size: {0}", stringBuilder.Length));
            }
            catch (IOException ex)
            {
                logger.Error(string.Format("Reading XML file failed - path: {0} error: {1}", fileName, ex.Message));
            }
            return stringBuilder.ToString();
        }

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Send a property change event.
        /// </summary>
        /// <param name="propertyName">name of the property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnPropertyChanged(string propertyName, string oldValue, string newValue)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedExtendedEventArgs(propertyName, oldValue, newValue));
        }

        public override bool Equals(object obj)
        {
            return obj is NmapWorker worker &&
                   EqualityComparer<Scan>.Default.Equals(Scan, worker.Scan);
        }

        public override int GetHashCode()
        {
            return -1675823544 + EqualityComparer<Scan>.Default.GetHashCode(Scan);
        }
        #endregion
    }

    public sealed class PropertyChangedExtendedEventArgs : PropertyChangedEventArgs
    {
        public PropertyChangedExtendedEventArgs(string propertyName, string oldValue, string newValue) : base(propertyName)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public string OldValue { get; }
        public string NewValue { get; }
    }
}
