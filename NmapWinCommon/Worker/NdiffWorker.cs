﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace NmapWinCommon.Worker
{
    [DesignerCategory("Code")]
    public class NdiffWorker : BackgroundWorker, INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(NmapWorker));

        /// <summary>
        /// Ndiff executable
        /// </summary>
        private string NdiffExecutable = @"C:\Program Files (x86)\Nmap\ndiff.exe";

        /// <summary>
        /// Ndiff working directory.
        /// </summary>
        private string NdiffWorkingDir = @"C:\Program Files (x86)\Nmap";

        /// <summary>
        /// Options database singleton.
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// XML output parser.
        /// </summary>
        private readonly XmlParser XmlParser = XmlParser.Instance;

        /// <summary>
        /// Nmap command
        /// </summary>
        private readonly Command Command = Command.Instance;

        /// <summary>
        /// Nmap worker manager
        /// </summary>
        private readonly NmapWorkerManager NmapWorkerManager = NmapWorkerManager.Instance;

        /// <summary>
        /// XML temporary file name for scan 1.
        /// </summary>
        private string xmlTempFile1;

        /// <summary>
        /// XML temporary file name for scan 2.
        /// </summary>
        private string xmlTempFile2;

        /// <summary>
        /// Current scans
        /// </summary>
        public List<Scan> Scans { get; set; }

        /// <summary>
        /// Output text.
        /// </summary>
        public string NdiffOutputText { get; set; }
        #endregion

        #region Initilization        
        /// <summary>
        /// Initialize the worker.
        /// </summary>
        public void InitializeWorker()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(NdiffWorkerDoWork);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(NmapWorkerRunWorkerCompleted);

            InitializeNdiffProcess();
        }

        /// <summary>
        /// Initialize the nmap process
        /// </summary>
        public void InitializeNdiffProcess()
        {
            // Get nmap executable path
            Option NdiffExecutablePathOption = DatabaseOption.FindOptionByName("ndiff.executable.path");
            if (NdiffExecutablePathOption != null)
            {
                NdiffExecutable = NdiffExecutablePathOption.Value;
            }

            // Get nmap base path
            Option NmapBasePathOption = DatabaseOption.FindOptionByName("nmap.base.path");
            if (NmapBasePathOption != null)
            {
                NdiffWorkingDir = NmapBasePathOption.Value;
            }
            logger.Info(string.Format("Worker initialized - executable: {0} workingDir: {1}", NdiffExecutable, NdiffWorkingDir));
        }
        #endregion

        /// <summary>
        /// This event handler is where the actual, potentially time-consuming work is done.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void NdiffWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Scans = (List<Scan>)e.Argument;
            WriteXmlFiles();
            RunNdiffProcess();
        }

        /// <summary>
        /// Actually run the nmap process.
        /// </summary>
        public void RunNdiffProcess()
        {
            logger.Info(string.Format("Ndiff worker defined - scan1: {0} scan2: {1}", Scans[0].Id, Scans[1].Id));

            ProcessStartInfo startInfo = new ProcessStartInfo(NdiffExecutable, xmlTempFile1 + " " + xmlTempFile2)
            {
                WorkingDirectory = NdiffWorkingDir,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            // Start the process.
            using Process ndiffProcess = Process.Start(startInfo);
            logger.Info(string.Format("Ndiff worker started - name: {0} pid: {1} command: {2}", ndiffProcess.ProcessName, ndiffProcess.Id, startInfo.Arguments));

            ndiffProcess.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
            {
                // Add the line to the scan output
                if (!string.IsNullOrEmpty(e.Data))
                {
                    NdiffOutputText += e.Data + Environment.NewLine;
                    OnPropertyChanged("NdiffOutputText");
                }
            });

            // Get process output
            ndiffProcess.BeginOutputReadLine();
            ndiffProcess.WaitForExit();

            // Handle exit
            int ExitCode = ndiffProcess.ExitCode;
            //Scan.Output += $"Process exit code: {nmapProcess.ExitCode}" + Environment.NewLine;
            logger.Info(string.Format("Ndiff worker finished - exitCode: {0}", ndiffProcess.ExitCode));

            // Wait for output
            ndiffProcess.WaitForExit();
            ndiffProcess.Close();
            logger.Info(string.Format("Ndiff worker closed - exitCode: {0}", ExitCode));

            // Complete job
            /*if (!CancellationPending && Scan.ExitCode == 0)
            {
                ProcessJobCompleted();
            }*/

            // Finished.            
            UpdateScan();
        }

        /// <summary>
        /// This event handler deals with the results of the background operation.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void NmapWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //logger.Info(string.Format("Worker completed - id: {0}", Scan.Id));

            // Handle the case where an exception was thrown.
            if (e.Cancelled)
            {
            }
            else if (e.Error != null)
            {
            }
        }

        public void ProcessJobCompleted()
        {
            //logger.Info(string.Format("Worker completed - scan: {0} uptime: {1}s", Scan.Id, new TimeSpan(Scan.Uptime).TotalSeconds));
        }

        /// <summary>
        /// Cancel a running task.
        /// </summary>
        public void Cancel()
        {
            if (CancellationPending)
            {
                return;
            }
            if (IsBusy)
            {
                CancelAsync();

                // Kill nmap process
                /*                Process p = Process.GetProcessById(Scan.Pid);
                                if (p != null)
                                {
                                    p.Kill();
                                    logger.Info(string.Format("Nmap process killed - pid: {0}", p.Id));
                                }*/
                logger.Info(string.Format("Worker cancelled"));
            }
        }

        /// <summary>
        /// Update the database and send notifications.
        /// </summary>
        private void UpdateScan()
        {
            //Scan = DatabaseScan.UpdateScan(Scan);
            //OnPropertyChanged("WorkerOutputChanged", null, Scan.Output);
        }

        /// <summary>
        /// Writes the XML output of the scans to temporary files.
        /// </summary>
        private void WriteXmlFiles()
        {
            xmlTempFile1 = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xml";
            File.WriteAllText(xmlTempFile1, Scans[0].OutputXml);
            logger.Debug(string.Format("Got temporary file 1 - path: {0}", xmlTempFile1));

            xmlTempFile2 = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xml";
            File.WriteAllText(xmlTempFile2, Scans[1].OutputXml);
            logger.Debug(string.Format("Got temporary file 2 - path: {0}", xmlTempFile2));
        }

        #region Event handling
        /// <summary>
        /// Property change event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Send a property change event.
        /// </summary>
        /// <param name="propertyName">name of the property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
