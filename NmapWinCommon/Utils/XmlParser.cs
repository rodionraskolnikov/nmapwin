﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace NmapWinCommon.Utils
{
    public class XmlParser
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(XmlParser));

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Option database singleton
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Host database singleton
        /// </summary>
        private readonly DatabaseHost DatabaseHost = DatabaseHost.Instance;

        /// <summary>
        /// Address database singleton
        /// </summary>
        private readonly DatabaseAddress DatabaseAddress = DatabaseAddress.Instance;

        /// <summary>
        /// OS database singleton
        /// </summary>
        private readonly DatabaseOs DatabaseOs = DatabaseOs.Instance;

        /// <summary>
        /// Traceroute database singleton
        /// </summary>
        private readonly DatabaseTraceRoute DatabaseTraceRoute = DatabaseTraceRoute.Instance;

        /// <summary>
        /// Method timer.
        /// </summary>
        private readonly Stopwatch Stopwatch = new Stopwatch();

        /// <summary>
        /// Current scan structure
        /// </summary>
        public Scan Scan { get; set; }

        /// <summary>
        /// Skip down hosts flag
        /// </summary>
        private bool SkipDownHosts = true;
        #endregion

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<XmlParser> lazy = new Lazy<XmlParser>(() => new XmlParser());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static XmlParser Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <remarks>The XML parser reads the temporary XML file written by the background nmap process and
        /// extracts all required information agout hosts, ports, services etc.</remarks>
        ///
        public XmlParser()
        {
            Option downHostOption = DatabaseOption.FindOptionByName("nmapwin.output.skipDown");
            SkipDownHosts = downHostOption != null && downHostOption.GetBool(false);
        }
        #endregion

        #region Bindings
        private string _xmlString;
        public string XmlString
        {
            get { return _xmlString; }
            set
            {
                if (value != null && !value.Equals(_xmlString))
                {
                    _xmlString = value;
                    ParseXmlOutput();
                }
            }
        }
        #endregion

        #region Parser

        /// <summary>
        /// Parse the nmap XML output.
        /// </summary>
        private void ParseXmlOutput()
        {
            logger.DebugFormat("XML parser starting - scan: {0}", Scan.Id);

            // Load document
            XmlDocument document = new XmlDocument();
            document.LoadXml(_xmlString);
            logger.DebugFormat("XML dicument loaded - scan: {0}", Scan.Id);

            // Get run related fields
            XmlNode xmlNmapRun = document.SelectSingleNode("nmaprun");
            Scan.Version = xmlNmapRun.Attributes.GetNamedItem("version").Value;
            Scan.XmlOutputVersion = xmlNmapRun.Attributes.GetNamedItem("xmloutputversion").Value;

            // Get verbose/debug level
            XmlNode xmlVerbose = document.SelectSingleNode("/nmaprun/verbose");
            Scan.VerbosityLevel = int.Parse(xmlVerbose.Attributes.GetNamedItem("level").Value);
            XmlNode xmlDebug = document.SelectSingleNode("/nmaprun/debugging");
            Scan.VerbosityLevel = int.Parse(xmlDebug.Attributes.GetNamedItem("level").Value);

            // Update scan in database
            Scan = DatabaseScan.UpdateScan(Scan);
            logger.DebugFormat("Converted XML scan - scan: {0}", Scan.Id);

            // Host list
            XmlNodeList hostList = document.SelectNodes("descendant::host");
            foreach (XmlNode xmlHost in hostList)
            {
                AddHost(xmlHost);
            }
            logger.DebugFormat("XML parser finished - scan: {0}", Scan.Id);
        }

        /// <summary>
        /// Add a host to the scan.
        /// </summary>
        /// <param name="scan">scan to add to</param>
        /// <param name="hostNode">XML host node</param>
        /// <returns>host object</returns>
        public Host AddHost(Scan scan, XmlNode hostNode)
        {
            Scan = scan;
            return AddHost(hostNode);
        }

        /// <summary>
        /// Add a host to the scan.
        /// </summary>
        /// <param name="hostNode">XML host node</param>
        /// <returns>host object</returns>
        public Host AddHost(XmlNode hostNode)
        {
            Stopwatch.Start();

            // Insert host
            Host host = ConvertNode<Host>(hostNode);
            logger.DebugFormat("Host converted - host: {0}", host.Identifier);

            if (SkipDownHosts && host.Status != null && host.Status.State == HostState.down)
            {
                logger.DebugFormat("Host skipped - host: {0}", host.Identifier);
                return null;
            }
            logger.DebugFormat("Host won't be skipped - host: {0}", host.Identifier);

            Host h = DatabaseHost.FindHostByAddress(host.PrimaryAddress);
            if (h == null)
            {
                host = DatabaseHost.InsertHost(host);
                logger.DebugFormat("Host added - scan: {0} host: {1} [{2}ms]", Scan.Identifier, host.Identifier, Stopwatch.ElapsedMilliseconds);
            }
            else
            {
                host.Id = h.Id;
                host = DatabaseHost.UpdateHost(host);
                logger.DebugFormat("Host updated - scan: {0} host: {1} [{2}ms]", Scan.Identifier, host.Identifier, Stopwatch.ElapsedMilliseconds);
            }

            Stopwatch.Stop();

            // Add to scan
            return DatabaseScan.AddHost(Scan, host);
        }

        private static T ConvertNode<T>(XmlNode node) where T : class
        {
            MemoryStream stm = new MemoryStream();

            StreamWriter stw = new StreamWriter(stm);
            stw.Write(node.OuterXml);
            stw.Flush();

            stm.Position = 0;

            XmlSerializer ser = new XmlSerializer(typeof(T));
            T result = (ser.Deserialize(stm) as T);

            return result;
        }
        #endregion
    }
}
