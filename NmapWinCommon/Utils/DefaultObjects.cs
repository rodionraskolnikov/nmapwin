﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinUI.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace NmapWinCommon.Utils
{
    public class DefaultObjects
    {
        #region Fields
        /// <summary>
        /// Options database singleton
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;

        /// <summary>
        /// Script database singleton
        /// </summary>
        private readonly DatabaseScript DatabaseScript = DatabaseScript.Instance;

        /// <summary>
        /// Job database singleton
        /// </summary>
        private readonly DatabaseJob DatabaseJob = DatabaseJob.Instance;
        #endregion

        #region Install/delete defaults        
        public void DeleteDefaults()
        {
            int count = 0;
            List<Option> Options = DatabaseOption.Instance.FindOptions();
            Options.ForEach(o =>
            {
                OnDatabaseChanged("Deleting default options...", "Deleting profile '" + o.Identifier + "...", 4, ++count, 5, Options.Count);
                DatabaseOption.Instance.DeleteOption(o);
            });

            List<Profile> Profiles = DatabaseProfile.Instance.FindProfiles();
            Profiles.ForEach(p =>
            {
                OnDatabaseChanged("Deleting default profiles...", "Deleting profile '" + p.Identifier + "...", 4, ++count, 5, Profiles.Count);
                DatabaseProfile.Instance.DeleteProfile(p);
            });

            List<Script> Scripts = DatabaseScript.Instance.FindScripts();
            Scripts.ForEach(s =>
            {
                OnDatabaseChanged("Deleting default scripts...", "Deleting script '" + s.Identifier + "...", 4, ++count, 5, Scripts.Count);
                DatabaseScript.Instance.DeleteScript(s);
            });

            List<Job> Jobs = DatabaseJob.Instance.FindJobs();
            Jobs.ForEach(j =>
            {
                OnDatabaseChanged("Deleting default jobs...", "Deleting job '" + j.Identifier + "...", 4, ++count, 5, Jobs.Count);
                DatabaseJob.Instance.DeleteJob(j);
            });
        }

        /// <summary>
        /// Install default values for options, profile, script and jobs.
        /// </summary>
        public void InstallDefaults()
        {
            if (!DatabaseOption.Exists())
            {
                OnDatabaseChanged("Installing default values...", "Installing default options...", 1, -1, 5, -1);
                InstallDefaultOptions();
            }

            if (!DatabaseProfile.Exists())
            {
                OnDatabaseChanged("Installing default values...", "Installing default profiles...", 2, -1, 5, -1);
                InstallDefaultProfiles();
            }

            if (!DatabaseScript.Exists())
            {
                OnDatabaseChanged("Installing default values...", "Installing default scripts...", 3, -1, 5, -1);
                InstallDefaultScripts();
            }
            if (!DatabaseJob.Exists())
            {
                OnDatabaseChanged("Installing default values...", "Installing default jobs...", 4, -1, 5, -1);
                InstallDefaultJobs();
            }
        }

        #region Reload defaults
        /// <summary>
        /// Reloads the default values.
        /// </summary>
        /// <remarks>This will also reinstall the detault options/profiles.</remarks>
        public void ReloadDefaults()
        {
            OnDatabaseChanged("Deleting defaults", null, 1, -1, 2, -1);
            DeleteDefaults();

            OnDatabaseChanged("Installing defaults", null, 2, -1, 2, -1);
            InstallDefaults();
        }
        #endregion

        /// <summary>
        /// Creates the default options.
        /// </summary>
        private void InstallDefaultOptions()
        {
            int count = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "options.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Option> options = JsonConvert.DeserializeObject<List<Option>>(JSONtxt);
                options.ForEach(o =>
                {
                    o = DatabaseOption.InsertOption(o);
                    OnDatabaseChanged("Installing default options...", "Option '" + o.Name + "' installed", -1, ++count, 0, options.Count);
                });
            }
        }

        /// <summary>
        /// Creates the default profiles.
        /// </summary>
        private void InstallDefaultProfiles()
        {
            int count = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "profiles.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Profile> profiles = JsonConvert.DeserializeObject<List<Profile>>(JSONtxt);
                profiles.ForEach(p =>
                {
                    p = DatabaseProfile.InsertProfile(p);
                    OnDatabaseChanged("Installing default profiles...", "Profile '" + p.Name + "' installed", -1, ++count, 0, profiles.Count);
                });
            }
        }

        /// <summary>
        /// Install all default scripts
        /// </summary>
        private void InstallDefaultScripts()
        {
            int count = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "scripts.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Script> scripts = JsonConvert.DeserializeObject<List<Script>>(JSONtxt);
                scripts.ForEach(s =>
                {
                    s = DatabaseScript.InsertScript(s);
                    OnDatabaseChanged("Installing default scripts...", "Script '" + s.Name + "' installed", -1, ++count, 0, scripts.Count);
                });
            }
        }

        /// <summary>
        /// Install all default jobs
        /// </summary>
        private void InstallDefaultJobs()
        {
            int count = 0;
            string fileName = Path.Combine(Constants.DatabaseDirectory, "jobs.json");
            if (File.Exists(fileName))
            {
                string JSONtxt = File.ReadAllText(@fileName);
                List<Job> jobs = JsonConvert.DeserializeObject<List<Job>>(JSONtxt);
                jobs.ForEach(j =>
                {
                    j = DatabaseJob.InsertJob(j);
                    OnDatabaseChanged("Installing default jobs...", "Job '" + j.Name + "' installed", -1, ++count, 0, jobs.Count);
                });
            }
        }
        #endregion

        #region Event handling
        public event EventHandler DatabaseEvent;

        private void OnDatabaseChanged(string mainMessage, string detailedMessage, double mainProgress, double detailedProgress, double mainMaximum, double detailedMaximum)
        {
            ProgressEventArgs args = new ProgressEventArgs
            {
                MainMessage = mainMessage,
                MainProgress = mainProgress,
                MainMaximum = mainMaximum,
                DetailedMessage = detailedMessage,
                DetailedProgress = detailedProgress,
                DetailedMaximum = detailedMaximum
            };
            if (DatabaseEvent != null)
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    DatabaseEvent(this, args);
                }));
        }
        #endregion
    }
}
