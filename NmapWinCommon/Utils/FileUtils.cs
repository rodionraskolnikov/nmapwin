﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
using log4net;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace NmapWinCommon.Utils
{
    /// <summary>
    /// File utilities.
    /// </summary>
    internal static class FileUtils
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(FileUtils));

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;

        private static bool IsFileLocked(Exception exception)
        {
            int errorCode = Marshal.GetHRForException(exception) & ((1 << 16) - 1);
            return errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION;
        }

        /// <summary>
        /// Wait for a file to get unlocked.
        /// </summary>
        /// <param name="fileName">name of the file to check</param>
        /// <param name="max">max time to wait in seconds</param>
        public static void WaitForFile(string fileName, int max)
        {
            int i = -1;
            while (++i < max)
            {
                try
                {
                    // The "using" is important because FileStream implements IDisposable and
                    // "using" will avoid a heap exhaustion situation when too many handles  
                    // are left undisposed.
                    using FileStream fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    if (fileStream != null) fileStream.Close();
                    return;
                }
                catch (IOException ex)
                {
                    if (IsFileLocked(ex))
                    {
                        logger.Warn(string.Format("File is locked for reading - path: {0}", fileName));
                    }
                }
                finally
                { }
            }
        }

        /// <summary>
        /// Check the file lock.
        /// </summary>
        /// <param name="file">file to check</param>
        /// <returns>true if file is locked</returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                logger.Debug(string.Format("File is not locked for reading - path: {0}", file.FullName));
            }
            catch (IOException)
            {
                logger.Warn(string.Format("File is locked for reading - path: {0}", file.FullName));
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
    }
}
