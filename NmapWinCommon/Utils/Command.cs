﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace NmapWinCommon.Utils
{
    public class Command : INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(Command));

        /// <summary>
        /// Ndesk option set
        /// </summary>
        private OptionSet optionSet;

        /// <summary>
        /// Extra options, which are not foud in the option set.
        /// </summary>
        private List<string> Extra;

        /// <summary>
        /// All available command options
        /// </summary>
        private List<CommandOption> AllCommandOptions { get; set; }

        /// <summary>
        /// Currently found command options
        /// </summary>
        private List<CommandOption> CurrentCommandOptions { get; set; }
        #endregion

        #region Bindings
        public string CommandString
        {
            get
            {
                return GetCommandString();
            }
            set
            {
                ParseCommand(value);
                OnPropertyChanged("CommandString");
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        private static readonly Lazy<Command> lazy = new Lazy<Command>(() => new Command());

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static Command Instance { get { return lazy.Value; } }

        public Command()
        {
            InitializeCommand();
        }

        public Command(string command)
        {
            InitializeCommand();

            CommandString = command;

            ParseCommand(command);
        }
        #endregion

        private void InitializeCommand()
        {
            CurrentCommandOptions = new List<CommandOption>();

            optionSet = new OptionSet() {
                // Scan page
                { "sA", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sA", true)) },
                { "sF", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sF", true)) },
                { "sM", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sM", true)) },
                { "sN", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sN", true)) },
                { "sS", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sS", true)) },
                { "sT", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sT", true)) },
                { "sW", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sW", true)) },
                { "sX", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sX", true)) },
                { "sU", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sU", true)) },
                { "sO", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sO", true)) },
                { "sL", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sL", true)) },
                { "sn", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sn", true)) },
                { "sY", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sY", true)) },
                { "sZ", "", v => CurrentCommandOptions.Add(new CommandOption("", "-sZ", true)) },
                { "T0", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T0", true)) },
                { "T1", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T1", true)) },
                { "T2", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T2", true)) },
                { "T3", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T3", true)) },
                { "T4", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T4", true)) },
                { "T5", "", v => CurrentCommandOptions.Add(new CommandOption("", "-T5", true)) },
                { "A", "", v => CurrentCommandOptions.Add(new CommandOption("AdvancedCheckBox", "-A")) },
                { "O", "", v => CurrentCommandOptions.Add(new CommandOption("OperatingSystemCheckBox", "-O")) },
                { "sV", "", v => CurrentCommandOptions.Add(new CommandOption("VersionDetectionCheckBox", "-sV")) },
                { "sI=", "", v => CurrentCommandOptions.Add(new CommandOption("IdleScanCheckBox", "IdleScanTextBox", "-sI", v)) },
                { "b=", "", v => CurrentCommandOptions.Add(new CommandOption("FtpBounceCheckBox", "FtpBounceTextBox", "-b", v)) },
                { "6", "", v => CurrentCommandOptions.Add(new CommandOption("Ipv6CheckBox", "-6")) },
                { "disable-arp-ping", "", v => CurrentCommandOptions.Add(new CommandOption("DisableArpPingCheckBox", "--disable-arp-ping")) },
                { "discovery-ignore-rst", "", v => CurrentCommandOptions.Add(new CommandOption("DiscoveryIgnoreRstCheckBox", "--discovery-ignore-rst")) },
                // Ping page
                { "Pn", "", v => CurrentCommandOptions.Add(new CommandOption("DontPingCheckBox", "-Pn")) },
                { "PE", "", v => CurrentCommandOptions.Add(new CommandOption("IcmpPingCheckBox", "-PE")) },
                { "PP", "", v => CurrentCommandOptions.Add(new CommandOption("IcmpTimestampCheckBox", "-PP")) },
                { "PM", "", v => CurrentCommandOptions.Add(new CommandOption("IcmpNetmaskCheckBox", "-PM")) },
                { "PA=", "", v => CurrentCommandOptions.Add(new CommandOption("AckPingCheckBox", "AckPingTextBox", "-PA", v)) },
                { "PS=", "", v => CurrentCommandOptions.Add(new CommandOption("SynPingCheckBox", "SynPingTextBox", "-PS", v)) },
                { "PU=", "", v => CurrentCommandOptions.Add(new CommandOption("UdpProbesCheckBox", "UdpProbesTextBox", "-PU", v)) },
                { "PO=", "", v => CurrentCommandOptions.Add(new CommandOption("IpProtoProbesCheckBox", "IpProtoProbesTextBox", "-PO", v)) },
                { "PY:", "", v => CurrentCommandOptions.Add(new CommandOption("SctpPingProbesCheckBox", "SctpPingProbesTextBox", "-PY", v)) },
                // Scripting page
                { "script=", "", v => CurrentCommandOptions.Add(new CommandOption("", "--script", v)) },
                { "script-args=", "", v => CurrentCommandOptions.Add(new CommandOption("", "--script-args", v)) },
                // Target page
                { "exclude=", "", v => CurrentCommandOptions.Add(new CommandOption("ExcludeCheckBox", "ExcludeTextBox", "--exclude", v)) },
                { "excludefile=", "", v => CurrentCommandOptions.Add(new CommandOption("ExcludeFileCheckBox", "ExcludeFileTextBox", "--excludefile", v.Replace("\"", string.Empty))) },
                { "iL=", "", v => CurrentCommandOptions.Add(new CommandOption("TargetListFileCheckBox", "TargetListFileTextBox", "-iL", v.Replace("\"", string.Empty))) },
                { "iR=", "", v => CurrentCommandOptions.Add(new CommandOption("ScanRandomHostCheckBox", "ScanRandomHostTextBox", "-iR", v)) },
                { "p=", "", v => CurrentCommandOptions.Add(new CommandOption("PortsToScanCheckBox", "PortsToScanTextBox", "-p", v)) },
                { "F", "", v => CurrentCommandOptions.Add(new CommandOption("FastScanCheckBox", "-F")) },
                // Source page
                { "D=", "", v => CurrentCommandOptions.Add(new CommandOption("", "-D", v)) },
                { "S=", "", v => CurrentCommandOptions.Add(new CommandOption("", "-S", v)) },
                { "source-port=", "", v => CurrentCommandOptions.Add(new CommandOption("", "--source-port", v)) },
                { "g=", "", v => CurrentCommandOptions.Add(new CommandOption("", "-g", v)) },
                { "e=", "", v => CurrentCommandOptions.Add(new CommandOption("", "-e", v)) },
                // Timing page
                { "host-timeout=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxScanTimeCheckBox", "MaxScanTimeTextBox", "--host-timeout", v)) },
                { "max-rtt-timeout=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxProbeTimeoutCheckBox", "MaxProbeTimeoutTextBox", "--max-rtt-timeout", v)) },
                { "min-rtt-timeout=", "", v => CurrentCommandOptions.Add(new CommandOption("MinProbeTimeoutCheckBox", "MinProbeTimeoutTextBox", "--min-rtt-timeout", v)) },
                { "initial-rtt-timeout=", "", v => CurrentCommandOptions.Add(new CommandOption("InitialProbeTimeoutCheckBox", "InitialProbeTimeoutTextBox", "--initial-rtt-timeout", v)) },
                { "max-host-group=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxHostGroupsCheckBox", "MaxHostGroupsTextBox", "--max-host-group", v)) },
                { "min-host-group=", "", v => CurrentCommandOptions.Add(new CommandOption("MinHostGroupsCheckBox", "MinHostGroupsTextBox", "--min-host-group", v)) },
                { "max-parallelism=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxOutStandingProbesCheckBox", "MaxOutStandingProbesTextBox", "--max-parallelism", v)) },
                { "min-parallelism=", "", v => CurrentCommandOptions.Add(new CommandOption("MinOutStandingProbesCheckBox", "MinOutStandingProbesTextBox", "--min-parallelism", v)) },
                { "max-scan_delay=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxScanDelayCheckBox", "MaxScanDelayTextBox", "--max-scan-delay", v)) },
                { "scan-delay=", "", v => CurrentCommandOptions.Add(new CommandOption("ScanDelayCheckBox", "ScanDelayTextBox", "--scan-delay", v)) },
                // Output page
                { "oN=", "", v => CurrentCommandOptions.Add(new CommandOption("OutputNormalCheckBox", "OutputNormalTextBox", "-oN", v.Replace("\"", string.Empty))) },
                { "oX=", "", v => CurrentCommandOptions.Add(new CommandOption("OutputXmlCheckBox", "OutputXmlTextBox", "-oX", v.Replace("\"", string.Empty))) },
                { "oS=", "", v => CurrentCommandOptions.Add(new CommandOption("OutputScriptCheckBox", "OutputScriptTextBox", "-oS", v.Replace("\"", string.Empty))) },
                { "oG=", "", v => CurrentCommandOptions.Add(new CommandOption("OutputGrepCheckBox", "OutputGrepTextBox", "-oG", v.Replace("\"", string.Empty))) },
                { "oA=", "", v => CurrentCommandOptions.Add(new CommandOption("OuputBaseCheckBox", "OuputBaseTextBox", "-oA", v.Replace("\"", string.Empty))) },
                { "reason", "", v => CurrentCommandOptions.Add(new CommandOption("ReasonCheckBox", "--reason")) },
                { "open", "", v => CurrentCommandOptions.Add(new CommandOption("OpenCheckBox", "--open")) },
                { "append-output", "", v => CurrentCommandOptions.Add(new CommandOption("OutputAppendCheckBox", "--append-output")) },
                { "resume=", "", v => CurrentCommandOptions.Add(new CommandOption("OutputResumeCheckBox", "--resume")) },
                // DNS page
                { "n", "", v => CurrentCommandOptions.Add(new CommandOption("DisableDnsCheckBox", "-n")) },
                { "resolve-all", "", v => CurrentCommandOptions.Add(new CommandOption("ResolveAllDnsCheckBox", "--resolve-all")) },
                { "system-dns", "", v => CurrentCommandOptions.Add(new CommandOption("SystemDnsCheckBox", "--system-dns")) },
                { "dns-servers=", "", v => CurrentCommandOptions.Add(new CommandOption("DnsServerCheckBox", "DnsServerTextBox", "--dns-servers", v)) },
                // Misc page
                { "ttl=", "", v => CurrentCommandOptions.Add(new CommandOption("IPv4TtlCheckBox", "IPv4TtlTextBox", "--ttl", v)) },
                { "f", "", v => CurrentCommandOptions.Add(new CommandOption("FragmentIpPacketsCheckBox", "-f")) },
                { "v:", "", v => CurrentCommandOptions.Add(new CommandOption("VerbosityLevelCheckBox", "VerbosityLevelTextBox", "-v", v != null ? v : "1")) },
                { "d:", "", v => CurrentCommandOptions.Add(new CommandOption("DebugLevelCheckBox", "DebugLevelTextBox", "-d", v != null ? v : "1")) },
                { "packettrace", "", v => CurrentCommandOptions.Add(new CommandOption("PacketTraceCheckBox", "--packettrace")) },
                { "r", "", v => CurrentCommandOptions.Add(new CommandOption("DisableRandomPortsCheckBox", "-r")) },
                { "traceroute", "", v => CurrentCommandOptions.Add(new CommandOption("TracerouteCheckBox", "--traceroute")) },
                { "max-retries=", "", v => CurrentCommandOptions.Add(new CommandOption("MaxRetriesCheckBox", "MaxRetriesTextBox", "--max-retries", v)) },
            };

            AllCommandOptions = new List<CommandOption>
            {
                // Scan page
                new CommandOption("AdvancedCheckBox", "-A"),
                new CommandOption("OperatingSystemCheckBox", "-O"),
                new CommandOption("VersionDetectionCheckBox", "-sV"),
                new CommandOption("IdleScanCheckBox", "IdleScanTextBox", "-sI", ""),
                new CommandOption("FtpBounceCheckBox", "FtpBounceTextBox", "-b", ""),
                new CommandOption("Ipv6CheckBox", "-6"),
                new CommandOption("DisableArpPingCheckBox", "--disable-arp-ping"),
                new CommandOption("", "-sA", false),
                new CommandOption("", "-sF", false),
                new CommandOption("", "-sM", false),
                new CommandOption("", "-sN", false),
                new CommandOption("", "-sS", false),
                new CommandOption("", "-sT", false),
                new CommandOption("", "-sW", false),
                new CommandOption("", "-sX", false),
                new CommandOption("", "-sU", false),
                new CommandOption("", "-sO", false),
                new CommandOption("", "-sL", false),
                new CommandOption("", "-sn", false),
                new CommandOption("", "-sY", false),
                new CommandOption("", "-sZ", false),
                new CommandOption("", "-T0"),
                new CommandOption("", "-T1"),
                new CommandOption("", "-T2"),
                new CommandOption("", "-T3"),
                new CommandOption("", "-T4"),
                new CommandOption("", "-T5"),
                new CommandOption("DontPingCheckBox", "-Pn"),
                new CommandOption("IcmpPingCheckBox", "-PE"),
                new CommandOption("IcmpTimestampCheckBox", "-PP"),
                new CommandOption("IcmpNetmaskCheckBox", "-PM"),
                new CommandOption("AckPingCheckBox", "AckPingTextBox", "-PA", ""),
                new CommandOption("SynPingCheckBox", "SynPingTextBox", "-PS", ""),
                new CommandOption("UdpProbesCheckBox", "UdpProbesTextBox", "-PU", ""),
                new CommandOption("IpProtoProbesCheckBox", "IpProtoProbesTextBox", "-PO", ""),
                new CommandOption("SctpPingProbesCheckBox", "SctpPingProbesTextBox", "-PY", ""),
                new CommandOption("DecoysCheckBox", "DecoysTextBox", "-D", ""),
                new CommandOption("SourceIPCheckBox", "SourceIPTextBox", "-S", ""),
                new CommandOption("SourcePortCheckBox", "SourcePortTextBox", "--source-port", ""),
                new CommandOption("NetworkInterfaceCheckBox", "NetworkInterfaceTextBox", "-e", ""),
                new CommandOption("DisableArpPingCheckBox", "--disable-arp-ping"),
                new CommandOption("DiscoveryIgnoreRstCheckBox", "--discovery-ignore-rst"),
                // Scripting page
                new CommandOption("", "--script"),
                new CommandOption("", "--script-args"),
                // Target page
                new CommandOption("ExcludeCheckBox", "ExcludeTextBox", "--exclude", ""),
                new CommandOption("ExcludeFileCheckBox", "ExcludeFileTextBox", "--excludefile", ""),
                new CommandOption("TargetListFileCheckBox", "TargetListFileTextBox", "-iL", ""),
                new CommandOption("ScanRandomHostCheckBox", "ScanRandomHostTextBox","-iR", ""),
                new CommandOption("PortsToScanCheckBox", "PortsToScanTextBox", "-p", ""),
                new CommandOption("FastScanCheckBox", "-F"),
                // Source page
                new CommandOption("DecoysCheckBox", "DecoysTextBox", "-D", ""),
                new CommandOption("SourceIPCheckBox", "SourceIPTextBox", "-S", ""),
                new CommandOption("SourcePortCheckBox", "SourcePortTextBox", "--source-port", ""),
                new CommandOption("SourcePortCheckBox", "SourcePortTextBox", "-g", ""),
                new CommandOption("NetworkInterfaceCheckBox", "NetworkInterfaceTextBox", "-s", ""),
                // Timing page
                new CommandOption("MaxScanTimeCheckBox", "MaxScanTimeTextBox", "--host-timeout", ""),
                new CommandOption("MaxProbeTimeoutCheckBox", "MaxProbeTimeoutTextBox", "--max-rtt-timeout", ""),
                new CommandOption("MinProbeTimeoutCheckBox", "MinProbeTimeoutTextBox", "--min-rtt-timeout", ""),
                new CommandOption("InitialProbeTimeoutCheckBox", "InitialProbeTimeoutTextBox", "--initial-rtt-timeout", ""),
                new CommandOption("MaxHostGroupsCheckBox", "MaxHostGroupsTextBox", "--max-hostgroup", ""),
                new CommandOption("MinHostGroupsCheckBox", "MinHostGroupsTextBox", "--min-hostgroup", ""),
                new CommandOption("MaxOutStandingProbesCheckBox", "MaxOutStandingProbesTextBox", "--max-parallelism", ""),
                new CommandOption("MinOutStandingProbesCheckBox", "MinOutStandingProbesTextBox", "--min-parallelism", ""),
                new CommandOption("MaxScanDelayCheckBox", "MaxScanDelayTextBox", "--max-scan-delay", ""),
                new CommandOption("ScanDelayCheckBox", "ScanDelayTextBox", "--scan-delay", ""),
                // Output page
                new CommandOption("OutputNormalCheckBox", "OutputNormalTextBox",  "-oN", ""),
                new CommandOption("OutputXmlCheckBox", "OutputXmlTextBox", "-oX", ""),
                new CommandOption("OutputScriptCheckBox", "OutputScriptTextBox", "-oS", ""),
                new CommandOption("OutputGrepCheckBox", "OutputGrepTextBox", "-oG", ""),
                new CommandOption("OutputBaseCheckBox", "OutputBaseTextBox", "-oA", ""),
                new CommandOption("ReasonCheckBox", "--reason"),
                new CommandOption("OpenCheckBox", "--open"),
                new CommandOption("AppendOutputCheckBox", "--append-output"),
                new CommandOption("OutputResumeCheckBox", "OutpuResumeTextBox", "--resume", ""),
                // DNS page
                new CommandOption("DisableDnsCheckBox", "-n"),
                new CommandOption("ResolveAllDnsCheckBox", "--resolve-all"),
                new CommandOption("SystemDnsCheckBox", "--system-dns"),
                new CommandOption("DnsServerCheckBox", "DnsServerTextBox", "--dns-servers", ""),
                // Misc page
                new CommandOption("ExtraUserOptionsCheckBox", "ExtraUserOptionsTextBox", "-U", ""),
                new CommandOption("IPv4TtlCheckBox", "IPv4TtlTextBox", "--ttl", ""),
                new CommandOption("FragmentIpPacketsCheckBox", "-f"),
                new CommandOption("VerbosityLevelCheckBox", "VerbosityLevelTextBox", "-v", ""),
                new CommandOption("DebugLevelCheckBox", "DebugLevelCheckBox", "-d", ""),
                new CommandOption("PacketTraceCheckBox", "--packet-trace"),
                new CommandOption("DisableRandomPortsCheckBox", "-r"),
                new CommandOption("TracerouteCheckBox", "--traceroute"),
                new CommandOption("MaxRetriesCheckBox", "MaxRetriesTextBox", "--max-retries", ""),
            };
        }

        public CommandOption FindByCheckBox(string name)
        {
            return AllCommandOptions.Find(o => o.CheckBoxName != null && o.CheckBoxName.Equals(name));
        }

        public CommandOption FindByOption(string option)
        {
            return CurrentCommandOptions.Find(o => o.Qualifier.Equals(option));
        }

        public bool IsChecked(string option)
        {
            return CurrentCommandOptions.Find(o => o.Qualifier.Equals(option)) != null;
        }

        public void SetCheckBoxOption(string qualifier, bool value)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            if (value && currentOption == null)
            {
                CurrentCommandOptions.Add(commandOption);
                OnPropertyChanged("CommandString");
            }
            else if (currentOption != null)
            {
                CurrentCommandOptions.Remove(commandOption);
                OnPropertyChanged("CommandString");
            }
        }

        public void SetCheckBoxTextOption(string qualifier, bool value, string text)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            if (currentOption == null && value)
            {
                commandOption.Modifier = text;
                CurrentCommandOptions.Add(commandOption);
            }
            else
            {
                if (value)
                {
                    currentOption.Modifier = text;
                }
                else
                {
                    CurrentCommandOptions.RemoveAll(o => o.Qualifier.Equals(commandOption.Qualifier));
                }
            }
            OnPropertyChanged("CommandString");
        }

        public void SetOption(string qualifier)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            if (currentOption == null)
            {
                CurrentCommandOptions.Add(commandOption);
                OnPropertyChanged("CommandString");
            }
        }

        public void UnsetOption(string qualifier)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            if (currentOption != null)
            {
                CurrentCommandOptions.Remove(commandOption);
                OnPropertyChanged("CommandString");
            }
        }

        public void UnsetScanOptions(List<ScanOption> options)
        {
            options.ForEach(o => UnsetOption(o.Option));
        }

        public void UnsetTimingTemplateOptions(List<TimingTemplate> options)
        {
            options.ForEach(o => UnsetOption(o.Option));
        }

        public void UnsetOutputOptions(List<OutputOption> options)
        {
            options.ForEach(o => UnsetOption(o.Option));
        }

        /// <summary>
        /// Using the random host scan option does not need a target
        /// </summary>
        /// <returns>true in case a traget ist needed, otherwise false</returns>
        public bool NeedsTarget()
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("-iR"));
            return currentOption == null;
        }

        public void CheckScripts(List<Script> scriptList)
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script"));
            if (currentOption != null)
            {
                scriptList.ForEach(s =>
                {
                    if (currentOption.Modifier.Contains(s.Name))
                    {
                        s.IsSelected = true;
                    }
                });
            }
        }

        public void AddScript(string name)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals("--script"));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script"));
            if (currentOption == null)
            {
                commandOption.HasModifier = true;
                commandOption.Modifier = name;
                CurrentCommandOptions.Add(commandOption);
            }
            else
            {
                currentOption.Modifier += "," + name;
            }
            OnPropertyChanged("CommandString");
        }

        public void RemoveScript(string name)
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script"));
            if (currentOption != null)
            {
                string[] scripts = currentOption.Modifier.Split(',');
                scripts = scripts.Where(s => !s.Equals(name)).ToArray();
                if (scripts.Length > 0)
                {
                    currentOption.Modifier = string.Join(",", scripts);
                }
                else
                {
                    CurrentCommandOptions.Remove(currentOption);
                }
            }
            OnPropertyChanged("CommandString");
        }

        public void CheckScriptArguments(List<ScriptArgument> scriptArgumentList)
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script-args"));
            if (currentOption != null)
            {
                scriptArgumentList.ForEach(a =>
                {
                    if (currentOption.Modifier.Contains(a.Name))
                    {
                        string[] scriptArgs = currentOption.Modifier.Split(',');
                        string scriptArg = scriptArgs.First(s => s.StartsWith(a.Name));
                        if (scriptArg != null)
                        {
                            string[] pair = scriptArg.Split('=');
                            a.Value = pair[1];
                        }
                    }
                });
            }
        }

        public void AddScriptArg(string name, string value)
        {
            CommandOption commandOption = AllCommandOptions.Find(o => o.Qualifier.Equals("--script-args"));
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script-args"));
            if (currentOption == null)
            {
                commandOption.Modifier = name + "=" + value;
                CurrentCommandOptions.Add(commandOption);
            }
            else
            {
                currentOption.Modifier += "," + name + "=" + value;
            }
            OnPropertyChanged("CommandString");
        }

        public void RemoveScriptArg(string name, string value)
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals("--script-args"));
            if (currentOption != null)
            {
                string arg = name + "=" + value;
                string[] scriptArgs = currentOption.Modifier.Split(',');
                scriptArgs = scriptArgs.Where(s => !s.Equals(arg)).ToArray();
                if (scriptArgs.Length > 0)
                {
                    currentOption.Modifier = string.Join(",", scriptArgs);
                }
                else
                {
                    CurrentCommandOptions.Remove(currentOption);
                }
            }
            OnPropertyChanged("CommandString");
        }

        public string GetModifier(string qualifier)
        {
            CommandOption currentOption = CurrentCommandOptions.Find(o => o.Qualifier.Equals(qualifier));
            return currentOption != null ? currentOption.Modifier : string.Empty;
        }

        /// <summary>
        /// Returns the debug level from the command line options.
        /// </summary>
        /// <returns>debug level or -1 if not found.</returns>
        public int GetDebugLevel()
        {
            string modifier = GetModifier("-d");
            if (!string.IsNullOrEmpty(modifier))
            {
                return int.Parse(modifier);
            }
            return 0;
        }

        /// <summary>
        /// Returns the verbosity level from the command line options.
        /// </summary>
        /// <returns>verbosity level or -1 if not found.</returns>
        public int GetVerbosityLevel()
        {
            string modifier = GetModifier("-v");
            if (!string.IsNullOrEmpty(modifier))
            {
                return int.Parse(modifier);
            }
            return 0;
        }

        /// <summary>
        /// Validate the all options, which need a argument actually have a argument.
        /// </summary>
        /// <returns>true if command options are valid, otherwise false.</returns>
        public bool ValidateOptions()
        {
            bool result = true;
            CurrentCommandOptions.ForEach(o =>
            {
                if (o.HasModifier && string.IsNullOrEmpty(o.Modifier))
                {
                    MessageBox.Show("Options " + o.Qualifier + " needs a value!", "Profiles", MessageBoxButton.OK, MessageBoxImage.Warning);
                    result = false;
                }
            });
            return result;
        }

        /// <summary>
        /// Validate the command line.
        /// </summary>
        /// <param name="command">command line to validate</param>
        /// <returns>string containing unknown options</returns>
        public string ValidateCommand(string command)
        {
            ParseCommand(command.Remove(0, 5));
            if (Extra.Count > 0)
            {
                return string.Join(",", Extra.ToArray());
            }
            return string.Empty;
        }

        /// <summary>
        /// Parse the given command line.
        /// </summary>
        /// <param name="command">command line to parse</param>
        private void ParseCommand(string command)
        {
            if (command == null)
            {
                logger.WarnFormat("Empty command found");
                return;
            }

            CurrentCommandOptions.Clear();

            string strRegex = @"[ ](?=(?:[^""]*""[^""]*"")*[^""]*$)";
            Regex myRegex = new Regex(strRegex, RegexOptions.Multiline);

            string[] args = myRegex.Split(command);
            try
            {
                Extra = optionSet.Parse(args);
            }
            catch (OptionException e)
            {
                logger.ErrorFormat("Could not parse command line - error: {0}", e.Message);
            }
        }

        /// <summary>
        /// Returns the current command line, build from the current options.
        /// </summary>
        /// <returns>full command line</returns>
        private string GetCommandString()
        {
            string commandString = "nmap ";
            CurrentCommandOptions.ForEach(o =>
            {
                if (o.Qualifier.Equals("-v") || o.Qualifier.Equals("-d"))
                {
                    commandString += o.Qualifier + (!string.IsNullOrEmpty(o.Modifier) && int.Parse(o.Modifier) > 1 ? o.Modifier : "") + " ";
                }
                else if (o.Qualifier.Equals("--excludefile") || o.Qualifier.Equals("-iL") || o.Qualifier.Equals("-oN") || o.Qualifier.Equals("-oX") || o.Qualifier.Equals("-oS") || o.Qualifier.Equals("-oG") || o.Qualifier.Equals("-oA"))
                {
                    commandString += o.Qualifier + " \"" + o.Modifier + "\" ";
                }
                else
                {
                    commandString += o.HasModifier && !string.IsNullOrEmpty(o.Modifier) ? o.Qualifier + " " + o.Modifier + " " : o.Qualifier + " ";
                }
            });
            return commandString.Trim();
        }

        /// <summary>
        /// Clear the current command line options.
        /// </summary>
        public void Clear()
        {
            CurrentCommandOptions.Clear();
        }

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
