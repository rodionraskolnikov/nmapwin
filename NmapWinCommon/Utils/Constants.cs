﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
#if !DEBUG
using System;
#endif
using System.IO;

namespace NmapWinUI.Utils
{
    public class Constants
    {
        /// <summary>
        /// Base directory
        /// </summary>
#if DEBUG
        public static readonly string BaseDirectory = @"D:\work\nmapwin";
#else
        public static readonly string BaseDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..");
#endif
        /// <summary>
        /// Database directory
        /// </summary>
        public static readonly string DatabaseDirectory = Path.Combine(BaseDirectory, "data");

        /// <summary>
        /// Database directory
        /// </summary>
        public static readonly string NmapHelpDirectory = Path.Combine(BaseDirectory, "help", "nmap");

        /// <summary>
        /// SQLServer CE file
        /// </summary>
        public static readonly string DatabaseFile = Path.Combine(DatabaseDirectory, "nmap.sdf");

        /// <summary>
        /// SQLServer CE connection string
        /// </summary>
        public static readonly string ConnectionString = @"Data Source=" + DatabaseFile + ";Persist Security Info=False; Max Database Size=4091;";

        /// <summary>
        /// Backup directory
        /// </summary>
        public static readonly string BackupDirectory = Path.Combine(BaseDirectory, "backup");

        public static string DateFormat { get; set; } = "HH:mm:ss";

        public static string TimeFormat { get; set; } = "HH:mm:ss";
    }
}

