﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
namespace NmapWinCommon.Utils
{
    public class CommandOption
    {
        public string CheckBoxName { get; set; }

        public string TextBoxName { get; set; }

        public string ComboBoxName { get; set; }

        public string Qualifier { get; set; }

        public string Modifier { get; set; }

        public bool HasModifier { get; set; }

        public bool IsComboBox { get; set; }

        public CommandOption(string comboBoxName, string qualifier, bool isComboBox)
        {
            ComboBoxName = comboBoxName;
            Qualifier = qualifier;
            IsComboBox = isComboBox;
        }

        public CommandOption(string checkBoxName, string qualifier)
        {
            CheckBoxName = checkBoxName;
            Qualifier = qualifier;
        }

        public CommandOption(string checkBoxName, string qualifier, string modifier)
        {
            CheckBoxName = checkBoxName;
            Qualifier = qualifier;
            Modifier = modifier;
            HasModifier = true;
        }

        public CommandOption(string checkBoxName, string textBoxName, string qualifier, string modifier)
        {
            CheckBoxName = checkBoxName;
            TextBoxName = textBoxName;
            Qualifier = qualifier;
            Modifier = modifier;
            HasModifier = true;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(CommandOption)) return false;
            return Qualifier == (obj as CommandOption).Qualifier;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
