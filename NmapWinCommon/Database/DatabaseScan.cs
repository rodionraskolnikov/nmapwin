﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseScan
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseScan> lazy = new Lazy<DatabaseScan>(() => new DatabaseScan());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseScan Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseScan()
        {

        }
        #endregion

        /// <summary>
        /// Returns a list of all scans.
        /// </summary>
        /// <returns>list of all scans</returns>
        public List<Scan> FindScans()
        {
            using var db = new DatabaseContext();
            var query = from s in db.Scans orderby s.Id select s;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all scans.
        /// </summary>
        /// <returns>list of all scans</returns>
        public List<Scan> FindFullScans()
        {
            using var db = new DatabaseContext();
            var query = from s in db.Scans
                        .Include("Hosts")
                        orderby s.Id
                        select s;
            return query.ToList();
        }

        /// <summary>
        /// Returns a single scan by ID.
        /// </summary>
        /// <returns>scan with given ID</returns>
        public Scan FindScanById(long id)
        {
            using var db = new DatabaseContext();
            return db.Scans.SingleOrDefault(s => s.Id == id);
        }

        /// <summary>
        /// Adds a new host to the scan.
        /// </summary>
        /// <param name="scan">scan to update</param>
        /// <param name="host">host to add</param>
        /// <returns>newly created host</returns>
        public Host AddHost(Scan scan, Host host)
        {
            using var db = new DatabaseContext();
            Scan s = db.Scans.SingleOrDefault(s => s.Id == scan.Id);
            Host h = db.Hosts.SingleOrDefault(h => h.Id == host.Id);
            s.Hosts.Add(h);
            db.SaveChanges();
            return host;
        }

        /// <summary>
        /// Insert a a new scan.
        /// </summary>
        /// <param name="scan">scan to insert</param>
        public Scan InsertScan(Scan scan)
        {
            using var db = new DatabaseContext();
            scan = db.Scans.Add(scan);
            db.SaveChanges();
            return scan;
        }

        /// <summary>
        /// Updates a scan
        /// </summary>
        /// <param name="scan">Scan to update</param>
        public Scan UpdateScan(Scan scan)
        {
            using var db = new DatabaseContext();
            Scan s = db.Scans.SingleOrDefault(s => s.Id == scan.Id);
            if (s != null)
            {
                db.Entry(s).CurrentValues.SetValues(scan);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Deletes a scan
        /// </summary>
        /// <param name="scan">Scan to delete</param>
        public void DeleteScan(Scan scan)
        {
            using var db = new DatabaseContext();
            Scan s = db.Scans.Include("Hosts").First(s => s.Id == scan.Id);
            s.Hosts.Clear();
            db.Scans.Remove(s);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes all scans
        /// </summary>
        public void DeleteAllScans()
        {
            List<Scan> scans = FindScans();
            scans.ForEach(s => DeleteScan(s));
        }
    }
}
