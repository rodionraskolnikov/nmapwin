﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using FluentMigrator.Runner;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Configuration;
using System.Data.SqlServerCe;
using System.Data.SQLite;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using NmapWinCommon.Migrations;
using NmapWinCommon.Utils;
using System.Data.Entity;

namespace NmapWinCommon.Database
{
    /// <summary>
    /// Database tools for creating, deleting, migration of the database.
    /// </summary>
    public class DatabaseTools
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(DatabaseTools));

        /// <summary>
        /// Database history singleton.
        /// </summary>
        public static DatabaseHistory DatabaseHistory = DatabaseHistory.Instance;

        /// <summary>
        /// Maximum value for the main progress bar
        /// </summary>
        private static readonly int MainMaximum = 3;

        /// <summary>
        /// Maximum value for the detailed progress bar
        /// </summary>
        private static readonly int DetailedMaximum = 5;
        #endregion

        #region Database migration
        /// <summary>
        /// Upgrade the database using fluent migration.
        /// </summary>
        public bool UpgradeDatabase()
        {
            string databaseShortName = GetDatabaseShortName();
            OnProgressChanged("Starting " + databaseShortName + " database...", "Checking migration...", 1, 0, MainMaximum, DetailedMaximum);
            logger.InfoFormat("Starting database upgrade");

            // Ensure database exists
            if (!EnsureDatabaseExists())
            {
                return false;
            }

            var serviceProvider = CreateServices();
            if (serviceProvider != null)
            {
                logger.InfoFormat("Starting database");

                // Put the database update into a scope to ensure that all resources will be disposed.
                using (var scope = serviceProvider.CreateScope())
                {
                    UpdateDatabase(scope.ServiceProvider);
                    logger.InfoFormat("Database migrated");
                    OnProgressChanged("Starting " + databaseShortName + " database...", "Database migrated...", 2, 0, MainMaximum, DetailedMaximum);
                }

                // Install default objects
                DefaultObjects defaultObjects = new DefaultObjects();
                defaultObjects.DatabaseEvent += HandleDatabaseChanges;
                defaultObjects.InstallDefaults();
                OnProgressChanged("Database " + databaseShortName + " migrated...", "Defaults installed...", 3, 0, MainMaximum, DetailedMaximum);
                logger.InfoFormat("Default objects loaded");
            }
            return true;
        }
        #endregion

        #region Delete database
        /// <summary>
        /// Delete the current database.
        /// </summary>
        public void DeleteDatabase()
        {
            string databaseShortName = GetDatabaseShortName();
            OnProgressChanged("Deleting " + databaseShortName + " database...", "Checking migration...", 1, 0, MainMaximum, DetailedMaximum);
            logger.InfoFormat("Starting database delete");

            var serviceProvider = CreateServices();
            if (serviceProvider != null)
            {
                logger.InfoFormat("Starting database");

                // Put the database update into a scope to ensure that all resources will be disposed.
                using var scope = serviceProvider.CreateScope();
                DeleteDatabase(scope.ServiceProvider);
                logger.InfoFormat("Database deleted");
                OnProgressChanged("Delete " + databaseShortName + " database...", "Database deleted...", 2, 0, MainMaximum, DetailedMaximum);

                UpdateDatabase(scope.ServiceProvider);
                logger.InfoFormat("Database created");
                OnProgressChanged("Starting " + databaseShortName + " database...", "Database created...", 2, 0, MainMaximum, DetailedMaximum);
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Configure the dependency injection services
        /// </summary>
        private IServiceProvider CreateServices()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            logger.InfoFormat("Got database configuration - provider: {0} connectionString: {1}", providerName, connectionstring);

            switch (providerName)
            {
                case "MySql.Data.MySqlClient":
                    logger.InfoFormat("Using MySQL database ");
                    return new ServiceCollection()
                        // Add common FluentMigrator services
                        .AddFluentMigratorCore()
                        .ConfigureRunner(rb => rb
                            // Add SQLite support to FluentMigrator
                            .AddMySql5()
                            // Set the connection string
                            .WithGlobalConnectionString(connectionstring)
                            // Define the assembly containing the migrations
                            .ScanIn(typeof(Initial).Assembly).For.Migrations())
                        // Enable logging to console in the FluentMigrator way
                        .AddLogging(lb => lb.AddFluentMigratorConsole())
                        // Build the service provider
                        .BuildServiceProvider(false);
                case "System.Data.SQLite":
                    logger.InfoFormat("Using SQLite database ");
                    return new ServiceCollection()
                        // Add common FluentMigrator services
                        .AddFluentMigratorCore()
                        .ConfigureRunner(rb => rb
                            // Add SQLite support to FluentMigrator
                            .AddSQLite()
                            // Set the connection string
                            .WithGlobalConnectionString(connectionstring)
                            // Define the assembly containing the migrations
                            .ScanIn(typeof(Initial).Assembly).For.Migrations())
                        // Enable logging to console in the FluentMigrator way
                        .AddLogging(lb => lb.AddFluentMigratorConsole())
                        // Build the service provider
                        .BuildServiceProvider(false);
                case "System.Data.SqlClient":
                    logger.InfoFormat("Using MSSQL database ");
                    return new ServiceCollection()
                        // Add common FluentMigrator services
                        .AddFluentMigratorCore()
                        .ConfigureRunner(rb => rb
                            // Add MSSQL Server support to FluentMigrator
                            .AddSqlServer()
                            // Set the connection string
                            .WithGlobalConnectionString(connectionstring)
                            // Define the assembly containing the migrations
                            .ScanIn(typeof(Initial).Assembly).For.Migrations())
                        // Enable logging to console in the FluentMigrator way
                        .AddLogging(lb => lb.AddFluentMigratorConsole())
                        // Build the service provider
                        .BuildServiceProvider(false);
                case "System.Data.SqlServerCe.4.0":
                    logger.InfoFormat("Using SQLServerCE database ");
                    return new ServiceCollection()
                        // Add common FluentMigrator services
                        .AddFluentMigratorCore()
                        .ConfigureRunner(rb => rb
                            // Add SQLServerCE support to FluentMigrator
                            .AddSqlServerCe()
                            // Set the connection string
                            .WithGlobalConnectionString(connectionstring)
                            // Define the assembly containing the migrations
                            .ScanIn(typeof(Initial).Assembly).For.Migrations())
                        // Enable logging to console in the FluentMigrator way
                        .AddLogging(lb => lb.AddFluentMigratorConsole())
                        // Build the service provider
                        .BuildServiceProvider(false);
                default:
                    return null;
            }
        }

        /// <summary>
        /// Update the database
        /// </summary>
        private void UpdateDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateUp();
        }

        /// <summary>
        /// Update the database
        /// </summary>
        private void DeleteDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateDown(0L);
        }

        /// <summary>
        /// Check existence of database file for SQLite and SQLServerCE and JDBC connectivity for a 
        /// MySQL or MSSQL database.
        /// </summary>
        private bool EnsureDatabaseExists()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            logger.InfoFormat("Ensure database exists - provider: {0} connectionString: {1}", providerName, connectionstring);

            switch (providerName)
            {
                case "System.Data.SqlServerCe.4.0":
                    OnProgressChanged("Starting SQL Server Compact database...", "Checking database connection...", 1, 0, MainMaximum, DetailedMaximum);
                    string[] tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                    string fileName = tmp[0].Split('=')[1];
                    logger.InfoFormat("Got database file - path: {0}", fileName);
                    if (!File.Exists(fileName))
                    {
                        SqlCeEngine en = new SqlCeEngine(connectionstring);
                        en.CreateDatabase();
                    }
                    return true;
                case "System.Data.SQLite":
                    OnProgressChanged("Starting SQLite database...", "Checking database connection...", 1, 0, MainMaximum, DetailedMaximum);
                    tmp = connectionstring.Split(new char[] { ';', '\n', '\r' });
                    fileName = tmp[0].Split('=')[1];
                    logger.InfoFormat("Got database file - path: {0}", fileName);
                    if (!File.Exists(fileName))
                    {
                        SQLiteConnection.CreateFile(fileName);
                    }
                    return true;
                case "MySql.Data.MySqlClient":
                    OnProgressChanged("Starting MySQL database...", "Checking database connection...", 1, 0, MainMaximum, DetailedMaximum);
                    using (LocalDatabaseContext dbContext = new LocalDatabaseContext())
                    {
                        try
                        {
                            dbContext.Database.Connection.Open();
                            dbContext.Database.Connection.Close();
                        }
                        catch (Exception ex)
                        {
                            logger.ErrorFormat("Cannot connect to database - provider: {0} connection: {1} error: {2}", providerName, connectionstring, ex.Message);
                            MessageBox.Show("Failed to open a MySQL database connection!", "Database connection", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                            return false;
                        }
                        return true;
                    }
                case "System.Data.SqlClient":
                    OnProgressChanged("Starting MSSQL database...", "Checking database connection...", 1, 0, MainMaximum, DetailedMaximum);
                    using (LocalDatabaseContext dbContext = new LocalDatabaseContext())
                    {
                        try
                        {
                            dbContext.Database.Connection.Open();
                            dbContext.Database.Connection.Close();
                        }
                        catch (Exception ex)
                        {
                            logger.ErrorFormat("Cannot connect to database - provider: {0} connection: {1} error: {2}", providerName, connectionstring, ex.Message);
                            MessageBox.Show("Failed to open a MSSQL database connection!", "Database connection", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                            return false;
                        }
                        return true;
                    }
            }
            return false;
        }

        private string GetDatabaseShortName()
        {
            string providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            return providerName switch
            {
                "System.Data.SqlServerCe.4.0" => "SQLServerCE",
                "System.Data.SQLite" => "SQLite",
                "MySql.Data.MySqlClient" => "MySQL",
                "System.Data.SqlClient" => "MSSQL",
                _ => "Unknown",
            };
        }
        #endregion

        #region Event handling
        /// <summary>
        /// Handle database change events.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void HandleDatabaseChanges(object sender, EventArgs e)
        {
            if (DatabaseEvent != null)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    DatabaseEvent(this, e);
                }));
            }
        }

        public event EventHandler DatabaseEvent;

        private void OnProgressChanged(string mainMessage, string detailedMessage, double mainProgress, double detailedProgress, double mainMaximum, double detailedMaximum)
        {
            ProgressEventArgs args = new ProgressEventArgs
            {
                MainMessage = mainMessage,
                MainProgress = mainProgress,
                MainMaximum = mainMaximum,
                DetailedMessage = detailedMessage,
                DetailedProgress = detailedProgress,
                DetailedMaximum = detailedMaximum
            };
            if (DatabaseEvent != null)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    DatabaseEvent(this, args);
                }));
            }
        }
        #endregion
    }

    /// <summary>
    /// Local database context, just to check connectivity.
    /// </summary>
    public class LocalDatabaseContext : DbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocalDatabaseContext() : base("name=ConnectionString")
        {
        }
    }
}
