﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseHostName
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseHostName> lazy = new Lazy<DatabaseHostName>(() => new DatabaseHostName());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseHostName Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseHostName()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all hostNames.
        /// </summary>
        /// <returns>list of all hostNames</returns>
        public List<HostName> FindHostNames()
        {
            using var db = new DatabaseContext();
            var query = from h in db.HostNames orderby h.Id select h;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all hostNames for a host.
        /// </summary>
        /// <returns>list of all hostNames for a host</returns>
        public List<HostName> FindHostNamesByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from h in db.HostNames where h.HostId == hostId orderby h.Name select h;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new hostName.
        /// </summary>
        /// <param name="hostName">hostName to insert</param>
        /// <returns>newly created hostName</returns>
        public HostName InsertHostName(HostName hostName)
        {
            using var db = new DatabaseContext();
            hostName = db.HostNames.Add(hostName);
            db.SaveChanges();
            return hostName;
        }

        /// <summary>
        /// Updates a hostName
        /// </summary>
        /// <param name="hostName">hostName to update</param>
        public HostName UpdateHostName(HostName hostName)
        {
            using var db = new DatabaseContext();
            HostName h = db.HostNames.SingleOrDefault(h => h.Id == hostName.Id);
            if (h != null)
            {
                db.Entry(h).CurrentValues.SetValues(hostName);
                db.SaveChanges();
            }
            return h;
        }

        /// <summary>
        /// Deletes a hostName
        /// </summary>
        /// <param name="hostName">hostName to delete</param>
        public void DeleteHostName(HostName hostName)
        {
            using var db = new DatabaseContext();
            HostName p = db.HostNames.SingleOrDefault(p => p.Id == hostName.Id);
            db.HostNames.Remove(p);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a hostName
        /// </summary>
        /// <param name="hostName">hostName to delete</param>
        public void DeleteHostNames(List<HostName> hostNames)
        {
            hostNames.ForEach(p => DeleteHostName(p));
        }

        /// <summary>
        /// Deletes all hostNames
        /// </summary>
        public void DeleteAllHostNames()
        {
            List<HostName> hostNames = FindHostNames();
            hostNames.ForEach(p => DeleteHostName(p));
        }
    }
}
