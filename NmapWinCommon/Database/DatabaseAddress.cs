﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseAddress
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseAddress> lazy = new Lazy<DatabaseAddress>(() => new DatabaseAddress());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseAddress Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseAddress()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all addresses.
        /// </summary>
        /// <returns>list of all addresses</returns>
        public List<Address> FindAddresses()
        {
            using var db = new DatabaseContext();
            var query = from a in db.Addresses orderby a.Id select a;
            return query.ToList();
        }

        /// <summary>
        /// Returns a single address by ID.
        /// </summary>
        /// <returns>address with given ID</returns>
        public Address FindAddressById(long id)
        {
            using var db = new DatabaseContext();
            return db.Addresses.SingleOrDefault(a => a.Id == id);
        }

        /// <summary>
        /// Returns a address by addr.
        /// </summary>
        /// <param name="addr">addr of the address</param>
        /// <returns>address with given IP address</returns>
        public Address FindAddressByAddr(string addr)
        {
            using var db = new DatabaseContext();
            return db.Addresses.SingleOrDefault(a => a.Addr.Equals(addr));
        }

        /// <summary>
        /// Returns all addresses of a host.
        /// </summary>
        /// <param name="hostId">ID of the host</param>
        /// <returns>addresses of the host with given ID</returns>
        public List<Address> FindAddressByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from a in db.Addresses where a.HostId == hostId orderby a.Id select a;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new address.
        /// </summary>
        /// <param name="address">address to insert</param>
        /// <param name="hostId">ID of the host</param>
        /// <returns>newly created address</returns>
        public Address InsertAddress(Address address)
        {
            using var db = new DatabaseContext();
            address = db.Addresses.Add(address);
            db.SaveChanges();
            return address;
        }

        /// <summary>
        /// Updates a address
        /// </summary>
        /// <param name="scan">address to update</param>
        public Address UpdateAddress(Address address)
        {
            using var db = new DatabaseContext();
            Address a = db.Addresses.SingleOrDefault(a => a.Id == address.Id);
            if (a != null)
            {
                db.Entry(a).CurrentValues.SetValues(address);
                db.SaveChanges();
            }
            return a;
        }

        /// <summary>
        /// Deletes a address
        /// </summary>
        /// <param name="address">address to delete</param>
        public void DeleteAddress(Address address)
        {
            using var db = new DatabaseContext();
            Address a = db.Addresses.First(a => a.Id == address.Id);
            db.Addresses.Remove(a);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a list of addresses
        /// </summary>
        /// <param name="addresses">list of addresses to delete</param>
        public void DeleteAddresses(List<Address> addresses)
        {
            addresses.ForEach(a => DeleteAddress(a));
        }

        /// <summary>
        /// Deletes all addresses
        /// </summary>
        public void DeleteAllAddresses()
        {
            List<Address> addresses = FindAddresses();
            addresses.ForEach(a => DeleteAddress(a));
        }
    }
}
