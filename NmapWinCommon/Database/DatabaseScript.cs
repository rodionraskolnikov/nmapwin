﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseScript
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseScript> lazy = new Lazy<DatabaseScript>(() => new DatabaseScript());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseScript Instance { get { return lazy.Value; } }

        /// <summary>
        /// Script arguments.
        /// </summary>
        public List<ScriptArgument> scriptArguments { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseScript()
        {
            scriptArguments = new List<ScriptArgument>();
        }
        #endregion

        /// <summary>
        /// Return total count of entities
        /// </summary>
        /// <returns>total count</returns>
        public int Count()
        {
            using var db = new DatabaseContext();
            return (from o in db.Scripts select o).Count();
        }

        /// <summary>
        /// Check existence
        /// </summary>
        /// <returns>true if existence, otherwise false</returns>
        public bool Exists()
        {
            try
            {
                using var db = new DatabaseContext();
                return (from s in db.Scripts select s).Count() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a list of all scripts.
        /// </summary>
        /// <remarks>The dependent script arguments are loaded as well.</remarks>
        /// <returns>list of all scripts</returns>
        public List<Script> FindScripts()
        {
            using var db = new DatabaseContext();
            var query = from s in db.Scripts
                        .Include("Arguments")
                        .Include("Types")
                        .Include("Categories")
                        orderby s.Name
                        select s;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all scripts by category.
        /// </summary>
        /// <remarks>The dependent script arguments are loaded as well.</remarks>
        /// <param name="category">ID of the Script</param>
        /// <returns>list of all scripts</returns>
        public List<Script> FindScriptsByCategory(ScriptCategory.ScriptCategories category)
        {
            using var db = new DatabaseContext();
            var query = from s in db.Scripts
                        .Include("Arguments")
                        .Include("Types")
                        .Include("Categories")
                        where s.Categories.Any(c => c.Category == category)
                        orderby s.Name
                        select s;
            return query.ToList();
        }

        /// <summary>
        /// Returns a script by ID.
        /// </summary>
        /// <param name="id">ID of the Script</param>
        /// <returns>script with given ID</returns>
        public Script FindScriptById(long id)
        {
            using var db = new DatabaseContext();
            return db.Scripts
                        .Include("Arguments")
                        .Include("Types")
                        .Include("Categories")
                        .FirstOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// Returns a script by name.
        /// </summary>
        /// <param name="name">name of the Script</param>
        /// <returns>script with given name</returns>
        public Script FindScriptByName(string name)
        {
            using var db = new DatabaseContext();
            return db.Scripts
                        .Include("Arguments")
                        .Include("Types")
                        .Include("Categories")
                        .FirstOrDefault(p => p.Name == name);
        }

        /// <summary>
        /// Insert a new script.
        /// </summary>
        /// <param name="script">script to insert</param>
        public Script InsertScript(Script script)
        {
            using var db = new DatabaseContext();
            script = db.Scripts.Add(script);
            db.SaveChanges();
            return script;
        }

        /// <summary>
        /// Insert a new script type.
        /// </summary>
        /// <param name="script">script type to insert</param>
        public ScriptType InsertScriptType(ScriptType scriptType)
        {
            using var db = new DatabaseContext();
            scriptType = db.ScriptTypes.Add(scriptType);
            db.SaveChanges();
            return scriptType;
        }

        /// <summary>
        /// Insert a new script category.
        /// </summary>
        /// <param name="script">script category to insert</param>
        public ScriptCategory InsertScriptCategory(ScriptCategory scriptCategory)
        {
            using var db = new DatabaseContext();
            scriptCategory = db.ScriptCategories.Add(scriptCategory);
            db.SaveChanges();
            return scriptCategory;
        }

        /// <summary>
        /// Adds a new script argument.
        /// </summary>
        /// <param name="scriptArgument">script argument to insert</param>
        public ScriptArgument InsertScriptArgument(ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();
            scriptArgument = db.ScriptArguments.Add(scriptArgument);
            db.SaveChanges();
            return scriptArgument;
        }

        /// <summary>
        /// Adds a new script type to a script.
        /// </summary>
        /// <param name="scriptType">script type to insert</param>
        public Script AddScriptType(Script script, ScriptType scriptType)
        {
            using var db = new DatabaseContext();
            Script s = db.Scripts.Include("Types").FirstOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.Types.Add(scriptType);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Removes a script type to from script.
        /// </summary>
        /// <param name="scriptType">script type to remove</param>
        public Script RemoveScriptType(Script script, ScriptType scriptType)
        {
            using var db = new DatabaseContext();

            // Delete script type relation
            ScriptType st = db.ScriptTypes.FirstOrDefault(st => st.Id == scriptType.Id);
            scriptType = db.ScriptTypes.Remove(st);

            // Update script
            Script s = db.Scripts
                .Include("Types")
                .Include("Categories")
                .Include("Arguments")
                .SingleOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.Types.Remove(scriptType);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Adds a new category type to a script.
        /// </summary>
        /// <param name="scriptCategory">category to add</param>
        public Script AddScriptCategory(Script script, ScriptCategory scriptCategory)
        {
            using var db = new DatabaseContext();
            Script s = db.Scripts
                .Include("Types")
                .Include("Categories")
                .Include("Arguments")
                .SingleOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.Categories.Add(scriptCategory);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Removes a category to from script.
        /// </summary>
        /// <param name="scriptCategory">category to remove</param>
        public Script RemoveCategory(Script script, ScriptCategory scriptCategory)
        {
            using var db = new DatabaseContext();

            // Delete category type relation
            ScriptCategory sc = db.ScriptCategories.FirstOrDefault(sc => sc.Id == scriptCategory.Id);
            scriptCategory = db.ScriptCategories.Remove(sc);

            // Update category
            Script s = db.Scripts.Include("Categories").FirstOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.Categories.Remove(scriptCategory);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Adds a new script argument to a script.
        /// </summary>
        /// <param name="scriptArgument">script argument to insert</param>
        public Script AddScriptArgument(Script script, ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();
            Script s = db.Scripts
                .Include("Types")
                .Include("Categories")
                .Include("Arguments")
                .SingleOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.AddArgument(scriptArgument);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Adds a new script argument to a script.
        /// </summary>
        /// <param name="scriptArgument">script argument to insert</param>
        public ScriptArgument UpdateScriptArgument(Script script, ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();
            ScriptArgument sa = db.ScriptArguments.SingleOrDefault(sa => sa.Id == scriptArgument.Id);
            if (sa != null)
            {
                db.Entry(sa).CurrentValues.SetValues(scriptArgument);
                db.SaveChanges();
            }
            return sa;
        }

        /// <summary>
        /// Removes a script argument to from script.
        /// </summary>
        /// <param name="scriptArgument">script argument to remove</param>
        public Script RemoveScriptArgument(Script script, ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();

            // Delete script argument relation
            ScriptArgument st = db.ScriptArguments.FirstOrDefault(st => st.Id == scriptArgument.Id);
            st = db.ScriptArguments.Remove(st);
            st.Script = null;
            st.ScriptId = 0;

            // Update script
            Script s = db.Scripts.Include("Arguments").FirstOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                s.Arguments.Remove(st);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Updates a script
        /// </summary>
        /// <param name="script">script to update</param>
        public Script UpdateScript(Script script)
        {
            using var db = new DatabaseContext();
            Script s = db.Scripts
                        .Include("Arguments")
                        .Include("Types")
                        .Include("Categories")
                        .SingleOrDefault(p => p.Id == script.Id);
            if (s != null)
            {
                db.Entry(s).CurrentValues.SetValues(script);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Updates a script argument
        /// </summary>
        /// <param name="scriptArgument">script argument to update</param>
        public ScriptArgument UpdateScriptArgument(ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();
            ScriptArgument s = db.ScriptArguments.SingleOrDefault(p => p.Id == scriptArgument.Id);
            if (s != null)
            {
                db.Entry(s).CurrentValues.SetValues(scriptArgument);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Deletes a script type.
        /// </summary>
        /// <param name="scriptType">script type to delete</param>
        public void DeleteScriptType(ScriptType scriptType)
        {
            using var db = new DatabaseContext();
            ScriptType s = db.ScriptTypes.SingleOrDefault(s => s.Id == scriptType.Id);
            if (s != null)
            {
                db.ScriptTypes.Remove(s);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes a script category.
        /// </summary>
        /// <param name="scriptCategory">script category to delete</param>
        public void DeleteScriptCategory(ScriptCategory scriptCategory)
        {
            using var db = new DatabaseContext();
            ScriptCategory c = db.ScriptCategories.SingleOrDefault(c => c.Id == scriptCategory.Id);
            if (c != null)
            {
                db.ScriptCategories.Remove(c);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes a script argument.
        /// </summary>
        /// <param name="scriptArgument">script argument to delete</param>
        public void DeleteScriptArgument(ScriptArgument scriptArgument)
        {
            using var db = new DatabaseContext();
            ScriptArgument a = db.ScriptArguments.SingleOrDefault(a => a.Id == scriptArgument.Id);
            if (a != null)
            {
                db.ScriptArguments.Remove(a);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes a script.
        /// </summary>
        /// <param name="script">script to delete</param>
        public void DeleteScript(Script script)
        {
            using var db = new DatabaseContext();
            Script s = db.Scripts.SingleOrDefault(s => s.Id == script.Id);
            if (s != null)
            {
                db.Scripts.Remove(s);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes all scripts
        /// </summary>
        public void DeleteAllScripts()
        {
            List<Script> scripts = FindScripts();
            scripts.ForEach(s => DeleteScript(s));
        }
    }
}
