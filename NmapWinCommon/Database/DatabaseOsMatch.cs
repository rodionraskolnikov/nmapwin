﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseOsMatch
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseOsMatch> lazy = new Lazy<DatabaseOsMatch>(() => new DatabaseOsMatch());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseOsMatch Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseOsMatch()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all OS matches.
        /// </summary>
        /// <returns>list of all OS matches</returns>
        public List<OsMatch> FindOsMatches()
        {
            using var db = new DatabaseContext();
            var query = from o in db.OsMatches
                        .Include("OsClasses")
                        orderby o.Id
                        select o;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all OS matches by host.
        /// </summary>
        /// <param name="hostId">ID of the host</param>
        /// <returns>list of all OS matches for a given host</returns>
        public List<OsMatch> FindOsMatchesByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from o in db.OsMatches
                        .Include("Os")
                        .Include("OsClasses")
                        .Include("OsClasses.Cpes")
                        where o.Os.HostId == hostId
                        orderby o.Id
                        select o;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new OS match.
        /// </summary>
        /// <param name="osMatch">OS match to insert</param>
        /// <returns>newly created OS match</returns>
        public OsMatch InsertOsMatch(OsMatch osMatch)
        {
            using var db = new DatabaseContext();
            osMatch = db.OsMatches.Add(osMatch);
            db.SaveChanges();
            return osMatch;
        }

        /// <summary>
        /// Updates a OS match
        /// </summary>
        /// <param name="osMatch">OS match to update</param>
        public OsMatch UpdateOsMatcht(OsMatch osMatch)
        {
            using var db = new DatabaseContext();
            OsMatch o = db.OsMatches
                .Include("Os")
                .Include("OsClasses")
                .Include("OsClasses.Cpes")
                .SingleOrDefault(o => o.Id == osMatch.Id);
            if (o != null)
            {
                db.Entry(o).CurrentValues.SetValues(osMatch);
                db.SaveChanges();
            }
            return o;
        }

        /// <summary>
        /// Deletes a OS match
        /// </summary>
        /// <param name="osMatch">OS match to delete</param>
        public void DeleteOsMatch(OsMatch osMatch)
        {
            using var db = new DatabaseContext();
            OsMatch o = db.OsMatches.First(o => o.Id == osMatch.Id);
            db.OsMatches.Remove(o);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes all OS matches
        /// </summary>
        public void DeleteAllOsMatches()
        {
            List<OsMatch> osMatches = FindOsMatches();
            osMatches.ForEach(o => DeleteOsMatch(o));
        }
    }
}
