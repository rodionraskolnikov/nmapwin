﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    /// <summary>
    /// Services database.
    /// </summary>
    public class DatabaseService
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseService> lazy = new Lazy<DatabaseService>(() => new DatabaseService());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseService Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseService()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all services.
        /// </summary>
        /// <returns>list of all services</returns>
        public List<PortService> FindServices()
        {
            using var db = new DatabaseContext();
            var query = from s in db.Services orderby s.Id select s;
            return query.ToList();
        }

        /// <summary>
        /// Returns a service for by its ID.
        /// </summary>
        /// <param name="id">ID of the service</param>
        /// <returns>service with given ID</returns>
        public PortService FindServiceById(int id)
        {
            using var db = new DatabaseContext();
            PortService service = db.Services.Include("Cpes").First(p => p.Id == id);
            return service;
        }

        /// <summary>
        /// Returns all services for a host.
        /// </summary>
        /// <param name="hostId">ID of the host</param>
        /// <returns>list of services for the given host</returns>
        public List<PortService> FindServicesByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from s in db.Services.Include("Port") where s.Port.Host.Id == hostId orderby s.Id select s;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new service.
        /// </summary>
        /// <param name="service">service to insert</param>
        /// <returns>newly created service</returns>
        public PortService InsertService(PortService service)
        {
            using var db = new DatabaseContext();
            service = db.Services.Add(service);
            db.SaveChanges();
            return service;
        }

        /// <summary>
        /// Updates a service
        /// </summary>
        /// <param name="service">service to update</param>
        public PortService UpdateService(PortService service)
        {
            using var db = new DatabaseContext();
            PortService s = db.Services.SingleOrDefault(s => s.Id == service.Id);
            if (s != null)
            {
                db.Entry(s).CurrentValues.SetValues(service);
                db.SaveChanges();
            }
            return s;
        }

        /// <summary>
        /// Deletes a service.
        /// </summary>
        /// <param name="service">service to delete</param>
        public void DeleteService(PortService service)
        {
            using var db = new DatabaseContext();
            PortService s = db.Services.SingleOrDefault(s => s.Id == service.Id);
            db.Services.Remove(s);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a list of services
        /// </summary>
        /// <param name="services">list of services to delete</param>
        public void DeleteServices(List<PortService> services)
        {
            services.ForEach(s => DeleteService(s));
        }

        /// <summary>
        /// Deletes all services.
        /// </summary>
        public void DeleteAllServices()
        {
            List<PortService> services = FindServices();
            services.ForEach(s => DeleteService(s));
        }
    }
}
