﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your job) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabasePerformance
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabasePerformance> lazy = new Lazy<DatabasePerformance>(() => new DatabasePerformance());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabasePerformance Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabasePerformance()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all performance data.
        /// </summary>
        /// <returns>list of all performance data</returns>
        public List<Performance> FindPerformanceData()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Performances
                        orderby p.Id
                        select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all performance data.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <returns>list of all performance data for a scan</returns>
        public List<Performance> FindPerformanceDataByScan(long scanId)
        {
            using var db = new DatabaseContext();
            var query = from p in db.Performances
                        where p.ScanId == scanId && p.Type == 1
                        select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns the minimum for a scan.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <returns>minimum timestamp</returns>
        public long FindMinimum(long scanId)
        {
            using var db = new DatabaseContext();
            if (db.Performances.Count(p => p.ScanId == scanId) == 0)
                return -1;
            return db.Performances.Where(p => p.ScanId == scanId).Min(p => p.Timestamp);
        }

        /// <summary>
        /// Returns the maximum for a scan.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <returns>maximum timestamp</returns>
        public long FindMaximum(long scanId)
        {
            using var db = new DatabaseContext();
            if (db.Performances.Count(p => p.ScanId == scanId) == 0)
                return -1;
            return db.Performances.Where(p => p.ScanId == scanId).Max(p => p.Timestamp);
        }

        /// <summary>
        /// Returns a list of all scans.
        /// </summary>
        /// <returns>list of all scans</returns>
        public List<long> FindScans()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Performances
                        select p;
            return query.Select(p => p.ScanId).Distinct().ToList();
        }

        /// <summary>
        /// Average for a period of time.
        /// </summary>
        /// <param name="scanId">scan ID</param>
        /// <param name="minimum">minimum timestamp</param>
        /// <param name="maximum">maximum timestamp</param>
        /// <returns></returns>
        public float Average(long scanId, long minimum, long maximum)
        {
            using var db = new DatabaseContext();
            return db.Performances
                .Where(p => p.ScanId == scanId && p.Timestamp >= minimum && p.Timestamp <= maximum)
                .Select(p => p.Cpu).DefaultIfEmpty(0f).Average(p => p);
        }

        /// <summary>
        /// CHeck existence
        /// </summary>
        /// <param name="scanId">scan ID</param>
        /// <param name="timestamp">timestamp</param>
        /// <param name="type">type</param>
        /// <returns></returns>
        public bool ExistsByType(long scanId, long timestamp, int type)
        {
            try
            {
                using var db = new DatabaseContext();
                return (from p in db.Performances where (p.ScanId == scanId && p.Timestamp == timestamp && p.Type == type) select p).Count() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Insert new performance data.
        /// </summary>
        /// <param name="performance">performance data to insert</param>
        public Performance InsertPerformance(Performance performance)
        {
            using var db = new DatabaseContext();
            performance = db.Performances.Add(performance);
            db.SaveChanges();
            return performance;
        }

        /// <summary>
        /// Updates performance data.
        /// </summary>
        /// <param name="performance">performance data to update</param>
        public Performance UpdatePerformance(Performance performance)
        {
            using var db = new DatabaseContext();
            Performance p = db.Performances
                                .SingleOrDefault(p => p.Id == performance.Id);
            if (p != null)
            {
                db.Entry(p).CurrentValues.SetValues(performance);
                db.SaveChanges();
            }
            return p;
        }

        /// <summary>
        /// Deletes performance data.
        /// </summary>
        /// <param name="performance">performance data to delete</param>
        public void DeletePerformance(Performance performance)
        {
            using var db = new DatabaseContext();
            Performance p = db.Performances.SingleOrDefault(p => p.Id == performance.Id);
            db.Performances.Remove(p);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes performance data for a scan.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        public void DeletePerformance(long scanId)
        {
            using var db = new DatabaseContext();
            List<Performance> performanceData = db.Performances.Where(p => p.ScanId == scanId).ToList();
            performanceData.ForEach(p => db.Performances.Remove(p));
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes performance data for a scan a type and time period.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <param name="type">type of performance data</param>
        /// <param name="start">start timestamp</param>
        /// <param name="end">end timestamp</param>
        public void DeletePerformance(long scanId, int type, long start, long end)
        {
            using var db = new DatabaseContext();
            List<Performance> performanceData = db.Performances
               .Where(p => p.ScanId == scanId && p.Type == type && p.Timestamp >= start && p.Timestamp <= end)
               .Select(p => p).ToList();
            performanceData.ForEach(p => db.Performances.Remove(p));
            db.SaveChanges();
        }
    }
}
