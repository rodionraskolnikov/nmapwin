﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseOsClass
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseOsClass> lazy = new Lazy<DatabaseOsClass>(() => new DatabaseOsClass());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseOsClass Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseOsClass()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all OS Classes.
        /// </summary>
        /// <returns>list of all OS Classes</returns>
        public List<OsClass> FindOsClasses()
        {
            using var db = new DatabaseContext();
            var query = from o in db.OsClasses orderby o.Id select o;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new OS Class.
        /// </summary>
        /// <param name="osClass">OS Class to insert</param>
        /// <returns>newly created OS Class</returns>
        public OsClass InsertOsClass(OsClass osClass)
        {
            using var db = new DatabaseContext();
            osClass = db.OsClasses.Add(osClass);
            db.SaveChanges();
            return osClass;
        }

        /// <summary>
        /// Deletes a OS Class
        /// </summary>
        /// <param name="osClass">OS Class to delete</param>
        public void DeleteOsClass(OsClass osClass)
        {
            using var db = new DatabaseContext();
            OsClass o = db.OsClasses.SingleOrDefault(o => o.Id == osClass.Id);
            db.OsClasses.Remove(o);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes all OS Classes
        /// </summary>
        public void DeleteAllOsClasses()
        {
            List<OsClass> osClasses = FindOsClasses();
            osClasses.ForEach(o => DeleteOsClass(o));
        }
    }
}
