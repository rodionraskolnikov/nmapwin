﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseHost
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseHost> lazy = new Lazy<DatabaseHost>(() => new DatabaseHost());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseHost Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseHost()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all hosts.
        /// </summary>
        /// <returns>list of all hosts</returns>
        public List<Host> FindHosts()
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hosts
                        orderby h.Id
                        select h;
            return query.ToList();
        }

        /// <summary>
        /// Return a fully eagerly loaded host.
        /// </summary>
        /// <returns></returns>
        public List<Host> FindHostsFull()
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hosts
                        .Include("Addresses")
                        .Include("HostNames")
                        .Include("OperatingSystems")
                        .Include("OperatingSystems.OsMatches")
                        orderby h.Id
                        select h;
            return query.ToList();
        }

        /// <summary>
        /// Returns a host by its ID.
        /// </summary>
        /// <param name="id">ID of the host</param>
        /// <returns>host with given ID</returns>
        public Host FindHostById(long id)
        {
            using var db = new DatabaseContext();
            return db.Hosts.SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// Returns a host by its ID.
        /// </summary>
        /// <param name="id">ID of the host</param>
        /// <returns>host with given ID</returns>
        public Host FindHostByIdFull(long id)
        {
            using var db = new DatabaseContext();
            return db.Hosts
                .Include("Addresses")
                .Include("HostNames")
                .SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// Select all host of a scan.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <returns>list of hosts</returns>
        public List<Host> FindHostsByScanId(long scanId)
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hosts
                        .Include("Addresses")
                        .Include("HostNames")
                        .Include("OperatingSystems")
                        .Include("OperatingSystems.OsMatches")
                        where h.Scans.Any(s => s.Id == scanId)
                        orderby h.Id
                        select h;
            return query.ToList();
        }

        /// <summary>
        /// Select all host of a scan.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <returns>list of hosts</returns>
        public List<Host> FindDownHostsByScanId(long scanId)
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hosts
                        .Include("Scans")
                        where h.Scans.Any(s => s.Id == scanId) && h.Status.State == HostState.down
                        orderby h.Id
                        select h;
            return query.ToList();
        }

        /// <summary>
        /// Returns a hosts by name.
        /// </summary>
        /// <param name="name">name of the host</param>
        /// <returns>host with given name</returns>
        public Host FindHostByName(string name)
        {
            using var db = new DatabaseContext();
            return db.Hosts.Include("HostNames").FirstOrDefault(h => h.HostNames.Exists(x => x.Name.Equals(name)));
        }

        /// <summary>
        /// Returns a hosts by IP address.
        /// </summary>
        /// <param name="addresses">IP adresses of the host</param>
        /// <returns>host with given IP address</returns>
        public Host FindHostByAddress(List<Address> addresses)
        {
            using var db = new DatabaseContext();
            foreach(Address a in addresses) {
                Host h = db.Hosts
                    .Include("Addresses")
                    .Include("HostNames")
                    .FirstOrDefault(h => h.Addresses.Any(x => x.Addr.Equals(a)));
                if (h != null)
                    return h;
            };
            return null;
        }

        /// <summary>
        /// Returns a hosts by IP address.
        /// </summary>
        /// <param name="ipAddr">IP adress of the host</param>
        /// <returns>host with given IP address</returns>
        public Host FindHostByAddress(string ipAddr)
        {
            using var db = new DatabaseContext();
            return db.Hosts
                .Include("HostNames")
                .Include("Addresses")
                .Include("OperatingSystems")
                .Include("OperatingSystems.OsMatches")
                .FirstOrDefault(h => h.Addresses.Any(x => x.Addr.Equals(ipAddr)));
        }

        /// <summary>
        /// Insert a new host.
        /// </summary>
        /// <param name="host">host to insert</param>
        public Host InsertHost(Host host)
        {
            using var db = new DatabaseContext();
            host = db.Hosts.Add(host);
            db.SaveChanges();
            return host;
        }

        /// <summary>
        /// Updates a host
        /// </summary>
        /// <param name="host">Host to update</param>
        public Host UpdateHost(Host host)
        {
            using var db = new DatabaseContext();
            Host h = db.Hosts
                .Include("Scans")
                .Include("Ports")
                .Include("HostNames")
                .Include("Addresses")
                .Include("OperatingSystems")
                .Include("TraceRoutes")
                .SingleOrDefault(h => h.Id == host.Id);
            if (h != null)
            {
                db.Entry(h).CurrentValues.SetValues(host);
                db.SaveChanges();
            }
            return h;
        }

        /// <summary>
        /// Deletes a host
        /// </summary>
        /// <param name="host">host to delete</param>
        public void DeleteHost(Host host)
        {
            using var db = new DatabaseContext();            
            Host h = db.Hosts.SingleOrDefault(h => h.Id == host.Id);
            db.Hosts.Remove(h);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a list of host
        /// </summary>
        /// <param name="hosts">hosts to delete</param>
        public void DeleteHosts(List<Host> hosts)
        {
            hosts.ForEach(h => DeleteHost(h));
        }

        /// <summary>
        /// Deletes a list of host, with status 'down'.
        /// </summary>
        /// <param name="hosts">hosts to delete</param>
        public void DeleteDownHosts(List<Host> hosts)
        {
            DeleteHosts(hosts.FindAll(h => h.Status.State == HostState.down));
        }

        /// <summary>
        /// Deletes all hosts of a scan.
        /// </summary>
        public void DeleteAllHosts()
        {
            List<Host> hosts = FindHosts();
            hosts.ForEach(h => DeleteHost(h));
        }

        #region Host scripts
        /// <summary>
        /// Find all scripts for a host
        /// </summary>
        /// <param name="hostId">ID pf host to look for</param>
        /// <returns>list of host scripts</returns>
        public List<HostScript> FindHostScriptsByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from s in db.HostScripts
                        where s.Host.Id == hostId
                        orderby s.Id
                        select s;
            return query.ToList();
        }
        #endregion
    }
}
