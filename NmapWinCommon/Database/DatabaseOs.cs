﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    /// <summary>
    /// Operating System database class.
    /// </summary>
    public class DatabaseOs
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseOs> lazy = new Lazy<DatabaseOs>(() => new DatabaseOs());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseOs Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseOs()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all operating systems.
        /// </summary>
        /// <returns>list of all CPEs</returns>
        public List<Os> FindOperatingSystems()
        {
            using var db = new DatabaseContext();
            var query = from c in db.Oses orderby c.Id select c;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new operating system.
        /// </summary>
        /// <param name="os">os to insert</param>
        /// <returns>newly created os</returns>
        public Os InsertOs(Os os)
        {
            using var db = new DatabaseContext();
            os = db.Oses.Add(os);
            db.SaveChanges();
            return os;
        }

        /// <summary>
        /// Updates a operating system.
        /// </summary>
        /// <param name="os">os to update</param>
        public Os UpdateOs(Os os)
        {
            using var db = new DatabaseContext();
            Os o = db.Oses.SingleOrDefault(o => o.Id == os.Id);
            if (o != null)
            {
                db.Entry(o).CurrentValues.SetValues(os);
                db.SaveChanges();
            }
            return o;
        }

        /// <summary>
        /// Deletes a os
        /// </summary>
        /// <param name="os">os to delete</param>
        public void DeleteOs(Os os)
        {
            using var db = new DatabaseContext();
            Os c = db.Oses.SingleOrDefault(c => c.Id == os.Id);
            db.Oses.Remove(c);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a os
        /// </summary>
        /// <param name="os">os to delete</param>
        public void DeleteOss(List<Os> operatingSystems)
        {
            operatingSystems.ForEach(o => DeleteOs(o));
        }

        /// <summary>
        /// Deletes all oss
        /// </summary>
        public void DeleteAllOss()
        {
            List<Os> operatingSystems = FindOperatingSystems();
            operatingSystems.ForEach(o => DeleteOs(o));
        }
    }
}
