﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseProfile
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseProfile> lazy = new Lazy<DatabaseProfile>(() => new DatabaseProfile());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseProfile Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseProfile()
        {

        }
        #endregion

        /// <summary>
        /// Return total count of entities
        /// </summary>
        /// <returns>total count</returns>
        public int Count()
        {
            using var db = new DatabaseContext();
            return (from o in db.Profiles select o).Count();
        }

        /// <summary>
        /// Check existence
        /// </summary>
        /// <returns>true if existence, otherwise false</returns>
        public bool Exists()
        {
            try
            {
                using var db = new DatabaseContext();
                return (from p in db.Profiles select p).Count() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a list of all profiles.
        /// </summary>
        /// <returns>list of all profiles</returns>
        public List<Profile> FindProfiles()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Profiles orderby p.Id select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns a profile by ID.
        /// </summary>
        /// <param name="id">profile ID</param>
        /// <returns>profile with the given ID or null</returns>
        public Profile FindProfileById(long id)
        {
            using var db = new DatabaseContext();
            return db.Profiles.SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// Returns a profile by name.
        /// </summary>
        /// <param name="name">profile name</param>
        /// <returns>profile with the given name or null</returns>
        public Profile FindProfileByName(string name)
        {
            using var db = new DatabaseContext();
            return db.Profiles.SingleOrDefault(p => p.Name.Equals(name));
        }

        /// <summary>
        /// Inserts a new profile
        /// </summary>
        /// <param name="profile">profile to insert</param>
        public Profile InsertProfile(Profile profile)
        {
            using var db = new DatabaseContext();
            profile = db.Profiles.Add(profile);
            db.SaveChanges();
            return profile;
        }

        /// <summary>
        /// Updates a profile
        /// </summary>
        /// <param name="profile">profile to update</param>
        public Profile UpdateProfile(Profile profile)
        {
            using var db = new DatabaseContext();
            Profile p = db.Profiles.SingleOrDefault(p => p.Id == profile.Id);
            if (p != null)
            {
                db.Entry(p).CurrentValues.SetValues(profile);
                db.SaveChanges();
            }
            return p;
        }

        /// <summary>
        /// Deletes a profile
        /// </summary>
        /// <param name="profile">profile to delete</param>
        public void DeleteProfile(Profile profile)
        {
            using var db = new DatabaseContext();
            Profile p = db.Profiles.SingleOrDefault(p => p.Id == profile.Id);
            db.Profiles.Remove(p);
            db.SaveChanges();

        }

        /// <summary>
        /// Deletes all profiles
        /// </summary>
        public void DeleteAllProfiles()
        {
            List<Profile> profiles = FindProfiles();
            profiles.ForEach(p => DeleteProfile(p));
        }
    }
}
