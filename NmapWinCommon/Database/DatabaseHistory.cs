﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Linq;

namespace NmapWinCommon.Database
{
    /// <summary>
    /// Database migration history.
    /// </summary>
    /// <remarks>
    /// NmapWin uses Fluent Migration for database migrations. Each migration leaves an entry
    /// in the version info table, with the current timestamp.
    /// </remarks>
    public class DatabaseHistory
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseHistory> lazy = new Lazy<DatabaseHistory>(() => new DatabaseHistory());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseHistory Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseHistory()
        {
        }
        #endregion

        /// <summary>
        /// Returns the latest database version.
        /// </summary>
        /// <returns>latest database migration version, which is basically timestamp.</returns>
        public string FindLatestVersion()
        {
            using var db = new DatabaseContext();
            const string GetMigrationSql = "SELECT * FROM versioninfo ORDER BY Version DESC";

            DatabaseStatus migration = db.Database.SqlQuery<DatabaseStatus>(GetMigrationSql).First();
            return migration.Version.ToString();
        }
    }
}
