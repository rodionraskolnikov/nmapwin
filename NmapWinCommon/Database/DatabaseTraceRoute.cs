﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseTraceRoute
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseTraceRoute> lazy = new Lazy<DatabaseTraceRoute>(() => new DatabaseTraceRoute());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseTraceRoute Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTraceRoute()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all trace routes.
        /// </summary>
        /// <returns>list of all trace routes</returns>
        public List<TraceRoute> FindTraceRoutes()
        {
            using var db = new DatabaseContext();
            var query = from t in db.TraceRoutes orderby t.Id select t;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all trace routes for a host.
        /// </summary>
        /// <returns>list of all trace routes for a host</returns>
        public List<TraceRoute> FindTraceRoutesByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from t in db.TraceRoutes where t.Host.Id == hostId orderby t.Id select t;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new trace route.
        /// </summary>
        /// <param name="traceRoute">trace route to insert</param>
        /// <returns>newly created trace route</returns>
        public TraceRoute InsertTraceRoute(TraceRoute traceRoute)
        {
            using var db = new DatabaseContext();
            traceRoute = db.TraceRoutes.Add(traceRoute);
            db.SaveChanges();
            return traceRoute;
        }

        /// <summary>
        /// Updates a trace route
        /// </summary>
        /// <param name="traceRoute">trace route to update</param>
        public TraceRoute UpdateTraceRoute(TraceRoute traceRoute)
        {
            using var db = new DatabaseContext();
            TraceRoute t = db.TraceRoutes.SingleOrDefault(t => t.Id == traceRoute.Id);
            if (t != null)
            {
                db.Entry(t).CurrentValues.SetValues(traceRoute);
                db.SaveChanges();
            }
            return t;
        }

        /// <summary>
        /// Deletes a trace route
        /// </summary>
        /// <param name="traceRoute">traceRoute to delete</param>
        public void DeleteTraceRoute(TraceRoute traceRoute)
        {
            using var db = new DatabaseContext();
            TraceRoute p = db.TraceRoutes.SingleOrDefault(p => p.Id == traceRoute.Id);
            db.TraceRoutes.Remove(p);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a trace route
        /// </summary>
        /// <param name="traceRoute">traceRoute to delete</param>
        public void DeleteTraceRoutes(List<TraceRoute> traceRoutes)
        {
            traceRoutes.ForEach(p => DeleteTraceRoute(p));
        }

        /// <summary>
        /// Deletes all trace routes
        /// </summary>
        public void DeleteAllTraceRoutes()
        {
            List<TraceRoute> traceRoutes = FindTraceRoutes();
            traceRoutes.ForEach(p => DeleteTraceRoute(p));
        }
    }
}
