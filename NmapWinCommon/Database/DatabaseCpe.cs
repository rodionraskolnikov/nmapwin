﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseCpe
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseCpe> lazy = new Lazy<DatabaseCpe>(() => new DatabaseCpe());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseCpe Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseCpe()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all CPEs.
        /// </summary>
        /// <returns>list of all CPEs</returns>
        public List<PortCpe> FindCpes()
        {
            using var db = new DatabaseContext();
            var query = from c in db.Cpes orderby c.Id select c;

            List<PortCpe> cpes = new List<PortCpe>();
            foreach (var cpe in query)
            {
                cpes.Add(cpe);
            }
            return cpes;
        }

        /// <summary>
        /// Returns a list of all CPEs for a service.
        /// </summary>
        /// <returns>list of all CPEs for a service</returns>
        public List<PortCpe> FindCpesByHost(int serviceId)
        {
            using var db = new DatabaseContext();
            var query = from c in db.Cpes.Include("Service") where c.Service.Id == serviceId orderby c.Id select c;

            List<PortCpe> cpes = new List<PortCpe>();
            foreach (var cpe in query)
            {
                cpes.Add(cpe);
            }
            return cpes;
        }

        /// <summary>
        /// Insert a new cpe.
        /// </summary>
        /// <param name="cpe">cpe to insert</param>
        /// <returns>newly created cpe</returns>
        public PortCpe InsertCpe(PortCpe cpe)
        {
            using var db = new DatabaseContext();
            cpe = db.Cpes.Add(cpe);
            db.SaveChanges();
            return cpe;
        }

        /// <summary>
        /// Updates a cpe
        /// </summary>
        /// <param name="cpe">cpe to update</param>
        public PortCpe UpdateCpe(PortCpe cpe)
        {
            using var db = new DatabaseContext();
            PortCpe c = db.Cpes.First(c => c.Id == cpe.Id);
            c.Content = cpe.Content;
            db.SaveChanges();
            return c;
        }

        /// <summary>
        /// Deletes a cpe
        /// </summary>
        /// <param name="cpe">cpe to delete</param>
        public void DeleteCpe(PortCpe cpe)
        {
            using var db = new DatabaseContext();
            PortCpe c = db.Cpes.First(c => c.Id == cpe.Id);
            db.Cpes.Remove(c);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a cpe
        /// </summary>
        /// <param name="cpe">cpe to delete</param>
        public void DeleteCpes(List<PortCpe> cpes)
        {
            cpes.ForEach(c => DeleteCpe(c));
        }

        /// <summary>
        /// Deletes all cpes
        /// </summary>
        public void DeleteAllCpes()
        {
            List<PortCpe> cpes = FindCpes();
            cpes.ForEach(c => DeleteCpe(c));
        }
    }
}
