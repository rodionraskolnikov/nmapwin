﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your job) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseJob
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseJob> lazy = new Lazy<DatabaseJob>(() => new DatabaseJob());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseJob Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseJob()
        {
        }
        #endregion

        /// <summary>
        /// Returns the total count
        /// </summary>
        /// <returns>total count</returns>
        public int Count()
        {
            using var db = new DatabaseContext();
            return (from j in db.Jobs select j).Count();
        }

        /// <summary>
        /// Check existence
        /// </summary>
        /// <returns>true if job table exists, otherwise false</returns>
        public bool Exists()
        {
            try
            {
                using var db = new DatabaseContext();
                return (from j in db.Jobs select j).Count() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Return max instance ID for a job key.
        /// </summary>
        /// <param name="name">job name</param>
        /// <param name="group">job group</param>
        /// <returns></returns>
        public int MaxInstanceId(string name, string group)
        {
            using var db = new DatabaseContext();
            Job job = db.Jobs.FirstOrDefault(j => j.Name == name && j.Group == group);
            job.InstanceId++;
            db.SaveChanges();
            return job.InstanceId;
        }

        /// <summary>
        /// Returns a list of all Jobs.
        /// </summary>
        /// <returns>list of all Jobs</returns>
        public List<Job> FindJobs()
        {
            using var db = new DatabaseContext();
            var query = from j in db.Jobs orderby j.Id select j;
            return query.ToList();
        }

        /// <summary>
        /// Returns an job by name.
        /// </summary>
        /// <param name="name">job name</param>
        /// <param name="group">job group</param>
        /// <returns>job if found, otherwise null</returns>
        public Job FindJobByName(string name, string group)
        {
            using var db = new DatabaseContext();
            return db.Jobs.FirstOrDefault(j => j.Name.Equals(name) && j.Group.Equals(group));
        }

        /// <summary>
        /// Returns an job by ID.
        /// </summary>
        /// <returns>job if found, otherwise null</returns>
        public Job FindJobById(long id)
        {
            using var db = new DatabaseContext();
            return db.Jobs.SingleOrDefault(j => j.Id == id);
        }

        /// <summary>
        /// Insert a new job
        /// </summary>
        /// <param name="job">Job to insert</param>
        public Job InsertJob(Job job)
        {
            using var db = new DatabaseContext();
            job = db.Jobs.Add(job);
            db.SaveChanges();
            return job;
        }

        /// <summary>
        /// Updates an job
        /// </summary>
        /// <param name="job">job to update</param>
        public Job UpdateJob(Job job)
        {
            using var db = new DatabaseContext();
            Job j = db.Jobs.SingleOrDefault(j => j.Id == job.Id);
            if (j != null)
            {
                db.Entry(j).CurrentValues.SetValues(job);
                db.SaveChanges();
            }
            return j;
        }

        /// <summary>
        /// Deletes an job
        /// </summary>
        /// <param name="job">job to delete</param>
        public void DeleteJob(Job job)
        {
            using var db = new DatabaseContext();
            Job o = db.Jobs.SingleOrDefault(o => o.Id == job.Id);
            db.Jobs.Remove(o);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes all job
        /// </summary>
        public void DeleteAllJobs()
        {
            List<Job> Jobs = FindJobs();
            Jobs.ForEach(o => DeleteJob(o));
        }
    }
}
