﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabasePort
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabasePort> lazy = new Lazy<DatabasePort>(() => new DatabasePort());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabasePort Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabasePort()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all ports.
        /// </summary>
        /// <returns>list of all ports</returns>
        public List<Port> FindPorts()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Ports orderby p.Id select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all ports.
        /// </summary>
        /// <returns>list of all ports</returns>
        public List<Port> FindPortsFull()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Ports
                        .Include("Host")
                        .Include("Host.Addresses")
                        .Include("Host.HostNames")
                        orderby p.Id
                        select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns a port by its ID.
        /// </summary>
        /// <param name="id">ID of the port</param>
        /// <returns>port with given ID</returns>
        public Port FindPortById(long id)
        {
            using var db = new DatabaseContext();
            return db.Ports.Include("PortScripts").SingleOrDefault(p => p.Id == id);
        }

        /// <summary>
        /// Returns a list of all ports for a host.
        /// </summary>
        /// <returns>list of all ports for a host</returns>
        public List<Port> FindPortsByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from p in db.Ports.Include("Services").Include("Services.Cpes") where p.Host.Id == hostId orderby p.Id select p;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new port.
        /// </summary>
        /// <param name="port">port to insert</param>
        /// <returns>newly created port</returns>
        public Port InsertPort(Port port)
        {
            using var db = new DatabaseContext();
            port = db.Ports.Add(port);
            db.SaveChanges();
            return port;
        }

        /// <summary>
        /// Updates a port
        /// </summary>
        /// <param name="port">port to update</param>
        public Port UpdatePort(Port port)
        {
            using var db = new DatabaseContext();
            Port p = db.Ports.SingleOrDefault(p => p.Id == port.Id);
            if (p != null)
            {
                db.Entry(p).CurrentValues.SetValues(port);
                db.SaveChanges();
            }
            return p;
        }

        /// <summary>
        /// Deletes a port
        /// </summary>
        /// <param name="port">port to delete</param>
        public void DeletePort(Port port)
        {
            using var db = new DatabaseContext();
            Port p = db.Ports.First(p => p.Id == port.Id);
            db.Ports.Remove(p);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a port
        /// </summary>
        /// <param name="port">port to delete</param>
        public void DeletePorts(List<Port> ports)
        {
            ports.ForEach(p => DeletePort(p));
        }

        /// <summary>
        /// Deletes all ports
        /// </summary>
        public void DeleteAllPorts()
        {
            List<Port> ports = FindPorts();
            ports.ForEach(p => DeletePort(p));
        }

        /// <summary>
        /// Find all scripts for a port
        /// </summary>
        /// <param name="port">port to look for</param>
        /// <returns>list of port scripts</returns>
        public List<PortScript> FindPortScriptsByPort(Port port)
        {
            using var db = new DatabaseContext();
            var query = from s in db.PortScripts
                        where s.Port.Id == port.Id
                        orderby s.Id
                        select s;
            return query.ToList();
        }
    }
}
