﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseHop
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseHop> lazy = new Lazy<DatabaseHop>(() => new DatabaseHop());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseHop Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseHop()
        {
        }
        #endregion

        /// <summary>
        /// Returns a list of all hops.
        /// </summary>
        /// <returns>list of all hops</returns>
        public List<Hop> FindHops()
        {
            using var db = new DatabaseContext();
            var query = from p in db.Hops orderby p.Id select p;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all hops for a host.
        /// </summary>
        /// <returns>list of all hops for a host</returns>
        public List<Hop> FindHopsByHost(long hostId)
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hops where h.TraceRoute.Host.Id == hostId orderby h.Id select h;
            return query.ToList();
        }

        /// <summary>
        /// Returns a list of all hops for a trace route.
        /// </summary>
        /// <param name="traceRouteId">ID of the trace route</param>
        /// <returns>list of all hops for a trace route</returns>
        public List<Hop> FindHopsByTraceRoute(long traceRouteId)
        {
            using var db = new DatabaseContext();
            var query = from h in db.Hops where h.TraceRoute.Id == traceRouteId orderby h.Id select h;
            return query.ToList();
        }

        /// <summary>
        /// Insert a new hop.
        /// </summary>
        /// <param name="hop">hop to insert</param>
        /// <returns>newly created hop</returns>
        public Hop InsertHop(Hop hop)
        {
            using var db = new DatabaseContext();
            hop = db.Hops.Add(hop);
            db.SaveChanges();
            return hop;
        }

        /// <summary>
        /// Updates a hop
        /// </summary>
        /// <param name="hop">hop to update</param>
        public Hop UpdateHop(Hop hop)
        {
            using var db = new DatabaseContext();
            Hop h = db.Hops.SingleOrDefault(h => h.Id == hop.Id);
            if (h != null)
            {
                db.Entry(h).CurrentValues.SetValues(hop);
                db.SaveChanges();
            }
            return h;
        }

        /// <summary>
        /// Deletes a hop
        /// </summary>
        /// <param name="hop">hop to delete</param>
        public void DeleteHop(Hop hop)
        {
            using var db = new DatabaseContext();
            Hop p = db.Hops.SingleOrDefault(p => p.Id == hop.Id);
            db.Hops.Remove(p);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a hop
        /// </summary>
        /// <param name="hop">hop to delete</param>
        public void DeleteHops(List<Hop> hops)
        {
            hops.ForEach(p => DeleteHop(p));
        }

        /// <summary>
        /// Deletes all hops
        /// </summary>
        public void DeleteAllHops()
        {
            List<Hop> hops = FindHops();
            hops.ForEach(p => DeleteHop(p));
        }
    }
}
