﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Entities;
using System.Data.Entity;

namespace NmapWinCommon.Database
{
    /// <summary>
    /// Database context
    /// </summary>
    /// <remarks>
    /// <list type="bullet">
    /// <item>SQLServer connection string: Server=localhost\SQLEXPRESS;Database=nmapwin;Trusted_Connection=True;</item>
    /// <item>MySQL connection string: server=SERVER;port=PORT;database=DBNAME;uid=DBUSER;password=DBPWD;</item>
    /// <item>SQLServer CE connection string: Data Source=..\data\nmapwin.sdf;Persist Security Info=False;Max Database Size=4091;</item>
    /// <item>SQLite connection string: Data Source=..\data\nmapwin.db;Version=3;</item>
    /// </list>
    /// </remarks>
    public class DatabaseContext : DbContext
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(DatabaseContext));
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseContext() : base("name=ConnectionString")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

            Option o = Options.SingleOrDefaultAsync(o => o.Name.Equals("nmapwin.logging.sql")).Result;
            if(o != null && o.GetBool())
            {
                Database.Log = s => LogSql(s);
            }
        }
        #endregion

        #region private Methods
        private void LogSql(string message)
        {
            if (logger.IsDebugEnabled)
            {
                logger.Debug(message.Trim());
            }
        }
        #endregion

        #region Database definitions
        public DbSet<Option> Options { get; set; }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Scan> Scans { get; set; }

        public DbSet<Host> Hosts { get; set; }

        public DbSet<HostName> HostNames { get; set; }

        public DbSet<HostScript> HostScripts { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Port> Ports { get; set; }

        public DbSet<PortScript> PortScripts { get; set; }

        public DbSet<PortService> Services { get; set; }

        public DbSet<PortCpe> Cpes { get; set; }

        public DbSet<TraceRoute> TraceRoutes { get; set; }

        public DbSet<Hop> Hops { get; set; }

        public DbSet<Os> Oses { get; set; }

        public DbSet<OsMatch> OsMatches { get; set; }

        public DbSet<OsClass> OsClasses { get; set; }

        public DbSet<Script> Scripts { get; set; }

        public DbSet<ScriptType> ScriptTypes { get; set; }

        public DbSet<ScriptCategory> ScriptCategories { get; set; }

        public DbSet<ScriptArgument> ScriptArguments { get; set; }

        public DbSet<Job> Jobs { get; set; }

        public DbSet<Performance> Performances { get; set; }
        #endregion

        #region Entity framework model
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Configure scan/host many-to-many relationship
            modelBuilder.Entity<Scan>()
                        .HasMany(s => s.Hosts)
                        .WithMany(h => h.Scans)
                        .Map(hs =>
                        {
                            hs.MapLeftKey("ScanId");
                            hs.MapRightKey("HostId");
                            hs.ToTable("ScanHost");
                        });

            // Configure host/address one-to-many relationship
            modelBuilder.Entity<Address>()
                        .HasRequired(address => address.Host)
                        .WithMany(host => host.Addresses)
                        .HasForeignKey(address => address.HostId)
                        .WillCascadeOnDelete();

            // Configure host/os one-to-one relationship
            modelBuilder.Entity<Os>()
                        .HasRequired(os => os.Host)
                        .WithMany(host => host.OperatingSystems)
                        .HasForeignKey(os => os.HostId)
                        .WillCascadeOnDelete(true);

            // Configure os/portused one-to-many relationship
            modelBuilder.Entity<PortUsed>()
                        .HasRequired(portUsed => portUsed.Os)
                        .WithMany(os => os.PortsUsed)
                        .HasForeignKey(os => os.OsId)
                        .WillCascadeOnDelete(true);

            // Configure host/traceroute one-to-one relationship
            modelBuilder.Entity<TraceRoute>()
                        .HasRequired(traceRoute => traceRoute.Host)
                        .WithMany(host => host.TraceRoutes)
                        .HasForeignKey(traceRoute => traceRoute.HostId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<HostName>()
                        .HasRequired(hostName => hostName.Host)
                        .WithMany(host => host.HostNames)
                        .HasForeignKey(hostName => hostName.HostId)
                        .WillCascadeOnDelete();

            // Configure host/port one-to-many relationship
            modelBuilder.Entity<Port>()
                        .HasRequired(port => port.Host)
                        .WithMany(host => host.Ports)
                        .HasForeignKey(port => port.HostId)
                        .WillCascadeOnDelete();

            // Configure port/service one-to-one relationship
            modelBuilder.Entity<PortService>()
                        .HasRequired(portService => portService.Port)
                        .WithMany(port => port.Services)
                        .HasForeignKey(portService => portService.PortId)
                        .WillCascadeOnDelete();

            // Configure port/script one-to-many relationship
            modelBuilder.Entity<PortScript>()
                        .HasRequired(portScript => portScript.Port)
                        .WithMany(port => port.PortScripts)
                        .HasForeignKey(portScript => portScript.PortId)
                        .WillCascadeOnDelete();

            // Configure race/hop one-to-many relationship
            modelBuilder.Entity<Hop>()
                        .HasRequired(hop => hop.TraceRoute)
                        .WithMany(traceRoute => traceRoute.Hops)
                        .HasForeignKey(hop => hop.TraceRouteId)
                        .WillCascadeOnDelete();

            // Configure os/osMatch one-to-many relationship
            modelBuilder.Entity<OsMatch>()
                        .HasRequired(osMatch => osMatch.Os)
                        .WithMany(os => os.OsMatches)
                        .HasForeignKey(osMatch => osMatch.OsId)
                        .WillCascadeOnDelete();

            // Configure osMatch/osClass one-to-many relationship
            modelBuilder.Entity<OsClass>()
                        .HasRequired(osClass => osClass.OsMatch)
                        .WithMany(osMatch => osMatch.OsClasses)
                        .HasForeignKey(osClass => osClass.OsMatchId)
                        .WillCascadeOnDelete();

            // Configure hostCpe/cpe one-to-many relationship
            modelBuilder.Entity<HostCpe>()
                        .HasRequired(c => c.OsClass)
                        .WithMany(s => s.Cpes)
                        .HasForeignKey(c => c.OsClassId)
                        .WillCascadeOnDelete();

            // Configure host/host script one-to-many relationship
            modelBuilder.Entity<HostScript>()
                        .HasRequired(hostScript => hostScript.Host)
                        .WithMany(host => host.HostScripts)
                        .HasForeignKey(hostScript => hostScript.HostId)
                        .WillCascadeOnDelete();

            // Configure service/cpe one-to-many relationship
            modelBuilder.Entity<PortCpe>()
                        .HasRequired(c => c.Service)
                        .WithMany(s => s.Cpes)
                        .HasForeignKey(c => c.ServiceId)
                        .WillCascadeOnDelete();

            // Configure script/script-argument one-to-many relationship
            modelBuilder.Entity<ScriptArgument>()
                        .HasRequired(argument => argument.Script)
                        .WithMany(script => script.Arguments)
                        .HasForeignKey(argument => argument.ScriptId)
                        .WillCascadeOnDelete();

            // Configure script/script-type one-to-many relationship
            modelBuilder.Entity<ScriptType>()
                        .HasRequired(type => type.Script)
                        .WithMany(script => script.Types)
                        .HasForeignKey(type => type.ScriptId)
                        .WillCascadeOnDelete();

            // Configure script/script-category one-to-many relationship
            modelBuilder.Entity<ScriptCategory>()
                        .HasRequired(category => category.Script)
                        .WithMany(script => script.Categories)
                        .HasForeignKey(category => category.ScriptId)
                        .WillCascadeOnDelete();

            // Add index to address
            modelBuilder.Entity<Address>()
                        .HasIndex(a => a.Addr);

            // Unique index for jobs
            modelBuilder.Entity<Job>()
                        .HasIndex(j => new { j.Name, j.Group })
                        .IsUnique();
        }
        #endregion
    }
}
