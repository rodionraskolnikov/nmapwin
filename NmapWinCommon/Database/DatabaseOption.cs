﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 

using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NmapWinCommon.Database
{
    public class DatabaseOption
    {
        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        ///
        private static readonly Lazy<DatabaseOption> lazy = new Lazy<DatabaseOption>(() => new DatabaseOption());

        /// <summary>
        /// Singleton instance
        /// </summary>
        ///
        public static DatabaseOption Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseOption()
        {
        }
        #endregion

        /// <summary>
        /// Return total count of entities
        /// </summary>
        /// <returns>total count</returns>
        public int Count()
        {
            using var db = new DatabaseContext();
            return (from o in db.Options select o).Count();
        }

        /// <summary>
        /// Check existence
        /// </summary>
        /// <returns>true if existence, otherwise false</returns>
        public bool Exists()
        {
            try
            {
                using var db = new DatabaseContext();
                return (from o in db.Options select o).Count() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a list of all options.
        /// </summary>
        /// <returns>list of all options</returns>
        public List<Option> FindOptions()
        {
            using var db = new DatabaseContext();
            var query = from o in db.Options orderby o.Id select o;
            return query.ToList();
        }

        /// <summary>
        /// Returns an option by name.
        /// </summary>
        /// <returns>option if found, otherwise null</returns>
        public Option FindOptionByName(string name)
        {
            using var db = new DatabaseContext();
            return db.Options.SingleOrDefault(o => o.Name.Equals(name));
        }

        /// <summary>
        /// Returns an option by ID.
        /// </summary>
        /// <returns>option if found, otherwise null</returns>
        public Option FindOptionById(long id)
        {
            using var db = new DatabaseContext();
            return db.Options.SingleOrDefault(o => o.Id == id);
        }

        /// <summary>
        /// Insert a new option
        /// </summary>
        /// <param name="option">Option to insert</param>
        public Option InsertOption(Option option)
        {
            using var db = new DatabaseContext();
            option = db.Options.Add(option);
            db.SaveChanges();
            return option;
        }

        /// <summary>
        /// Updates an option
        /// </summary>
        /// <param name="option">option to update</param>
        public Option UpdateOption(Option option)
        {
            using var db = new DatabaseContext();
            Option o = db.Options.SingleOrDefault(o => o.Id == option.Id);
            if (o != null)
            {
                db.Entry(o).CurrentValues.SetValues(option);
                db.SaveChanges();
            }
            return o;
        }

        /// <summary>
        /// Updates a value of an option
        /// </summary>
        /// <param name="name">option name</param>
        /// <param name="value">new option value</param>
        /// <returns>updated option</returns>
        public Option UpdateOptionValue(string name, string value)
        {
            using var db = new DatabaseContext();
            Option o = db.Options.SingleOrDefault(o => o.Name.Equals(name));
            if (o != null)
            {
                o.Value = value;
                db.SaveChanges();
            }
            return o;
        }

        /// <summary>
        /// Deletes an option
        /// </summary>
        /// <param name="option">option to delete</param>
        public void DeleteOption(Option option)
        {
            using var db = new DatabaseContext();
            Option o = db.Options.First(o => o.Id == option.Id);
            db.Options.Remove(o);
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes all option
        /// </summary>
        public void DeleteAllOptions()
        {
            List<Option> options = FindOptions();
            options.ForEach(o => DeleteOption(o));
        }
    }
}
