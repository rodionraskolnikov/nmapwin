﻿using NmapWinCommon.Entities;
using System.Collections.Generic;
using System.ServiceModel;

namespace NmapWinCommon.Wcf
{
    [ServiceContract]
    [ServiceKnownType(typeof(JobState))]
    [ServiceKnownType(typeof(JobType))]
    public interface ICommandListener
    {
        [OperationContract]
        Job AddJob(Job job);

        [OperationContract]
        Job UpdateJob(Job job);

        [OperationContract]
        Job RemoveJob(Job job);

        [OperationContract]
        List<Job> GetJobList();

        [OperationContract]
        Job PauseJob(Job job);

        [OperationContract]
        Job ResumeJob(Job job);
    }
}
