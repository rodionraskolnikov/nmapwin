﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("ScriptCategory")]
    [Table("scriptcategory")]
    public class ScriptCategory
    {
        public long Id { get; set; }

        public long ScriptId { get; set; }

        public virtual Script Script { get; set; }

        [Column("category")]
        public ScriptCategories Category { get; set; }

        public ScriptCategory()
        {
        }

        public ScriptCategory(ScriptCategories category)
        {
            Category = category;
        }

        public override bool Equals(object obj)
        {
            return obj is ScriptCategory category &&
                   Category == category.Category;
        }

        public override int GetHashCode()
        {
            return 373242005 + Category.GetHashCode();
        }

        public enum ScriptCategories
        {
            AUTH,
            BROADCAST,
            BRUTE,
            DEFAULT,
            DISCOVERY,
            DOS,
            EXPLOIT,
            EXTERNAL,
            FUZZER,
            INTRUSIVE,
            MALWARE,
            SAFE,
            VERSION,
            VULN
        }
    }
}
