﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("ScriptType")]
    [Table("scripttype")]
    public class ScriptType
    {
        [Key]
        public long Id { get; set; }

        public long ScriptId { get; set; }

        public virtual Script Script { get; set; }

        [Column("type")]
        public ScriptTypes Type { get; set; }

        public ScriptType()
        {
        }

        public ScriptType(ScriptTypes type)
        {
            Type = type;
        }

        public override bool Equals(object obj)
        {
            return obj is ScriptType type &&
                   Type == type.Type;
        }

        public override int GetHashCode()
        {
            return 2049151605 + Type.GetHashCode();
        }

        public enum ScriptTypes
        {
            PRERULE,
            HOSTRULE,
            PORTRULE,
            POSTRULE
        }
    }
}
