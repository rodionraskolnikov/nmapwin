﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("script")]
    [Table("script")]
    public class Script : INotifyPropertyChanged, IIdentifier
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Html { get; set; }

        [MaxLength]
        public string Description { get; set; }

        public string Example { get; set; }

        [MaxLength]
        public string Output { get; set; }

        [MaxLength]
        public string Code { get; set; }

        public string Requires { get; set; }

        public string Author { get; set; }

        public string License { get; set; } = "https://nmap.org/book/man-legal.html";

        public List<ScriptType> Types { get; set; }

        public void AddType(ScriptType scriptType)
        {
            if (!Types.Contains(scriptType))
            {
                Types.Add(scriptType);
                scriptType.Script = this;
                scriptType.ScriptId = Id;
            }
        }

        public void RemoveType(ScriptType scriptType)
        {
            if (Types.Contains(scriptType))
            {
                Types.Remove(scriptType);
                scriptType.Id = 0;
                scriptType.Script = null;
            }
        }

        public List<ScriptCategory> Categories { get; set; }

        public void AddCategory(ScriptCategory scriptCategory)
        {
            if (!Categories.Contains(scriptCategory))
            {
                Categories.Add(scriptCategory);
                scriptCategory.Script = this;
                scriptCategory.ScriptId = Id;
            }
        }

        public void RemoveCategory(ScriptCategory scriptCategory)
        {
            if (Categories.Contains(scriptCategory))
            {
                Categories.Remove(scriptCategory);
                scriptCategory.ScriptId = 0;
                scriptCategory.Script = null;
            }
        }

        public List<ScriptArgument> Arguments { get; set; }

        public void AddArgument(ScriptArgument scriptArgument)
        {
            if (!Arguments.Contains(scriptArgument))
            {
                Arguments.Add(scriptArgument);
                scriptArgument.Script = this;
                scriptArgument.ScriptId = Id;
            }
        }

        public void RemoveArgument(ScriptArgument scriptArgument)
        {
            if (Arguments.Contains(scriptArgument))
            {
                Arguments.Remove(scriptArgument);
                scriptArgument.ScriptId = 0;
                scriptArgument.Script = null;
            }
        }

        [JsonIgnore]
        private bool _isSelected = false;
        [JsonIgnore]
        [NotMapped]
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                if (value)
                    OnPropertyChanged("ScriptChecked");
                else
                    OnPropertyChanged("ScriptUnchecked");
            }
        }

        public Script()
        {
            Arguments = new List<ScriptArgument>();
            Types = new List<ScriptType>();
            Categories = new List<ScriptCategory>();
        }

        public override bool Equals(object obj)
        {
            return obj is Script script &&
                   Name == script.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }

        #region Event handling
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
