﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("osclass")]
    [Table("osclass")]
    public class OsClass : IIdentifier
    {
        [Key]
        public long Id { get; set; }

        public long OsMatchId { get; set; }

        public OsMatch OsMatch { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("vendor")]
        public string Vendor { get; set; }

        [XmlAttribute("osfamily")]
        public string Family { get; set; }

        [XmlAttribute("osgen")]
        public string Gen { get; set; }

        [XmlAttribute("accuracy")]
        public int Accuracy { get; set; }

        [XmlElement("cpe")]
        public List<HostCpe> Cpes { get; set; }

        public OsClass()
        {
            Cpes = new List<HostCpe>();
        }

        public string Identifier
        {
            get
            {
                return Type;
            }
        }
    }
}
