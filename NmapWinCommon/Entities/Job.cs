﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NmapWinCommon.Entities
{
    [Table("job")]
    public class Job : IIdentifier
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Group { get; set; }

        public long ProfileId { get; set; }

        public string ProfileName { get; set; }

        public string Target { get; set; }

        public JobType Type { get; set; }

        public string Cron { get; set; }

        public long StartTime { get; set; }

        public long EndTime { get; set; }

        public int Interval { get; set; }

        public long Last { get; set; }

        public long Next { get; set; }

        public JobState State { get; set; }

        public int InstanceId { get; set; }

        public Job()
        {
        }

        public Job(string name, string group)
        {
            Name = name;
            Group = group;
        }

        public override bool Equals(object obj)
        {
            return obj is Job job &&
                   Name == job.Name &&
                   Group == job.Group &&
                   ProfileId == job.ProfileId &&
                   Target == job.Target;
        }

        public override int GetHashCode()
        {
            int hashCode = 350545045;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Group);
            hashCode = hashCode * -1521134295 + ProfileId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Target);
            return hashCode;
        }

        public string Identifier
        {
            get
            {
                return Group + "." + Name;
            }
        }
    }
}
