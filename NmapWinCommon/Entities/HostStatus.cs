﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("status")]
    [ComplexType]
    public class HostStatus
    {
        [XmlAttribute("state")]
        [Column("StatusState")]
        public HostState State { get; set; }

        [XmlAttribute("reason")]
        [Column("StatusReason")]
        public string Reason { get; set; }

        [XmlAttribute("reason_ttl")]
        [Column("StatusReasonTtl")]
        public int ReasonTtl { get; set; }

        public override string ToString()
        {
            return "HostStatus=[State = " + State + ", " +
                "Reason=" + Reason + ", " +
                "ReasonTtl=" + ReasonTtl + "]";
        }
    }
}
