﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("service")]
    [Table("portservice")]
    public class PortService : IIdentifier
    {
        [Key]
        public long Id { get; set; }

        public long? PortId { get; set; }

        public virtual Port Port { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("method")]
        public string Method { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }

        [XmlAttribute("product")]
        public string Product { get; set; }

        [XmlAttribute("conf")]
        public int Conf { get; set; }

        [XmlAttribute("extrainfo")]
        public string ExtraInfo { get; set; }

        [XmlElement("cpe")]
        public virtual List<PortCpe> Cpes { get; set; }

        public PortService()
        {
            Cpes = new List<PortCpe>();
        }

        [NotMapped]
        [JsonIgnore]
        public string CpeDetails
        {
            get
            {
                if (Cpes == null || Cpes.Count == 0)
                {
                    return "None";
                }
                return string.Join(",", Cpes.Select(c => c.Content));
            }
        }

        [NotMapped]
        [JsonIgnore]
        public string Identifier
        {
            get
            {
                return Name;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is PortService service &&
                   Id == service.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }
}
