﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("address")]
    [Table("address")]
    public class Address : IIdentifier
    {
        public long Id { get; set; }

        public long? HostId { get; set; }

        public virtual Host Host { get; set; }

        [XmlAttribute("addr")]
        public string Addr { get; set; }

        [XmlAttribute("addrtype")]
        public AddressType AddrType { get; set; }

        [XmlAttribute("vendor")]
        public string Vendor { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Address)) return false;
            return Id == (obj as Address).Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string Identifier
        {
            get
            {
                return Addr;
            }
        }

        public override string ToString()
        {
            return "Address=[id=" + Id + "," +
                "Addr=" + Addr + "," +
                "AddrType=" + AddrType + "," +
                "Vendor=" + Vendor + "]";
        }
    }
}
