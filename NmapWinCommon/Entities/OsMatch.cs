﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("osmatch")]
    [Table("osmatch")]
    public class OsMatch : IIdentifier
    {
        [Key]
        public long Id { get; set; }

        public long OsId { get; set; }

        public Os Os { get; set; }

        [XmlAttribute("name")]
        [MaxLength(255)]
        public string Name { get; set; }

        [XmlAttribute("accuracy")]
        public int Accuracy { get; set; }

        [XmlAttribute("line")]
        public int Line { get; set; }

        [XmlElement("osclass")]
        public List<OsClass> OsClasses { get; set; }

        public OsMatch()
        {
            OsClasses = new List<OsClass>();
        }

        [NotMapped]
        public string Details
        {
            get
            {
                if (OsClasses.Count == 0)
                {
                    return "None";
                }
                return string.Join(",", OsClasses.Select(o => o.Type).ToArray());
            }
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }
    }
}
