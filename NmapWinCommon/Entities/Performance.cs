﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [Table("performance")]
    public class Performance
    {
        [Key]
        public long Id { get; set; }

        public long ScanId { get; set; }

        public long Timestamp { get; set; }

        public float Cpu { get; set; }

        public int Type { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Performance performance &&
                   ScanId == performance.ScanId &&
                   Timestamp == performance.Timestamp;
        }

        public override int GetHashCode()
        {
            int hashCode = 1286165562;
            hashCode = hashCode * -1521134295 + ScanId.GetHashCode();
            hashCode = hashCode * -1521134295 + Timestamp.GetHashCode();
            return hashCode;
        }
    }
}
