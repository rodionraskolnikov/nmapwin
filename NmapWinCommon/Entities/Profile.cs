﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NmapWinCommon.Entities
{
    [Table("profile")]
    public class Profile : INotifyPropertyChanged, IIdentifier
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Command { get; set; }

        private string _target;

        public string Target
        {
            get
            {
                return _target;
            }
            set
            {
                if (_target == null || !_target.Equals(value))
                {
                    _target = value;
                    OnPropertyChanged("Target");
                }
            }
        }

        public string Description { get; set; }

        public Profile()
        {
        }

        public Profile(int id, string name, string command, string description)
        {
            Id = id;
            Name = name;
            Command = command;
            Description = description;
        }

        public void AddOption(string option)
        {
            Command = Command + " " + option;
        }

        public void RemoveOption(string option)
        {
            Command = Command.Replace(option, "").Trim();
        }

        public void RemoveOptions(params string[] Options)
        {
            if (Command == null)
            {
                return;
            }
            foreach (string o in Options)
            {
                Command = Command.Replace(o, "").Trim();
            }
        }

        public int CheckOptions(params string[] Options)
        {
            if (Command == null)
            {
                return -1;
            }
            int index = 0;
            foreach (string o in Options)
            {
                if (Command.Contains(o))
                {
                    return index;
                }
                index++;
            }
            return 0;
        }

        public bool CheckOption(string option)
        {
            if (Command == null)
            {
                return false;
            }
            return Command.Contains(option);
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }

        #region Event Handling
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override bool Equals(object obj)
        {
            return obj is Profile profile &&
                   Id == profile.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
        #endregion
    }
}
