﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("trace")]
    [Table("traceroute")]
    public class TraceRoute : IIdentifier
    {
        public long Id { get; set; }

        public long? HostId { get; set; }

        public Host Host { get; set; }

        [XmlAttribute("proto")]
        public string Proto { get; set; }

        [XmlAttribute("port")]
        public int Port { get; set; }

        [XmlElement("hop")]
        public List<Hop> Hops { get; set; }

        public TraceRoute()
        {
            Hops = new List<Hop>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(TraceRoute)) return false;
            return Id == (obj as TraceRoute).Id;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string Identifier
        {
            get
            {
                return string.Format("{0}", Id);
            }
        }
    }
}
