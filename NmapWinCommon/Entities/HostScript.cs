﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("script")]
    [Table("hostscript")]
    public class HostScript
    {
        [Key]
        [XmlIgnore]
        public long Id { get; set; }

        [Column("hostid")]
        public long? HostId { get; set; }

        public virtual Host Host { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "output")]
        public string Output { get; set; }

        [XmlElement("table")]
        public List<HostScriptTable> Tables { get; set; }

        [XmlElement("elem")]
        public List<HostScriptElement> Elements { get; set; }

        /// <summary>
        /// Constrcutor
        /// </summary>
        public HostScript()
        {
            Tables = new List<HostScriptTable>();
            Elements = new List<HostScriptElement>();
        }

        public override bool Equals(object obj)
        {
            return obj is PortScript results &&
                   Name == results.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }
    }
}
