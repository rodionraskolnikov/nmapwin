﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("hostname")]
    [Table("hostname")]
    public class HostName : IIdentifier
    {
        public long Id { get; set; }

        public long HostId { get; set; }

        public virtual Host Host { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public HostNameType Type { get; set; }

        public override bool Equals(object obj)
        {
            return obj is HostName name &&
                   Name == name.Name &&
                   Type == name.Type;
        }

        public override int GetHashCode()
        {
            int hashCode = -243844509;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + Type.GetHashCode();
            return hashCode;
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }

        public override string ToString()
        {
            return "HostName=[id=" + Id + ", " +
                "Name=" + (string.IsNullOrEmpty(Name) ? "null" : Name.ToString()) + ", " +
                "Type=" + Type.ToString() + "]";
        }
    }
}
