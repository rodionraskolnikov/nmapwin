﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("host")]
    [Table("host")]
    public class Host : IIdentifier
    {
        [Key]
        public long Id { get; set; }

        [XmlArray("hostnames")]
        [XmlArrayItem("hostname", typeof(HostName))]
        public virtual List<HostName> HostNames { get; set; }

        [XmlAttribute("comment")]
        public string Comment { get; set; }

        [XmlAttribute("starttime")]
        public long StartTime { get; set; }

        [XmlAttribute("endtime")]
        public long EndTime { get; set; }

        [XmlElement("status")]
        public HostStatus Status { get; set; }

        [XmlElement("distance")]
        public Distance Distance { get; set; }

        [XmlElement("uptime")]
        public Uptime Uptime { get; set; }

        [XmlElement("times")]
        public HostTimes HostTimes { get; set; }

        [XmlElement("tcpsequence")]
        public HostTcpSequences HostTcpSequences { get; set; }

        [XmlElement("tcptssequence")]
        public HostTcpTsSequences HostTcpTsSequences { get; set; }

        [XmlElement("ipidsequence")]
        public HostIpSequences HostIpSequences { get; set; }

        public virtual List<Scan> Scans { get; set; }

        [XmlElement("address")]
        public virtual List<Address> Addresses { get; set; }

        [XmlArray("ports")]
        [XmlArrayItem("port", typeof(Port))]
        public virtual List<Port> Ports { get; set; }

        [XmlElement("os")]
        public virtual List<Os> OperatingSystems { get; set; }

        [XmlElement("trace")]
        public virtual List<TraceRoute> TraceRoutes { get; set; }

        [XmlElement("script")]
        public virtual List<HostScript> HostScripts { get; set; }

        public Host()
        {
            Scans = new List<Scan>();
            Ports = new List<Port>();
            HostNames = new List<HostName>();
            Addresses = new List<Address>();
            OperatingSystems = new List<Os>();
            TraceRoutes = new List<TraceRoute>();
            HostScripts = new List<HostScript>();
            Distance = new Distance();
            Uptime = new Uptime();
            Status = new HostStatus();
            HostTcpSequences = new HostTcpSequences();
            HostTcpTsSequences = new HostTcpTsSequences();
            HostIpSequences = new HostIpSequences();
        }

        [NotMapped]
        [JsonIgnore]
        public string PortDetails
        {
            get
            {
                if (Ports == null || Ports.Count == 0)
                {
                    return "None";
                }
                return string.Join(",", Ports.Select(p => string.Format("Port: {0:d} Protocol: {1} State: {2}", p.PortId, p.Protocol, p.Status.State)));
            }
        }

        [NotMapped]
        [JsonIgnore]
        public string NameDetails
        {
            get
            {
                if (HostNames == null || HostNames.Count == 0)
                {
                    return "None";
                }
                return string.Join(",", HostNames.Select(h => h.Name));
            }
        }

        [NotMapped]
        [JsonIgnore]
        public string OsDetails
        {
            get
            {
                if (OperatingSystems == null || OperatingSystems.Count == 0)
                {
                    return "None";
                }
                return string.Join(",", OperatingSystems.Select(h => h.OsMatches.Select(m => string.Format("{0} ({1})", m.Name, m.Accuracy))));
            }
        }

        [NotMapped]
        [JsonIgnore]
        public string PrimaryAddress
        {
            get
            {
                if (Addresses == null || Addresses.Count == 0)
                {
                    return "None";
                }
                Address addr = Addresses.First(a => a.AddrType == AddressType.ipv4);
                if (addr != null)
                {
                    return addr.Addr;
                }
                else
                {
                    addr = Addresses.First(a => a.AddrType == AddressType.ipv6);
                    if (addr != null)
                    {
                        return addr.Addr;
                    }
                    else
                    {
                        addr = Addresses.First();
                        return addr.Addr;
                    }
                }
            }
        }

        [NotMapped]
        [JsonIgnore]
        public string PrimaryOs
        {
            get
            {
                if (OperatingSystems == null || OperatingSystems.Count == 0)
                {
                    return null;
                }
                if(OperatingSystems[0].OsMatches.Count>0) { 
                    OsMatch osMatch = OperatingSystems[0].OsMatches.OrderByDescending(m => m.Accuracy).First();
                    return osMatch.Name;
                }
                return null;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Host)) return false;
            return Id == (obj as Host).Id;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns the identifier.
        /// </summary>
        /// <remarks>If the host names are available, take the first host name, otherwise check the address. If the address
        /// is available, take the address, otherwise take the primary key.</remarks>
        /// <returns>host identifier</returns>
        public string Identifier
        {
            get
            {
                if (HostNames != null && HostNames.Count > 0)
                {
                    return HostNames[0].Name;
                }
                /*else if (Address != null && Address.Addr != null)
                {
                    return Address.Addr;
                }*/
                return string.Format("{0}", Id);
            }
        }
    }
}
