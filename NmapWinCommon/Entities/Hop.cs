﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("hop")]
    [Table("hop")]
    public class Hop
    {
        public long Id { get; set; }

        public long? TraceRouteId { get; set; }

        public virtual TraceRoute TraceRoute { get; set; }

        [XmlAttribute("ttl")]
        public int Ttl { get; set; }

        [XmlAttribute("rtt")]
        public double Rtt { get; set; }

        [XmlAttribute("ipaddr")]
        public string IpAddr { get; set; }

        [XmlAttribute("host")]
        public string HostName { get; set; }

        public Hop()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Hop)) return false;
            return Id == (obj as Hop).Id;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
