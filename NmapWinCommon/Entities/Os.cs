﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    /// <summary>
    /// Operating system class.
    /// </summary>
    /// <remaks>
    /// Operating systems are attached to host objects. They can have several matches with an
    /// associated accurary. Each OS match has a corresponding OS class, which can have several CPEs.
    /// </remaks>
    [Serializable()]
    [XmlRoot("os")]
    [Table("os")]
    public class Os : IIdentifier
    {
        public long Id { get; set; }

        public long? HostId { get; set; }

        public virtual Host Host { get; set; }

        [XmlElement("portused")]
        public virtual List<PortUsed> PortsUsed { get; set; }

        [XmlElement("osmatch")]
        public virtual List<OsMatch> OsMatches { get; set; }

        public Os()
        {
            PortsUsed = new List<PortUsed>();
            OsMatches = new List<OsMatch>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(Os)) return false;
            return Id == (obj as Os).Id;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string Identifier
        {
            get
            {
                return string.Format("{0}", Id);
            }
        }
    }
}
