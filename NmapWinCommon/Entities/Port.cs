﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("port")]
    [Table("port")]
    public class Port : IIdentifier
    {
        [Key]
        public long Id { get; set; }

        public long HostId { get; set; }

        public virtual Host Host { get; set; }

        [XmlAttribute("portid")]
        public int PortId { get; set; }

        [XmlIgnore]
        [Column("Protocol")]
        public Protocol Protocol { get; set; }

        [XmlElement("state")]
        public PortStatus Status { get; set; }

        [XmlElement("service")]
        public virtual List<PortService> Services { get; set; }

        [XmlElement("script")]
        public virtual List<PortScript> PortScripts { get; set; }

        public Port()
        {
            PortScripts = new List<PortScript>();
            Status = new PortStatus();
            Services = new List<PortService>();
        }

        [XmlAttribute("protocol")]
        [NotMapped]
        public string ProtocolStr
        {
            get
            {
                return nameof(Protocol);
            }
            set
            {
                try
                {
                    Protocol = (Protocol)Enum.Parse(typeof(Protocol), value, true);
                }
                catch (ArgumentException)
                {
                    Protocol = Protocol.unknown;
                }
            }
        }

        [NotMapped]
        [JsonIgnore]
        public PortService ServiceDetails
        {
            get
            {
                if (Services != null && Services.Count == 1)
                {
                    return Services[0];
                }
                return null;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Port port &&
                   PortId == port.PortId &&
                   Protocol == port.Protocol;
        }

        public override int GetHashCode()
        {
            int hashCode = -836801656;
            hashCode = hashCode * -1521134295 + PortId.GetHashCode();
            hashCode = hashCode * -1521134295 + Protocol.GetHashCode();
            return hashCode;
        }

        public string Identifier
        {
            get
            {
                return string.Format("{0}/{1}", Protocol, PortId);
            }
        }
    }
}
