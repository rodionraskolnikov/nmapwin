﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.Runtime.Serialization;

namespace NmapWinCommon.Entities
{
    [DataContract]
    public enum JobState
    {
        /// <summary>
        /// Indicates that the Quartz.ITrigger is in the "normal" state.
        /// </summary>
        [EnumMember]
        Normal = 0,
        /// <summary>
        /// Indicates that the Quartz.ITrigger is in the "paused" state.
        /// </summary>
        [EnumMember]
        Paused = 1,
        /// <summary>
        /// Indicates that the Quartz.ITrigger is in the "complete" state.
        /// </summary>
        /// <Remarks>"Complete" indicates that the trigger has not remaining fire-times in its schedule.</Remarks>
        [EnumMember]
        Complete = 2,
        /// <summary>
        /// Indicates that the Quartz.ITrigger is in the "error" state.
        /// </summary>
        /// <remarks>
        /// A Quartz.ITrigger arrives at the error state when the scheduler attempts to fire
        /// it, but cannot due to an error creating and executing its related job. Often
        /// this is due to the Quartz.IJob's class not existing in the classpath.
        /// When the trigger is in the error state, the scheduler will make no attempts to
        /// fire it.
        /// </remarks>
        [EnumMember]
        Error = 3,
        /// <summary>
        /// Indicates that the Quartz.ITrigger is in the "blocked" state.
        /// </summary>
        /// <remarks>
        /// A Quartz.ITrigger arrives at the blocked state when the job that it is associated
        /// with has a Quartz.DisallowConcurrentExecutionAttribute and it is currently executing.
        /// </remarks>
        [EnumMember]
        Blocked = 4,
        /// <summary>
        /// Indicates that the Quartz.ITrigger does not exist.
        /// </summary>
        [EnumMember]
        None = 5
    }
}
