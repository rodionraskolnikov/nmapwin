﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace NmapWinCommon.Entities
{
    [Serializable()]
    [XmlRoot("ScriptArgument")]
    [Table("scriptargument")]
    public class ScriptArgument
    {
        public long Id { get; set; }

        public long ScriptId { get; set; }

        public virtual Script Script { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Value { get; set; }

        [MaxLength(255)]
        public string DefaultValue { get; set; }

        [MaxLength(4000)]
        public string Description { get; set; }

        public ScriptArgument()
        {
        }

        public ScriptArgument(string name, string defaultValue)
        {
            Name = name;
            DefaultValue = defaultValue;
        }

        public ScriptArgument(string name, string defaultValue, string description)
        {
            Name = name;
            DefaultValue = defaultValue;
            Description = description;
        }

        /*[JsonIgnore]
        [NotMapped]
        public Visibility IsButtonVisible
        {
            get
            {
                return string.IsNullOrEmpty(Name) ? Visibility.Hidden : Visibility.Visible;
            }
        }*/

        public override bool Equals(object obj)
        {
            return obj is ScriptArgument argument &&
                   Name == argument.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }
    }
}
