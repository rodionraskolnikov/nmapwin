﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NmapWinCommon.Entities
{
    [Table("option")]
    public class Option : IIdentifier
    {
        public long Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Value { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }

        #region Constructor
        public Option()
        {
        }

        public Option(string name, string value, string description)
        {
            Name = name;
            Value = value;
            Description = description;
        }

        public Option(int id, string name, string value, string description)
        {
            Id = id;
            Name = name;
            Value = value;
            Description = description;
        }
        #endregion

        public int GetInt()
        {
            return int.Parse(Value);
        }

        public void SetInt(int value)
        {
            Value = value.ToString();
        }

        public long GetLong()
        {
            return long.Parse(Value);
        }

        public void SetLong(long value)
        {
            Value = value.ToString();
        }

        public float GetFloat()
        {
            return float.Parse(Value);
        }

        public void SetFloat(float value)
        {
            Value = value.ToString();
        }

        public double GetDouble()
        {
            return double.Parse(Value);
        }

        public void SetDouble(double value)
        {
            Value = value.ToString();
        }

        public bool GetBool()
        {
            return bool.Parse(Value);
        }

        public bool GetBool(bool defaultValue)
        {
            if (Value == null)
            {
                return defaultValue;
            }
            return bool.Parse(Value);
        }

        public void SetBool(bool value)
        {
            Value = value.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is Option option &&
                   Name == option.Name;
        }

        public override int GetHashCode()
        {
            int hashCode = 19757871;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }


    }
}
