﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using NmapWinCommon.Worker;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NmapWinCommon.Entities
{
    [Table("scan")]
    public class Scan : IIdentifier
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Target { get; set; }

        public long ProfileId { get; set; }

        public string Command { get; set; }

        public string Version { get; set; }

        public string XmlOutputVersion { get; set; }

        public int Pid { get; set; }

        public int ExitCode { get; set; }

        public string ProcessName { get; set; }

        public virtual List<Host> Hosts { get; set; }

        public long Uptime { get; set; }

        public long LastUpdate { get; set; }

        [MaxLength]
        public string Output { get; set; }

        [MaxLength]
        public string OutputXml { get; set; }

        public long StartTime { get; set; }

        public long EndTime { get; set; }

        public int DebugLevel { get; set; }

        public int VerbosityLevel { get; set; }

        public NmapWorkerStatus Status { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Scan()
        {
            Hosts = new List<Host>();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is Scan scan &&
                   Id == scan.Id &&
                   Name == scan.Name;
        }

        public override int GetHashCode()
        {
            int hashCode = -1919740922;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        public string Identifier
        {
            get
            {
                return Name;
            }
        }
    }
}
