﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace NmapWinCommon.Monitor
{
    public class ProcessMonitorConsolidator : BackgroundWorker
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ProcessMonitor));

        /// <summary>
        /// Performance database singleton
        /// </summary>
        private readonly DatabasePerformance DatabasePerformance = DatabasePerformance.Instance;

        /// <summary>
        /// Timeout for the performance worker in ms.
        /// </summary>
        private readonly int SleepTimeout = 300000;
        #endregion

        #region Constructor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        private static readonly Lazy<ProcessMonitorConsolidator> lazy = new Lazy<ProcessMonitorConsolidator>(() => new ProcessMonitorConsolidator());

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static ProcessMonitorConsolidator Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public ProcessMonitorConsolidator()
        {
            // Configure worker
            WorkerReportsProgress = false;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(ConsolidatorDoWork);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(ConsolidatorRunWorkerCompleted);
            logger.Info("Consolidator initialized");
        }
        #endregion

        #region Worker callbacks
        /// <summary>
        /// This event handler is where the actual, potentially time-consuming work is done.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void ConsolidatorDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }

                int period = 300;
                List<long> scans = DatabasePerformance.FindScans();
                scans.ForEach(s =>
                {
                    long minimum = DatabasePerformance.FindMinimum(s);
                    long maximum = DatabasePerformance.FindMaximum(s);
                    minimum = DateTimeUtils.RoundDown(new DateTime(minimum), TimeSpan.FromSeconds(period)).Ticks;
                    maximum = DateTimeUtils.RoundUp(new DateTime(maximum), TimeSpan.FromSeconds(period)).Ticks;
                    logger.DebugFormat("Range defined - scan: {0} minimum: {1} maximum: {2}", s, new DateTime(minimum), new DateTime(maximum));
                    if ((maximum - minimum) < TimeSpan.FromSeconds(period).Ticks)
                    {
                        if (!DatabasePerformance.ExistsByType(s, minimum, 1))
                        {
                            float average = DatabasePerformance.Average(s, minimum, maximum);
                            Performance performance = new Performance
                            {
                                ScanId = s,
                                Timestamp = minimum,
                                Cpu = average,
                                Type = 1
                            };
                            DatabasePerformance.InsertPerformance(performance);
                        }
                    }
                    else
                    {
                        for (long i = minimum; i < maximum; i += TimeSpan.FromSeconds(period).Ticks)
                        {
                            if (!DatabasePerformance.ExistsByType(s, i, 1))
                            {
                                float average = DatabasePerformance.Average(s, i, i + TimeSpan.FromSeconds(period).Ticks);
                                Performance performance = new Performance
                                {
                                    ScanId = s,
                                    Timestamp = i,
                                    Cpu = average,
                                    Type = 1
                                };
                                DatabasePerformance.InsertPerformance(performance);
                                DatabasePerformance.DeletePerformance(s, 0, i, i + TimeSpan.FromSeconds(period).Ticks);
                            }
                        }
                    }
                });
                Thread.Sleep(SleepTimeout);
            }
        }

        /// <summary>
        /// This event handler deals with the results of the background operation.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void ConsolidatorRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Debug("Consolidator has been shutdown");
        }
        #endregion
    }
}
