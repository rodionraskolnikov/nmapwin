﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace NmapWinCommon.Monitor
{
    public class ProcessMonitor : BackgroundWorker
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(ProcessMonitor));

        /// <summary>
        /// Options database singleton
        /// </summary>
        private readonly DatabaseOption DatabaseOption = DatabaseOption.Instance;

        /// <summary>
        /// Performance database singleton
        /// </summary>
        private readonly DatabasePerformance DatabasePerformance = DatabasePerformance.Instance;

        /// <summary>
        /// Hash map of CPU utilizations
        /// </summary>
        private readonly Dictionary<long, PerformanceCounter> CpuList = new Dictionary<long, PerformanceCounter>();

        /// <summary>
        /// Timeout for the performance worker in ms.
        /// </summary>
        private readonly int SleepTimeout = 5000;
        #endregion

        #region Constrcutor
        /// <summary>
        /// Lazy singleton instance
        /// </summary>
        private static readonly Lazy<ProcessMonitor> lazy = new Lazy<ProcessMonitor>(() => new ProcessMonitor());

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static ProcessMonitor Instance { get { return lazy.Value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public ProcessMonitor()
        {
            // Configure worker
            WorkerReportsProgress = false;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(MonitorDoWork);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(MonitorRunWorkerCompleted);

            // Get options
            Option sleepTimeoutOption = DatabaseOption.FindOptionByName("nmapsrv.process.sleepTimeout");
            if (sleepTimeoutOption != null)
            {
                SleepTimeout = sleepTimeoutOption.GetInt() * 1000;
            }
            logger.Info("Monitor initialized");
        }
        #endregion

        #region Worker callbacks
        /// <summary>
        /// This event handler is where the actual, potentially time-consuming work is done.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void MonitorDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }
                List<long> removeScans = new List<long>();

                lock (CpuList)
                {
                    foreach (long scanId in CpuList.Keys)
                    {
                        try
                        {
                            float cpu = CpuList[scanId].NextValue();
                            Performance performance = new Performance
                            {
                                ScanId = scanId,
                                Timestamp = DateTime.Now.Ticks,
                                Cpu = cpu,
                                Type = 0
                            };
                            DatabasePerformance.InsertPerformance(performance);
                        }
                        catch (InvalidOperationException ex)
                        {
                            removeScans.Add(scanId);
                            logger.ErrorFormat("Error in monitor - error: {0}", ex.Message);
                        }
                    }

                    // Remove not existing scans.
                    removeScans.ForEach(s => CpuList.Remove(s));
                }
                Thread.Sleep(SleepTimeout);
            }
        }

        /// <summary>
        /// This event handler deals with the results of the background operation.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event class</param>
        private void MonitorRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lock (CpuList)
            {
                foreach (long key in CpuList.Keys)
                {
                    CpuList[key].Dispose();
                }
            }
            logger.Debug("Monitor has been shutdown");
        }

        /// <summary>
        /// Adds a new process to the monitor lists
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <param name="pid">PID of the process</param>
        public void AddProcess(long scanId, int pid)
        {
            logger.DebugFormat("Adding process - pid: {0}", pid);

            string instanceName = GetProcessInstanceName(pid);
            logger.DebugFormat("Found instance name - pid: {0} instance: {1}", pid, instanceName);
            lock (CpuList)
            {
                CpuList.Add(scanId, new PerformanceCounter("Process", "% Processor Time", instanceName));
            }
            logger.DebugFormat("Process added - scanId: {0} pid: {1} instance: {2}", scanId, pid, instanceName);
        }

        /// <summary>
        /// Removes a counter from the list.
        /// </summary>
        /// <param name="scanId">ID of the scan</param>
        /// <param name="pid"></param>
        public void RemoveProcess(long scanId, int pid)
        {
            logger.DebugFormat("Removing process - pid: {0}", pid);
            lock (CpuList)
            {
                CpuList.Remove(scanId);
            }
            logger.DebugFormat("Counter removed - pid: {0}", pid);
        }

        /// <summary>
        /// Returns the instance name of the process
        /// </summary>
        /// <param name="pid">PID of the process</param>
        /// <returns>instance name</returns>
        private static string GetProcessInstanceName(int pid)
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");
            string[] instances = cat.GetInstanceNames().Where(inst => inst.StartsWith("nmap")).ToArray();

            foreach (string instance in instances)
            {
                using PerformanceCounter cnt = new PerformanceCounter("Process", "ID Process", instance, true);
                int val = (int)cnt.RawValue;
                if (val == pid)
                {
                    return instance;
                }
            }
            throw new Exception("Could not find performance counter instance name for current process");
        }
        #endregion
    }
}
