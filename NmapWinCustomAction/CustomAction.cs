// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using Microsoft.Deployment.WindowsInstaller;
using System;
using System.IO;
using System.Xml;

namespace NmapWinCustomAction
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ReadXMLNodeValueAction(Session session)
        {
            try
            {
                session.Log("Begin ReadXMLNodeValueAction");
                var existingfilepath = session["WIN_CONFIGFILE"];

                string connectionString = null, providerName = null;
                if (File.Exists(existingfilepath))
                {
                    ReadConnectionString(existingfilepath, out connectionString, out providerName);
                    session.Log("Reading connection values - connectionString: {0} provider: {1} file: {2}", connectionString, providerName, existingfilepath);

                    session["ORIGINAL_CONNECTION_STRING"] = connectionString;
                    session["ORIGINAL_PROVIDER_NAME"] = providerName;
                    session.Log("Connection string values found");
                }
                session.Log("End ReadXMLNodeValueAction");
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action ReadXMLNodeValueAction {0}", ex.ToString());
                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult SetXMLNodeValueAction(Session session)
        {
            try
            {
                var dbTypeSelection = session["DB_TYPE_STRING"];
                session.Log("Begin SetXMLNodeValueAction - dbType: {0}", dbTypeSelection);

                var winExistingFilepath = session["WIN_CONFIG_FILE"];
                var srvExistingFilepath = session["SRV_CONFIG_FILE"];
                session.Log("Database configuration - winConfig: {0} srvConfig: {1}", winExistingFilepath, srvExistingFilepath);

                var originalConnectionString = session["ORIGINAL_CONNECTION_STRING"];
                var originalProviderName = session["ORIGINAL_PROVIDER_NAME"];
                session.Log("Original values - connectionString: {0} provider: {1}", originalConnectionString, originalProviderName);

                string connectionString = null, providerName = null;
                if (!originalConnectionString.Equals("{}") && !originalProviderName.Equals("{}"))
                {
                    connectionString = originalConnectionString;
                    providerName = originalProviderName;
                }
                else
                {
                    switch (dbTypeSelection)
                    {
                        case "SQLite":
                            var filePath = session["DB_SQLITE_FILE_NAME"];
                            var dataDir = session["DATADIR"];
                            connectionString = "Data Source=" + dataDir + filePath + ";Version=3";
                            providerName = "System.Data.SQLite";
                            break;
                        case "MySQL Server":
                            var server = session["DB_MYSQL_SERVER"];
                            var port = session["DB_MYSQL_PORT"];
                            var name = session["DB_MYSQL_NAME"];
                            var user = session["DB_MYSQL_USER"];
                            var pwd = session["DB_MYSQL_PWD"];
                            connectionString = "server=" + server + ";port=" + port + ";database=" + name + ";uid=" + user + ";password=" + pwd + ";";
                            providerName = "MySql.Data.MySqlClient";
                            break;
                        case "MSSQL Server":
                            server = session["DB_MSSQL_SERVER"];
                            port = session["DB_MSSQL_PORT"];
                            name = session["DB_MSSQL_NAME"];
                            user = session["DB_MSSQL_USER"];
                            pwd = session["DB_MSSQL_PWD"];
                            connectionString = "Server=" + server + "," + port + ";Database=" + name + ";User Id=" + user + ";Password=" + pwd + ";Persist Security Info=True";
                            providerName = "System.Data.SqlClient";
                            break;
                        case "SQL Server CE":
                            filePath = session["DB_SQLCE_FILE_NAME"];
                            dataDir = session["DATADIR"];
                            connectionString = "Data Source=" + dataDir + filePath;
                            providerName = "System.Data.SqlServerCe.4.0";
                            break;
                    }
                }
                session.Log("Database configuration - type: {0} connectionString: {1} providerName: {2}", dbTypeSelection, connectionString, providerName);
                if (!File.Exists(winExistingFilepath))
                {
                    session.Log("Windows configuration file not found");
                    return ActionResult.NotExecuted;
                }
                else
                {
                    SetConnectionString(winExistingFilepath, connectionString, providerName);
                }

                if (!File.Exists(srvExistingFilepath))
                {
                    session.Log("Service configuration file not found");
                    return ActionResult.NotExecuted;
                }
                else
                {
                    SetConnectionString(srvExistingFilepath, connectionString, providerName);
                }

                session.Log("End SetXMLNodeValueAction");
            }
            catch (Exception ex)
            {
                session.Log("ERROR in custom action SetXMLNodeValueAction {0}", ex.ToString());
                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }

        public static void SetConnectionString(string fileName, string connectionString, string providerName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            XmlNode node = xmlDoc.SelectSingleNode("/configuration/connectionStrings/add");
            if (node != null)
            {
                node.Attributes["connectionString"].Value = connectionString;
                node.Attributes["providerName"].Value = providerName;
            }
            xmlDoc.Save(fileName);
        }

        public static void ReadConnectionString(string fileName, out string connectionString, out string providerName)
        {
            connectionString = null;
            providerName = null;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            XmlNode node = xmlDoc.SelectSingleNode("/configuration/connectionStrings/add");
            if (node != null)
            {
                connectionString = node.Attributes["connectionString"].Value;
                providerName = node.Attributes["providerName"].Value;
            }
        }
    }
}
