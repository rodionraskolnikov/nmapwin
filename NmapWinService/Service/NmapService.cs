﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Monitor;
using NmapWinCommon.Wcf;
using NmapWinService.Scheduler;
using NmapWinService.Service;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace NmapWinService
{
    public class NmapService : ServiceBase
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(NmapService));

        /// <summary>
        /// Job database singleton
        /// </summary>
        private readonly DatabaseJob DatabaseJob = DatabaseJob.Instance;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components;

        /// <summary>
        /// QUartz schedulr
        /// </summary>
        private static IScheduler scheduler;

        /// <summary>
        /// Service host
        /// </summary>
        public ServiceHost serviceHost;
        #endregion

        #region Constructor
        public NmapService()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new Container();
            ServiceName = "nmapserv";
        }
        #endregion

        #region Start/stop handler
        /// <summary>
        /// Windows service on start handler.
        /// </summary>
        /// <param name="args">service arguments</param>
        protected override async void OnStart(string[] args)
        {
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            logger.InfoFormat("Current working directory set - path: {0}", AppDomain.CurrentDomain.BaseDirectory);

            ProcessMonitor.Instance.RunWorkerAsync();
            ProcessMonitorConsolidator.Instance.RunWorkerAsync();
            logger.Info("Process monitor started");

            await StartSchedulerAsync();
            logger.Info("Scheduler started");

            StartJobs();

            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost for the CommandListener type and provide the base addresses.
            serviceHost = new ServiceHost(typeof(CommandListener), new Uri("http://localhost:8000"));
            serviceHost.AddServiceEndpoint(typeof(ICommandListener), new BasicHttpBinding(), "Command");
            serviceHost.Open();
            logger.InfoFormat("Command listener started");
        }

        /// <summary>
        /// Windows service on stop handler.
        /// </summary>
        protected override void OnStop()
        {
            ProcessMonitor.Instance.CancelAsync();
            logger.Info("Process monitor stopped");

            StopSchedulerAsync().Wait();
            logger.Info("Scheduler stopped");

            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
                logger.Info("Service stopped");
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            logger.Info("Starting shutdown of NmapWinServ");
            if (disposing && (components != null))
            {
                components.Dispose();
                logger.Info("System resources released");
            }
            logger.Info("Finished shutdown of NmapWinServ");
            base.Dispose(disposing);
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Start the scheduler.
        /// </summary>
        /// <returns>asynchronous task</returns>
        private static async Task StartSchedulerAsync()
        {
            logger.Info("Starting Quartz scheduler");

            // Grab the Scheduler instance from the Factory
            scheduler = SchedulerFactory.GetScheduler();

            // and start it off
            await scheduler.Start();

            logger.Info("Quartz scheduler started");
        }

        /// <summary>
        /// Stop schedule.
        /// </summary>
        /// <returns>asynchronous task</returns>
        private static async Task StopSchedulerAsync()
        {
            // Shut down the scheduler when you are ready to close your program
            await scheduler.Shutdown();
            logger.Info("Quartz scheduler stopped");
        }

        /// <summary>
        /// Start all existing jobs.
        /// </summary>
        private void StartJobs()
        {
            logger.Info("Starting scheduler jobs");

            List<Job> jobs = DatabaseJob.FindJobs();
            if (jobs.Count == 0)
            {
                logger.Info("No jobs to start");
                return;
            }
            logger.InfoFormat("Found jobs - count: {0}", jobs.Count);

            CommandListener commandListener = new CommandListener();
            jobs.ForEach(j =>
            {
                commandListener.AddJob(j);
                if(j.State == JobState.Paused)
                {
                    commandListener.PauseJob(j);
                    logger.InfoFormat("Job paused - name: {0} group: {1}", j.Name, j.Group);
                }
                logger.InfoFormat("Job scheduled - name: {0} group: {1}", j.Name, j.Group);
            });
        }
        #endregion
    }
}
