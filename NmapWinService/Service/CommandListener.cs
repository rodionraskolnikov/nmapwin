﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Wcf;
using NmapWinService.Jobs;
using NmapWinService.Scheduler;
using NmapWinService.Utils;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NmapWinService.Service
{

    public class CommandListener : ICommandListener
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(CommandListener));

        /// <summary>
        /// Job database singleton
        /// </summary>
        private readonly DatabaseJob DatabaseJob = DatabaseJob.Instance;

        /// <summary>
        /// QUartz scheduler
        /// </summary>
        private static readonly IScheduler Scheduler = SchedulerFactory.GetScheduler();
        #endregion

        /// <summary>
        /// Returns a list of jobs.
        /// </summary>
        /// <returns>list of all scheduled jobs</returns>
        public List<Job> GetJobList()
        {
            logger.Info(string.Format("Received job list command"));

            var SchedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = SchedulerFactory.GetScheduler().Result;

            List<Job> jobs = new List<Job>();
            IReadOnlyCollection<string> jobGroups = scheduler.GetJobGroupNames().Result;
            foreach (string group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = scheduler.GetJobKeys(groupMatcher).Result;
                foreach (var jobKey in jobKeys)
                {
                    var detail = scheduler.GetJobDetail(jobKey).Result;
                    var triggers = scheduler.GetTriggersOfJob(jobKey).Result;
                    logger.Info(string.Format("Check job - group: {0} name: {1}", jobKey.Name, jobKey.Group));
                    foreach (ITrigger trigger in triggers)
                    {
                        Job job = new Job(jobKey.Name, jobKey.Group)
                        {
                            Next = TriggerUtil.ConvertNextToTimeStamp(trigger),
                            Last = TriggerUtil.ConvertLastToTimeStamp(trigger),
                            State = scheduler.GetTriggerState(trigger.Key) != null ? (JobState)scheduler.GetTriggerState(trigger.Key).Result : JobState.None,
                        };
                        job = DatabaseJob.UpdateJob(job);
                        jobs.Add(job);
                    }
                }
            }
            logger.Info(string.Format("Finished job list request - count: {0}", jobs.Count));
            return jobs;
        }

        /// <summary>
        /// Adds a job by ID to the scheduler.
        /// </summary>
        /// <param name="job">database job to add</param>
        /// <returns>added job</returns>
        public Job AddJob(Job job)
        {
            Task<Job> task = Task<Job>.Factory.StartNew(() =>
            {
                logger.Info(string.Format("Received add job command - id: {0}", job.Id));

                if (Scheduler.IsStarted)
                {
                    bool exists = QuartzJobExistsAsync(job).Result;
                    logger.Info(string.Format("Check quartz job - exists: {0}", exists));
                    if (exists)
                    {
                        job.State = JobState.Error;
                        return job;
                    }
                    else
                    {
                        // Define the job and tie it to our NmapJob class
                        IJobDetail quartzJob = JobBuilder.Create<NmapJob>()
                            .WithIdentity(job.Name, job.Group)
                            .SetJobData(GetJobDataMap(job))
                            .Build();
                        logger.InfoFormat("Job created - key: {0}.{1}", job.Group, job.Name);

                        // Define trigger
                        ITrigger trigger = GetTrigger(job);
                        Scheduler.ScheduleJob(quartzJob, trigger);
                        logger.InfoFormat("Job scheduled - nextStart: {0}", trigger.GetNextFireTimeUtc().Value.ToLocalTime().ToString());

                        // Update job
                        job.Next = TriggerUtil.ConvertNextToTimeStamp(trigger);
                        job.Last = TriggerUtil.ConvertLastToTimeStamp(trigger);
                        job = DatabaseJob.UpdateJob(job);
                        logger.InfoFormat("Job updated in database - key: {0}.{1}", job.Group, job.Name);
                        return job;
                    }
                }
                else
                {
                    job.State = JobState.Error;
                    logger.Error(string.Format("Scheduler not started"));
                    return job;
                }
            });
            return task.Result;
        }

        /// <summary>
        /// Updates a job by stopping and restarted the Quartz job.
        /// </summary>
        /// <param name="job">job to update</param>
        /// <returns>updated job</returns>
        public Job UpdateJob(Job job)
        {
            Task<Job> task = Task<Job>.Factory.StartNew(() =>
            {
                logger.InfoFormat("Received update job command - id: {0}", job.Id);

                if (Scheduler.IsStarted)
                {
                    bool exists = QuartzJobExistsAsync(job).Result;
                    logger.InfoFormat("Check quartz job - exists: {0}", exists);
                    if (!exists)
                    {
                        job.State = JobState.Error;
                        logger.ErrorFormat("Job not found in quartz - id: {0}", job.Id);
                    }
                    else
                    {
                        ITrigger trigger = GetTrigger(job);
                        Scheduler.RescheduleJob(GetTriggerKey(job), trigger);
                        logger.InfoFormat("Job rescheduled - id: {0} nextStart: {1}", job.Id, trigger.GetNextFireTimeUtc().Value.ToLocalTime().ToString());

                        // Update jobs
                        job.Next = TriggerUtil.ConvertNextToTimeStamp(trigger);
                        job.Last = TriggerUtil.ConvertLastToTimeStamp(trigger);
                        job = DatabaseJob.UpdateJob(job);
                        logger.InfoFormat("Job updated - id: {0} nextStart: {1}", job.Id, trigger.GetNextFireTimeUtc().Value.ToLocalTime().ToString());
                    }
                }
                else
                {
                    job.State = JobState.Error;
                    logger.ErrorFormat("Scheduler not started");
                }
                logger.InfoFormat("Finished update job command - id: {0}", job.Id);
                return job;
            });
            return task.Result;
        }

        /// <summary>
        /// Pause a job in the scheduler.
        /// </summary>
        /// <param name="job">job to pause</param>
        /// <returns></returns>
        public Job PauseJob(Job job)
        {
            Task<Job> task = Task<Job>.Factory.StartNew(() =>
            {
                logger.Info(string.Format("Received pause job command - id: {0}", job.Id));

                if (Scheduler.IsStarted)
                {
                    bool exists = QuartzJobExistsAsync(job).Result;
                    logger.Info(string.Format("Check quartz job - exists: {0}", exists));
                    if (!exists)
                    {
                        logger.Error(string.Format("Job not found in quartz - id: {0}", job.Id));
                        job.State = JobState.Error;
                        return job;
                    }
                    else
                    {
                        Scheduler.PauseJob(GetJobKey(job));
                        logger.Info(string.Format("Job paused - id: {0}", job.Id));
                        job.State = JobState.Paused;
                        return job;
                    }
                }
                else
                {
                    logger.Error(string.Format("Scheduler not started"));
                    job.State = JobState.Error;
                    return job;
                }
            });
            return task.Result;
        }

        /// <summary>
        /// Restart a job in the scheduler.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public Job ResumeJob(Job job)
        {
            Task<Job> task = Task<Job>.Factory.StartNew(() =>
            {
                logger.Info(string.Format("Received resume job command - id: {0}", job.Id));

                if (Scheduler.IsStarted)
                {
                    bool exists = QuartzJobExistsAsync(job).Result;
                    logger.Info(string.Format("Check quartz job - exists: {0}", exists));
                    if (!exists)
                    {
                        logger.Error(string.Format("Job not found in quartz - id: {0}", job.Id));
                        job.State = JobState.Error;
                        return job;
                    }
                    else
                    {
                        Scheduler.ResumeJob(GetJobKey(job));
                        logger.Info(string.Format("Job resumed - id: {0}", job.Id));
                        job.State = JobState.Normal;
                        return job;
                    }
                }
                else
                {
                    logger.Error(string.Format("Scheduler not started"));
                    job.State = JobState.Error;
                    return job;
                }
            });
            return task.Result;
        }

        /// <summary>
        /// Remove a job by ID from the scheduler.
        /// </summary>
        /// <param name="job">job to remove</param>
        /// <returns>service error code</returns>
        public Job RemoveJob(Job job)
        {
            Task<Job> task = Task<Job>.Factory.StartNew(() =>
            {
                logger.Info(string.Format("Received remove job command - id: {0}", job.Id));

                if (Scheduler.IsStarted)
                {
                    bool exists = QuartzJobExistsAsync(job).Result;
                    logger.Info(string.Format("Check quartz job - exists: {0}", exists));
                    if (!exists)
                    {
                        logger.ErrorFormat("Job not found in quartz - id: {0}", job.Id);
                        job.State = JobState.Error;
                        return job;
                    }
                    else
                    {
                        Scheduler.UnscheduleJob(GetTriggerKey(job));
                        logger.InfoFormat("Job unscheduled - id: {0}", job.Id);
                        job.State = JobState.None;
                        return job;
                    }
                }
                else
                {
                    logger.Error(string.Format("Scheduler not started"));
                    job.State = JobState.Error;
                    return job;
                }
            });
            return task.Result;
        }

        /// <summary>
        /// Check existence.
        /// </summary>
        /// <param name="job">job to check</param>
        /// <returns>true if job exists in quartz scheduler, otherwise false</returns>
        private async Task<bool> QuartzJobExistsAsync(Job job)
        {
            return await Scheduler.CheckExists(GetJobKey(job));
        }

        /// <summary>
        /// Create the job details data map.
        /// </summary>
        /// <param name="job">job to execute</param>
        /// <returns>job data map</returns>
        private JobDataMap GetJobDataMap(Job job)
        {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.Put("target", job.Target);
            jobDataMap.Put("profileId", job.ProfileId);
            jobDataMap.Put("profileName", job.ProfileName);
            logger.DebugFormat("Job data map filled - target: {0} profileId: {1} profileName: {2}", job.Target, job.ProfileId, job.ProfileName);
            return jobDataMap;
        }

        /// <summary>
        /// Returns a trigger dependeing on the job type.
        /// </summary>
        /// <param name="job">job to get trigger from</param>
        /// <returns>Quartz trigger</returns>
        private ITrigger GetTrigger(Job job)
        {
            logger.DebugFormat("Starting get trigger - key: {0}.{1} type: {2}", job.Group, job.Name, job.Type);
            if (job.Type == JobType.Simple)
            {
                return GetSimpleTrigger(job);
            }
            else
            {
                return GetCronTrigger(job);
            }
        }

        /// <summary>
        /// Returns a simple trigger
        /// </summary>
        /// <remarks>Misfire policy set to fire now.</remarks>
        /// <param name="job">job to get the trigger for</param>
        /// <returns>quartz trigger</returns>
        private ITrigger GetSimpleTrigger(Job job)
        {
            DateTime startTime = new DateTime(job.StartTime).ToLocalTime();
            DateTime endTime = job.EndTime > 0 ? new DateTime(job.EndTime).ToLocalTime() : DateTime.MaxValue;
            logger.DebugFormat("Creating simple trigger - key: {0}.{1} start: {2} end: {3} interval: {4}", job.Group, job.Name, startTime.ToString(), endTime.ToString(), job.Interval);

            // Trigger the job to run now, and then repeat every 10 seconds
            try
            {
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(GetTriggerKey(job))
                    .StartAt(new DateTimeOffset(startTime))
                    .EndAt(new DateTimeOffset(endTime))
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(job.Interval)
                        .RepeatForever()
                        .WithMisfireHandlingInstructionFireNow())
                    .Build();
                logger.InfoFormat("Trigger defined - key: {0}.{1}", job.Group, job.Name);
                return trigger;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not create simple trigger - error: {0}", ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Returns a cron trigger
        /// </summary>
        /// <param name="job">job to get the trigger for</param>
        /// <remarks>Misfire policy set to fire and proceed.</remarks>
        /// <returns>quartz trigger</returns>
        private ITrigger GetCronTrigger(Job job)
        {
            DateTime startTime = new DateTime(job.StartTime).ToLocalTime();
            DateTime endTime = job.EndTime > 0 ? new DateTime(job.EndTime).ToLocalTime() : DateTime.MaxValue;
            logger.DebugFormat("Creating cron trigger - key: {0}.{1} start:{2} end: {3} cron: {4}", job.Group, job.Name, startTime.ToString(), endTime.ToString(), job.Cron);

            try
            {
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(GetTriggerKey(job))
                    .WithCronSchedule(job.Cron, x => x
                        .InTimeZone(TimeZoneInfo.Local)
                        .WithMisfireHandlingInstructionFireAndProceed())
                    .StartAt(startTime)
                    .EndAt(endTime)
                    .Build();
                logger.Info(string.Format("Cron trigger defined - key: {0}.{1}", job.Group, job.Name));
                return trigger;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not create cron trigger - error: {0}", ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Returns the job key.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>job key consisting of group.name</returns>
        private JobKey GetJobKey(Job job)
        {
            return new JobKey(job.Name, job.Group);
        }

        /// <summary>
        /// Returns the trigger key.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>trigger key consisting of group.name</returns>
        private TriggerKey GetTriggerKey(Job job)
        {
            return new TriggerKey(job.Name, job.Group);
        }
    }
}
