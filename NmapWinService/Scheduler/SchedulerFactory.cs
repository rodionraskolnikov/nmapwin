﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinService.Listener;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using System;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Remoting;
using System.Threading.Tasks;

namespace NmapWinService.Scheduler
{
    class SchedulerFactory
    {
        private static IScheduler schedulerInstance;
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// Return singleton instance
        /// </summary>
        /// <returns></returns>
        public static IScheduler GetScheduler()
        {
            if (schedulerInstance == null)
            {
                lock (SyncRoot)
                {
                    if (schedulerInstance == null)
                    {
                        // Create instance
                        schedulerInstance = Task.Run(() => GetSchedulerAsync()).GetAwaiter().GetResult();

                        // Add trigger listener
                        schedulerInstance.ListenerManager.AddTriggerListener(new GlobalTriggerListener(), GroupMatcher<TriggerKey>.AnyGroup());
                    }
                }
            }
            return schedulerInstance;
        }

        /// <summary>
        /// Create scheduler
        /// </summary>
        /// <returns></returns>
        private static async Task<IScheduler> GetSchedulerAsync()
        {
            var SchedulerFactory = new StdSchedulerFactory();

            try
            {
                return await SchedulerFactory.GetScheduler();
            }
            catch (TypeInitializationException e)
            {
                var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                log.Error("Attempt to access job scheduler failed with unexpected intialization error (the scheduler might not be running)\n", e);
            }
            catch (SocketException e)
            {
                var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                log.Error("Attempt to access job scheduler failed with unexpected socket error (the scheduler might not be running)\n", e);
            }
            catch (RemotingException e)
            {
                var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                log.Error("Attempt to access job scheduler failed with unexpected remoting error (the scheduler might not be running)\n", e);
            }
            catch (SchedulerException e)
            {
                var log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                log.Error("Attempt to access job scheduler failed with scheduler error (the scheduler might not be running)\n", e);
            }

            return null;
        }
    }
}
