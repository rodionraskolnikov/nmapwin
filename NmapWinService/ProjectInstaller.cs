﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace NmapWinService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            nmapServiceProcessInstaller = new ServiceProcessInstaller();
            nmapServiceInstaller = new ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            nmapServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            nmapServiceProcessInstaller.Password = null;
            nmapServiceProcessInstaller.Username = null;
            // 
            // serviceInstaller1
            // 
            nmapServiceInstaller.Description = "Windows nmap service.";
            nmapServiceInstaller.DisplayName = "NmapWin Service";
            nmapServiceInstaller.ServiceName = "nmapserv";
            // 
            // ProjectInstaller
            // 
            Installers.AddRange(new Installer[]
            {
                nmapServiceProcessInstaller,
                nmapServiceInstaller
            });
        }

        #endregion

        private ServiceProcessInstaller nmapServiceProcessInstaller;
        private ServiceInstaller nmapServiceInstaller;
    }
}
