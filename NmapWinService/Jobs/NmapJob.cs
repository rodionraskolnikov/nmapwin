﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your schedule) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinCommon.Worker;
using Quartz;
using System;
using System.Threading.Tasks;

namespace NmapWinService.Jobs
{
    [DisallowConcurrentExecution]
    public class NmapJob : IJob
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(NmapJob));

        /// <summary>
        /// Scan database singleton
        /// </summary>
        private readonly DatabaseScan DatabaseScan = DatabaseScan.Instance;

        /// <summary>
        /// Profile database singleton
        /// </summary>
        private readonly DatabaseProfile DatabaseProfile = DatabaseProfile.Instance;
        #endregion

        /// <summary>
        /// Job executor
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            Task task = Task.Run(() =>
            {
                logger.InfoFormat("Job instance starting - executionId: {0}", context.FireInstanceId);

                string target = context.JobDetail.JobDataMap.GetString("target");
                int profileId = context.JobDetail.JobDataMap.GetInt("profileId");
                string profileName = context.JobDetail.JobDataMap.GetString("profileName");
                logger.DebugFormat("Got job data map - target: {0} profileId: {1} profileName: {2}", target, profileId, profileName);

                int instanceId = DatabaseJob.Instance.MaxInstanceId(context.JobDetail.Key.Name, context.JobDetail.Key.Group);
                string jobName = context.JobDetail.Key.Group + "." + context.JobDetail.Key.Name + "." + instanceId;
                logger.DebugFormat("Got job name - name: {0}", jobName);

                // Create instance
                Profile profile = DatabaseProfile.FindProfileByName(profileName);
                Scan scan = new Scan
                {
                    Command = profile.Command,
                    Target = target,
                    Name = jobName,
                    ProfileId = profileId,
                    StartTime = DateTime.Now.Ticks,
                    Status = NmapWorkerStatus.STARTED
                };
                scan = DatabaseScan.InsertScan(scan);
                logger.DebugFormat("Scan inserted - id: {0} name: {1}", scan.Id, jobName);

                // Define worker
                NmapWorker nmapWorker = new NmapWorker
                {
                    Scan = scan
                };

                // Run worker
                nmapWorker.InitializeNmapProcess();
                nmapWorker.RunNmapProcess();
                nmapWorker.ProcessJobCompleted();

                logger.InfoFormat("Job instance completed - instanceId: {0}", instanceId);
            });
            return task;
        }
    }
}
