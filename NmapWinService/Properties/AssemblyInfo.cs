
using System.Reflection;
using System.Resources;

// General Information
[assembly: AssemblyTitle("NmapServ")]
[assembly: AssemblyDescription("NmapWin Windows Service")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rodion Raskolnikov")]
[assembly: AssemblyProduct("nmapwin")]
[assembly: AssemblyCopyright("Copyright � 2020 Rodion Raskolnikov")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version informationr(
[assembly: AssemblyVersion("1.0.81.118")]
[assembly: AssemblyFileVersion("1.0.81.118")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]

// Logging
[assembly: log4net.Config.XmlConfigurator]

