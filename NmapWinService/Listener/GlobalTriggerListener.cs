﻿// Copyright (C) nmapwin Rodion Raskolnikov
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// 
using log4net;
using NmapWinCommon.Database;
using NmapWinCommon.Entities;
using NmapWinService.Utils;
using Quartz;
using System.Threading;
using System.Threading.Tasks;

namespace NmapWinService.Listener
{
    class GlobalTriggerListener : ITriggerListener
    {
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(GlobalTriggerListener));

        /// <summary>
        /// Job database singleton
        /// </summary>
        private readonly DatabaseJob DatabaseJob = DatabaseJob.Instance;

        /// <summary>
        /// Listener name
        /// </summary>
        public string Name => "GlobalTriggerListener";

        /// <summary>
        /// Trigger fired.
        /// </summary>
        /// <param name="trigger">quartz trigger</param>
        /// <param name="context">job execution context</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns></returns>
        public Task TriggerFired(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                JobKey jobKey = context.JobDetail.Key;
                Job job = DatabaseJob.FindJobByName(jobKey.Name, jobKey.Group);
                job.Next = TriggerUtil.ConvertNextToTimeStamp(trigger);
                job.Last = TriggerUtil.ConvertLastToTimeStamp(trigger);
                job = DatabaseJob.UpdateJob(job);
                logger.Debug(string.Format("Trigger fired - key: {0}", trigger.Key));
            });
        }

        /// <summary>
        /// Veto job.
        /// </summary>
        /// <param name="trigger">quartz trigger</param>
        /// <param name="context">job execution context</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns></returns>
        public Task<bool> VetoJobExecution(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => false);
        }

        /// <summary>
        /// Trigger misfired.
        /// </summary>
        /// <param name="trigger">quartz trigger</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns></returns>
        public Task TriggerMisfired(ITrigger trigger, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => logger.Warn(string.Format("Trigger misfired - key: {0}", trigger.Key)));
        }

        /// <summary>
        /// Trigger completed.
        /// </summary>
        /// <param name="trigger">quartz trigger</param>
        /// <param name="context">job execution context</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns></returns>
        public Task TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                JobKey jobKey = context.JobDetail.Key;
                Job job = DatabaseJob.FindJobByName(jobKey.Name, jobKey.Group);
                job.Next = TriggerUtil.ConvertNextToTimeStamp(trigger);
                job.Last = TriggerUtil.ConvertLastToTimeStamp(trigger);
                job = DatabaseJob.UpdateJob(job);
                logger.Debug(string.Format("Trigger completed - key: {0}", trigger.Key));
            });
        }
    }
}
