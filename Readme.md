# NmapWin

## Introduction

NmapWin is a native Windows UI for the [nmap network scanner](https://nmap.org/). It's totally written in C#/.NET and has some similarities to
Zenmap the official Windows nmap frontend. Scans and profiles are stored in an embedded SQL Server CE database or an external MySQL/MSSQL
database.

## Features

* Multitasking scans: Scans can be started as background threads.
* Intuitive UI design, similar to zenmap.
* Embedded SQL Server CE file based database.
* C# Entity manager framework.
* Graphical network map.
* Geo-Location map (experimental).
* Support for several databases (currently SQLServer CE, MySQL, MSSQL Server).
* Online update functionality.
* Windows service containing a Quartz scheduler for job submission.

## Prerequisites

The nmap command line tool must be installed prior to using NmapWin. The Windows version of the nmap command line tool can be downloaded from 
the nmap [download site](https://nmap.org/download.html) site.

## Installation

### Prerequisites

NmapWin needs some prerequisites. As NmapWin has an embedded SQL Server CE database by default, at least the SQL Server CE Runtime must be installed. The following
table lists all prerequisites:

  * [.NET Framework 4.7.2](https://support.microsoft.com/de-de/help/4054530/microsoft-net-framework-4-7-2-offline-installer-for-windows), is normally already installed
  * [SQL Server CE runtime](https://www.microsoft.com/en-US/download/details.aspx?id=30709).
  * [MySQL Server .NET Connector](https://dev.mysql.com/downloads/connector/net/), if you want to use a MySQL server instance.

### Binary packages

NmapWin uses the gitlab CI/CD features to publish binary packages to the [Gitlab package registry](https://gitlab.com/rodionraskolnikov/nmapwin/-/packages).
From there you can download binary packages for all availabble version. A binary package is a Wix Installer MSI packages. Just downoaded the MSI file 
start the installation by executing the installer.

During the first startup the embedded database will be created with some default profiles and options. All scan, host and other nmap related tables are empty.

## Uninstall

In order to uninstall NmapWin open the 'Apps and Features' in the Windows system settings and search for 'NmapWin'. Uninstall the MSI package by clicking on the 
Uninstall button in the application list. It will completely uninstall all components and data. Don't forget to delete the NmapWin directory in case you put some
files into the main application directory which uninstall does not recognize (JSON exports, database files etc.).

## Updates

NmapWin has a buildin update feature, which allows the download and installation of the latest version from the UI. Go to Help -> Check for Updates, to start
the upgrade process. If a new version is available (NmapWin uses a Microsoft-like versioning system: Major.Minor.Build.Revision), it will be downloaded,
unpacked and installed. Be patient, the UI will automatically restart.

## Bug reports and feature requests

If you found a bug or wants to submit an feature request, file an issue with the [Gitlab issue tracking system](https://gitlab.com/rodionraskolnikov/nmapwin/-/issues).

## Changes

### 1.0

 * Initial commit to gitlab.
 * First help screens.
 * Adds some more nmap scripts.
 * Validation for all profile page input fields.
 * Validation for all scan page input fields.
 * Validation for all ping page input fields.
 * Use C# EntityManager.
 * Adds some more nmap scripts.
 * Validation for all profile pages.
 * Revamp XML parser
 * New layout with labels above widgets
 * Add CPEs for ports/hosts
 * Adding network Map
 * Analyse traceroutes/hops
 * Support for MySQL

### 1.1
 * Bug fixes:
	* Fixed simple schedules for quartz scheduler.
	* Fixed port state problem for open|filtered, closed|filtered
	* Fixed NPE in report XML output
 * New features
	* Add port scripts (scripts executed by nmap on discovered open ports).
	* Add name selection to scans.
	* Implementing port details dialog.
	* Implementing host list dialog.
	* MSI installation project using Wix installer added.
	* Support for MSSQL Server databases.
	* Support for SQLite Server databases.
	* SQLServer CE deprecated, will be removed in one of the following releases.

### 1.2
 * Bug fixes:
 * New features
	* Added advanced tab on host details dialog.
	* Added comment tab on host details dialog.

## Known issues

 * Network map has some issues. Actually, there is no reliable public domain network graph package for C# existing. If you have an idea, let me know.